package com.kkoma.kia.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackArrayTest {

    @Test
    public void stackTest() {
        StackArray stack = new StackArray(5);

        // push
        stack.push(1);

        // pop
        int item = stack.pop();
        assertEquals(1, item);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> stack.pop());

        // peek
        stack.push(1);
        stack.push(2);
        stack.push(3);

        item = stack.peek();
        assertEquals(3, item);
    }
}