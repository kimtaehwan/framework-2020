package com.kkoma.kia.stack;

public class StackArray {
    private int[] stack;
    private int size;
    private int top;

    public StackArray(int size) {
        this.size = size;
        this.stack = new int[size];
        this.top = -1;
    }

    public void push(int item) {
        stack[++top] = item;
    }

    public int pop() throws ArrayIndexOutOfBoundsException{
        if (top < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return stack[top--];
    }

    public int peek() {
        return stack[top];
    }
}
