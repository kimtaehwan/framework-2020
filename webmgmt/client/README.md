# Fuse - Angular

Material Design Admin Template with Angular 8 and Angular Material

## The Community

Share your ideas, discuss Fuse and help each other.

[Click here](http://fusetheme.com/community) to see our Community page.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Create Spec file 
> npm i angular-spec-generator
> ex) angular-spec-generator D:\framework-2020\framework-2020-0615\webmgmt\client\src\app\main\terms\

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Angular update
check to Angular version `ng update --next`.

Command to update
* ng update @angular/cdk @angular/core  @angular/common typescript @angular/flex-layout angular-in-memory-web-api @angular-devkit/build-angular --force
* ng update @angular/cli
* ng update @angular/material @ngrx/store rxjs
* ng update @swimlane/ngx-dnd
    
## FormArray
* https://stackoverflow.com/questions/40927167/angular-reactiveforms-producing-an-array-of-checkbox-values   

## date-time-picker
* https://github.com/danielmoncada/date-time-picker

## Angular update
* https://dschci.tistory.com/103