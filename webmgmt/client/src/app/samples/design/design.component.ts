import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss']
})
export class DesignComponent implements OnInit {

    blankText = '  공  백   ';
    index: boolean[] = [];

  constructor() { }

  ngOnInit(): void {
      this.index.push(false);
      this.index.push(true);
  }

  test(): void {
      console.log('1. length: ', this.blankText);
      console.log('1. length: ', this.blankText.length);

      console.log('2. length: ', this.blankText.trim());
      console.log('2. length: ', this.blankText.trim().length);
  }

}
