import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignComponent } from './design.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../main/common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: DesignComponent
    }
];

@NgModule({
  declarations: [DesignComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule
  ]
})
export class DesignModule { }
