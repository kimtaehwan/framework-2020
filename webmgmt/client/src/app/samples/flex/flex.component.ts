import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-flex',
  templateUrl: './flex.component.html',
  styleUrls: ['./flex.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FlexComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
