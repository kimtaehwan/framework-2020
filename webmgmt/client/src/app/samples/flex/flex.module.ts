import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FlexComponent} from './flex.component';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {MaterialModule} from '../../main/common/utils/material/material.module';

const routes: Routes = [
    {
        path: '**',
        component: FlexComponent
    }
];

@NgModule({
  declarations: [FlexComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule
  ]
})
export class FlexModule { }
