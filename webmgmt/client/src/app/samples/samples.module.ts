import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MailComponent} from './mail/mail.component';
import {MaterialModule} from '../main/common/utils/material/material.module';
import {FuseSharedModule} from '../../@fuse/shared.module';
import {FlexComponent} from './flex/flex.component';
import { TableDragComponent } from './table-drag/table-drag.component';

const routes: Routes = [
    {
        path: 'mail',
        component: MailComponent
    },
    {
        path: 'flex',
        loadChildren: () => import('./flex/flex.module').then(m => m.FlexModule)
    },
    {
        path: 'table-drag',
        loadChildren: () => import('./table-drag/table-drag.module').then(m => m.TableDragModule)
    },
    {
        path: 'idv',
        loadChildren: () => import('./idv/idv.module').then(m => m.IdvModule)
    },
    {
        path: 'design',
        loadChildren: () => import('./design/design.module').then(m => m.DesignModule)
    },
    {
        path: '**',
        redirectTo: 'table-drag'
    }
];

@NgModule({
  declarations: [
      MailComponent,
      TableDragComponent
  ],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule
  ]
})
export class SamplesModule { }
