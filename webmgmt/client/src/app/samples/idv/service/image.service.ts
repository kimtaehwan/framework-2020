import { Injectable } from '@angular/core';
import {EMPTY, Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {catchError, retry, retryWhen, shareReplay, switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getImage(url): Observable<String> {
      return this.httpClient.get<String>(url, {headers: this.getHeaders()});
  }

  getHeaders(): HttpHeaders {
      const headers = new HttpHeaders()
          .set('mimeType', 'text/plain; charset=x-user-defined');

      return headers;
  }
}
