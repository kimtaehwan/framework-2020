import { Injectable } from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {DcsStandard} from '../../../main/models/applications/dcs-standard';
import {ApiConstant} from '../../../main/utils/constants/api-constant';
import {HttpClient} from '@angular/common/http';
import {ApplicationData} from '../../../main/models/applications/application-data';
import {IdvTransaction} from '../../../main/models/applications/apps/idv-transaction';

@Injectable({
  providedIn: 'root'
})
export class IdvService {

    loadedApplications = new Map<string, ApplicationData>();

  constructor(
      private httpClient: HttpClient
  ) {
  }

  openTransaction(appRefId: string, callback: any): void {
      const documentResultLogList$ = this.getDocumentResultLogList(appRefId);

      forkJoin([
          documentResultLogList$
      ]).subscribe(
          (results: any[]) => {
              const applicationData: ApplicationData = new ApplicationData();

              const dataDocumentResultLogList = results[0].contents;
              this.bindDocumentResultLogList(applicationData, dataDocumentResultLogList);

              this.loadedApplications.set(appRefId, applicationData);
          },
          () => {},
          () => {
              callback();
          }
      );
  }

  bindDocumentResultLogList(applicationData: ApplicationData, docResultLogList: any): void {
      if (!!docResultLogList) {
          docResultLogList.forEach((value, index) => {
              applicationData.idvTransactions[index] = new IdvTransaction();
              Object.assign(applicationData.idvTransactions[index], value);
          });
      }
  }

  getDocumentResultLogList(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_CUSTOMER_SUPPORT_URL + '/idv/api/v1/documents/' + appRefId + '/logs';
      return this.httpClient.get<DcsStandard>(url);
  }
}
