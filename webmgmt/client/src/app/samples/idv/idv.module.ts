import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdvComponent } from './idv.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../main/common/utils/material/material.module';

const routes: Routes = [
    {
        path: '**',
        component: IdvComponent
    }
];

@NgModule({
  declarations: [IdvComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule
  ]
})
export class IdvModule { }
