import {AfterViewInit, Component, OnInit} from '@angular/core';
import {IdvService} from './service/idv.service';
import {ApplicationData} from '../../main/models/applications/application-data';
import {ValueConstant} from '../../main/utils/constants/value-constant';
import {ApiConstant} from '../../main/utils/constants/api-constant';
import {ImageService} from './service/image.service';
import {UrlConstant} from '../../main/utils/constants/url-constant';
import {blob} from 'aws-sdk/clients/frauddetector';
import {reject} from 'q';

@Component({
  selector: 'app-idv',
  templateUrl: './idv.component.html',
  styleUrls: ['./idv.component.scss']
})
export class IdvComponent implements OnInit, AfterViewInit {

    processType = 'IDV';
    referenceId = 'o0okjie0v9di';
    imagePath = '';

    token = 'Bearer eyJraWQiOiJcL3NtWDRTYmZKU09IcVRITmdERWM0cWtCbFwvTkNDVG16QWFNbGU4S0dLVlU9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI4ODI5NjdkMC04MjliLTQ2M2ItYmM3Ny0zNTVkNjMyNzBiMmYiLCJhdWQiOiI3ajhnZWdxYTNtNGxsNXA5Y3RhcTBhc2VtbSIsImNvZ25pdG86Z3JvdXBzIjpbImRjcy1hZG1pbiJdLCJldmVudF9pZCI6IjliOWE3MTVkLWE5OTAtNGFlNy1iMGVhLTY5ZDc2YjAxYjk5NiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjA3MzIyODU5LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9LQWo2VFdpWWIiLCJjdXN0b206ZGNzOnByaXZpbGVnZXMiOiJbXCJhZy4yLmVkaXRcIixcImFnLjIudmlld1wiLFwiYWcuOS5lZGl0XCIsXCJhZy45LnZpZXdcIixcImFnLjEwLmVkaXRcIixcImFnLjEwLnZpZXdcIixcImFnLjExLmVkaXRcIixcImFnLjExLnZpZXdcIixcImFnLjEyLmVkaXRcIixcImFnLjEyLnZpZXdcIixcImFnLjI5LmVkaXRcIixcImFnLjI5LnZpZXdcIixcImFnLjEzLmVkaXRcIixcImFnLjEzLnZpZXdcIixcImFnLjE0LmVkaXRcIixcImFnLjE0LnZpZXdcIixcImFnLjE1LmVkaXRcIixcImFnLjE1LnZpZXdcIixcImFnLjE2LmVkaXRcIixcImFnLjE2LnZpZXdcIixcImFnLjE3LmVkaXRcIixcImFnLjE3LnZpZXdcIixcImFnLjI2LmVkaXRcIixcImFnLjI2LnZpZXdcIixcImFnLjI4LmVkaXRcIixcImFnLjI4LnZpZXdcIixcImFnLjIwLmVkaXRcIixcImFnLjIwLnZpZXdcIixcImFnLjIxLmVkaXRcIixcImFnLjIxLnZpZXdcIixcImFnLjI3LmVkaXRcIixcImFnLjI3LnZpZXdcIixcImFnLjguZWRpdFwiLFwiYWcuOC52aWV3XCIsXCJhZy4yMy5lZGl0XCIsXCJhZy4yMy52aWV3XCIsXCJhZy4yNS5lZGl0XCIsXCJhZy4yNS52aWV3XCJdIiwiY29nbml0bzp1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjA3MzI2NDU5LCJjdXN0b206b3N0eXBlIjoiYWRtaW4iLCJpYXQiOjE2MDczMjI4NTl9.FZKDmU-QC8yUQaw-PAzVRYlYjzDpZjpoO75BHf1DIb1I__lh6i8msTd5E9uzJ1cBTS15eKpPtItdwh2kvoHgYtWDfMqPsmVipESjC089Y7WtvZOAoHSAFpZS7JEY6VM-iZScxZfFkpWietH1o9pRPMWpBOH1eKFIvFPHBEj8VXVfdwBdUK1PIAvUcBiw-YbTHhlG9LBOYAIwzTY7kkPPRacKkX1xGfDX2bgVBtWUCSZLidCBxl5fRuwwajtT-Ag95a43wRPgJY6uQFcMpggae1up6GhXE22Uh_k7Pnuw8p3zYCh6jOkiHTaelGk9GHLF832pbstmRO-BLLHK947Q8w';

    loadedApplications = this.idvService.loadedApplications;

  constructor(
      private idvService: IdvService,
      private imageService: ImageService
  ) {
  }

  ngOnInit(): void {
      this.loadedApplications.set(this.referenceId, null);

      this.idvService.openTransaction(this.referenceId, () => {
          this.initializeGemaltoFrame();
      });

      this.getImage('');
  }

  ngAfterViewInit(): void {
  }

  initializeGemaltoFrame(): void {
      const currentApplicationData = this.loadedApplications.get(this.referenceId);
      const idvTxs = currentApplicationData.idvTransactions;
      const transactionsLength = idvTxs.length;

      console.log('### initializeGemaltoFrame - currentApplicationData: ', currentApplicationData);
  }

  getLoadedApplication(appRefId: string): ApplicationData {
      return this.loadedApplications.get(appRefId);
  }

  getSrc(path): string {
      const temp = '/2020/12/07/03/56/14/a407afc984f1014c-a407afc984f1014c-req-docFront.jpg.encrypted';

      return ApiConstant.API_IDV_RESULT_IMAGES + temp;
  }

  getImage(path): void {
      const temp = '/2020/12/07/03/56/14/a407afc984f1014c-a407afc984f1014c-req-docFront.jpg.encrypted';
      path = temp;

      const url = UrlConstant.getIdvImageUrl(path);
      // this.imageService.getImage(url)
      //     .subscribe(
      //         (resp) => {
      //             console.log('resp: ', this.base64encode(resp));
      //         }
      //     );

      // console.log('#### resp: ', this.base64encode(this.getBinary(url)));
      this.toDataURL(url, (data) => {
          console.log('result: ', data);
          this.imagePath = data;
      });

      // this.toDataURL2(url)
      //     .then(dataUrl => {
      //         console.log('@@@222 : ', dataUrl);
      //         this.imagePath = dataUrl;
      //     });

      // console.log('imagePath: ', this.imagePath);
  }

  toDataURL2 = url => fetch(url, {
          headers: {
              Authorization: 'Bearer ' +localStorage.getItem('access_token')
          }
        })
      .then(response => response.blob())
      .then(blob => new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.onloadend = () => resolve(reader.result);
          reader.onerror = reject;
          console.log('blob: ', blob.text());
          reader.readAsDataURL(blob);
      }));

  toDataURL(url, callback) {
      var xhr = new XMLHttpRequest();

      xhr.onload = () => {
          var reader = new FileReader();
          reader.onloadend = () => {
              callback(reader.result);
          };

          reader.readAsDataURL(xhr.response);
      };

      xhr.open('GET', url);
      xhr.setRequestHeader("Authorization", 'Bearer ' +localStorage.getItem('access_token'));
      xhr.responseType = 'blob';
      xhr.send();
  }
}
