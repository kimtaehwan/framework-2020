import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDragComponent } from './table-drag.component';

describe('TableDragComponent', () => {
  let component: TableDragComponent;
  let fixture: ComponentFixture<TableDragComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDragComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
