import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from './user';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-table-drag',
  templateUrl: './table-drag.component.html',
  styleUrls: ['./table-drag.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TableDragComponent implements OnInit {

    users: User[] = [
        {
            first: 'Everette',
            last: 'Cremin',
            email: 'Everette.Cremin@brody.org',
            address: '2830 Adan Courts',
            order: 1
        },
        {
            first: 'Rosie',
            last: 'Monahan',
            email: 'Rosie.Monahan@van.net',
            address: '01602 Jacobi Manor',
            order: 2
        },
        {
            first: 'Braeden',
            last: 'Roob',
            email: 'Braeden.Roob@bartholome.info',
            address: '8682 Rosalia Squares',
            order: 3
        },
        {
            first: 'Myron',
            last: 'Harvey',
            email: 'lavendergiraffe57@gmail.com',
            address: '10939 Williamson Highway',
            order: 4
        },
        {
            first: 'Christ',
            last: 'Goodwin',
            email: 'Christ.Goodwin@tod.info',
            address: '814 Prohaska Roads',
            order: 5
        }
    ];

  constructor() { }

  ngOnInit(): void {
  }

  onDrop(event: CdkDragDrop<string[]>): void {
      moveItemInArray(this.users, event.previousIndex, event.currentIndex);
      this.users.forEach((user, idx) => {
          user.order = idx + 1;
      });
  }
}

