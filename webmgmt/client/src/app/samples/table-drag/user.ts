export class User {
    order?: number;
    first: string;
    last: string;
    email: string;
    address: string;
}
