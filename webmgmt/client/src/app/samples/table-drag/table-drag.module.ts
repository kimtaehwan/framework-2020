import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TableDragComponent} from './table-drag.component';
import {MaterialModule} from '../../main/common/utils/material/material.module';

const routes: Routes = [
    {
        path: '**',
        component: TableDragComponent
    }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule
  ]
})
export class TableDragModule { }
