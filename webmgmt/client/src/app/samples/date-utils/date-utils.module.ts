import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../main/common/utils/material/material.module';
import {RouterModule, Routes} from '@angular/router';
import {DateUtilsComponent} from './date-utils.component';

const routes: Routes = [
    {
        path: '**',
        component: DateUtilsComponent
    }
];

@NgModule({
  declarations: [DateUtilsComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule
  ]
})
export class DateUtilsModule { }
