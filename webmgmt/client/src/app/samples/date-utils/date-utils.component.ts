import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-date-utils',
  templateUrl: './date-utils.component.html',
  styleUrls: ['./date-utils.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DateUtilsComponent implements OnInit {

    resumeDays: number;
    resumeHours: number;
    resumeMins: number;

  constructor() { }

  ngOnInit(): void {
      this.setResume(100);
  }

  setResume(time: number): void {
      this.resumeDays = this.getResumeDays(time);
  }

  getResumeDays(time: number): number {
      const result = time / 60;
      return result / 24;
  }

}
