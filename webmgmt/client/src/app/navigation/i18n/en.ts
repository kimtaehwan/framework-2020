export const locale = {
    lang: 'en',
    data: {
        NAV: {
            'APPLICATIONS': 'Applications',
            'JOURNEY_MANAGEMENT': 'Journey Management',
            'CONTENT_MANAGEMENT': 'Content Management',
            'FI_MANAGEMENT': 'FI Management',
            'SAMPLES' : {
                TITLE: 'Samples',
                BADGE: '25'
            },
            'PROCESS' : {
                TITLE: 'Process'
            },
            'JOURNEY': {
                TITLE: 'Journey'
            },
            'CRUD': {
                CREATE: 'Create',
                EDIT: 'Edit',
                DETAIL: 'Detail'
            },
            'PRODUCT': 'Product',
            'TERMS': 'Terms',
            'FI_ONBOARDING': 'Fi Onboarding',
            'LOGIN': 'Login',
            'LOGOUT': 'Logout',
            'QUICK-ACCESS': 'Quick Access',
            'JOURNEY-MANAGEMENT': 'Journey Management',
            'CONTENT-MANAGEMENT': 'Content Management',
            'FI-MANAGEMENT': 'FI Management',
            'MEMBERSHIP': 'Membership',
            'DASHBOARD': 'Dashboard',
            'CUSTOMER-SUPPORT': 'Customer Support',
            'VIDEO-CHAT-CENTER': 'Video Chat Center',
            'IDV-PROTOTYPE': 'IDV Prototype',
            'APPLICATION-LIST': 'Application List',
            'IDV-TEST': 'IDV Test',
            'REPORTS': 'Reports',
            'FI-ONBOARDING': 'Fi Onboarding',
            'FI-CONFIG': 'Fi Config',
            'JOURNEY-POINT': 'Journey Point',
            'JOURNEY-LIST': 'Journey List',
            'JOURNEY-SUMMARY': 'Summary',
            'CONSENT': 'Consent',
            'ELIGIBILITY': 'Eligibility',
            'E-SIGNATURE': 'E-Signature',
            'APPLICATION-FORM': 'Application Form',
            'BENEFIT-SELECTION': 'Benefit Selection',
            'MASTERCARD-ON': 'Mastercard On',
            'ADMIN-GROUP': 'Admin Group',
            'IT': 'IT',
            'USER-ACCESS-MANAGEMENT': 'User Access Management',
            'USER_GROUPS': 'User Groups',
            'EXPORT-DATA': 'Export Data',
            'GLOBAL': 'Global',
            'POLICY_MANAGEMENT': 'Policy Management',
            'POLICY': {
                TITLE: 'Policy'
            }
        },
        TEXT: {
            SEARCH_TITLE: 'Search by FI Name / FI Token / Journey Name',
            SEARCH_JOURNEY_STATUS: 'Journey Status'
        },
        CONTENT_MANAGEMENT: {
            SEARCH_FI: 'Search in Specific FI',
            SEARCH_TITLE: 'Search by Journey Name or Journey ID',
            SEARCH_JOURNEY_STATUS: 'Journey Status',
            SEARCH_DATE_RANGE: 'Enter date range'
        },
        DATETIME_PICKER: {
            BUTTON: {
                SET: 'Set',
                CLEAR: 'Clear'
            }
        },
        FORMAT: {
            DATE: {
                DATETIME: 'YY-MMMM-DD · HH:mm z',
                DATE: 'YY-MMMM-DD',
            }
        },
    }
};
