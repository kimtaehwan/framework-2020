export const locale = {
    lang: 'tr',
    data: {
        NAV: {
            APPLICATIONS: 'Programlar',
            SAMPLE        : {
                TITLE: 'Örnek',
                BADGE: '15'
            },
            CONTENT_MANAGEMENT: {
                SEARCH_TITLE: 'Search by FI Name / FI Token / Journey Name',
                SEARCH_JOURNEY_STATUS: 'Journey Status'
            },
        }
    }
};
