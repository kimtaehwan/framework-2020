import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'customer-support',
        title: 'Customer Support',
        translate: 'NAV.CUSTOMER-SUPPORT',
        type: 'collapsable',
        icon: '',
        children: [
            {
                id: 'application-list',
                title: 'Application List',
                translate: 'NAV.APPLICATION-LIST',
                type: 'item',
                url: '/main/applications'
            }
        ]
    },
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'samples',
                title    : 'Samples',
                translate: 'NAV.SAMPLES.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/samples',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLES.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'process',
                title    : 'Process',
                translate: 'NAV.PROCESS.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/process'
            }
        ]
    },
    {
        id       : 'journey-management',
        title    : 'Journey Management',
        translate: 'NAV.JOURNEY_MANAGEMENT',
        type     : 'collapsable',
        icon     : 'view_list',
        children : [
            {
                id       : 'journey',
                title    : 'Journey',
                translate: 'NAV.JOURNEY.TITLE',
                type     : 'item',
                icon     : 'credit_card',
                url      : '/main/journey',
                children : [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/journey/create',
                    }
                ]
            }
        ]
    },
    {
        id       : 'policy-management',
        title    : 'Policy Management',
        translate: 'NAV.POLICY_MANAGEMENT',
        type     : 'collapsable',
        icon     : 'view_list',
        children : [
            {
                id       : 'policy',
                title    : 'Policy',
                translate: 'NAV.POLICY.TITLE',
                type     : 'item',
                icon     : 'credit_card',
                url      : '/main/policy',
                children : [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/policy/create',
                    }
                ]
            }
        ]
    },
    {
        id       : 'content-management',
        title    : 'Content Management',
        translate: 'NAV.CONTENT_MANAGEMENT',
        type     : 'collapsable',
        icon     : 'view_list',
        children : [
            {
                id       : 'product',
                title    : 'Product',
                translate: 'NAV.PRODUCT',
                type     : 'item',
                icon     : 'credit_card',
                url      : '/main/product',
                children : [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/product/create',
                    },
                    {
                        id       : 'edit',
                        title    : 'Edit',
                        translate: 'NAV.CRUD.EDIT',
                        type     : 'item',
                        url      : '/product/edit',
                        hidden   : true
                    }
                ]
            },
            {
                id       : 'terms',
                title    : 'Terms',
                translate: 'NAV.TERMS',
                type     : 'item',
                icon     : 'credit_card',
                url      : '/main/terms',
                children : [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/terms/create',
                    },
                    {
                        id       : 'edit',
                        title    : 'Edit',
                        translate: 'NAV.CRUD.EDIT',
                        type     : 'item',
                        url      : '/terms/edit',
                        hidden   : true
                    }
                ]
            }
        ]
    },
    {
        id       : 'fi-management',
        title    : 'FI Management',
        translate: 'NAV.FI_MANAGEMENT',
        type     : 'collapsable',
        icon     : 'view_list',
        children : [
            {
                id       : 'fi-onboarding',
                title    : 'FI Onboarding',
                translate: 'NAV.FI_ONBOARDING',
                type     : 'item',
                url      : '/main/fiOnboarding',
                children : [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/fiOnboarding/create',
                    },
                    {
                        id       : 'edit',
                        title    : 'Edit',
                        translate: 'NAV.CRUD.EDIT',
                        type     : 'item',
                        url      : '/fiOnboarding/edit',
                        hidden   : true
                    },
                ]
            }
        ]
    },

    {
        id: 'id',
        title: 'IT',
        translate: 'NAV.IT',
        type: 'collapsable',
        icon: 'people',
        children: [
            {
                id: 'global',
                title: 'Global',
                translate: 'NAV.GLOBAL',
                type: 'item',
                url: 'main/global'
            },
            {
                id: 'export-job',
                title: 'Export Data',
                translate: 'NAV.EXPORT-DATA',
                type: 'item',
                url: 'main/exportJob'
            }
        ]
    },
    {
        id       : 'user-access',
        title    : 'User Access Management',
        translate: 'NAV.USER-ACCESS-MANAGEMENT',
        type     : 'collapsable',
        icon     : 'people', // \e805
        children : [
            {
                id       : 'user-groups',
                title    : 'User Groups',
                translate: 'NAV.USER_GROUPS',
                type     : 'item',
                url      : '/main/userGroups'
            }
        ]
    },
    {
        id       : 'logout',
        title    : 'Logout',
        translate: 'NAV.LOGOUT',
        type     : 'group',
        icon     : 'view_list',
        children : [
            {
                id       : 'logout',
                title    : 'Logout',
                translate: 'NAV.LOGOUT',
                type     : 'item',
                icon     : 'exit_to_app',
                url      : '/auth/login'
            }
        ]
    }
];
