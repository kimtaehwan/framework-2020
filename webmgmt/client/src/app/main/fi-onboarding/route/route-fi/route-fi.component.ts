import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {ValueConstant} from '../../../utils/constants/value-constant';
import {Country} from '../../../models/fi-onboarding/country';
import {Metadata} from '../../../models/common/metadata';
import {FiOnboardingService} from '../../../service/fi-onboarding.service';

@Component({
  selector: 'app-route-fi',
  templateUrl: './route-fi.component.html',
  styleUrls: ['./route-fi.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RouteFiComponent implements OnInit {

    MIN_ITEM_NUM = 1;
    MAX_ITEM_NUM = 4;

    editId = 0;
    fiForm: FormGroup;
    metadataList: Metadata[] = [];

    countries: Country[];

    isReadOnly = false;
    isView: boolean;

  constructor(
      private formBuilder: FormBuilder,
      private dialog: MatDialog,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private fiOnboardingService: FiOnboardingService
  ) { }

  ngOnInit(): void {
      this.fiForm = this.initForm();
      this.getCountries();
  }

  saveFi(): void {
      const saveData = {
          name: this.fiForm.value.name,
          token: this.fiForm.value.token,
          tokenAlias1: this.fiForm.value.tokenAlias1,
          tokenAlias2: this.fiForm.value.tokenAlias2,
          contactEmailAddress: this.fiForm.value.contactEmailAddress,
          billingEmailAddress: this.fiForm.value.billingEmailAddress,
          sourceEmailAddress: this.fiForm.value.sourceEmailAddress,
          defaultHomeCountryId: this.fiForm.value.defaultHomeCountryId
      };

      if ( !!this.editId ) {
          // update
      } else {
          this.createFi(saveData);
      }
  }

  createFi(saveData): void {
      this.fiOnboardingService.postFi(saveData)
          .subscribe(
              (resp) => {
                  console.log('createFi: ', resp);
                  this.router.navigate([this.goBack()], {relativeTo: this.activatedRoute});
              }
          );
  }

  getCountries(): void {
      this.fiOnboardingService.getCountries()
          .subscribe(
              (resp) => {
                  console.log('getCountries: ', resp);
                  this.countries = resp.body;
              }
          );
  }

  initForm(): FormGroup {
      return this.formBuilder.group({
          name: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(1),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_EXCEPT_SCRIPT)
          ])),
          contactEmailAddress: new FormControl('', Validators.compose([
              Validators.pattern(ValueConstant.PATTERN_EMAIL)
          ])),
          billingEmailAddress: new FormControl('', Validators.compose([
              Validators.pattern(ValueConstant.PATTERN_EMAIL)
          ])),
          sourceEmailAddress: new FormControl('', Validators.compose([
              Validators.required,
              Validators.pattern(ValueConstant.PATTERN_EMAIL)
          ])),
          token: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(1),
              Validators.maxLength(20),
              Validators.pattern(ValueConstant.PATTERN_ALPHA_NUMERIC)
          ])),
          tokenAlias1: new FormControl(''),
          tokenAlias2: new FormControl(''),
          defaultHomeCountryId: new FormControl('', Validators.compose( [
              Validators.required
          ])),
          metadataList: new FormArray([])
      });
  }

  get metadatas(): FormArray {
      return this.fiForm.get('metadataList') as FormArray;
  }

  addMetadata(metadata: Metadata): void {
      if (!metadata) {
          metadata = new Metadata();
      }

      this.metadatas.push(this.newMetadata(metadata));
      this.metadataList.push(metadata);

      console.log('addMetadata: ', metadata);
  }

  newMetadata(metadata: Metadata): FormGroup {
      return this.formBuilder.group({
          key: new FormControl(metadata.key, Validators.compose([Validators.required])),
          value: new FormControl(metadata.value, Validators.compose([Validators.required])),
          description: metadata.description
      });
  }

  removeMetadata(): void {
      const lastIndex = this.metadatas.length - 1;
      this.metadatas.removeAt(lastIndex);
      this.metadataList.pop();
  }

  cancel(): void {
      this.router.navigate([this.goBack()], {relativeTo: this.activatedRoute});
  }

  goBack(): string {
      if ( this.editId ) {
          return '../..';
      }

      return '..';
  }

}
