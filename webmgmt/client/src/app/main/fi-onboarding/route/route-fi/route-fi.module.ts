import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteFiComponent } from './route-fi.component';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {RouterModule, Routes} from '@angular/router';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';

const routes: Routes = [
    {
        path: ':editId',
        component: RouteFiComponent
    },
    {
        path: '**',
        component: RouteFiComponent
    }
];

@NgModule({
  declarations: [RouteFiComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule,

      BuilderTitleModule
  ]
})
export class RouteFiModule { }
