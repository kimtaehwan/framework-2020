import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ValueConstant} from '../utils/constants/value-constant';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {FiOnboardingService} from '../service/fi-onboarding.service';
import {UrlConstant} from '../utils/constants/url-constant';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {DeleteFiComponent} from './dialog/delete-fi/delete-fi.component';
import {Fi} from '../models/fi-onboarding/fi';
import {CommonSearch} from '../models/common/common-search';

@Component({
  selector: 'app-fi-onboarding',
  templateUrl: './fi-onboarding.component.html',
  styleUrls: ['./fi-onboarding.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FiOnboardingComponent implements OnInit {

    commonSearch = new CommonSearch();
    dataSource = new MatTableDataSource([]);
    type = ValueConstant.TYPE_FI_ONBOARDING;

  constructor(
      private dialog: MatDialog,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private fiOnboardingService: FiOnboardingService
  ) { }

  ngOnInit(): void {
      this.search();
  }

  search(): void {
      console.log('### search(): ', this.commonSearch);

      const url = UrlConstant.getFiContentsUrl(this.commonSearch);

      this.fiOnboardingService.getFiContents(url)
          .subscribe(
              (resp) => {
                  console.log('search fi: ', resp.body);
                  this.dataSource = new MatTableDataSource(resp.body.content);
              }
          );
  }

  create(): void {
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

  view(id: number): void {
      this.router.navigate(['./view', id], {relativeTo: this.activatedRoute});
  }

  edit(id: number): void {
      console.log('fi edit id: ', id);
      this.router.navigate(['./edit', id], {relativeTo: this.activatedRoute});
  }

  delete(id: number): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.width = '800px';
      dialogConfig.data = id;
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      const deleteDialogRef = this.dialog.open(DeleteFiComponent, dialogConfig);

      deleteDialogRef.afterClosed().subscribe(
          result => {
              this.search();
          }
      );
  }
}
