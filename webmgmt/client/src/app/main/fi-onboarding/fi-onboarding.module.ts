import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiOnboardingComponent } from './fi-onboarding.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import { DeleteFiComponent } from './dialog/delete-fi/delete-fi.component';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import {MgmtTableListModule} from '../common/module/mgmt-table-list/mgmt-table-list.module';

const routes: Routes = [
    {
        path: '**',
        component: FiOnboardingComponent
    }
];

@NgModule({
  declarations: [FiOnboardingComponent, DeleteFiComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      TopSearchBarModule,
      MgmtTableHeaderModule,
      MgmtTableListModule,
      FuseSharedModule
  ],
    entryComponents: [DeleteFiComponent]
})
export class FiOnboardingModule { }
