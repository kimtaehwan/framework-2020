import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportJobComponent } from './export-job.component';

describe('ExportJobComponent', () => {
  let component: ExportJobComponent;
  let fixture: ComponentFixture<ExportJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
