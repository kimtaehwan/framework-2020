import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ValueConstant} from '../utils/constants/value-constant';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-export-job',
  templateUrl: './export-job.component.html',
  styleUrls: ['./export-job.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExportJobComponent implements OnInit {

    dataSource = new MatTableDataSource([]);
    name = ValueConstant.NAME_EXPORT_JOB;
    type = ValueConstant.TYPE_EXPORT_JOB;

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  create(): void {
      console.log('export job create');
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

  view(id: number): void {
      this.router.navigate(['..', 'view', id], {relativeTo: this.activatedRoute});
  }

  edit(id: number): void {
      this.router.navigate(['..', 'edit', id], {relativeTo: this.activatedRoute});
  }
}
