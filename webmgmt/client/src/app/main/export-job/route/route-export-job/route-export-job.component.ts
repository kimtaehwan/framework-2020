import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApplicationStatus} from '../../../models/export-job/application-status';
import {ValueConstant} from '../../../utils/constants/value-constant';
import {SatPopover} from '@ncstate/sat-popover';

@Component({
  selector: 'app-route-export-job',
  templateUrl: './route-export-job.component.html',
  styleUrls: ['./route-export-job.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RouteExportJobComponent implements OnInit {

    commonForm: FormGroup;
    exportForm: FormGroup;
    scheduleForm: FormGroup;

    applicationStatusList: ApplicationStatus[] = [];
    dateTimeFromTo: Array<Date> = new Array(2) ;
    fromDateTime: Date;
    toDateTime: Date;

    isNotProcessedFromDateTime: boolean;
    isNotProcessedToDateTime: boolean;

    scheduleExport: string;
    scheduleDate: string = "123";

    singleMode = true;

  constructor(
      private formBuilder: FormBuilder
  ) {
      this.getApplicationStatus();
  }

  ngOnInit(): void {
      this.initForm();
      this.initScheduleForm();
  }

  initForm(): void {
      this.exportForm = this.formBuilder.group({
          applicationStatusList: new FormArray([]),
          scheduleExport: new FormControl(''),
          email1: new FormControl('', Validators.compose([
              Validators.required,
              Validators.pattern(ValueConstant.PATTERN_EMAIL)])),
          email2: new FormControl('', Validators.compose([
              Validators.required,
              Validators.pattern(ValueConstant.PATTERN_EMAIL)]))
      });
  }

  initScheduleForm(): void {
      this.scheduleForm = this.formBuilder.group({
          startDate: new FormControl(),
          endDate: new FormControl()
      });
  }

  getApplicationStatus(): void {
      let applicationStatus = new ApplicationStatus();
      applicationStatus.value = 'APPROVED_CARD_ISSUED';
      applicationStatus.description = 'Approved & Card Issued';
      this.applicationStatusList.push(applicationStatus);

      applicationStatus = new ApplicationStatus();
      applicationStatus.value = 'APPROVED_CARD_NOT_ISSUED';
      applicationStatus.description = 'Approved & Card Not Issued';
      this.applicationStatusList.push(applicationStatus);

      applicationStatus = new ApplicationStatus();
      applicationStatus.value = 'NOT_APPROVED';
      applicationStatus.description = 'Not Approved';
      this.applicationStatusList.push(applicationStatus);

      applicationStatus = new ApplicationStatus();
      applicationStatus.value = 'DATA_COLLECTION_IN_PROGRESS';
      applicationStatus.description = 'Data Collection — In progress';
      this.applicationStatusList.push(applicationStatus);

      applicationStatus = new ApplicationStatus();
      applicationStatus.value = 'DATA_COLLECTION_FAIL';
      applicationStatus.description = 'Data Collection — Fail';
      this.applicationStatusList.push(applicationStatus);
  }

  onCheckChange(status: ApplicationStatus): void {
      console.log('status: ', status);
  }

  onOpenFromDateTimePicker(): void {
      this.isNotProcessedFromDateTime = true;
  }

  onCloseFromDateTimePicker(): void {
      if ( this.isNotProcessedFromDateTime && !this.singleMode) {
          this.dateTimeFromTo = [...this.dateTimeFromTo];

          this.dateTimeFromTo[0] = new Date(this.fromDateTime);
          if ( !this.fromDateTime ){
              this.dateTimeFromTo[0] = null;
          }
      }
  }

  onCloseToDateTimePicker(): void {
      if (this.isNotProcessedToDateTime) {
          this.dateTimeFromTo = [...this.dateTimeFromTo];

          this.dateTimeFromTo[1] = new Date(this.toDateTime);
          if (!this.toDateTime) {
              this.dateTimeFromTo[1] = null;
          }

          return;
      }
  }

  onOpenToDateTimePicker(): void {
      this.isNotProcessedToDateTime = true;
  }

  onChangedFrom(): void {
      this.scheduleDate = 'change';
  }

  onChangedTo(): void {
      if (!this.dateTimeFromTo[0]) {
          this.fromDateTime = null;
      }
  }

  clearToDate(popover: SatPopover): void {
      this.isNotProcessedToDateTime = false;
      this.toDateTime = null;
      this.dateTimeFromTo[1] = null;

      popover.close();
  }

  setToDate(popover: SatPopover): void {
      this.isNotProcessedFromDateTime = false;
      this.fromDateTime = this.dateTimeFromTo[0];

      popover.close();
  }

  getDateTime(): void {
      console.log('### this.dateTimeFromTo[0]: ', this.dateTimeFromTo[0]);
      console.log('### this.dateTimeFromTo[1]: ', this.dateTimeFromTo[1]);
  }
}
