import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteExportJobComponent } from './route-export-job.component';

describe('RouteExportJobComponent', () => {
  let component: RouteExportJobComponent;
  let fixture: ComponentFixture<RouteExportJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteExportJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteExportJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
