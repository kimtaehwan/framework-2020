import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouteExportJobComponent} from './route-export-job.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {BuilderAttacherModule} from '../../../common/module/builder-attacher/builder-attacher.module';
import {TranslateModule} from '@ngx-translate/core';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from '@danielmoncada/angular-datetime-picker';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';

const routes: Routes = [
    {
        path: ':editId',
        component: RouteExportJobComponent
    },
    {
        path: '**',
        component: RouteExportJobComponent
    }
];

@NgModule({
  declarations: [RouteExportJobComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule,
      TranslateModule,
      SatPopoverModule,
      OwlDateTimeModule,
      OwlNativeDateTimeModule,

      BuilderPageModule,
      BuilderBodyModule,
      BuilderTitleModule,
      BuilderAttacherModule
  ]
})
export class RouteExportJobModule { }
