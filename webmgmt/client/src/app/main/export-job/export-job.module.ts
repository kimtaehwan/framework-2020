import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ExportJobComponent} from './export-job.component';
import {MaterialModule} from '../common/utils/material/material.module';
import {TranslateModule} from '@ngx-translate/core';
import {BuilderPageModule} from '../common/module/builder-page/builder-page.module';
import {BuilderBodyModule} from '../common/module/builder-body/builder-body.module';
import {BuilderTitleModule} from '../common/module/builder-title/builder-title.module';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import {MgmtTableListModule} from '../common/module/mgmt-table-list/mgmt-table-list.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: ExportJobComponent
    }
];

@NgModule({
  declarations: [ExportJobComponent],
  imports: [
    CommonModule,

      MaterialModule,
      RouterModule.forChild(routes),

      TranslateModule,
      BuilderPageModule,
      BuilderBodyModule,
      BuilderTitleModule,
      TopSearchBarModule,

      MgmtTableHeaderModule,
      MgmtTableListModule,

      FuseSharedModule
  ]
})
export class ExportJobModule { }
