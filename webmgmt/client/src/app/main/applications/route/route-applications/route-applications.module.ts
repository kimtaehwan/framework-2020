import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RouteApplicationsComponent} from './route-applications.component';

const routes: Routes = [
    {
        path: ':applicationReferenceId',
        component: RouteApplicationsComponent
    },
    {
        path: '**',
        component: RouteApplicationsComponent
    }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

      RouterModule.forChild(routes)

  ]
})
export class RouteApplicationsModule { }
