import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteApplicationsComponent } from './route-applications.component';

describe('RouteApplicationsComponent', () => {
  let component: RouteApplicationsComponent;
  let fixture: ComponentFixture<RouteApplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteApplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
