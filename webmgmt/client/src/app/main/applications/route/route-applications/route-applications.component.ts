import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApplicationsService} from '../../../service/applications.service';

@Component({
  selector: 'app-route-applications',
  templateUrl: './route-applications.component.html',
  styleUrls: ['./route-applications.component.scss']
})
export class RouteApplicationsComponent implements OnInit {

    currentApplicationReferenceId: string;

  constructor(
      private route: ActivatedRoute,
      private appListService: ApplicationsService
  ) { }

  ngOnInit(): void {
      this.currentApplicationReferenceId = this.route.snapshot.paramMap.get('applicationReferenceId');

      console.log('### currentApplicationReferenceId: ', this.currentApplicationReferenceId);

      this.openTransaction(this.currentApplicationReferenceId);
  }

  openTransaction(appRefId: string): void {
      this.appListService.openTransaction(appRefId, () => {
          console.log('callback');
      });
  }
}
