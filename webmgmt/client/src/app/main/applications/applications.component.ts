import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ValueConstant} from '../utils/constants/value-constant';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

    dataSource = new MatTableDataSource([]);
    name = ValueConstant.NAME_APPLICATIONS;
    type = ValueConstant.TYPE_APPLICATIONS;

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  create(): void {

      this.router.navigate(['./view', 'm08k9dzwkkh4'], {relativeTo: this.activatedRoute});
  }
}
