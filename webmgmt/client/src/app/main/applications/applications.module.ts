import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationsComponent } from './applications.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import {MgmtTableListModule} from '../common/module/mgmt-table-list/mgmt-table-list.module';

const routes: Routes = [
    {
        path: '**',
        component: ApplicationsComponent
    }
];

@NgModule({
  declarations: [ApplicationsComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),
      MaterialModule,

      FuseSharedModule,
      TopSearchBarModule,
      MgmtTableHeaderModule,
      MgmtTableListModule
  ]
})
export class ApplicationsModule { }
