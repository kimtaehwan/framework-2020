import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EligibilityComponent } from './eligibility.component';
import { RouteEligibilityComponent } from './route/route-eligibility/route-eligibility.component';
import { DeleteEligibilityComponent } from './dialog/delete-eligibility/delete-eligibility.component';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MaterialModule} from '../common/utils/material/material.module';

const routes: Routes = [
    {
        path: '**',
        component: EligibilityComponent
    }
];

@NgModule({
  declarations: [EligibilityComponent, DeleteEligibilityComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      TranslateModule,
      MaterialModule
  ],
    entryComponents: [DeleteEligibilityComponent]
})
export class EligibilityModule { }
