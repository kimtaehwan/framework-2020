import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteEligibilityComponent } from './route-eligibility.component';

describe('RouteEligibilityComponent', () => {
  let component: RouteEligibilityComponent;
  let fixture: ComponentFixture<RouteEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
