import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEligibilityComponent } from './delete-eligibility.component';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DeleteEligibilityComponent', () => {
  let component: DeleteEligibilityComponent;
  let fixture: ComponentFixture<DeleteEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteEligibilityComponent ],
        imports: [
            MatDialogModule,
            HttpClientTestingModule
        ],
        providers: [
            {
                provide: MAT_DIALOG_DATA, useValue: {}
            }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
