import {Component, Inject, OnInit} from '@angular/core';
import {EligibilityService} from '../../../service/eligibility.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ApiConstant} from '../../../utils/constants/api-constant';

@Component({
  selector: 'app-delete-eligibility',
  templateUrl: './delete-eligibility.component.html',
  styleUrls: ['./delete-eligibility.component.scss']
})
export class DeleteEligibilityComponent implements OnInit {

  constructor(
      @Inject(MAT_DIALOG_DATA) public eligibilityId: number,
      private eligibilityService: EligibilityService
  ) { }

  ngOnInit(): void {
  }

  deleteConfirm(): void {
      const url = ApiConstant.API_ELIGIBILITY_URL + '/' + this.eligibilityId;
      this.eligibilityService.deleteEligibility(url);
  }

}
