import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.scss']
})
export class JourneyComponent implements OnInit {

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  create(): void {
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

}
