import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JourneyComponent} from './journey.component';
import {RouterModule, Routes} from '@angular/router';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import {MaterialModule} from '../common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: JourneyComponent
    }
];

@NgModule({
  declarations: [JourneyComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      TopSearchBarModule,
      MgmtTableHeaderModule,

      MaterialModule,
      FuseSharedModule
  ]
})
export class JourneyModule { }
