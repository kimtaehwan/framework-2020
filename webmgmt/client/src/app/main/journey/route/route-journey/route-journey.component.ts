import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValueConstant} from '../../../utils/constants/value-constant';
import {Service} from '../../../models/journey/service';
import {ApiConstant} from '../../../utils/constants/api-constant';
import {JourneyService} from '../../../service/journey.service';
import {ServiceList} from '../../../models/journey/service-list';
import * as _ from 'underscore';
import {DragulaService} from 'ng2-dragula';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-route-journey',
  templateUrl: './route-journey.component.html',
  styleUrls: ['./route-journey.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RouteJourneyComponent implements OnInit, AfterViewInit {

    commonForm: FormGroup;

    SERVICES = 'SERVICES';
    servicesData: Service[] = [];
    displayServices: Service[] = [];
    beforeDragServices: Service[] = [];

    subs = new Subscription();

  constructor(
      private formBuilder: FormBuilder,
      private dragulaService: DragulaService,
      private journeyService: JourneyService
  ) { }

  ngOnInit(): void {
      this.commonForm = this.initCommonForm();

      this.subs.add(
          this.dragulaService.dropModel(this.SERVICES)
              .subscribe( ({name, sourceModel, targetModel, item}) => {
              })
      );

      this.dragulaService.createGroup( this.SERVICES, {
          moves:  (el, container, handle) => {
              return true;
          }
      });

      this.subs.add(this.dragulaService.drag( this.SERVICES )
          .subscribe(({ name, el, source }) => {
              this.beforeDragServices = this.displayServices.concat([]);
          })
      );

      this.subs.add(this.dragulaService.drop( this.SERVICES)
          .subscribe (({name, el, source}) => {
          })
      );
  }

  ngAfterViewInit(): void {
      this.getJourneyConfig();

  }

  getJourneyConfig(): void {
      const url = ApiConstant.JOURNEY_WEB_URL + '/journey-config';
      this.journeyService.getServices(url)
          .subscribe(
              (resp) => {
                  const serviceList: ServiceList = resp.body;
                  this.servicesData = _.sortBy(serviceList.services, 'position');

                  // this.setDefaultValueGroup(serviceList);
              }
          );
  }

  onProcessSelection(event: Event, selectedList: any[]): void {
      console.log('selectedList: ', selectedList);
      const enabledServiceArray = [];

      _.each(this.servicesData, (service) => {
          service.enabled = false;

          _.each(selectedList, (selected) => {
              if (Number(selected.value) === service.id) {
                  service.enabled = true;
              }
          });
      });

      this.displayServices = this.filterValidServicesOnly(this.servicesData);
  }

  filterValidServicesOnly(services: Service[]): Service[] {
      const enabledServiceArray: Service[] = [];
      const disabledServiceArray: Service[] = [];

      _.each(services, (service) => {
          if ( !!service.enabled ) {
              enabledServiceArray.push(service);
          } else {
              disabledServiceArray.push(service);
          }
      });

      return enabledServiceArray.concat(disabledServiceArray);
  }

  onDragulaModelChange(event: Service[]): void {

  }

  //
  // setDefaultValueGroup(serviceList: ServiceList): void {
  //     this.servicesData = _.sortBy(serviceList.services, 'position');
  //
  //     this.setDefaultEnumeration();
  //     this.setDefaultSplitIdList();
  // }
  //
  // setDefaultEnumeration(): void {
  //     _.each(this.servicesData, (service) => {
  //         _.each( service.vendors, (serviceVendor) => {
  //             _.each( serviceVendor.serviceVendorFields, (serviceVendorField) => {
  //                 _.each( serviceVendorField.serviceFields, (serviceField) => {
  //                     const fiedlFormat = serviceField.fieldFormat;
  //
  //                     if (fiedlFormat === ValueConstant.FIELD_FORMAT_ENUMERATION) {
  //                         serviceField.serviceFieldEnumerations = this.getServiceFieldEnumConvertValue(serviceField.serviceFieldEnumerations);
  //                     }
  //                 });
  //             });
  //         });
  //     });
  // }
  //
  // getServiceFieldEnumConvertValue(serviceFieldEnumerations: ServiceFieldEnumeration[]): ServiceFieldEnumeration[] {
  //     const results: ServiceFieldEnumeration[] = [];
  //
  //     _.each(serviceFieldEnumerations, (fieldEnum) => {
  //         fieldEnum.value = fieldEnum.value + '';
  //         results.push(fieldEnum);
  //     });
  //
  //     return results;
  // }
  //
  // setDefaultSplitIdList(): void {
  //     _.each(this.servicesData, (service) => {
  //         _.each(service.vendors, (serviceVendor) => {
  //             _.each(serviceVendor.serviceVendorFields, (serviceVendorField) => {
  //                 _.each(serviceVendorField.serviceFields, (serviceField) => {
  //                     const fieldFormat = serviceField.fieldFormat;
  //
  //                     this.setSplitIdList(fieldFormat, serviceField);
  //                 });
  //             });
  //         });
  //     });
  // }
  //
  // setSplitIdList(fieldFormat: string, serviceField: ServiceField): void {
  //
  // }

  initCommonForm(): FormGroup {
      return this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required)
      });
  }
}
