import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RouteJourneyComponent} from './route-journey.component';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {BuilderLeftBarModule} from '../../../common/module/builder-left-bar/builder-left-bar.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';
import {DragulaModule} from 'ng2-dragula';

const routes: Routes = [
    {
        path: '**',
        component: RouteJourneyComponent
    }
];

@NgModule({
  declarations: [RouteJourneyComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),
      DragulaModule.forRoot(),

      MaterialModule,

      BuilderPageModule,
      BuilderLeftBarModule,
      BuilderTitleModule,
      BuilderBodyModule
  ]
})
export class RouteJourneyModule { }
