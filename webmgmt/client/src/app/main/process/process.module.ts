import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessComponent } from './process.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '**',
        component: ProcessComponent
    }
];

@NgModule({
  declarations: [ProcessComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes)
  ]
})
export class ProcessModule { }
