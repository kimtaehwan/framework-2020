import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFileComponent } from './view-file.component';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';

describe('ViewFileComponent', () => {
    let component: ViewFileComponent;
    let fixture: ComponentFixture<ViewFileComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ViewFileComponent],
            imports: [
                MatDialogModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA, useValue: {}
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewFileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});