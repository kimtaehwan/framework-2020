import {Component, Inject, OnInit} from '@angular/core';
import {ViewFile} from '../../../models/terms/view-file';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-view-file',
  templateUrl: './view-file.component.html',
  styleUrls: ['./view-file.component.scss']
})
export class ViewFileComponent implements OnInit {

    fileContent = '';

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: ViewFile,
  ) { }

  ngOnInit(): void {
      this.fileContent = this.data.fileContent;
  }

  download(): void {
      const blob = new Blob([this.fileContent], {type: 'text/html'});
      const url = URL.createObjectURL(blob);
      const link = document.getElementById('link');

      link.setAttribute('href', url);
      link.click();
  }

}
