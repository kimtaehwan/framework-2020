import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TermsComponent} from './terms.component';
import {MaterialModule} from '../common/utils/material/material.module';
import {RouterModule, Routes} from '@angular/router';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import { ViewFileComponent } from './dialog/view-file/view-file.component';

const routes: Routes = [
    {
        path: '**',
        component: TermsComponent
    }
];

@NgModule({
  declarations: [TermsComponent, ViewFileComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      TopSearchBarModule,
      MgmtTableHeaderModule
  ]
})
export class TermsModule { }
