import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValueConstant} from '../../../utils/constants/value-constant';
import {ErrorMessage} from '../../../models/utils/error-message';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ErrorMessageComponent} from '../../../common/dialog/error-message/error-message.component';
import {ApiConstant} from '../../../utils/constants/api-constant';
import {TermsService} from '../../../service/terms.service';
import {StorageService} from '../../../service/storage.service';
import {ViewFile} from '../../../models/terms/view-file';
import {ViewFileComponent} from '../../dialog/view-file/view-file.component';

@Component({
  selector: 'app-route-terms',
  templateUrl: './route-terms.component.html',
  styleUrls: ['./route-terms.component.scss']
})
export class RouteTermsComponent implements OnInit {

    commonForm: FormGroup;
    termsForm: FormGroup;

    fileName = '';
    fileContent = '';
    originalFileName = '';
    downloadUrl = '';

    errorMessage: ErrorMessage = new ErrorMessage();

  constructor(
      private formBuilder: FormBuilder,
      private dialog: MatDialog,
      private termsService: TermsService,
      private storageService: StorageService
  ) {
      this.initCommonForm();
      this.initForm();
  }

  ngOnInit(): void {
  }

  initCommonForm(): void {
      this.commonForm = this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required)
      });
  }

  initForm(): void {
      this.termsForm = this.formBuilder.group({
          filePath: new FormControl('', Validators.required)
      });
  }

  onFileChange(file): void {
      if (!!file) {
          if ( !(ValueConstant.REG_EXP_HTML).test(file.name)) {
              this.errorMessage.name = 'Image save error';
              this.errorMessage.message = 'Only \'html\', \'htm\' extensions files can be registered.';

              this.showErrorMessage();
              return ;
          }

          this.fileName = file.name;
          this.setDownloadUrl(file.name);

          const fileReader: FileReader = new FileReader();
          fileReader.onloadend = (e) => {
              this.fileContent = fileReader.result + '';
              this.saveFile(file);
          };

          fileReader.readAsText(file);
      }
  }

  saveFile(file: File): void {
      const formData = new FormData();
      formData.append('file', file);
      console.log('saveFile: ', file);

      this.storageService.postHtml(formData)
          .subscribe(
              (resp) => {
                  console.log('resp.body: ', resp.body);
                  this.termsForm.get('filePath').setValue(resp.body.path);
              }
          );
  }

  setDownloadUrl(fileName: string): void {
      this.downloadUrl = ApiConstant.API_STORAGE_URL + '/text/' + fileName;
  }

  onView(): void {
      const data = new ViewFile();
      data.fileName = this.fileName;
      data.fileContent = this.fileContent;
      data.downloadUrl = this.downloadUrl;

      const dialogConfig = new MatDialogConfig();
      dialogConfig.width = '80%';
      dialogConfig.height = '80%';
      dialogConfig.data = data;
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      const dialogRef = this.dialog.open(ViewFileComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(
          (result) => {
              console.log('result: ', result);
          }
      );
  }

  showErrorMessage(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.width = '800px';
      dialogConfig.data = this.errorMessage;
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      this.dialog.open(ErrorMessageComponent, dialogConfig);
  }

  saveData(): void {
      console.log('this.commomForm: ', this.commonForm.value);
      const saveData = this.getSaveData();

      console.log('saveData: ', saveData);

      this.createTerms(saveData);
  }

  getSaveData(): object {
      return {
          contentTitle: this.commonForm.get('contentTitle').value,
          fiToken: this.commonForm.get('fiToken').value,
          filePath: this.termsForm.get('filePath').value,
          originalFileName: this.fileName
      };
  }

  createTerms(saveData: object): void {
      this.termsService.postTerms(saveData)
          .subscribe(
              (resp) => {
                  console.log('resp: ', resp.body);
              }
          );
  }
}
