import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteTermsComponent } from './route-terms.component';
import {RouteTermsModule} from './route-terms.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FUSE_CONFIG, FuseConfigService} from '../../../../../@fuse/services/config.service';
import {RouterTestingModule} from '@angular/router/testing';
import {fuseConfig} from '../../../../fuse-config';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('RouteTermsComponent', () => {
  let component: RouteTermsComponent;
  let fixture: ComponentFixture<RouteTermsComponent>;
  let termsServiceStub;

  beforeEach(async(() => {
      termsServiceStub = {
          postTerms: (saveData) => {
              return true;
          }
      }
  }));


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteTermsComponent ],
        imports: [
            NoopAnimationsModule,
            HttpClientTestingModule,
            RouteTermsModule,
            RouterTestingModule.withRoutes([
                {
                    path: '**',
                    component: RouteTermsComponent
                }
            ])
        ],
        providers: [
            FuseConfigService,
            {
                provide: FUSE_CONFIG,
                useValue: fuseConfig
            }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
