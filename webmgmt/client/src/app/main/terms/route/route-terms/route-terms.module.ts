import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RouteTermsComponent} from './route-terms.component';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {BuilderAttacherModule} from '../../../common/module/builder-attacher/builder-attacher.module';
import {BuilderPhoneViewModule} from '../../../common/module/builder-phone-view/builder-phone-view.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: RouteTermsComponent
    }
];

@NgModule({
  declarations: [RouteTermsComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule,

      BuilderPageModule,
      BuilderBodyModule,
      BuilderTitleModule,
      BuilderAttacherModule,
      BuilderPhoneViewModule
  ]
})
export class RouteTermsModule { }
