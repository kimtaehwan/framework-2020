import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '../../../@fuse/services/translation-loader.service';
import { locale as navigationEnglish} from '../../navigation/i18n/en';
import { locale as navigationEspanol} from '../../navigation/i18n/tr';
import {PageEvent} from '@angular/material/paginator';
import {ValueConstant} from '../utils/constants/value-constant';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

    pageIndex = 0;
    pageSize = ValueConstant.PAGE_SIZE;
    pageSizeOptions = ValueConstant.PAGE_SIZE_OPTIONS;

  constructor(
      private dialog: MatDialog,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private _fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
      this._fuseTranslationLoaderService.loadTranslations(navigationEnglish, navigationEspanol);
  }

  ngOnInit(): void {
      this.search(null);
  }

  search(event: PageEvent): void {
      if (!!event) {
          this.pageIndex = event.pageIndex;
          this.pageSize = event.pageSize;
      }
  }

  create(): void {
      console.log('create');
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

}
