import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsComponent } from './terms.component';
import {MatDialogModule} from '@angular/material/dialog';
import {RouterTestingModule} from '@angular/router/testing';
import {FUSE_CONFIG, FuseConfigService} from '../../../@fuse/services/config.service';
import {fuseConfig} from '../../fuse-config';
import {TranslateModule} from '@ngx-translate/core';

describe('TermsComponent', () => {
    let component: TermsComponent;
    let fixture: ComponentFixture<TermsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TermsComponent],
            imports: [
                MatDialogModule,
                RouterTestingModule.withRoutes([
                    {
                        path: '**',
                        component: TermsComponent
                    }
                ]),
                TranslateModule.forRoot()
            ],
            providers: [
                FuseConfigService,
                {
                    provide: FUSE_CONFIG, useValue: fuseConfig
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TermsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});