import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditUnderwritingComponent } from './credit-underwriting.component';

describe('CreditUnderwritingComponent', () => {
  let component: CreditUnderwritingComponent;
  let fixture: ComponentFixture<CreditUnderwritingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditUnderwritingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditUnderwritingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
