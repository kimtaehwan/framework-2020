import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteCreditUnderwritingComponent } from './route-credit-underwriting.component';

describe('RouteCreditUnderwritingComponent', () => {
  let component: RouteCreditUnderwritingComponent;
  let fixture: ComponentFixture<RouteCreditUnderwritingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteCreditUnderwritingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteCreditUnderwritingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
