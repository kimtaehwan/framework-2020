import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValueConstant} from '../../../utils/constants/value-constant';

@Component({
  selector: 'app-route-credit-underwriting',
  templateUrl: './route-credit-underwriting.component.html',
  styleUrls: ['./route-credit-underwriting.component.scss']
})
export class RouteCreditUnderwritingComponent implements OnInit {

    method: string;
    selectedId: number;

    commonForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute
  ) {
      this.method = this.activatedRoute.snapshot.paramMap.get('method');
      const strId = this.activatedRoute.snapshot.paramMap.get('id');

      if (!!strId) {
          this.selectedId = Number(strId);
      }

      console.log('### method: ', this.method);
      console.log('### selectedId: ', this.selectedId);
  }

  ngOnInit(): void {
      this.initCommonForm();
  }

  initCommonForm(): void {
      this.commonForm = this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required)
      });
  }

}
