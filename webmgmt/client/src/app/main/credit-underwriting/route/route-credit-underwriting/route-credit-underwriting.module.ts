import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RouteCreditUnderwritingComponent} from './route-credit-underwriting.component';
import {AuthGuard} from '../../../../auth/_helpers/auth.guard';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';

const routes: Routes = [
    {
        path: ':id',
        component: RouteCreditUnderwritingComponent
    },
    {
        path: '**',
        component: RouteCreditUnderwritingComponent
    }
];

@NgModule({
  declarations: [RouteCreditUnderwritingComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      BuilderPageModule,
      BuilderTitleModule,
      BuilderBodyModule
  ]
})
export class RouteCreditUnderwritingModule { }
