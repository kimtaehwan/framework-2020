import { Component, OnInit } from '@angular/core';
import {CommonSearch} from '../models/common/common-search';
import {MatTableDataSource} from '@angular/material/table';
import {ValueConstant} from '../utils/constants/value-constant';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-credit-underwriting',
  templateUrl: './credit-underwriting.component.html',
  styleUrls: ['./credit-underwriting.component.scss']
})
export class CreditUnderwritingComponent implements OnInit {

    commonSearch = new CommonSearch();
    dataSource = new MatTableDataSource([]);
    type = ValueConstant.TYPE_CREDIT_UNDERWRITING;

  constructor(
      private dialog: MatDialog,
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  create(): void {
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

  edit(id: number): void {
      this.router.navigate(['./edit', id], {relativeTo: this.activatedRoute});
  }
}
