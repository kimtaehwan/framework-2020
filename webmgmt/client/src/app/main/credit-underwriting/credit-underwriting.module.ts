import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CreditUnderwritingComponent} from './credit-underwriting.component';
import {TranslateModule} from '@ngx-translate/core';
import {MaterialModule} from '../common/utils/material/material.module';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import {MgmtTableListModule} from '../common/module/mgmt-table-list/mgmt-table-list.module';
import { RouteCreditUnderwritingComponent } from './route/route-credit-underwriting/route-credit-underwriting.component';
import {AuthGuard} from '../../auth/_helpers/auth.guard';

const routes: Routes = [
    {
        path: '**',
        component: CreditUnderwritingComponent
    }
];

@NgModule({
  declarations: [CreditUnderwritingComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      TranslateModule,
      MaterialModule,

      TopSearchBarModule,
      MgmtTableHeaderModule,
      MgmtTableListModule
  ]
})
export class CreditUnderwritingModule { }
