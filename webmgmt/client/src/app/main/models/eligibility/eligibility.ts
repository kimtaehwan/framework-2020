import {Fi} from '../fi-onboarding/fi';
import {Criteria} from './criteria';

export class Eligibility {
    id: number;
    financialInstitution: Fi = new Fi();
    contentTitle: string;
    title: string;
    message: string;
    created: string;
    criteriaList: Criteria[];

    constructor() {
        this.id = 0;
        this.financialInstitution = new Fi();
        this.contentTitle = '';
        this.title = '';
        this.message = '';
        this.created = '';
        this.criteriaList = [];
    }
}
