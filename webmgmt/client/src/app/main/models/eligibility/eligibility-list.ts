import {Pageable} from '../utils/pageable';
import {Eligibility} from './eligibility';

export class EligibilityList {
    content: Eligibility[];
    totalElements: number;
    pageable: Pageable;
    number: number;
    size: number;

    constructor() {
        this.content = [];
        this.totalElements = 0;
        this.pageable = new Pageable();
        this.number = 0;
        this.size = 0;
    }
}
