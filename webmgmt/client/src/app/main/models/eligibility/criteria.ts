export class Criteria {
    id: number;
    content: string;
    created: string;
    lastModified: string;
    isDelete: boolean;

    constructor() {
        this.id = 0;
        this.content = '';
        this.created = '';
        this.lastModified = '';
        this.isDelete = false;
    }
}
