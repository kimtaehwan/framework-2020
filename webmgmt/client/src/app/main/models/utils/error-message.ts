export class ErrorMessage {
    menu: string;
    name: string;
    message: string;

    constructor() {
        this.menu = '';
        this.name = '';
        this.message = '';
    }
}
