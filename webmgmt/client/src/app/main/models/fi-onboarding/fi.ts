import {Country} from './country';

export class Fi {
    id: number;
    name: string;
    contactEmailAddress: string;
    contactPhoneNumber: any;
    billingEmailAddress: string;
    sourceEmailAddress: string;
    token: string;
    tokenAlias1: string;
    tokenAlias2: string;
    defaultHomeCountry: Country;
    created: string;
    lastModified: string;
    deleted: string;
    status: string;

    constructor() {
        this.id = 0;
        this.name = '';
        this.token = '';
        this.status = '';
        this.tokenAlias1 = '';
        this.tokenAlias2 = '';
        this.contactEmailAddress = '';
        this.billingEmailAddress = '';
        this.sourceEmailAddress = '';
        this.defaultHomeCountry = new Country();
        this.created = '';
    }
}
