import {ListedFi} from './listed-fi';

export class FiContents {
    totalElements: number;
    content: ListedFi[];
    number: number;
    size: number;
}
