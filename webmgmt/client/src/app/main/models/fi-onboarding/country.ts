export class Country {
    id: number;
    code: string;
    threeLetterCode: string;
    phoneCode: string;
    label: string;
}
