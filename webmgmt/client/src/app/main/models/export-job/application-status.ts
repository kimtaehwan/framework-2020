export class ApplicationStatus {
    value: string;
    description: string;

    constructor() {
        this.value = '';
        this.description = '';
    }
}
