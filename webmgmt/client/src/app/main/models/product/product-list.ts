import {Product} from './product';
import {Pageable} from '../utils/pageable';

export class ProductList {
    content: Product[];
    totalElements: number;
    pageable: Pageable;
    number: number;
    size: number;
}
