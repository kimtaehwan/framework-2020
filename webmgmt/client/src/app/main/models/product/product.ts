import {Image} from './image';
import {Fi} from '../fi-onboarding/fi';
import {Metadata} from '../common/metadata';

export class Product {
    id: number;
    name: string;
    description: string;
    image: Image = new Image();
    imageSrc: string;
    originalName: string;
    originalFileName: string;
    created: string;
    creator: string;
    uri: string;
    contentTitle: string;
    financialInstitution: Fi = new Fi();
    metadataList: Metadata[] = [];

    constructor() {
        this.id = 0;
        this.name = '';
        this.description = '';
        this.image = new Image();
        this.imageSrc = '';
        this.originalName = '';
        this.originalFileName = '';
        this.created = '';
        this.creator = '';
        this.uri = '';
        this.contentTitle = '';
        this.financialInstitution = new Fi();
        this.metadataList = [];
    }
}
