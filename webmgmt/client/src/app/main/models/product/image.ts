export class Image {
    id: number;
    uri: string;
    filename: string;
    originalName: string;
}
