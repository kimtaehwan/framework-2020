export class Metadata {
    category: string;
    keyName: string;
    keyType: string;
    keyValueOptions: string;
    description: string;
    key: string;
    value: string;

    constructor() {
        this.category = '';
        this.keyName = '';
        this.keyType = '';
        this.keyValueOptions = '';
        this.description = '';
        this.key = '';
        this.value = '';
    }
}
