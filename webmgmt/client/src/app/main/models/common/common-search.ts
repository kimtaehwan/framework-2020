import {PageEvent} from '@angular/material/paginator';
import {ValueConstant} from '../../utils/constants/value-constant';
import {HttpParams} from '@angular/common/http';

export class CommonSearch {
    query: string;
    from: Date;
    to: Date;
    fiIds: number[];
    pageEvent: PageEvent;

    constructor(query?: string, from?: Date, to?: Date, fiIds?: number[], pageEvent?: PageEvent) {
        const event = new PageEvent();
        event.pageIndex = 0;
        event.pageSize = ValueConstant.PAGE_SIZE;

        this.query = query;
        this.from = from;
        this.to = to;
        this.fiIds = fiIds || [];
        this.pageEvent = pageEvent || event;
    }

    getQueryString(): string {
        let params = new HttpParams();

        if ( !!this.query ) {
            params = params.set('q', this.query);
        }

        if ( !!this.from ) {
            params = params.set('from', this.convertDate(this.from));
        }

        if ( !!this.to ) {
            params = params.set('to', this.convertDate(this.to));
        }

        if ( !!this.fiIds.length ) {
            params = params.set('fiId', this.fiIds.join(','));
        }

        params = params.set('page', this.pageEvent.pageIndex.toString());
        params = params.set('size', this.pageEvent.pageSize.toString());

        return params.toString();
    }

    convertDate(date: Date): string {
        return date.toISOString().replace('Z', this.getEncodeURIComponent());
    }

    getEncodeURIComponent(): string {
        return encodeURIComponent('+') + '0000';
    }
}
