export class ResponseFile {
    name: string;
    originalName: string;
    size: number;
    type: string;
    path: string;

    constructor() {
        this.name = '';
        this.originalName = '';
        this.size = 0;
        this.type = '';
        this.path = '';
    }
}
