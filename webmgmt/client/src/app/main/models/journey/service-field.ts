import {ServiceFieldEnumeration} from './service-field-enumeration';
import {FormatStore} from './format-store';
import {PossibleValue} from './possible-value';

export class ServiceField {
    id: number;
    key: string;
    name: string;
    value: string;
    fieldFormat: string;
    possibleValues: string[];
    possibleValuesArr: PossibleValue[];
    regexp: string;
    minLen: string;
    maxLen: string;
    isRequired: boolean;
    position: number;
    searchable: boolean;
    defaultValue: string;
    addable: boolean;
    visible: boolean;
    multiple: boolean;
    formatStore: string;
    formatStoreObj: FormatStore = new FormatStore();
    serviceFieldEnumerations: ServiceFieldEnumeration[];

    constructor() {
        this.id = 0;
        this.key = '';
        this.name = '';
        this.value = '';
        this.fieldFormat = '';
        this.possibleValues = [];
        this.possibleValuesArr = [];
        this.regexp = '';
        this.minLen = '';
        this.maxLen = '';
        this.isRequired = false;
        this.position = 0;
        this.searchable = false;
        this.defaultValue = '';
        this.addable = false;
        this.visible = false;
        this.multiple = false;
        this.formatStore = '';
        this.formatStoreObj = new FormatStore();
        this.serviceFieldEnumerations = [];
    }
}
