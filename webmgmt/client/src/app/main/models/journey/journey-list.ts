import {ServiceList} from './service-list';
import {Pageable} from '../util/pageable';

export class JourneyList {
    content: ServiceList[] = [];
    totalElements: number;
    pageable: Pageable;
    number: number;
    size: number;

    constructor() {
        this.content = [];
        this.totalElements = 0;
        this.pageable = new Pageable();
        this.number = 0;
        this.size = 0;
    }
}
