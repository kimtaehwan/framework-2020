import {Service} from './service';
import {Fi} from '../fi-onboarding/fi';

export class ServiceList {
    id: number;
    name: string;
    fiName: string;
    fiToken: string;
    financialInstitution: Fi = new Fi();
    productName: string;
    productType: string;
    status: number;
    services: Service[];

    constructor() {
        this.id = 0;
        this.name = '';
        this.fiToken = '';
        this.fiName = '';
        this.productName = '';
        this.productType = '';
        this.status = 0;
        this.services = [];
        this.financialInstitution = new Fi();
    }
}
