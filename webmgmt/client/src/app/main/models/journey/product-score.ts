export class ProductScore {
    name: string;
    riskGraphWidth: string;
    creditScore: number;
    productId: number;

    constructor() {
        this.name = '';
        this.riskGraphWidth = '';
        this.creditScore = 0;
        this.productId = 0;
    }
}
