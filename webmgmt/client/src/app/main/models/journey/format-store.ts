export class FormatStore {
    style: string;
    value: string;

    constructor() {
        this.style = '';
        this.value = '';
    }
}
