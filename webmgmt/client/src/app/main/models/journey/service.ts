import {ServiceVendor} from './service-vendor';

export class Service {
    id: number;
    name: string;
    type: string;
    status: boolean;
    position: number;

    vendors: ServiceVendor[];

    seq = 0;
    enabled = false;
    selected = false;
    addReady = false;

    constructor() {
        this.id = 0;
        this.name = '';
        this.type = '';
        this.status = false;
        this.position = 0;
        this.vendors = [];

        this.seq = 0;
        this.enabled = false;
        this.selected = false;
        this.addReady = false;
    }
}
