import {ServiceValue} from './service-value';

export class SaveJourney {
    id: number;
    name: string;
    fiToken: string;
    status: string;
    journeyServices: string[] = [];
    journeyServiceValues: ServiceValue[] = [];

    constructor() {
        this.id = 0;
        this.name = '';
        this.fiToken = '';
        this.status = '';
        this.journeyServices = [];
        this.journeyServiceValues = [];
    }
}
