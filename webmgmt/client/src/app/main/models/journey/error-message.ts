export class ErrorMessage {
    isEmptyJourneyName: boolean;
    type: string;
    serviceName: string;
    beforeServiceName: string;
    serviceFieldName: string;

    constructor() {
        this.isEmptyJourneyName = false;
        this.type = '';
        this.serviceName = '';
        this.beforeServiceName = '';
        this.serviceFieldName = '';
    }
}
