import {ServiceVendorField} from './service-vendor-field';

export class ServiceVendor {
    name: string;
    type: string;
    position: number;
    serviceVendorFields: ServiceVendorField[];

    constructor() {
        this.name = '';
        this.type = '';
        this.position = 0;
        this.serviceVendorFields = [];
    }
}
