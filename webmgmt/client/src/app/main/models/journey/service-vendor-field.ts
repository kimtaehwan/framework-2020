import {ServiceField} from './service-field';

export class ServiceVendorField {
    columns: number;
    name: string;
    key: string;
    position: number;
    serviceFields: ServiceField[];

    constructor() {
        this.columns = 0;
        this.name = '';
        this.key = '';
        this.position = 0;
        this.serviceFields = [];
    }
}
