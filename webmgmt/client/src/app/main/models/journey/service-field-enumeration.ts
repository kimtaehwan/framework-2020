export class ServiceFieldEnumeration {
    name: string;
    value: string;
    checked: boolean;
    position: number;

    constructor() {
        this.name = '';
        this.value = '';
        this.checked = false;
        this.position = 0;
    }
}
