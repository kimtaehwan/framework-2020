export class ServiceValueField {
    serviceFieldId: number;
    values: string[] = [];

    constructor() {
        this.serviceFieldId = 0;
        this.values = [];
    }
}
