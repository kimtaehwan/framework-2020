import {ServiceValueField} from './service-value-field';

export class ServiceValue {
    id: number;
    type: string;
    fields: ServiceValueField[] = [];

    constructor() {
        this.id = 0;
        this.type = '';
        this.fields = [];
    }
}
