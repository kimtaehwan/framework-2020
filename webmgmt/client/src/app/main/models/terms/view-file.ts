export class ViewFile {
    fileName: string;
    fileContent: string;
    downloadUrl: string;

    constructor() {
        this.fileName = '';
        this.fileContent = '';
        this.downloadUrl = '';
    }
}
