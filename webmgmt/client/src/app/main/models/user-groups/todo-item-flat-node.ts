export class TodoItemFlatNode {
    id: number;
    name: string;
    level: number;
    expandable: boolean;

    constructor() {
        this.id = 0;
        this.name = '';
        this.level = 0;
        this.expandable = false;
    }
}
