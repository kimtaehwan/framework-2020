export class JourneyProcessTypeLog {
    createdTime: Date;
    lastModifiedTime: Date;
    processId: number;
    processPosition: number;
    processStatus: string;
    processType: string;

    constructor() {
        this.createdTime = new Date();
        this.lastModifiedTime = new Date();
        this.processId = 0;
        this.processPosition = 0;
        this.processStatus = '';
        this.processType = '';
    }
}
