import {JourneyProcessTypeLog} from './journey-process-type-log';
import {ProductInfo} from './app-detail/product-info';
import {RiskReportLog} from './app-detail/risk-report-log';
import {MfaOtpMapping} from './app-detail/mfa-otp-mapping';
import {UserVerificationLog} from './app-detail/user-verification-log';
import {CreditReportLog} from './app-detail/credit-report-log';
import {GeolocationRef} from './app-detail/geolocation-ref';
import {ProductDuplicationDetectionLog} from './app-detail/product-duplication-detection-log';
import {MfaContent} from './app-detail/mfa-content';
import {LoginCreationResult} from './app-detail/login-creation-result';
import {UserIdentificationLog} from './app-detail/user-identification-log';
import {InstantIssuanceInfo} from './app-detail/instant-issuance-info';
import {PushProvisioningResult} from './app-detail/push-provisioning-result';
import {EligibilityLog} from './app-detail/eligibility-log';
import {UserAcceptanceLog} from './app-detail/user-acceptance-log';
import {TermsStatusLog} from './app-detail/terms-status-log';
import {EsigDocModel} from './app-detail/esig-doc-model';
import {InstantIssuanceReport} from './app-detail/instant-issuance-report';
import {MobileDevices} from './app-detail/mobile-devices';
import {CustomerVerifiedApplicationForm} from './app-detail/customer-verified-application-form';
import {CustomerInfo} from './app-detail/customer-info';
import {IdvTransaction} from './apps/idv-transaction';
import {ApplicationReference} from './apps/application-reference';

export class ApplicationData {
    idvTransactions: IdvTransaction[];
    mobileDevices: MobileDevices;
    customerInfo: CustomerInfo;
    customerVerifiedApplicationForm: CustomerVerifiedApplicationForm;
    productInfo: ProductInfo;
    creditReportLog: CreditReportLog;
    geolocationRef: GeolocationRef;
    applicationReference: ApplicationReference = new ApplicationReference();
    mfaList: MfaContent[];
    journeyProcessTypeLogList: Array<JourneyProcessTypeLog>;
    latitude: number;
    longitude: number;
    addressFromLatLng: string;
    mfaOtpMapping: MfaOtpMapping;
    riskReportLog: RiskReportLog;
    termsStatusLog: TermsStatusLog;
    eligibilityLog: EligibilityLog;
    esigDocModel: EsigDocModel;
    instantIssuanceInfo: InstantIssuanceInfo;
    instantIssuanceReport: InstantIssuanceReport;
    pushProvisioningResult: PushProvisioningResult;
    loginCreationResult: LoginCreationResult;
    userVerificationLog: UserVerificationLog;
    userIdentificationLog: UserIdentificationLog;
    productDuplicationDetectionLog: ProductDuplicationDetectionLog;
    homeAddress: string;
    userAddress: string;
    userAcceptanceLog: UserAcceptanceLog;

    constructor(){
        this.latitude = null;
        this.longitude = null;
        this.addressFromLatLng = null;

        this.idvTransactions = [];
    }
}
