export class ProductInfo{
    id: number;
    type: string;
    name: string;
    image: string;
    description: string;
    created: string;
    lastModified: string;
    status: string;
    creator: string;

    offers: Array<string>;
}
