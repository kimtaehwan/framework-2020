export class TaskResponseDataV2{
    body: any;
    contentType: string;
    httpCode: number;
    httpDescription: string;
    httpStatus: string;
}
