import { Criteria } from './criteria';

export class EligibilityLog {
    applicationReferenceId: string;
    fiToken: string;
    criterias: Array<Criteria>;
    createdTime: Date;
    eligibilityContentTitle: string;
    eligibilityTitle: string;
    eligibilityMessage: string;
    fiName: string;
    selectedCriteria: number;
    
    selectedCriteriaDetail: string;
} 
