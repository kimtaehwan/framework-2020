import { TaskRequestData } from './task-request-data';
import { TaskResponseDataV2 } from './task-response-data-v2';
import {TaskResultStatus} from '../ui/apps/task-result-status';

export class CreditReportLog extends TaskResultStatus{
    applicationId: string;
    creditScore: number;
    /********** deprecated ***************/
    experianResponse: string;
    ondotRequest: string;
    ondotResponse: string;
    resultStatus: number;
    statusHttpCode: number;
    statusMsg: string;
    /*************************************/
    reqTime: Date;
    respTime: Date;
    requestData: TaskRequestData;
    responseData: TaskResponseDataV2;

    constructor(){
        super();
    }
}
