import { TaskResponseData } from './task-response-data';
import { TaskRequestData } from './task-request-data';
import {TaskResultStatus} from '../ui/apps/task-result-status';

export class RiskReportLog extends TaskResultStatus{
    opac: boolean;
    interpol: boolean;
    accept: boolean;
    applicationId: string;
    fiToken: string;
    journeyId: string;
    reqTime: Date;
    requestData: TaskRequestData;
    respTime: Date;
    responseData: TaskResponseData;

    riskScore: number;
    ofacMatch: string;
    decision: string;

    constructor(){
        super();
    }
}
