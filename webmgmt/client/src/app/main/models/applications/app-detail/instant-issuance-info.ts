export class InstantIssuanceInfo {
    applicationReferenceId: string;
    firstName: string;
    lastName: string;
    pan: string;
    expiryDate: string;
    cvv: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
}
