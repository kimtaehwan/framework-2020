export class CustomerInfo{
    dob: string;
    firstName: string;
    json: any;
    parsed: object;
    lastName: string;
    referenceId: string;
    ssn: string;
}
