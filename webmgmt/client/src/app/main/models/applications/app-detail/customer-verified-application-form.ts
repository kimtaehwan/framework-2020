import { ApplicationForm } from './application-form';

export class CustomerVerifiedApplicationForm {
    applicationForm: ApplicationForm[];
    applicationFormId: number;
    applicationReferenceId: string;

    selectedPage = 0;


    constructor() {
        this.applicationForm = [];
    }
}
