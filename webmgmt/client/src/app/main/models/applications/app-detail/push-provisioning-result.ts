export class PushProvisioningResult {
    walletType: string;
    userWalletId: string;
    resultCode: string;
    results: string;
    provisioningToken: string;
}
