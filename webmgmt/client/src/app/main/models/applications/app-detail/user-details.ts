export class UserDetails {
    userReferenceId: string;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    state: string;

    constructor() {
        this.userReferenceId = '';
        this.firstName = '';
        this.lastName = '';
        this.address = '';
        this.city = '';
        this.state = '';
    }
}
