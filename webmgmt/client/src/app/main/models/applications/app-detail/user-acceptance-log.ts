export class UserAcceptanceLog {
    applicationReferenceId: string;
    fiId: number;
    productId: number;
    stateStatus: string;
    pendingDate: Date;
    acceptedDate: Date;

    constructor() {
        this.applicationReferenceId = '';
        this.fiId = 0;
        this.productId = 0;
        this.stateStatus = '';
        this.pendingDate = new Date();
        this.acceptedDate = new Date();
    }
}
