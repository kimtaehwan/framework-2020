export class GeolocationRef {
    allowedDistance: number;
    applicationReferenceId: string;
    createdTime: Date;
    inRange: boolean;
    userDistance: number;
    userLatitude: number;
    userLongitude: number;
    zipCentralLatitude: number;
    zipCentralLongitude: number;
    address: string;
}
