export class LoginCreationResult {
  applicationReferenceId: string;
  created: string;
  modified: string;
  subscriberFailedReason: string;
  subscriberReferenceId: string;
  subscriberUserEmail: string;
  subscriberUserFullName: string;
  subscriberUserLogin: string;
  subscriberUserStatus: string;
}
