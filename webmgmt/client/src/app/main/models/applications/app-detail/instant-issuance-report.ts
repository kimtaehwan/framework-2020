import {UserDetails} from './user-details';
import {CardDetail} from './card-detail';
import {ProductDetails} from './product-details';

export class InstantIssuanceReport {
    applicationReferenceId: string;
    fiId: number;
    productId: number;
    stateStatus: string;
    userDetails: UserDetails;
    cardDetails: CardDetail[];
    productDetails: ProductDetails;

    constructor() {
        this.applicationReferenceId = '';
        this.fiId = 0;
        this.productId = 0;
        this.stateStatus = '';
        this.userDetails = new UserDetails();
        this.cardDetails = [];
        this.productDetails = new ProductDetails();
    }
}
