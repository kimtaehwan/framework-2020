export class TaskRequestData{
    body: string;
    contentType: string;
    method: string;
    url: string;
}
