export class ProductDuplicationDetectionLog{
    applicationReferenceId: string;
    productDuplicationDetectedResult: boolean;
}
