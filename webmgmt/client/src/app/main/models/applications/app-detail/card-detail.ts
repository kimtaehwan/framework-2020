export class CardDetail {
    cardReferenceId: number;
    cardType: string;
    cardPan: string;
    cardExpDate: string;
    cardImage: string;

    constructor() {
        this.cardReferenceId = 0;
        this.cardType = '';
        this.cardPan = '';
        this.cardExpDate = '';
        this.cardImage = '';
    }
}
