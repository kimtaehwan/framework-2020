export class EsigDocModel {
    applicationId: string;
    created: Date;
    esigItem: any;
    esignatureContentTitle: string;
    esignatureFile: string;
    esignatureId: number;
    esignatureMessage: string;
    esignatureTitle: string;
    fiId: string;
    journeyId: number;
    lastModified: Date;
} 
