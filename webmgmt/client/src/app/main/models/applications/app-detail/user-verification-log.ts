export class UserVerificationLog{
  applicationReferenceId: string;
  externalCustomerId: string;
  stateStatus: string;
  verificationFieldType: string;
  userSsn: string;
  userPhoneNumber: string;
  userDob: string;
}
