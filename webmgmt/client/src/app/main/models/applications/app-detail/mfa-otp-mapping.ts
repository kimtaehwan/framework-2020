export class MfaOtpMapping {
    appToken: string;
    applicationReferenceId: string;
    attempts: number;
    createdTime: string;
    fiToken: string;
    index: number;
    otpCode: string;
    serviceType: string;
    phoneNumber: string;
    status: number;
    uniqueDeviceId: string;
}
