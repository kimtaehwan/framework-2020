export class MfaContent {
    appToken: string;
    applicationReferenceId: string;
    attempts: number;
    createdTime: Date;
    fiToken: string;
    index: number;
    otpCode: string;
    serviceType: string;
    status: number;
    uniqueDeviceId: string;
}
