export class TermsStatusLog {
    applicationReferenceId: string;
    createdTime: Date;
    fiToken: string;
    htmlFileLocation: string;
    status: number;
    termId: number;
    contentTitle: string;

}
