import {TaskResponseData} from './task-response-data';
import {TaskRequestData} from './task-request-data';
import {TaskResultStatus} from '../ui/apps/task-result-status';
import {Status} from '../status';

export class Task extends TaskResultStatus{
    reqData: TaskRequestData;
    reqTime: Date;
    respData: TaskResponseData;
    respTime: Date;
    status: Status;
    taskName: string;
    taskSeq: number;
    traceId: string;

    constructor(){
        super();
    }

}
