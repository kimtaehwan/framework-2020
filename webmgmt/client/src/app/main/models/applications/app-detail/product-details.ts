export class ProductDetails {
    accountDetail: string;
    creditLimit: string;
    apr: string;
    productDescriptions: object;

    constructor() {
        this.accountDetail = '';
        this.creditLimit = '';
        this.apr = '';
        this.productDescriptions = {};
    }
}
