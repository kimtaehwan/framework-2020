export class JourneyProcessTypeLog {
    createdTime: Date;
    processId: number;
    processStatus: string;
    processType: string;
    lastModifiedTime: Date;
    processPosition: number;
}
