import { ApplicationFormPage } from './application-form-page';

export class ApplicationForm {
    pageTitle: string;
    forms: ApplicationFormPage[];

    constructor() {
        this.forms = [];
    }
}
