export class TaskResponseData{
    body: any;
    contentType: string;
    statusCode: number;
    statusText: string;
}
