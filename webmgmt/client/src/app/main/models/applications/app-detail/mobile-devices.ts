import {Footmark} from '../apps/footmark';

export class MobileDevices{
    countryCode: string;
    createdTime: string;
    deviceManufacturer: string;
    deviceModel: string;
    deviceType: string;
    deviceUniqueId: string;
    footmark: Footmark;
    lastModifiedTime: string;
    logEndTime: string;
    logStartTime: string;
    mobileNetwork: string;
    osVersion: string;
}
