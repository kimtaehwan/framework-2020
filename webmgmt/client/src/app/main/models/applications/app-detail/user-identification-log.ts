export class UserIdentificationLog{
  applicationReferenceId: string;
  subscriberReferenceId: string;
  externalCustomerId: string;
  externalIdToName: string[];
  identificationStatus: string;
  userFullName: string;
}
