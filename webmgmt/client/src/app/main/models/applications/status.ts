export class Status {
    code: number;
    httpCode: number;
    message: string;
}
