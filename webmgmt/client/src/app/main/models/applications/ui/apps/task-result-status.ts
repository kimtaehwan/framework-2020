
export class TaskResultStatus{
    isReqBodyFABOpen: boolean;
    isRespBodyFABOpen: boolean;
    reqJsonCollapseLevel: number;
    respJsonCollapseLevel: number;
    reqJsonTreeMode: boolean;
    respJsonTreeMode: boolean;
    taskColorScheme: object;
    constructor(){
        this.isReqBodyFABOpen = false;
        this.isRespBodyFABOpen = false;
        this.reqJsonCollapseLevel = 0;
        this.respJsonCollapseLevel = 0;
        this.reqJsonTreeMode = true;
        this.respJsonTreeMode = true;
    }
}
