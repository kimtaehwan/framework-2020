import { PageEvent } from '@angular/material/paginator';
import {ValueConstant} from '../../../../utils/constants/value-constant';

export class TransactionList {
    opened: boolean[] = [];
    pageSizeOptions: number[];
    pageEvent: PageEvent;
    pageLineup: number[];
    pageLineupLength: number;
    constructor(size: number){
        this.opened.length = size;
        this.pageEvent = new PageEvent();
        this.pageEvent.pageIndex = 0;
        this.pageEvent.pageSize = ValueConstant.PAGE_SIZE;
        this.pageSizeOptions = ValueConstant.PAGE_SIZE_OPTIONS;
        this.pageLineup = [];
        this.pageLineupLength = 2;
    }
}
