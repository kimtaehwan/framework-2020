import {Status} from './status';

export class DcsStandard {
    content: any;
    contents: any[];
    status: Status;
    totalCount: number;
    totalPages: number;
    responseDataList: any[];
}
