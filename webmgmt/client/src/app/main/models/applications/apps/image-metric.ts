export class ImageMetric {
    isValid: boolean;
    glareMetric: number;
    sharpnessMetric: number;
    isCropped: boolean; 
    isTampered: boolean;
}
