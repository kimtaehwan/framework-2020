import {ProductInfo} from '../app-detail/product-info';
import {CustomerInfo} from '../app-detail/customer-info';


export class ApplicationReference {
    applicationReferenceId: string;
    appToken: string;
    fiToken: string;
    deviceUniqueId: string;
    phoneNumber: string;
    createdTime: Date;
    lastModifiedTime: Date;
    lastName: string;
    firstName: string;
    customerInfo: CustomerInfo;
    productInfo: ProductInfo;
    journeyStatus: string;
    journeyPointId: string;
    journeyId: string;
    processId: string;
    processStatus: string;
    processType: string;
    processPosition: number;
    processLastPosition: number;

    mapCircleBounds: any;
}
