import { AuthExtractData } from './auth-extract-data';
import { ImageMetric } from './image-metric';
import { FaceMatch } from './face-match';

export class DocVerificationResult {
    authExtractData: AuthExtractData;
    backImageMetric: ImageMetric;
    docInstanceId: string;
    faceMatch: FaceMatch;
    faceMatchTraceId: string;
    frontImageMetric: ImageMetric;
    ticketId: string;
    ticketUseCount: number;
    ticketUseLimit: number;
    traceId: string;
}


