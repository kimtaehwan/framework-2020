export class Footmark {
    clientIp: string;
    deviceUniqueId: string;
    latitude: number;
    longitude: number;
}
