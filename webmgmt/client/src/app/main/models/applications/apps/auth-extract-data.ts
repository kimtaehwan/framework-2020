export class AuthExtractData {
    addressLine1: string;
    addressLine2: string;
    age: number;
    authResult: string;
    birthDate: Date;
    city: string;
    expirationDate: Date;
    firstName: string;
    lastName: string;
    state: string;
    zipCode: string;
}
