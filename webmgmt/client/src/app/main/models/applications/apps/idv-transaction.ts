import { DocVerificationResult } from './doc-verification-result';
import { Footmark } from './footmark';
import { Task } from '../app-detail/task';
import {Status} from '../status';
import { GalleryItem, ImageItem, Gallery, GalleryRef } from '@ngx-gallery/core';

export class IdvTransaction {
    docBackImagePath: string;
    docFrontImagePath: string;
    docPhotoImagePath: string;
    docType: number;
    docVerificationResult: DocVerificationResult;
    footmark: Footmark;
    logEndTime: Date;
    logStartTime: Date;
    reqTime: Date;
    respTime: Date;
    selfieImagePath: string;
    status: Status;
    ticketId: string;
    ticketUsageCount: number;
    ticketUsageLimit: number;
    traceId: string;

    tasks: Task[];
    faceMatchTasks: Task[];

    expanded: boolean;
    frontGalleryId: string;
    frontGalleryImages: GalleryItem[];
    frontGalleryRef: GalleryRef;
    backGalleryId: string;
    backGalleryImages: GalleryItem[];
    backGalleryRef: GalleryRef;
    extractedGalleryId: string;
    extractedGalleryImages: GalleryItem[];
    extractedGalleryRef: GalleryRef;
    selfieGalleryId: string;
    selfieGalleryImages: GalleryItem[];
    selfieGalleryRef: GalleryRef;

    taskChartData: any;
    chartXScaleMax: number;
    faceTaskChartData: any;
    faceChartXScaleMax: number;

    docFrontResultData: any;
    docBackResultData: any;
    docFrontSize: any;
    docBackSize: any;

    isDocFrontValid: boolean;
    isDocBackValid: boolean;


    constructor(){
        this.frontGalleryId = 'frontBox';
        this.backGalleryId = 'backBox';
        this.extractedGalleryId = 'extractedBox';
        this.selfieGalleryId = 'selfieBox';
        this.frontGalleryImages = [];
        this.backGalleryImages = [];
        this.extractedGalleryImages = [];
        this.selfieGalleryImages = [];
        this.expanded = true;

        this.isDocFrontValid = true;
        this.isDocBackValid = true;
    }
}
