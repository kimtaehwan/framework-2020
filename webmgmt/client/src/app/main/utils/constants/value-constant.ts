export class ValueConstant {
    public static PAGE_SIZE = 25;
    public static PAGE_SIZE_OPTIONS: number[] = [25, 50, 100];

    public static SLICE_SIZE = 30;
    public static SHORTEN = '...';

    public static DATE_FORMAT = 'dd-MMM-yyyy HH:mm:ss';

    public static PATTERN_DASH = '^[a-zA-Z0-9_.,-\\s]*$';
    public static PATTERN_EXCEPT_SCRIPT = '^[^<>]*$';
    public static PATTERN_ALPHA_NUMERIC = '^[a-zA-Z0-9\\s]*$';
    public static PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$';

    public static REG_EXP_HTML = /\.(html|htm)$/i;

    public static FIELD_FORMAT_ENUMERATION = 'enumeration';
    public static FIELD_FORMAT_PRODUCT = 'product';
    public static FIELD_FORMAT_TNC = 'tnc';
    public static FIELD_FORMAT_ELIGIBILITY = 'eligibility';
    public static FIELD_FORMAT_ESIGNATURE = 'esignature';
    public static FIELD_FORMAT_CINFO = 'cinfo';

    public static FIELD_FORMAT_OBJ_RADIO = 'radio';
    public static FIELD_FORMAT_OBJ_CHECK_BOX = 'check_box';
    public static FIELD_FORMAT_OBJ_SLIDE_SCORE = 'slideScore';
    public static FIELD_FORMAT_OBJ_PRODUCT_SLIDE_SCORE = 'product+slideScore';

    public static KEY_CONFIGURATION = 'CONFIGURATION';
    public static KEY_BUSINESS_RULES = 'BUSINESS_RULES';

    public static COLUMN_NO = 'No';
    public static COLUMN_ID = 'ID';
    public static COLUMN_TYPE = 'Type';
    public static COLUMN_NAME = 'Name';
    public static COLUMN_LAST_UPDATED = 'Last Updated';
    public static COLUMN_FI_NAME = 'FI Name';
    public static COLUMN_FI_TOKEN = 'FI Token';
    public static COLUMN_APP_TOKEN = 'App Token';
    public static COLUMN_JOURNEY_POINT_NAME = 'Journey Point Name';
    public static COLUMN_JOURNEY_NAME = 'Journey Name';
    public static COLUMN_BY = 'By';
    public static COLUMN_STATUS = 'Status';
    public static COLUMN_ACTIONS = 'Actions';
    public static COLUMN_NEXT_EXECUTION_DATE = 'Next Execution Date';
    public static COLUMN_JOB_EXECUTION_SCHEDULE = 'Job Execution Schedule';
    public static COLUMN_DATE_TIME = 'Date Time';
    public static COLUMN_CUSTOMER_ID = 'Customer ID';
    public static COLUMN_PRODUCT_TYPE = 'Product Type';
    public static COLUMN_PRODUCT_NAME = 'Product Name';
    public static COLUMN_STEPS = 'Steps';
    public static COLUMN_PROCESS_STAGE = 'Process Stage';

    public static COMMON_NAME_MIN_LENGTH = 10;
    public static COMMON_NAME_MAX_LENGTH = 30;
    public static COMMON_TITLE_MAX_LENGTH = 50;
    public static COMMON_MESSAGE_MAX_LENGTH = 60;
    public static COMMON_MAX_LENGTH_150 = 150;

    public static TYPE_FI_ONBOARDING = 'FI_ONBOARDING';
    public static TYPE_EXPORT_JOB = 'EXPORT_JOB';
    public static TYPE_APPLICATIONS = 'APPLICATIONS';
    public static TYPE_CREDIT_UNDERWRITING = 'CREDIT_UNDERWRITING';

    public static NAME_EXPORT_JOB = 'Export Job';
    public static NAME_APPLICATIONS = 'Applications';
}
