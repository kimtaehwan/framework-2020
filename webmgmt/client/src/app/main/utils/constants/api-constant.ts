import {environment} from '../../../../environments/environment';

export class ApiConstant {

    public static API_BASE_URL = environment.apiBaseUrl;
    public static AUTH_URL = environment.authUrl;

    public static API_PRODUCT_URL = environment.apiBaseUrl + '/products';
    public static API_FI_ONBOARDING_URL = environment.apiBaseUrl + '/fiOnboarding';
    public static API_IMAGES_URL = environment.apiBaseUrl + '/images';
    public static API_STORAGE_URL = environment.apiBaseUrl + '/storage';

    public static API_ELIGIBILITY_URL = environment.apiBaseUrl + '/eligibility';
    public static API_TERMS_URL = environment.apiBaseUrl + '/terms';

    public static JOURNEY_WEB_URL = environment.apiBaseUrl + '/journeys';
    public static API_JOURNEY_URL = environment.journeyBaseUrl + '/api';
    public static API_USER_GROUPS_URL = environment.apiBaseUrl + '/userGroups';

    public static API_IDV_URL = environment.idvApiBaseUrl;
    public static API_IDV_RESULT_IMAGES = ApiConstant.API_IDV_URL + '/v1/documents/images';
    public static API_CUSTOMER_SUPPORT_URL = environment.customerSupportUrl;
    public static API_USER_GROUPS_MENU_LIST_URL = environment.apiBaseUrl + '/menuList';
}
