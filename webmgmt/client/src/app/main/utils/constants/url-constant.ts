import {ApiConstant} from './api-constant';
import {CommonSearch} from '../../models/common/common-search';

export class UrlConstant {

    static getLoginKakaoUrl(): string {
        return ApiConstant.API_BASE_URL + '/kakao';
    }

    // Product Url
    static getProductSearchUrl(commonSearch: CommonSearch): string {
        return ApiConstant.API_PRODUCT_URL + '?' + commonSearch.getQueryString();
    }

    static deleteProductUrl(productId): string {
        return ApiConstant.API_PRODUCT_URL + '/' + productId;
    }

    // FiOnboarding Url
    static getFiContentsUrl(commonSearch: CommonSearch): string {
        return ApiConstant.API_FI_ONBOARDING_URL + '/financialInstitutions?' + commonSearch.getQueryString();
    }

    // User-Group Url
    static getUserGroupsUrl(): string {
        return ApiConstant.API_USER_GROUPS_URL;
    }

    static getMenuListUrl(): string {
        return ApiConstant.API_USER_GROUPS_MENU_LIST_URL;
    }

    static getIdvImageUrl(path): string {
        return ApiConstant.API_IDV_RESULT_IMAGES + path;
    }
}
