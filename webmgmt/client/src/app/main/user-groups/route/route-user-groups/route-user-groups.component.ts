import {Component, Injectable, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValueConstant} from '../../../utils/constants/value-constant';
import {BehaviorSubject} from 'rxjs';
import {TodoItemNode} from '../../../models/user-groups/todo-item-node';
import {UserGroupsService} from '../../../service/user-groups.service';
import {UrlConstant} from '../../../utils/constants/url-constant';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import {TodoItemFlatNode} from '../../../models/user-groups/todo-item-flat-node';
import {FuseTranslationLoaderService} from '../../../../../@fuse/services/translation-loader.service';
import {locale as english} from '../../../../navigation/i18n/en';
import {locale as turkish} from '../../../../navigation/i18n/tr';

@Injectable()
export class ChecklistDatabase {
    dataChange = new BehaviorSubject<TodoItemNode[]>([]);

    get data(): TodoItemNode[] {
        return this.dataChange.value;
    }

    constructor(
        private userGroupsService: UserGroupsService
    ) {
        this.getMenuList();
    }

    getMenuList(): void {
        const url = UrlConstant.getMenuListUrl();

        this.userGroupsService.getMenuList(url)
            .subscribe((resp) => {
                this.dataChange.next([resp.body]);
            });
    }
}

@Component({
  selector: 'app-route-user-groups',
  templateUrl: './route-user-groups.component.html',
  styleUrls: ['./route-user-groups.component.scss'],
    providers: [ChecklistDatabase]
})
export class RouteUserGroupsComponent implements OnInit {
    commonForm: FormGroup;
    isView: boolean;

    /** Map from flat node to nested node. This helps us finding the nested node to be modified */
    flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

    /** Map from nested node to flattened node. This helps us to keep the same object for selection */
    nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

    treeControl: FlatTreeControl<TodoItemFlatNode>;
    treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
    dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

    /** The selection for checklist */
    INDEX_MENU = 0;
    INDEX_VIEW = 1;
    INDEX_EDIT = 2;

    checklistSelections =  [
        new SelectionModel<TodoItemFlatNode>(true /* multiple */),
        new SelectionModel<TodoItemFlatNode>(true /* multiple */),
        new SelectionModel<TodoItemFlatNode>(true /* multiple */)
    ];

  constructor(
      private formBuilder: FormBuilder,
      private _database: ChecklistDatabase,
      private _fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
      this._fuseTranslationLoaderService.loadTranslations(english, turkish);

      this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
      this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
      this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (num: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  transformer = (node: TodoItemNode, level: number) => {
      const existingNode = this.nestedNodeMap.get(node);
      const flatNode = !!existingNode && existingNode.id === node.id ? existingNode : new TodoItemFlatNode();
      flatNode.id = node.id;
      flatNode.name = node.name;
      flatNode.level = level;
      flatNode.expandable = !!node.children;

      this.flatNodeMap.set(flatNode, node);
      this.nestedNodeMap.set(node, flatNode);
      return flatNode;
  }

  ngOnInit(): void {
      console.log('route-user-groups');
      this.initCommonForm();
      this.setDatabase();
  }

  setDatabase(): void {
      this._database.dataChange.subscribe((data) => {
          if ( !!data.length ) {
              this.dataSource.data = data;
              this.initSetting();
          }
      });
  }

  initSetting(): void {

  }

  todoLeafItemSelectionToggle(node: TodoItemFlatNode, targetArr: number): void {
      this.checklistSelections[targetArr].toggle(node);
      this.checkAllParentsSelection(node, targetArr);
  }

  checkAllParentsSelection(node: TodoItemFlatNode, targetArr: number): void {
      let parent: TodoItemFlatNode | null = this.getParentNode(node);
      while (parent) {
          this.checkRootNodeSelection(parent, targetArr);
          parent = this.getParentNode(parent);
      }
  }

  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
      const currentLevel = this.getLevel(node);

      if (currentLevel < 1) {
          return null;
      }

      const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

      for (let i = startIndex; i >= 0; i--) {
          const currentNode = this.treeControl.dataNodes[i];

          if (this.getLevel(currentNode) < currentLevel) {
              return currentNode;
          }
      }
      return null;
  }

  checkRootNodeSelection(node: TodoItemFlatNode, targetArr: number): void {
      const nodeSelected = this.checklistSelections[targetArr].isSelected(node);
      const descendants = this.treeControl.getDescendants(node);
      const descAllSelected = descendants.every(child =>
          this.checklistSelections[targetArr].isSelected(child)
      );
      if (nodeSelected && !descAllSelected) {
          this.checklistSelections[targetArr].deselect(node);
      } else if (!nodeSelected && descAllSelected) {
          this.checklistSelections[targetArr].select(node);
      }
  }

  descendantsAllSelected(node: TodoItemFlatNode, targetArr: number): boolean {
      if (!this.treeControl.dataNodes) {
          return false;
      }
      const descendants = this.treeControl.getDescendants(node);
      return descendants.every(child =>
          this.checklistSelections[targetArr].isSelected(child)
      );
  }

  descendantsPartiallySelected(node: TodoItemFlatNode, targetArr: number): boolean {
      if (!this.treeControl.dataNodes) {
          return false;
      }

      const descendants = this.treeControl.getDescendants(node);
      const result = descendants.some(child => this.checklistSelections[targetArr].isSelected(child));
      return result && !this.descendantsAllSelected(node, targetArr);
  }

  todoItemSelectionToggle(node: TodoItemFlatNode, targetArr: number): void {
      this.checklistSelections[targetArr].toggle(node);
      const descendants = this.treeControl.getDescendants(node);
      this.checklistSelections[targetArr].isSelected(node)
          ? this.checklistSelections[targetArr].select(...descendants)
          : this.checklistSelections[targetArr].deselect(...descendants);

      // Force update for the parent
      let result: any = descendants.every(child =>
          this.checklistSelections[targetArr].isSelected(child)
      );

      // sonarqube bug fix
      result = result ? null : undefined;

      this.checkAllParentsSelection(node, targetArr);

      return result;
  }

  initCommonForm(): void {
      this.commonForm = this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required)
      });
  }

}
