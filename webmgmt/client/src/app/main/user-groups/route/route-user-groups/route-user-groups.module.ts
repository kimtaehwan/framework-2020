import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteUserGroupsComponent } from './route-user-groups.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {TranslateModule} from '@ngx-translate/core';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: ':editId',
        component: RouteUserGroupsComponent
    },
    {
        path: '**',
        component: RouteUserGroupsComponent
    }
];

@NgModule({
  declarations: [RouteUserGroupsComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,

      BuilderBodyModule,
      BuilderPageModule,
      BuilderTitleModule,

      TranslateModule,
      FuseSharedModule
  ]
})
export class RouteUserGroupsModule { }
