import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {CommonSearch} from '../models/common/common-search';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.scss']
})
export class UserGroupsComponent implements OnInit {

    statusControl = new FormControl();

    commonSearch = new CommonSearch();

    dataSource = new MatTableDataSource([]);

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  create(): void {
      console.log('user-groups create');
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

}
