import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FuseSharedModule} from '../../@fuse/shared.module';
import {MaterialModule} from './common/utils/material/material.module';
import {AuthGuard} from '../auth/_helpers/auth.guard';

const routes: Routes = [
    {
        path    : 'process',
        loadChildren : () => import('./process/process.module').then(m => m.ProcessModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'journey/:method',
        loadChildren: () => import('./journey/route/route-journey/route-journey.module').then(m => m.RouteJourneyModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'journey',
        loadChildren: () => import('./journey/journey.module').then(m => m.JourneyModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'product/edit',
        loadChildren: () => import('./product/route/route-product/route-product.module').then(m => m.RouteProductModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'product/create',
        loadChildren: () => import('./product/route/route-product/route-product.module').then(m => m.RouteProductModule),
        canActivate: [AuthGuard]
    },
    {
        path    : 'product',
        loadChildren    : () => import('./product/product.module').then(m => m.ProductModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'terms/create',
        loadChildren: () => import('./terms/route/route-terms/route-terms.module').then(m => m.RouteTermsModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'terms',
        loadChildren: () => import('./terms/terms.module').then(m => m.TermsModule),
        canActivate: [AuthGuard]
    },

    {
        path    : 'fiOnboarding/create',
        loadChildren : () => import('./fi-onboarding/route/route-fi/route-fi.module').then(m => m.RouteFiModule),
        canActivate: [AuthGuard]
    },
    {
        path    : 'fiOnboarding/edit',
        loadChildren : () => import('./fi-onboarding/route/route-fi/route-fi.module').then(m => m.RouteFiModule),
        canActivate: [AuthGuard]
    },
    {
        path    : 'fiOnboarding',
        loadChildren : () => import('./fi-onboarding/fi-onboarding.module').then(m => m.FiOnboardingModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'userGroups/:method',
        loadChildren: () => import('./user-groups/route/route-user-groups/route-user-groups.module').then(m => m.RouteUserGroupsModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'userGroups',
        loadChildren: () => import('./user-groups/user-groups.module').then(m => m.UserGroupsModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'exportJob/:method',
        loadChildren: () => import('./export-job/route/route-export-job/route-export-job.module').then(m => m.RouteExportJobModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'exportJob',
        loadChildren: () => import('./export-job/export-job.module').then(m => m.ExportJobModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'applications/:method',
        loadChildren: () => import('./applications/route/route-applications/route-applications.module').then(m => m.RouteApplicationsModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'applications',
        loadChildren: () => import('./applications/applications.module').then(m => m.ApplicationsModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'policy/:method',
        loadChildren: () => import('./credit-underwriting/route/route-credit-underwriting/route-credit-underwriting.module').then(m => m.RouteCreditUnderwritingModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'policy',
        loadChildren: () => import('./credit-underwriting/credit-underwriting.module').then(m => m.CreditUnderwritingModule),
        canActivate: [AuthGuard]
    }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      FuseSharedModule,
      MaterialModule
  ]
})
export class MainModule { }
