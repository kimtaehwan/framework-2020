import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MgmtTableListComponent } from './mgmt-table-list.component';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';



@NgModule({
  declarations: [MgmtTableListComponent],
  imports: [
    CommonModule,

      MaterialModule,
      FuseSharedModule
  ],
    exports: [
        MgmtTableListComponent
    ]
})
export class MgmtTableListModule { }
