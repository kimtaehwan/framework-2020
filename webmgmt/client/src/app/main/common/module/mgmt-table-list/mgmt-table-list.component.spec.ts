import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgmtTableListComponent } from './mgmt-table-list.component';

describe('MgmtTableListComponent', () => {
  let component: MgmtTableListComponent;
  let fixture: ComponentFixture<MgmtTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgmtTableListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgmtTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
