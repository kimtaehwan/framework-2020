import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ValueConstant} from '../../../utils/constants/value-constant';

@Component({
  selector: 'app-mgmt-table-list',
  templateUrl: './mgmt-table-list.component.html',
  styleUrls: ['./mgmt-table-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MgmtTableListComponent implements OnInit {

    @Input() dataSource: MatTableDataSource<any>;
    @Input() type: string;
    @Input() name: string;

    @Output() edit: EventEmitter<number> = new EventEmitter();

    displayedColumns: string[] = [];
    sliceSize: number = ValueConstant.SLICE_SIZE;
    shorten: string = ValueConstant.SHORTEN;
    dateFormat: string = ValueConstant.DATE_FORMAT;

    TYPE_EXPORT_JOB = ValueConstant.TYPE_EXPORT_JOB;
    TYPE_APPLICATIONS = ValueConstant.TYPE_APPLICATIONS;

    COLUMNS_APPLICATIONS = [
        ValueConstant.COLUMN_DATE_TIME,
        ValueConstant.COLUMN_FI_TOKEN,
        ValueConstant.COLUMN_CUSTOMER_ID,
        ValueConstant.COLUMN_PRODUCT_TYPE,
        ValueConstant.COLUMN_PRODUCT_NAME,
        ValueConstant.COLUMN_STEPS,
        ValueConstant.COLUMN_PROCESS_STAGE,
        ValueConstant.COLUMN_STATUS
    ];

    COLUMNS_EXPORT_JOB = [
        ValueConstant.COLUMN_ID,
        ValueConstant.COLUMN_LAST_UPDATED,
        ValueConstant.COLUMN_FI_NAME,
        ValueConstant.COLUMN_NAME,
        ValueConstant.COLUMN_NEXT_EXECUTION_DATE,
        ValueConstant.COLUMN_JOB_EXECUTION_SCHEDULE,
        ValueConstant.COLUMN_STATUS,
        ValueConstant.COLUMN_ACTIONS
    ];

    COLUMNS_DEFAULT =  [
        ValueConstant.COLUMN_NO,
        ValueConstant.COLUMN_ID,
        ValueConstant.COLUMN_LAST_UPDATED,
        ValueConstant.COLUMN_FI_NAME,
        ValueConstant.COLUMN_NAME,
        ValueConstant.COLUMN_BY,
        ValueConstant.COLUMN_STATUS,
        ValueConstant.COLUMN_ACTIONS
    ];

  constructor() { }

  ngOnInit(): void {
      this.displayedColumns = this.getDisplayedColumns();
  }

  getDisplayedColumns(): string[] {
      console.log('### mgmt-table-list type: ', this.type);

      if (this.type === this.TYPE_APPLICATIONS) {
          return this.COLUMNS_APPLICATIONS;
      }

      if (this.type === this.TYPE_EXPORT_JOB) {
          return this.COLUMNS_EXPORT_JOB;
      }

      if ( this.type === ValueConstant.TYPE_FI_ONBOARDING ) {
          this.COLUMNS_DEFAULT.splice(this.COLUMNS_DEFAULT.indexOf(ValueConstant.COLUMN_LAST_UPDATED), 1);
          this.COLUMNS_DEFAULT.splice(this.COLUMNS_DEFAULT.indexOf(ValueConstant.COLUMN_FI_NAME), 1);
      }

      return this.COLUMNS_DEFAULT;
  }

  onEdit(id: number): void {
      console.log('onEdit: ', id);
      this.edit.emit(id);
  }

}
