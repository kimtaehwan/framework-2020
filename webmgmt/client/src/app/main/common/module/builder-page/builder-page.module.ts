import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderPageComponent } from './builder-page.component';
import {MgmtTableHeaderModule} from '../mgmt-table-header/mgmt-table-header.module';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';
import {BuilderBodyModule} from '../builder-body/builder-body.module';



@NgModule({
  declarations: [BuilderPageComponent],
  imports: [
    CommonModule,

      MaterialModule,

      MgmtTableHeaderModule,
      FuseSharedModule
  ],
    exports: [
        BuilderPageComponent
    ]
})
export class BuilderPageModule { }
