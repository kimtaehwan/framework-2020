import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewEncapsulation
} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ListedFi} from '../../../models/fi-onboarding/listed-fi';
import {FiOnboardingService} from '../../../service/fi-onboarding.service';
import {UrlConstant} from '../../../utils/constants/url-constant';
import {CommonSearch} from '../../../models/common/common-search';
import {ValueConstant} from '../../../utils/constants/value-constant';

@Component({
  selector: 'app-builder-page',
  templateUrl: './builder-page.component.html',
  styleUrls: ['./builder-page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BuilderPageComponent implements OnInit, OnChanges {

    @Input() builderName: string;

    @Input() editId: number;

    @Input() commonForm: FormGroup;

    @Input() isUserGroup = false;

    @Output() save: EventEmitter<FormGroup> = new EventEmitter();

    fiList: ListedFi[] = [];

    statusList: string[] = ['Enable', 'Disable'];

    commonSearch: CommonSearch = new CommonSearch();

  constructor(
      private _elementRef: ElementRef,
      private formBuilder: FormBuilder,
      private fiOnboardingService: FiOnboardingService
  ) {
  }

  ngOnInit(): void {
      this.initCommonForm();
      this.getFiList();
  }

  ngOnChanges(changes: SimpleChanges): void {
      console.log('changes.commonForm: ', changes.commonForm);
  }

  _hasHostAttributes(...attributes: string[]): boolean {
      return attributes.some(attribute => this._getHostElement().hasAttribute(attribute));
  }

  _getHostElement(): any {
      return this._elementRef.nativeElement;
  }

  initCommonForm(): void {
      this.commonForm = this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(ValueConstant.COMMON_NAME_MIN_LENGTH),
              Validators.maxLength(ValueConstant.COMMON_TITLE_MAX_LENGTH),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required),
          status: new FormControl('', Validators.required)
      });
  }

  getFiList(): void {
      const url = UrlConstant.getFiContentsUrl(this.commonSearch);
      this.fiOnboardingService.getFiContents(url)
          .subscribe(
              (resp) => {
                  this.fiList = resp.body.content;
              }
          );
  }

  cancel(): void {

  }

  saveAll(): void {
      this.save.emit();
  }
}
