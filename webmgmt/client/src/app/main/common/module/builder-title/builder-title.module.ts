import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderTitleComponent } from './builder-title.component';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';



@NgModule({
  declarations: [BuilderTitleComponent],
  imports: [
    CommonModule,

      MaterialModule,
      FuseSharedModule
  ],
    exports: [
        BuilderTitleComponent
    ]
})
export class BuilderTitleModule { }
