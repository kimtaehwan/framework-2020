import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-builder-title',
  templateUrl: './builder-title.component.html',
  styleUrls: ['./builder-title.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BuilderTitleComponent implements OnInit {

    @Input() disabledAdd = false;

    @Input() disabledRemove = false;

    @Input() isMetadata = false;

    @Output() addItem: EventEmitter<null> = new EventEmitter();

    @Output() removeItem: EventEmitter<null> = new EventEmitter();

    @Output() addMetadata: EventEmitter<null> = new EventEmitter();

    @Output() removeMetadata: EventEmitter<null> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onAddItem(): void {
      this.addItem.emit();
  }

  onRemoveItem(): void {
      this.removeItem.emit();
  }

  onAddMetadata(): void {
      this.addMetadata.emit();
  }

  onRemoveMetadata(): void {
      this.removeMetadata.emit();
  }
}
