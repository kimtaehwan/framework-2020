import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderTitleComponent } from './builder-title.component';

describe('BuilderTitleComponent', () => {
  let component: BuilderTitleComponent;
  let fixture: ComponentFixture<BuilderTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
