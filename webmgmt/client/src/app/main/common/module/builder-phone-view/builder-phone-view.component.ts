import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-builder-phone-view',
  templateUrl: './builder-phone-view.component.html',
  styleUrls: ['./builder-phone-view.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BuilderPhoneViewComponent implements OnInit {

    @Input() backgroundImage: string;

  constructor() { }

  ngOnInit(): void {
  }

}
