import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderPhoneViewComponent } from './builder-phone-view.component';

describe('BuilderPhoneViewComponent', () => {
  let component: BuilderPhoneViewComponent;
  let fixture: ComponentFixture<BuilderPhoneViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderPhoneViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderPhoneViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
