import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderPhoneViewComponent } from './builder-phone-view.component';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';



@NgModule({
  declarations: [BuilderPhoneViewComponent],
  imports: [
    CommonModule,

      MaterialModule,
      FuseSharedModule
  ],
    exports: [
        BuilderPhoneViewComponent
    ]
})
export class BuilderPhoneViewModule { }
