import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DateFnsConfigurationService, DateFnsModule} from 'ngx-date-fns';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
      DateFnsModule
  ],
    exports: [
        DateFnsModule
    ]
})
export class CommonLocaleModule {
    static forRoot(): ModuleWithProviders<CommonLocaleModule> {
        return {
            ngModule: CommonLocaleModule,
            providers: [
                DateFnsConfigurationService
            ]
        }
    }
}
