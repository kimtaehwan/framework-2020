import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderBodyComponent } from './builder-body.component';

describe('BuilderBodyComponent', () => {
  let component: BuilderBodyComponent;
  let fixture: ComponentFixture<BuilderBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
