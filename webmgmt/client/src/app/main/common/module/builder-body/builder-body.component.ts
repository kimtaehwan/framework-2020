import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-builder-body',
  templateUrl: './builder-body.component.html',
  styleUrls: ['./builder-body.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BuilderBodyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
