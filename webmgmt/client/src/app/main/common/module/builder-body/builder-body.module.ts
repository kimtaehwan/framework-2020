import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderBodyComponent } from './builder-body.component';



@NgModule({
  declarations: [BuilderBodyComponent],
  imports: [
    CommonModule
  ],
    exports: [
        BuilderBodyComponent
    ]
})
export class BuilderBodyModule { }
