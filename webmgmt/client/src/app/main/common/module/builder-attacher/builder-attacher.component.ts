import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-builder-attacher',
  templateUrl: './builder-attacher.component.html',
  styleUrls: ['./builder-attacher.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BuilderAttacherComponent implements OnInit {

    @Input() attachType = 'image';

    @Input() fileName: string;

    @Output() changeFile: EventEmitter<any> = new EventEmitter();

    @Output() view: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onChangeFile(event): void {
      const files = event.target.files;

      if (files.length) {
          const file = files[0];
          this.fileName = file.name;
          this.changeFile.emit(file);
      }
  }

  onViewHtml(): void {
      this.view.emit();
  }

}
