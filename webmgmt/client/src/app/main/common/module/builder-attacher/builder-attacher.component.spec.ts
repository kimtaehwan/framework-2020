import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderAttacherComponent } from './builder-attacher.component';

describe('BuilderAttacherComponent', () => {
  let component: BuilderAttacherComponent;
  let fixture: ComponentFixture<BuilderAttacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderAttacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderAttacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
