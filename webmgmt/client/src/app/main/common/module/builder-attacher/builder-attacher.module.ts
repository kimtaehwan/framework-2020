import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderAttacherComponent } from './builder-attacher.component';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';



@NgModule({
  declarations: [BuilderAttacherComponent],
  imports: [
    CommonModule,

      MaterialModule,
      FuseSharedModule
  ],
    exports: [
        BuilderAttacherComponent
    ]
})
export class BuilderAttacherModule { }
