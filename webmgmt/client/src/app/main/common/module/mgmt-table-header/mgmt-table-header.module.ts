import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MgmtTableHeaderComponent} from './mgmt-table-header.component';
import {MaterialModule} from '../../utils/material/material.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';


@NgModule({
  declarations: [MgmtTableHeaderComponent],
  imports: [
    CommonModule,

      MaterialModule,
      FuseSharedModule
  ],
    exports: [
        MgmtTableHeaderComponent
    ]
})
export class MgmtTableHeaderModule { }
