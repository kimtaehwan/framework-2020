import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-mgmt-table-header',
  templateUrl: './mgmt-table-header.component.html',
  styleUrls: ['./mgmt-table-header.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MgmtTableHeaderComponent implements OnInit {

    @Input() disabledSingle: boolean;

    @Output() create: EventEmitter<null> = new EventEmitter();

  constructor(

  ) { }

  ngOnInit(): void {
  }

  onAddClick(): void {
      this.create.emit();
  }
}
