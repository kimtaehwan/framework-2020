import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MgmtTableHeaderComponent } from './mgmt-table-header.component';

describe('MgmtTableHeaderComponent', () => {
  let component: MgmtTableHeaderComponent;
  let fixture: ComponentFixture<MgmtTableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MgmtTableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MgmtTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
