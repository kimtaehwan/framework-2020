import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TopSearchBarComponent} from './top-search-bar.component';
import {MaterialModule} from '../../utils/material/material.module';
import {TranslateModule} from '@ngx-translate/core';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';

@NgModule({
  declarations: [TopSearchBarComponent],
  imports: [
    CommonModule,
      MaterialModule,
      TranslateModule,

      FuseSharedModule
  ],
    exports: [
        TopSearchBarComponent
    ]
})
export class TopSearchBarModule { }
