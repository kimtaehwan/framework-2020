import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FuseTranslationLoaderService} from '../../../../../@fuse/services/translation-loader.service';

import {locale as english} from '../../../../navigation/i18n/en';
import {locale as turkish} from '../../../../navigation/i18n/tr';
import {UrlConstant} from '../../../utils/constants/url-constant';
import {CommonSearch} from '../../../models/common/common-search';
import {PageEvent} from '@angular/material/paginator';
import {FiOnboardingService} from '../../../service/fi-onboarding.service';
import {Fi} from '../../../models/fi-onboarding/fi';
import {ListedFi} from '../../../models/fi-onboarding/listed-fi';

@Component({
  selector: 'app-top-search-bar',
  templateUrl: './top-search-bar.component.html',
  styleUrls: ['./top-search-bar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TopSearchBarComponent implements OnInit {

    commonSearch: CommonSearch;
    @Output() clickCommonSearch: EventEmitter<CommonSearch> = new EventEmitter<CommonSearch>();

    fiList: ListedFi[] = [];

  constructor(
      private _fuseTranslationLoaderService: FuseTranslationLoaderService,
      private fiOnboardingService: FiOnboardingService
  ) {
      this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  ngOnInit(): void {
      this.initCommonSearch();
      this.getFiContents();
  }

  initCommonSearch(): void {
      const pageEvent = new PageEvent();
      pageEvent.pageIndex = 0;
      pageEvent.pageSize = 10;

      this.commonSearch = new CommonSearch('', null, null, null, pageEvent);
  }

  getFiContents(): void {
      console.log('### top-search-bar ::: commonSearch: ', this.commonSearch);

      const url = UrlConstant.getFiContentsUrl(this.commonSearch);

      console.log('getFiContents - url: ', url);
      // this.fiOnboardingService.getFiContents(url)
      //     .subscribe(
      //         (resp) => {
      //             console.log('### top-search-bar ::: fiList: ', resp.body);
      //             this.fiList = resp.body.content;
      //         }
      //     );

  }

  onClickCommonSearch(event: Event): void {
      this.clickCommonSearch.emit(this.commonSearch);
  }

}
