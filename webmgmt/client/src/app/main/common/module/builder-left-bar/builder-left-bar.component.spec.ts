import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderLeftBarComponent } from './builder-left-bar.component';

describe('BuilderLeftBarComponent', () => {
  let component: BuilderLeftBarComponent;
  let fixture: ComponentFixture<BuilderLeftBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderLeftBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderLeftBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
