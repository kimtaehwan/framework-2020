import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuilderLeftBarComponent } from './builder-left-bar.component';
import {MaterialModule} from '../../utils/material/material.module';



@NgModule({
  declarations: [BuilderLeftBarComponent],
  imports: [
    CommonModule,
      MaterialModule
  ],
    exports: [
        BuilderLeftBarComponent
    ]
})
export class BuilderLeftBarModule { }
