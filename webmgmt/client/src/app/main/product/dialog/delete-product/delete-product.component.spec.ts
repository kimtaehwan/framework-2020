import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteProductComponent } from './delete-product.component';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';
import {TranslateModule} from '@ngx-translate/core';
import {ProductService} from '../../../service/product.service';

describe('DeleteProductComponent', () => {
  let component: DeleteProductComponent;
  let fixture: ComponentFixture<DeleteProductComponent>;

  beforeEach(async(() => {
      const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'delete']);
      let mockService = new ProductService(<any> httpClientSpy);

    TestBed.configureTestingModule({
      declarations: [ DeleteProductComponent ],
        imports: [
          MatDialogModule
        ],
        providers: [
            {
                provide: MAT_DIALOG_DATA, useValue: {}
            },
            {
                provide: ProductService, useValue: mockService
            }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
