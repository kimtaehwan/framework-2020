import {Component, Inject, OnInit} from '@angular/core';
import {ProductService} from '../../../service/product.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UrlConstant} from '../../../utils/constants/url-constant';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.scss']
})
export class DeleteProductComponent implements OnInit {

  constructor(
      @Inject(MAT_DIALOG_DATA) public productId: number,
      private productService: ProductService
  ) { }

  ngOnInit(): void {
  }

  deleteConfirm(): void {
      const url = UrlConstant.deleteProductUrl(this.productId);

      this.productService.deleteProduct(url)
          .subscribe(
              result => {
                  console.log('deleteConfirm(): ', result);
              }
          );
  }
}
