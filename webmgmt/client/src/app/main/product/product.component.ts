import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import {ValueConstant} from '../utils/constants/value-constant';
import {UrlConstant} from '../utils/constants/url-constant';
import {ProductService} from '../service/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Product} from '../models/product/product';
import {CommonSearch} from '../models/common/common-search';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProductComponent implements OnInit {

    commonSearch = new CommonSearch();
    dataSource = new MatTableDataSource([]);

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private productService: ProductService
  ) { }

  ngOnInit(): void {
      this.search();
  }

  search(): void {
      const searchUrl = UrlConstant.getProductSearchUrl(this.commonSearch);

      this.productService.getProductList(searchUrl).subscribe(
          (resp) => {
              this.dataSource = new MatTableDataSource(resp.body.content);
              this.commonSearch.pageEvent.length = resp.body.totalElements;
          }
      );
  }

  createProduct(): void {
      this.router.navigate(['./create'], {relativeTo: this.activatedRoute});
  }

  editProduct(id: number): void {
      this.router.navigate(['./edit/' + id], {relativeTo: this.activatedRoute});
  }
}
