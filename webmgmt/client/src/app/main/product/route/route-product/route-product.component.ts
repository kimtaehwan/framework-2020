import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValueConstant} from '../../../utils/constants/value-constant';
import * as _ from 'underscore';
import {ActivatedRoute} from '@angular/router';
import {Metadata} from '../../../models/common/metadata';
import {ApiConstant} from '../../../utils/constants/api-constant';
import {ProductService} from '../../../service/product.service';

@Component({
  selector: 'app-route-product',
  templateUrl: './route-product.component.html',
  styleUrls: ['./route-product.component.scss']
})
export class RouteProductComponent implements OnInit {

    MIN_ITEM_NUM = 1;
    MAX_ITEM_NUM = 4;

    commonForm: FormGroup;
    productForm: FormGroup;

    fileName: string;
    preName = '';
    previewUrl: string;
    items = [];
    metadataList: Metadata[] = [];

    isView: boolean;
    editId = 0;

    commonNameMaxLength = ValueConstant.COMMON_NAME_MAX_LENGTH;

  constructor(
      private formBuilder: FormBuilder,
      private activatedRoute: ActivatedRoute,
      private productService: ProductService
  ) {
      this.initCommonForm();
      this.initForm();
  }

  ngOnInit(): void {
      const tempEditId = this.activatedRoute.snapshot.paramMap.get('editId');

      if ( tempEditId ) {
          this.editId = Number(tempEditId);
          this.getProduct(this.editId);
      } else {
          this.addItem('');
          this.addMetadata(null);
      }
  }

  getProduct(editId): void {
      const url = ApiConstant.API_PRODUCT_URL + '/' + editId;

      this.productService.getProduct(url)
          .subscribe(
              resp => {
                  console.log('resp - getProduct: ', resp);
              }
          );
  }

  addItem(value: string): void {
      const itemNo = this.items.length + 1;
      const placeholder = 'Enter Feature ' + itemNo;
      const name = 'name' + itemNo;

      if ( _.isEmpty(value) ) {
          value = '';
      }

      const item = {
          placeholder: placeholder,
          name: name,
          value: value
      };

      this.items.push(item);
  }

  addMetadata(metadata: Metadata): void {
      if (!metadata) {
          metadata = new Metadata();
      }

      this.metadatas.push(this.newMetadata());
      this.metadataList.push(metadata);
  }

  get metadatas(): FormArray {
      return this.productForm.get('metadataList') as FormArray;
  }

  newMetadata(): FormGroup {
      return this.formBuilder.group({
          key: '',
          value: '',
          description: ''
      });
  }

  removeMetadata(): void {
      console.log('removeMetadata: ', this.metadataList);
  }

  removeItem(): void {
      if (this.items.length > 1) {
          this.items.pop();
      }
  }

  initCommonForm(): void {
      this.commonForm = this.formBuilder.group({
          contentTitle: new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          fiToken: new FormControl('', Validators.required)
      });
  }

  initForm(): void {
      this.productForm = this.formBuilder.group({
          image: null,
          name:  new FormControl('', Validators.compose([
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(256),
              Validators.pattern(ValueConstant.PATTERN_DASH)
          ])),
          features0: new FormControl(''),
          features1: new FormControl(''),
          features2: new FormControl(''),
          features3: new FormControl(''),
          features4: new FormControl(''),
          metadataList: new FormArray([])
      });
  }

  onChangeFile(file: any): void {
      const formData = new FormData();
      formData.append('file', file);

      this.productService.postProductImage(formData)
          .subscribe(
              (resp) => {
                  this.productForm.get('image').setValue(resp.body);
                  this.fileName = resp.body.originalFileName;

                  this.previewUrl = this.getPreviewUrl(resp.body.uri);
              }
          );
  }

  getPreviewUrl(path): string {
      return ApiConstant.API_BASE_URL + path;
  }

}
