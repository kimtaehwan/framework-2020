import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RouteProductComponent} from './route-product.component';
import {MaterialModule} from '../../../common/utils/material/material.module';
import {BuilderPageModule} from '../../../common/module/builder-page/builder-page.module';
import {FuseSharedModule} from '../../../../../@fuse/shared.module';
import {BuilderTitleModule} from '../../../common/module/builder-title/builder-title.module';
import {BuilderBodyModule} from '../../../common/module/builder-body/builder-body.module';
import {BuilderAttacherModule} from '../../../common/module/builder-attacher/builder-attacher.module';
import {BuilderPhoneViewModule} from '../../../common/module/builder-phone-view/builder-phone-view.module';

const routes: Routes = [
    {
        path: ':editId',
        component: RouteProductComponent
    },
    {
        path: '**',
        component: RouteProductComponent
    }
];

@NgModule({
  declarations: [RouteProductComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule,

      BuilderPageModule,
      BuilderBodyModule,
      BuilderTitleModule,
      BuilderAttacherModule,
      BuilderPhoneViewModule
  ]
})
export class RouteProductModule { }
