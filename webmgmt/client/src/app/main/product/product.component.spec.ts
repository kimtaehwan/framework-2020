import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductComponent} from './product.component';
import {ProductModule} from './product.module';
import {Router} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;

  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductComponent ],
        imports: [
            NoopAnimationsModule,
            ProductModule,
            TranslateModule.forRoot()
        ],
        providers: [
            {
                provide: Router, useValue: routerSpy
            }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
