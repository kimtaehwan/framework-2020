import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import {RouterModule, Routes} from '@angular/router';
import {TopSearchBarModule} from '../common/module/top-search-bar/top-search-bar.module';
import {MaterialModule} from '../common/utils/material/material.module';
import { RouteProductComponent } from './route/route-product/route-product.component';
import {MgmtTableHeaderModule} from '../common/module/mgmt-table-header/mgmt-table-header.module';
import { DeleteProductComponent } from './dialog/delete-product/delete-product.component';
import {MgmtTableListModule} from '../common/module/mgmt-table-list/mgmt-table-list.module';

const routes: Routes = [
    {
        path: '**',
        component: ProductComponent
    }
];

@NgModule({
  declarations: [ProductComponent, DeleteProductComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      TopSearchBarModule,
      MgmtTableHeaderModule,
      MgmtTableListModule,

      MaterialModule
  ]
})
export class ProductModule { }
