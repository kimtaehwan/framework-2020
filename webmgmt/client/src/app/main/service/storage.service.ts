import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ResponseFile} from '../models/storage/response-file';
import {Observable} from 'rxjs';
import {ApiConstant} from '../utils/constants/api-constant';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
      private httpClient: HttpClient
  ) { }

  postHtml(formData: FormData): Observable<HttpResponse<ResponseFile>> {
      return this.httpClient.post<ResponseFile>(ApiConstant.API_STORAGE_URL + '/files', formData, {observe: 'response'});
  }

}
