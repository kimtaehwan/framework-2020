import { Injectable } from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {ApiConstant} from '../utils/constants/api-constant';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {DcsStandard} from '../models/applications/dcs-standard';
import {JourneyProcessTypeLog} from '../models/applications/journey-process-type-log';
import {ApplicationData} from '../models/applications/application-data';
import {IdvTransaction} from '../models/applications/apps/idv-transaction';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

    TYPE_PRODUCT = 'PRODUCT';
    TYPE_MFA = 'MFA';
    TYPE_IDV = 'IDV';
    TYPE_CREDIT_CHECK = 'CREDIT_CHECK';
    TYPE_INSTANT_ISSUANCE = 'INSTANT_ISSUANCE';
    TYPE_ACTIVATE_CARD = 'ACTIVATE_CARD';
    TYPE_PUSH_PROVISIONING = 'PUSH_PROVISIONING';

    TYPE_EMAIL_CHECKING = 'EMAIL_CHECKING';
    TYPE_CARDHOLDER_VERIFICATION = 'CARDHOLDER_VERIFICATION';
    TYPE_HOME_ADDRESS_VERIFICATION = 'HOME_ADDRESS_VERIFICATION';
    TYPE_TERMS_AND_CONDITIONS = 'TERMS_AND_CONDITIONS';
    TYPE_ESIGNATURE = 'ESIGNATURE';
    TYPE_CINFO = 'CINFO';
    TYPE_RISK_ASSESSMENT = 'RISK_ASSESSMENT';
    TYPE_ELIGIBILITY_CHECK = 'ELIGIBILITY_CHECK';
    TYPE_LOGIN_CREATION = 'LOGIN_CREATION';
    TYPE_USER_IDENTIFICATION = 'USER_IDENTIFICATION';
    TYPE_USER_VERIFICATION = 'USER_VERIFICATION';
    TYPE_PRODUCT_DUPLICATE_DETECTION = 'PRODUCT_DUPLICATE_DETECTION';
    TYPE_USER_ACCEPTANCE = 'USER_ACCEPTANCE';

    FILE_TYPE_BASE64 = 'base64';
  constructor(
      private httpClient: HttpClient
  ) { }

  openTransaction(appRefId: string, callback: any): void {
      const logs$ = this.getLogsById(appRefId);

      logs$.subscribe((resp) => {
          const content = resp.content;

          if ( !!content && !!content.journeyProcessTypeLogList) {
              const journeyProcessTypeLogList = content.journeyProcessTypeLogList;
              const applicationData = this.getApplicationData(appRefId, journeyProcessTypeLogList);

              const appRefData$ = this.getReferenceById(appRefId);
              appRefData$.subscribe((resp) => {
                  applicationData.applicationReference = resp.content;
              });

              applicationData.idvTransactions = [];
              applicationData.journeyProcessTypeLogList = journeyProcessTypeLogList;
              applicationData.mobileDevices = content.mobileDevice;
          }
      });

      callback();
  }

  getApplicationData(appRefId: string, journeyProcessTypeLogList: JourneyProcessTypeLog[]): ApplicationData {
      const applicationData: ApplicationData = new ApplicationData();

      journeyProcessTypeLogList.forEach((journeyProcessTypeLog: JourneyProcessTypeLog) => {
          const processStatus = journeyProcessTypeLog.processStatus;
          const processType = journeyProcessTypeLog.processType;

          if (processStatus !== 'NOT_STARTED') {
              if ( processType === this.TYPE_PRODUCT ) {
                  this.getProductInfo(appRefId).subscribe((resp) => {
                      this.bindProductInfo(applicationData, this.getResultData(resp, this.TYPE_PRODUCT));
                  });
              } else if ( processType === this.TYPE_MFA ) {
                  this.getMfa(appRefId).subscribe((resp) => {
                      this.bindMfaOtpMapping(applicationData, this.getResultData(resp, this.TYPE_MFA));
                  });
              } else if ( processType === this.TYPE_IDV ) {
                  // getDocumentResultLogList
                  this.getIdv(appRefId).subscribe((resp) => {
                      this.bindDocumentResultLogList(applicationData, this.getResultData(resp, this.TYPE_IDV));
                  });
              } else if ( processType === this.TYPE_CREDIT_CHECK ) {

              } else if ( processType === this.TYPE_INSTANT_ISSUANCE ) {

              } else if ( processType === this.TYPE_ACTIVATE_CARD ) {

              } else if ( processType === this.TYPE_PUSH_PROVISIONING ) {

              } else if ( processType === this.TYPE_EMAIL_CHECKING ) {

              } else if ( processType === this.TYPE_CARDHOLDER_VERIFICATION ) {

              } else if ( processType === this.TYPE_HOME_ADDRESS_VERIFICATION ) {

              } else if ( processType === this.TYPE_TERMS_AND_CONDITIONS ) {

              } else if ( processType === this.TYPE_ESIGNATURE ) {

              } else if ( processType === this.TYPE_CINFO ) {

              } else if ( processType === this.TYPE_RISK_ASSESSMENT ) {

              } else if ( processType === this.TYPE_ELIGIBILITY_CHECK ) {

              } else if ( processType === this.TYPE_LOGIN_CREATION ) {

              } else if ( processType === this.TYPE_USER_IDENTIFICATION ) {

              } else if ( processType === this.TYPE_USER_VERIFICATION ) {

              } else if ( processType === this.TYPE_PRODUCT_DUPLICATE_DETECTION ) {

              } else if ( processType === this.TYPE_USER_ACCEPTANCE ) {

              }
          }
      });

      return applicationData;
  }

  bindProductInfo(applicationData: ApplicationData, productInfo: any): void {
      applicationData.productInfo = productInfo;

      if (!!applicationData.productInfo && !!applicationData.productInfo.description ){
          applicationData.productInfo.offers = applicationData.productInfo.description.split(/\r\n|\r|\n/); // U+21B5
      }
  }

  bindMfaOtpMapping(applicationData: ApplicationData, mfaOtpMapping: any): void {
      applicationData.mfaOtpMapping = mfaOtpMapping;
  }

  bindDocumentResultLogList(applicationData: ApplicationData, docResultLogList: any): void {
      if (!!docResultLogList) {
          for (let i = 0; i < docResultLogList.length; i++) {
              applicationData.idvTransactions[i] = new IdvTransaction();
              Object.assign(applicationData.idvTransactions[i], docResultLogList[i]);
          }
      }
  }

  getResultData(result: DcsStandard, processType: string ): any {
      if (!!result ) {
          if ( processType === this.TYPE_IDV || processType === this.TYPE_MFA) {
              return result.contents;
          }

          if ( processType === this.TYPE_USER_IDENTIFICATION
              || processType === this.TYPE_INSTANT_ISSUANCE
              || processType === this.TYPE_USER_VERIFICATION
              || processType === this.TYPE_USER_ACCEPTANCE
          ) {
              return result.responseDataList[0];
          }

          return result.content;
      }
  }

  getLogsById(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_JOURNEY_URL + '/v2/applications/references/' + appRefId + '/process-log';
      return this.httpClient.get<DcsStandard>(url);
  }

  getReferenceById(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_JOURNEY_URL + '/v2/applications/references/' + appRefId;
      return this.httpClient.get<DcsStandard>(url);
  }

  getProductInfo(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_CUSTOMER_SUPPORT_URL + '/productinfo/v2/product';
      return this.httpClient.get<DcsStandard>(url, this.getHeaderOptions(appRefId, this.FILE_TYPE_BASE64, this.TYPE_PRODUCT));
  }

  getMfa(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_CUSTOMER_SUPPORT_URL + '/mfa/v1/list?applicationReferenceId=' + appRefId;
      return this.httpClient.get<DcsStandard>(url);
  }

  getIdv(appRefId: string): Observable<DcsStandard> {
      const url = ApiConstant.API_CUSTOMER_SUPPORT_URL + '/idv/api/v1/documents/' + appRefId + '/logs';
      return this.httpClient.get<DcsStandard>(url);
  }

  getHeaderOptions(appRefId: string, fileType: string, type: string): object {
      return {
          headers: this.getHeaders(appRefId, fileType, type)
      };
  }

  getHeaders(appRefId: string, fileType: string, type: string): HttpHeaders {
      const options = {
          'x-ondotsystems-applicationReferenceId': appRefId,
          'x-ondotsystems-fiToken': '',
          'x-ondotsystems-journeyId': '',
          'x-ondotsystems-fileType': fileType
      };

      if ( type === this.TYPE_PRODUCT) {
          options['image-type'] = 'url';
      }

      return new HttpHeaders(options);
  }
}
