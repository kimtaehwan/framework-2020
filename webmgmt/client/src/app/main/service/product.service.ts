import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductList} from '../models/product/product-list';
import {Product} from '../models/product/product';
import {ApiConstant} from '../utils/constants/api-constant';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getProductList(url): Observable<HttpResponse<ProductList>> {
      return this.httpClient.get<ProductList>(url, {observe: 'response'});
  }

  getProduct(url): Observable<HttpResponse<Product>> {
      return this.httpClient.get<Product>(url, {observe: 'response'});
  }

  postProductImage(formData: FormData): Observable<HttpResponse<Product>> {
      return this.httpClient.post<Product>(ApiConstant.API_IMAGES_URL, formData, {observe: 'response'});
  }

  deleteProduct(url): Observable<HttpResponse<Response>> {
      return this.httpClient.delete<Response>(url, {observe: 'response'});
  }
}
