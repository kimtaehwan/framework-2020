import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginKakao} from '../models/login-kakao';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getLoginKakao(url): Observable<HttpResponse<LoginKakao>> {
      return this.httpClient.get<LoginKakao>(url, {observe: 'response'});
  }
}
