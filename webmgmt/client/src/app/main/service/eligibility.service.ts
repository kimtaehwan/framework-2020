import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Eligibility} from '../models/eligibility/eligibility';
import {ApiConstant} from '../utils/constants/api-constant';

@Injectable({
  providedIn: 'root'
})
export class EligibilityService {

  constructor(
      private httpClient: HttpClient
  ) { }

    postEligibility(saveData): Observable<HttpResponse<Eligibility>> {
      return this.httpClient.post<Eligibility>(ApiConstant.API_ELIGIBILITY_URL, saveData, {observe: 'response'});
    }

    deleteEligibility(url): Observable<HttpResponse<Response>> {
      return this.httpClient.delete<Response>(url, {observe: 'response'});
    }
}
