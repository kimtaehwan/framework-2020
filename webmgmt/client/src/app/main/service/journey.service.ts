import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServiceList} from '../models/journey/service-list';

@Injectable({
  providedIn: 'root'
})
export class JourneyService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getServices(url: string): Observable<HttpResponse<ServiceList>> {
      return this.httpClient.get<ServiceList>(url, {observe: 'response'});
  }
}
