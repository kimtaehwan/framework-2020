import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FiContents} from '../models/fi-onboarding/fi-contents';
import {Fi} from '../models/fi-onboarding/fi';
import {ApiConstant} from '../utils/constants/api-constant';
import {Country} from '../models/fi-onboarding/country';

@Injectable({
  providedIn: 'root'
})
export class FiOnboardingService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getFiContents(url): Observable<HttpResponse<FiContents>> {
      return this.httpClient.get<FiContents>(url, {observe: 'response'});
  }

  postFi(saveData): Observable<HttpResponse<Fi>> {
      const url = ApiConstant.API_FI_ONBOARDING_URL + '/financialInstitutions';
      return this.httpClient.post<Fi>(url, saveData, {observe: 'response'});
  }

  getCountries(): Observable<HttpResponse<Country[]>> {
      const url = ApiConstant.API_FI_ONBOARDING_URL + '/countries';
      return this.httpClient.get<Country[]>(url, {observe: 'response'});
  }
}
