import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiConstant} from '../utils/constants/api-constant';

@Injectable({
  providedIn: 'root'
})
export class TermsService {

  constructor(
      private httpClient: HttpClient
  ) { }

  postTerms(saveData: object): Observable<HttpResponse<Response>> {
      return this.httpClient.post<Response>(ApiConstant.API_TERMS_URL, saveData, {observe: 'response'});
  }
}
