import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../main/common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: LoginComponent
    }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,

      FuseSharedModule
  ]
})
export class LoginModule { }
