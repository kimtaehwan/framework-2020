import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FuseConfigService} from '../../../@fuse/services/config.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {fuseAnimations} from '../../../@fuse/animations';
import {AuthenticationService} from '../service/authentication.service';
import {first} from 'rxjs/operators';
import {error} from 'selenium-webdriver';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    returnUrl: string;

  constructor(
      private _fuseConfigService: FuseConfigService,
      private _formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this._fuseConfigService.config = {
          layout: {
              navbar: {
                  hidden: true
              },
              toolbar: {
                  hidden: true
              },
              footer: {
                  hidden: true
              },
              sidepanel: {
                  hidden: true
              }
          }
      };
  }

  ngOnInit(): void {
      this.initForm();

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      console.log('### returnUrl: ', this.returnUrl);
      console.log('### returnUrl: ', this.returnUrl);

      localStorage.removeItem('access_token');
  }

  initForm(): void {
      this.loginForm = this._formBuilder.group({
          username: ['', [
              Validators.required
          ]],
          password: ['', Validators.required]
      });


  }

  onSubmit(): void {
      const basicToken = this.generateBasicToken();

      console.log('# basicToken: ', basicToken);

      this.authenticationService.login(basicToken)
          .pipe(first())
          .subscribe(
              (resp) => {
                  console.log('login data: ', resp);
                  console.log('returnUrl: ', this.returnUrl);

                  if (resp.level === 'onSuccess') {
                      localStorage.setItem('access_token', resp.idToken);
                      this.router.navigate([this.returnUrl]);
                  }
              },
              (err) => {
                  console.log('error: ', err);
              });
  }

  generateBasicToken(): string {
      const username = this.loginForm.get('username').value;
      const password = this.loginForm.get('password').value;
      const token = `${username}:${password}`;

      return `Basic ${btoa(token)}`;
  }

}
