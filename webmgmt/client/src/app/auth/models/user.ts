export class User {
    level: string;
    tokenType: string;
    expiresIn: number;
    accessToken: string;
    idToken: string;
    refreshToken: string;

    constructor() {
        this.level = '';
        this.tokenType = '';
        this.expiresIn = 0;
        this.accessToken = '';
        this.idToken = '';
        this.refreshToken = '';
    }
}
