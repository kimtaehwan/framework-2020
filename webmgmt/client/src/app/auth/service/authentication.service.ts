import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {ApiConstant} from '../../main/utils/constants/api-constant';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    TOKEN_NAME = 'access_token';

  constructor(
      private httpClient: HttpClient,
      private jwtHelper: JwtHelperService
  ) {
  }

  isAuthenticated(): boolean {
      const token = this.getToken();
      return token ? !this.isTokenExpired(token) : false;
  }

  isTokenExpired(token: string): boolean {
      return this.jwtHelper.isTokenExpired(token);
  }

  getToken(): string {
      return localStorage.getItem(this.TOKEN_NAME);
  }

  login(basicToken: string): any {
      const authorization = new HttpHeaders({
          Authorization: basicToken
      });

      const headers = {
          headers: authorization
      };

      return this.httpClient.post<User>(ApiConstant.AUTH_URL + '', null, headers);
  }

  signUp(data: object): Observable<HttpResponse<User>> {
      return this.httpClient.post<User>(ApiConstant.AUTH_URL + '/signUp', data, {observe: 'response'});
  }

  logout(): void {
      localStorage.removeItem('access_token');
  }
}
