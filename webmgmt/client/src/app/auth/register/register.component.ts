import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '../../../@fuse/animations';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {FuseConfigService} from '../../../@fuse/services/config.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AuthenticationService} from '../service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy {

    registerForm: FormGroup;

    private _unsubscribeAll: Subject<any>;

  constructor(
      private _fuseConfigService: FuseConfigService,
      private _formBuilder: FormBuilder,
      private authenticationService: AuthenticationService
  ) {
      // Configure the layout
      this._fuseConfigService.config = {
          layout: {
              navbar   : {
                  hidden: true
              },
              toolbar  : {
                  hidden: true
              },
              footer   : {
                  hidden: true
              },
              sidepanel: {
                  hidden: true
              }
          }
      };

      this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {

      this.registerForm = this._formBuilder.group({
          name: [ '', Validators.required ],
          email: ['', Validators.compose([
              Validators.required,
              Validators.email
          ])],
          password: ['', Validators.required],
          passwordConfirm: ['', Validators.compose([
              Validators.required,
              confirmPasswordValidator
          ])]
      });

      this.registerForm.get('password').valueChanges
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
              this.registerForm.get('passwordConfirm').updateValueAndValidity();
          });
  }

  ngOnDestroy(): void {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  signUp(): void {
      console.log('signUp');
      const saveData = this.getSaveData();

      this.authenticationService.signUp(saveData)
          .subscribe((resp) => {
              console.log('signUp: ', resp);
          });
  }

  getSaveData(): object {
      return {
          name: this.registerForm.get('name').value,
          email: this.registerForm.get('email').value,
          password: this.registerForm.get('password').value
      };
  }
}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    if ( !control.parent || !control ) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {
        'passwordsNotMatching': true
    };
};
