import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/_helpers/auth.guard';

const appRoutes: Routes = [
    {
        path    : 'main',
        loadChildren    : () => import('./main/main.module').then(m => m.MainModule),
        canActivate: [AuthGuard]
    },

    {
        path : 'auth',
        loadChildren : () => import('./auth/auth.module').then(m => m.AuthModule)
    },

    {
        path    : 'samples',
        loadChildren    : () => import('./samples/samples.module').then(m => m.SamplesModule)
    },

    {
        path      : '**',
        redirectTo: 'main'
    }
];

@NgModule({
  declarations: [],
  imports: [
      RouterModule.forRoot(
          appRoutes
      ),
  ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
