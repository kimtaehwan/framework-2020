import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';

import {fuseConfig} from 'app/fuse-config';

import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {AppRoutingModule} from './app-routing.module';
import {ErrorMessageComponent} from './main/common/dialog/error-message/error-message.component';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {MaterialModule} from './main/common/utils/material/material.module';
import {ErrorInterceptor} from './interceptor/error.interceptor';
import {JWT_OPTIONS, JwtModule} from '@auth0/angular-jwt';
import {TokenService} from './auth/service/token.service';

export const jwtOptionsFactory = (tokenService: TokenService) => ({
    tokenGetter: () => tokenService.getAccessToken(),
    allowedDomains: ['localhost:3000', 'localhost:5000']
});

@NgModule({
    declarations: [
        AppComponent,
        ErrorMessageComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MaterialModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        AppRoutingModule,

        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useFactory: jwtOptionsFactory,
                deps: [TokenService]
            }
        })
    ],
    bootstrap   : [
        AppComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        TokenService
    ]
})
export class AppModule
{
}
