export const environment = {
    production: true,
    hmr       : false,
    apiBaseUrl: '/api',
    idvApiBaseUrl: '/idv',
    oauthUrl: '/oauth'
};
