const express = require('express');
const app = express();
const httpProxy = require('http-proxy');
const cors = require('cors');
const bodyParser = require('body-parser');

const proxy = httpProxy.createProxyServer().on('error', (e) => {
    console.log('### proxy error: ', e);
});

const corsOptionsDelegate = (req, callback) => {
    corsOptions = {
        origin: true,
        credentials: true,
        exposedHeaders: ['location']
    };

    callback(null, corsOptions);
};

app.use(cors(corsOptionsDelegate));

// https://guestbook.dcs-devel-test-domain.gq/ - DEV1
// https://dcs-dev2-52180915.us-east-1.elb.amazonaws.com/ - DEV2
// https://dcs-customer-qa-1-alb-994926339.us-east-1.elb.amazonaws.com/ - Customer QA1

app.use((req, res) => {
    console.log('req: ', req.headers);
    proxy.web(req, res, {
        target: 'https://guestbook.dcs-devel-test-domain.gq/',
        changeOrigin: true,
        rejectUnauthorized: false,
        secure: false,
        requestCert: false
    });
}).listen(5000);


