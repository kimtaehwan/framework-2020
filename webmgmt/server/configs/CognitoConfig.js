export const CognitoConfig = {
    region: process.env.AUTH_REGION,
    userPoolId: process.env.AUTH_USER_POOL_ID,
    appClientId: process.env.AUTH_APP_CLIENT_ID
};
