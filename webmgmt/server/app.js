const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const os = require('os');
const dotenv = require('dotenv');

if ( !process.env.NODE_ENV ) {
  console.log('### !process.env.NODE_ENV');
}

const indexRouter = require('./routes/index');
const authRouter = require('./routes/auth/auth');
const productsRouter = require('./routes/api/products');
const fiOnboardingRouter = require('./routes/api/fi-onboarding');
const imagesRouter = require('./routes/api/images');
const storageRouter = require('./routes/api/storage');
const termsRouter = require('./routes/api/terms');
const journeyRouter = require('./routes/api/journey');
const userGroupsRouter = require('./routes/api/user-groups');

// samples
const mailRouter  = require('./routes/samples/mail');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

setEnv();
console.log('process.env.HELLO: ', process.env.HELLO);
console.log('procsss.env.NODE_ENV: ', process.env.NODE_ENV);


// cors 가 app.use 보다 먼저 실행되어야 함.
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader(path.resolve('./')+ '/app.properties');

app.locals.FI_ONBOARDING_URL = getFiOnboardingUrl();

const whiteList = ['http://localhost:4200', 'http://localhost:3000', 'http://localhost:5200'];
const corsOptionsDelegate = function (req, callback) {
  let corsOptions = {origin: false};

  if ( whiteList.indexOf(req.header('Origin')) !== -1 ) {
    corsOptions = {origin: true};
  }

  callback(null, corsOptions);
};

app.use(cors(corsOptionsDelegate));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use(authRouter().getRouter());
app.use('/mail', mailRouter);
app.use('/api/products', productsRouter);
app.use('/api/fiOnboarding', fiOnboardingRouter);
app.use('/api/images', imagesRouter);
app.use('/api/storage', storageRouter);
app.use('/api/terms', termsRouter);
app.use('/api/journeys', journeyRouter);
app.use('/api/userGroups', userGroupsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// ignore TLS authorization
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

function getFiOnboardingUrl() {
  const apiProtocol = properties.get('fi.onboarding.http');
  const apiUrl = properties.get('fi.onboarding.url');
  const apiPort = properties.get('fi.onboarding.port');

  const resultUrl = apiProtocol + apiUrl + ':' + apiPort;

  console.log('# Fi-Onboarding Url: ', resultUrl);
  return resultUrl;
}

function isLocalEnvironment() {
  for(let addresses of Object.values(os.networkInterfaces())) {
    for(let add of addresses) {
      if(add.address.startsWith('192.168.')) {
        return true;
      }
    }
  }
  return false;
}

function setEnv() {
  const nodeEnv = process.env.NODE_ENV;
  console.log('### nodeEnv: ', nodeEnv);

  if (nodeEnv === 'dcs') {
    dotenv.config({
      overwrite: true,
      path: path.join(__dirname, '.env.dcs')
    });
  } else if (nodeEnv === 'production') {
    dotenv.config({
      overwrite: true,
      path: path.join(__dirname, '.env.production')
    });
  } else {
    dotenv.config({
      overwrite: true,
      path: path.join(__dirname, '.env.local')
    });
  }
}

module.exports = app;
