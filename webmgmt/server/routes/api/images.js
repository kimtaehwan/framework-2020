const express = require('express');
const asyncify = require('express-asyncify');
const fetch = require('node-fetch');
const FormData = require('form-data');

const multer = require('multer');
const upload = multer();

const cpUpload = upload.fields([
    {
        name: 'file',
        maxCount: 1
    }
]);

const router = asyncify(express.Router());

router.post('/', cpUpload, async (req, res, next) => {
    let url = req.app.locals.FI_ONBOARDING_URL + '/images';
    let file = req.files['file'][0];
    let form = new FormData();
    form.append('file', file.buffer, file.originalname);

    await fetch(url, {method: 'POST', body: form})
        .then((response) => {
            return response.json();
        }).then( (json) => {
            json.originalName = file.originalname;
            res.json(json);
        });
});

router.get('/:fileName', async (req, res, next) => {
    const fileName = req.params.fileName;
    const url = req.app.locals.FI_ONBOARDING_URL+ '/images/' +fileName;
    let resHeaders = {};

    await fetch(url, {method: 'GET'})
        .then((response) => {
            let rawHeaders = response.headers.raw();

            for( let i in rawHeaders ) {
                resHeaders[i] = rawHeaders[i][0];
            }

            return response.buffer();
        }).then((data) => {
            res.set(resHeaders);
            res.send(data);
        }).catch((err) => {
            console.log('error: ', err);
        });
});

module.exports = router;