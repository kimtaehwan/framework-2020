const express = require('express');
const asyncify = require('express-asyncify');
const fetch = require('node-fetch');
const FormData = require('form-data');

const multer = require('multer');
const upload = multer();

const cpUpload = upload.fields([
    {
        name: 'file',
        maxCount: 1
    }
]);

const router = asyncify(express.Router());

router.post('/files', cpUpload, async (req, res, next) => {
   const url =  req.app.locals.FI_ONBOARDING_URL + '/files';
   const file = req.files['file'][0];
   let formData = new FormData();
   formData.append('file', file.buffer, file.originalname);

   try {
       await fetch(url, {method: 'POST', body: formData})
           .then( (response) => {
               return response.json();
           }).then( (json) => {
               json.originalName = file.originalname;
               console.log('json: ', json);
               res.json(json);
           });
   } catch (e) {
       console.error(e);
   }
});

module.exports = router;