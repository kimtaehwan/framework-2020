var express = require('express');
var asyncify = require('express-asyncify');
var axios = require('axios');

var router = asyncify(express.Router());

var options = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    method: '',
    url: '',
    data: {}
};

router.get('/', async (req, res, next) => {
    const page = req.query.page;
    const size = req.query.size;
    const q = req.query.q;

    options.url = getProductListUrl(req, page, size, q);
    options.method = 'GET';

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

router.get('/:productId', async (req, res, next) => {
    const productId = req.params.productId;

    options.url = getProductUrl(req, productId);
    options.method = 'GET';

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

function getProductListUrl(req, page, size, q) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/products?page=' +page+ '&size=' +size+ '&title=' +q;
    return resultUrl;
}

function getProductUrl(req, productId) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/products/' +productId;
    return resultUrl;
}

module.exports = router;