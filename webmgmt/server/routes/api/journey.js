const express = require('express');
const asyncify = require('express-asyncify');
const axios = require('axios');

let router = asyncify(express.Router());

let options = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    method: '',
    url: '',
    data: {}
};

router.get('/journey-config', async (req, res, next) => {
    const url = getConfigUrl(req);
    options.url = url;
    options.method = 'GET';

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.log(e);
    }
});

function getConfigUrl(req) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/journey-config';
    return resultUrl;
}

module.exports = router;