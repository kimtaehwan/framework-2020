var express = require('express');
var asyncify = require('express-asyncify');
var axios = require('axios');

var router = asyncify(express.Router());

var options = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    method: '',
    url: '',
    data: {}
};

router.post('/', async (req, res, next) => {
    options.url = postTermsUrl(req);
    options.method = 'POST';
    options.data = req.body;

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

function postTermsUrl(req) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/terms';
    return resultUrl;
}

module.exports = router;
