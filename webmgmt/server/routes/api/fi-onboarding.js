var express = require('express');
var asyncify = require('express-asyncify');
var axios = require('axios');

var router = asyncify(express.Router());

var options = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    method: '',
    url: '',
    data: {}
};

router.get('/financialInstitutions', async (req, res, next) => {
    const page = req.query.page;
    const size = req.query.size;
    const q = '';

    options.url = getFiContentsUrl(req, page, size, q);
    options.method = 'GET';

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

router.post('/financialInstitutions', async(req, res, next) => {
    options.url = postFiUrl(req);
    options.method = 'POST';
    options.data = req.body;

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

router.get('/countries', async (req, res, next) => {
    options.url = req.app.locals.FI_ONBOARDING_URL+ '/countries';
    options.method = 'GET';

    try {
        let response = await axios(options);
        res.json(response.data);
    } catch (e) {
        console.error(e);
    }
});

function getFiContentsUrl(req, page, size, q) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/financialInstitutions?page=' +page+ '&size=' +size+ '&q=' +q;
    return resultUrl;
}

function postFiUrl(req) {
    const resultUrl = req.app.locals.FI_ONBOARDING_URL+ '/financialInstitutions';
    return resultUrl;
}

module.exports = router;
