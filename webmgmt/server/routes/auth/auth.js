var express = require('express');
var asyncify = require('express-asyncify');
var axios = require('axios');

const bearerToken = require('express-bearer-token');
const jwtDecode = require('jwt-decode');
const AWS = require('aws-sdk');
const basicAuth = require("basic-auth");
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');

const CognitoSecretCodeMap = {};
const CognitoSecretCodeMapObj = {};
const refreshTokenMap = {};
const serviceProvider = new AWS.CognitoIdentityServiceProvider();

const TEMP_NEW_PASSWORD = 'Ondot@1234';
const TOKEN_EXPIRATION = 5;

class AwsCognito {
    constructor(options) {
        this.router = express.Router();

        this.router.use(bearerToken());

        this.router.post("/auth", login);
    }

    getRouter() {
        return this.router;
    }

    getThis() {
        return this;
    }
}

const getCognitoUserPool = () => {
    const USER_POOL_DATA = {
        UserPoolId: process.env.COGNITO_USER_POOL_ID,
        ClientId: process.env.COGNITO_USER_POOL_ID
    };

    return new AmazonCognitoIdentity.CognitoUserPool(USER_POOL_DATA);
};

const getCredentials = (req) => {
    return basicAuth(req);
};

const getAuthenticationData = (req) => {
    return {
        Username: getCredentials(req).name,
        Password: getCredentials(req).pass
    };
};

const getUserData = (username) => {
    return {
        Username: username,
        Pool: getCognitoUserPool()
    };
};

const login = async (req, res, next) => {
    console.log('login: ', req.token);
    if (!!req.token) { // token 이 존재할 때
        try {
            const decoded = jwtDecode(req.token);

            console.log('### decoded: ', decoded);
        } catch (e) {
            console.error(e);
            return res.status(401).send(e);
        }
    }

    /**
     * token 이 아닌 경우,
     * ex) 로그인시 username, password 사용시
     */
    const authenticationData = getAuthenticationData(req);
    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(getUserData(authenticationData.Username));
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: async (result) => {
            console.log('### result: ', result);
            const accessTokenDecoded = jwtDecode(result.getAccessToken().getJwtToken());

            console.log('1. Access Token: ', JSON.stringify(accessTokenDecoded));
            console.log('2. Id Token: ', JSON.stringify(jwtDecode(result.getIdToken().getJwtToken())));
            console.log('3. Refresh Token: ', JSON.stringify(result.getRefreshToken().getToken()));

            refreshTokenMap[accessTokenDecoded.username] = result.getRefreshToken().getToken();

            res.header('Cache-Control', 'no-store');
            res.header('Pragma', 'no-cache');
            res.json({
                level: 'onSuccess',
                tokenType: 'bearer',
                expiresIn: TOKEN_EXPIRATION,
                accessToken: result.getAccessToken().getJwtToken(),
                idToken: result.getIdToken().getJwtToken(),
                refreshToken: result.getRefreshToken().getToken()
            });
        },
        mfaRequired: (codeDeliveryDetails) => {
            console.log('### mfaRequired - codeDeliveryDetails: ', codeDeliveryDetails);
            console.log('### mfaRequired - req.body: ', req.body);

            CognitoSecretCodeMap[req.headers['authorization']].sendMFACode(
                req.body.code,
                this
            );
        },
        mfaSetup: (challengeName, challengeParameters) => {
            CognitoSecretCodeMap[req.headers['authorization']].associateSoftwareToken(this);
        },
        associateSecretCode: function (secretCode, otpCode) {
            console.log('### associateSecretCode - secretCode: ', secretCode);
            console.log('### associateSecretCode - otpCode: ', otpCode);

            if ( !otpCode ) {
                CognitoSecretCodeMapObj[req.headers['authorization']] = this;
                res.json({
                    level: 'associateSecretCode',
                    code: secretCode
                });
            } else {
                CognitoSecretCodeMap[req.headers['authorization']].verifySoftwareToken(otpCode, 'Logged Device', this);
            }
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
            console.log('### newPasswordRequired - userAttributes: ', userAttributes);
            console.log('### newPasswordRequired - requiredAttributes: ', requiredAttributes);
            CognitoSecretCodeMap[req.headers['authorization']].completeNewPasswordChallenge(TEMP_NEW_PASSWORD, {}, this);
        },
        onFailure: (err) => {
            console.log('# onFailure: ', err);
            res.status(403).json(err);
        },
    });
};

module.exports = (options) => {
    if (!options) {
        options = {};
    }

    const awsCognito = new AwsCognito(options);
    return awsCognito.getThis();
};
