var https = require('https');
var httpsAgent = https.globalAgent;
httpsAgent.maxSockets = 10000;

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var httpProxy = require('http-proxy');
var cors = require('cors');
var proxy = httpProxy.createProxyServer().on('error', (e) => {
    console.log(JSON.stringify(e, null, ''));
});

var corsOptionsDelegate = (req, callback) => {
    corsOptions = {
        origin: true,
        credentials: true,
        exposedHeaders: ['location']
    };
    callback(null, corsOptions);
};

// dev1 - https://guestbook-977982599.us-east-1.elb.amazonaws.com/
// dev2 - https://dcs-dev2-52180915.us-east-1.elb.amazonaws.com/
// dev3 - https://dev3.ondot-dcs.gq/
// qa1 - https://dcs-customer-qa-1-alb-994926339.us-east-1.elb.amazonaws.com/
app.use(cors(corsOptionsDelegate));
app.use((req, res) => {
    proxy.web(req, res, {
        target: 'https://dcs-dev2-52180915.us-east-1.elb.amazonaws.com/',
        changeOrigin: true,
        rejectUnauthorized: false,
        secure: false,
        requestCert: false
    });
}).listen(3000);
