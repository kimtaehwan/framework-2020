package com.ondot.mconsole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MnetBootAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MnetBootAppApplication.class, args);
    }

}
