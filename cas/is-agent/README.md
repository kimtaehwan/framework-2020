# is-agent

## Markdown
* https://gist.github.com/ihoneymon/652be052a0727ad59601

## Reference
* [URLClassLoader](https://daily-study.tistory.com/3)
* [SpringBoot 다중 커넥터, https 설정](https://engkimbs.tistory.com/756)
* SpringBoot 에서 Multi DB resource 사용
    * https://goni9071.tistory.com/275
    * https://gigas-blog.tistory.com/122
* [embedded tomcat multi port 사용](https://cnpnote.tistory.com/entry/SPRING-Spring-Boot-tomcat-%EC%BB%A4%EB%84%A5%ED%84%B0%EB%A5%BC-%EC%B6%94%EA%B0%80%ED%95%98%EC%97%AC-%EC%BB%A8%ED%8A%B8%EB%A1%A4%EB%9F%AC%EC%97%90-%EB%B0%94%EC%9D%B8%EB%94%A9%ED%95%98%EB%8A%94-%EB%B0%A9%EB%B2%95)
    * https://stackoverflow.com/questions/26111050/spring-boot-how-can-i-add-tomcat-connectors-to-bind-to-controller
* [import resource](https://jaehun2841.github.io/2018/10/21/2018-10-21-java-config-import/#import)

### application.properties
* [spring.main.allow-bean-definition-overriding=true](https://java.ihoney.pe.kr/530)
 
