package com.ondot.isagent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class AgentBootAppApplication {

    private static String JBOSS_SERVER_NAME = System.getProperty("jboss.server.name", "isagent1");

    public static void main(String[] args) {
        setSystemProperties();
        loadExternalResources();

        SpringApplication.run(AgentBootAppApplication.class, args);
    }

    private static void loadExternalResources() {
        loadJbossModulesResources();
    }

    private static void loadJbossModulesResources() {
        List<String> jbossModulesConfigDirList = Arrays.asList(
                "/opt/ondot/current/springboot/isagent1/config"
        );

        RuntimeResourceLoader.loadDir(jbossModulesConfigDirList);

        List<String> jbossModulesJarDirList = Arrays.asList(
                "/opt/ondot/current/jboss/modules/system/layers/base/org/jboss/logmanager/main"
        );
        for (String jarDir : jbossModulesJarDirList) {
            RuntimeResourceLoader.loadJarIndDir(jarDir);
        }

        List<String> jarFileList = Arrays.asList(
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/SettlementProcessorConnector.jar",
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/com.amazonaws.aws-java-sdk-core.jar",
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/com.amazonaws.aws-java-sdk-sqs.jar",
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/com.amazonaws.amazon-sqs-java-messaging-lib.jar",
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/CloudTransactionDriverConnector.jar",
                "/opt/ondot/current/jboss/modules/com/ondot/isagent/connectors/main/SQSConnectors.jar"
        );
        RuntimeResourceLoader.loadJarFiles(jarFileList);
    }

    private static void setSystemProperties() {
        System.setProperty("jboss.server.name", JBOSS_SERVER_NAME);
        System.setProperty("log4j.configurationFile", "file:/opt/ondot/current/jboss/isagent1/modules/conf/main/config/log4j2.xml");
        System.setProperty("logging.configuration", "file:/opt/ondot/current/jboss/isagent1/configuration/logging.properties");
    }

    public static class RuntimeResourceLoader {
        public static void loadDir(List<String> dirList) {

            try {
                List<File> fileList = new ArrayList<>();
                for (String dir : dirList) {
                    File dirFile = new File(dir);
                    if (dirFile.isDirectory() && dirFile.canRead()) {
                        fileList.add(dirFile);
                    }
                }

                if (fileList.isEmpty()) {
                    return;
                }

                loadFiles(fileList);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static ClassLoader getClassLoader() {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            if (classLoader == null) {
                classLoader = ClassLoader.getSystemClassLoader();
            }
            return (URLClassLoader) classLoader;
        }

        private static void loadFiles(List<File> fileList) throws NoSuchMethodException {
            final URLClassLoader loader = (URLClassLoader) getClassLoader();
            final Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);

            for (File file : fileList) {
                try {
                    method.invoke(loader, new Object[]{file.toURI().toURL()});
                    System.out.println(file.getPath() + " is loaded.");
                } catch (Exception e) {
                    System.out.println(file.getPath() + " can't load.");
                }
            }
        }

        public static void loadJarIndDir(String dir) {
            try {
                File[] jarFiles = new File(dir).listFiles(new FileFilter() {
                    public boolean accept(File jar) {
                        if (jar.toString().toLowerCase().contains(".jar")) {
                            return true;
                        }
                        return false;
                    }
                });

                if (jarFiles == null || jarFiles.length == 0) {
                    return;
                }

                loadFiles(Arrays.asList(jarFiles));

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static void loadJarFiles(List<String> jarFileList) {
            try {
                List<File> fileList = new ArrayList<>();
                for (String jarFile : jarFileList) {
                    File jar = new File(jarFile);
                    if (jar.isFile() && jar.canRead()) {
                        if (jar.toString().toLowerCase().contains(".jar")) {
                            fileList.add(jar);
                        }
                    }
                }

                if (fileList.isEmpty()) {
                    return;
                }

                loadFiles(fileList);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
