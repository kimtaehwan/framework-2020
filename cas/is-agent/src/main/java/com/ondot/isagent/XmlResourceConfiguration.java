package com.ondot.isagent;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {
        "agent-server-applicationContext.xml"
})
public class XmlResourceConfiguration {
}
