package com.ondot.isagent;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.apache.tomcat.util.descriptor.web.NamingResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.boot.web.server.Ssl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
public class EmbeddedTomcatConfiguration {

    private static String ORACLE_URL = System.getProperty("oracle.url", "jdbc:oracle:thin:@192.168.1.235:1521/XE");

    @Autowired
    private ServerProperties serverProperties;

    @Value("${server.additionalPorts:2443}")
    private List<Integer> additionalPorts;

    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        final TomcatServletWebServerFactory tomcatServletWebServerFactory = new TomcatServletWebServerFactory() {

            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                tomcat.enableNaming();
                return new TomcatWebServer(tomcat);
            }

            @Override
            protected void postProcessContext(Context context) {
                NamingResources namingResources = context.getNamingResources();
                namingResources.addResource(FiServerDB());
            }

            private ContextResource FiServerDB() {
                ContextResource resource = new ContextResource();
                resource.setName("jdbc/FiServerDB");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", ORACLE_URL);
                resource.setProperty("username", "ONDOTFIS_A");
                resource.setProperty("password", "o0n9d8o7t");
                return resource;
            }

            private ContextResource PlatformCredentialDB() {
                ContextResource resource = new ContextResource();
                resource.setName("jdbc/PlatformCredentialDB");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", ORACLE_URL);
                resource.setProperty("username", "ONDOTFIP_C");
                resource.setProperty("password", "o0n9d8o7t");
                return resource;
            }

            private ContextResource PlatformDB() {
                ContextResource resource = new ContextResource();
                resource.setName("jdbc/PlatformDB");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", ORACLE_URL);
                resource.setProperty("username", "ONDOTFIP_A");
                resource.setProperty("password", "o0n9d8o7t");
                return resource;
            }

            private ContextResource QuartzDB() {
                ContextResource resource = new ContextResource();
                resource.setName("jdbc/QuartzDB");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", ORACLE_URL);
                resource.setProperty("username", "ONDOTFIS_A");
                resource.setProperty("password", "o0n9d8o7t");
                return resource;
            }

            private ContextResource ReportingDB() {
                ContextResource resource = new ContextResource();
                resource.setName("jdbc/ReportingDB");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", ORACLE_URL);
                resource.setProperty("username", "ONDOTFIS_A");
                resource.setProperty("password", "o0n9d8o7t");
                return resource;
            }
        };

        Connector[] additionalConnectors = this.additionalConnector(this.additionalPorts);
        if (additionalConnectors != null && additionalConnectors.length > 0) {
            tomcatServletWebServerFactory.addAdditionalTomcatConnectors(additionalConnectors);
        }

        return tomcatServletWebServerFactory;
    }

    private Connector[] additionalConnector(List<Integer> additionalPorts) {
        if (additionalPorts == null && additionalPorts.isEmpty()) {
            return null;
        }

        List<Connector> result = new ArrayList<>();

        for (Integer port : additionalPorts) {
            if (port == null) {
                continue;
            }

            // Create HTTP connector
            Connector connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
            Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
            protocol.setPort(port);
            protocol.setAddress(serverProperties.getAddress());

            if (serverProperties.getSsl() != null && serverProperties.getSsl().isEnabled()) {
                // Enable SSL
                connector.setScheme("https");
                connector.setSecure(true);
                protocol.setSSLEnabled(true);

                // Keystore config
                Ssl ssl = serverProperties.getSsl();
                String keystoreFile = ssl.getKeyStore();
                String keystorePassword = ssl.getKeyStorePassword();
                String keystoreType = ssl.getKeyStoreType();
                String keyAlias = ssl.getKeyAlias();

                File keystore = Paths.get(keystoreFile).toFile();
                protocol.setKeystoreFile(keystore.getAbsolutePath());
                protocol.setKeystorePass(keystorePassword);
                protocol.setKeystoreType(keystoreType);
                protocol.setKeyAlias(keyAlias);
            }

            result.add(connector);
        }

        return result.toArray(new Connector[result.size()]);
    }
}
