## Kubernetes Cluster installation through ansible

### Prerequisites 
    Docker


#### Version specifications: 
        yum_kubernetes_package_version: 1.14.8-0

#### Variables:

    AWX:
        - hosts_k8s ( specifies which hosts to run on)
        - hosts_k8s_master ( specifies which host is master node to run playbook kubernetes_cluster_master.yml)
        - hosts_k8s_workers ( specifies which host is worker nodes to run playbook kubernetes_cluster_workers.yml)
        - master_node_ip ( IP of the master node)
    ENV:
        - yum_kubernetes_package_version
        - ports ( Required for firewalld to open for internal cluster communication)
        - cidr_v ( CIDR used for pod network cidr)
        - ansible_mount_path_k8 ( Path for where token_file will be placed in worker nodes)
        - token_file ( token file required for joining nodes)
        - flannel_iface ( Required for using multiple network adapter only )

#### Tasks explained
    
kubernetes_upgrade.yml:

    1. Ensure docker service is installed and running
    2. Create file kubernetes.repo to be used by yum
    3. Write specifications for kubernetes installation
    4. Install kubelet through yum
    5. Install kubectl through yum
    6. Install kubeadm through yum
    7. Ensure services are running
    8. Enable bridge rule for Docker

firewalld.yml:

    1. Ensure firewalld service is installed and running
    2. Allow network ports for ports given in env/environment_variables for kubernetes internal communication
    3. firewalld set IP masquerade 
    4. Restart firewalld

kubernetes_cluster_master.yml

    1. Pull images required for setting up kubernetes cluster
    2. Reset kubeadm
    3. Initalize kubernetes cluster given master_node_ip and cidr_v
    4. Ensure ansible mount path exists
    5. Store contents of generated token to be shared
    6. Copy required files for cluster
    7. Copy kube-flannel.yml from files directory
    8. Install kubernetes network add-on (flannel)

kubernetes_cluster_workers.yml

    1. Create/ensure ansible mount directory
    2. Copy token
    3. Ensure ~/.kube directory exists
    4. Ensure ~/.kube/config file exists
    5. Join the worker nodes to kubernetes cluster
    6. Output the kube config for external use