## Istio installation through ansible

### Prerequisites 
    kubectl (k8s cluster version 1.14+)
    kube-prometheus

#### Version specifications: 
        istio_version: 1.5.2

#### Variables:
    Required:
        - hosts_kubectl  ( specifies which hosts to run on ) [kubectl should be targeted cluster what you want]
        - istio_profile_name ( DCS only now )
  
    Selective:
        - ansible_mount_services_istio
        - istio_version (1.5.2 / minimum 1.4.0)
        
#### Tasks explained
    
istio_installation.yml:

    1. Copy profiles to installer path
    2. Install Istio with profile file
    3. Patch add-ons for Istio
     - Ensure Istio dashboards directory exists
     - Copy Istio dashboards json files
     - Add Grafana dashboards for Istio
     - Patch Grafana Deployment for adding dashboards
     - Copy kubelet ServiceMonitor CRD patch files
     - Fix kubelet metrics scraping endpoint bug
    4. Patch add-ons for Istio according to profile

    (DCS Profile add-ons patch)
    - Add Grafana dashboards for Spring Boot 2+
    - Patch Grafana Deployment for adding dashboards
    - Enable NodePort to Istio Ingress Gateway (If exists MetalLB)