## kube-prometheus installation through ansible

### Prerequisites 
    kubectl

#### Version specifications: 
        
| kube-prometheus stack | Kubernetes 1.14 | Kubernetes 1.15 | Kubernetes 1.16 | Kubernetes 1.17 | Kubernetes 1.18 |
|-----------------------|-----------------|-----------------|-----------------|-----------------|-----------------|
| `0.3.0`         | ✔              | ✔              | ✔              | ✔              | ✗
| `0.4.0`         | ✗              | ✗              | ✔ (v1.16.5+)   | ✔              | ✗
| `0.5.0`         | ✗              | ✗              | ✗              | ✔              | ✔


#### Variables:
    Required:
        - hosts_kubectl  ( specifies which hosts to run on ) [kubectl should be targeted cluster what you want]
        - kube_prometheus_version

        
#### Tasks explained
    
istio_installation.yml:

    1. Check k8s cluster/kube-promethus version and download archive
     - Get k8s clkuster version
     - Download kube-prometheus (selected version)
     - Check for kube-prometheus archive
     - Ensure archive exists
    2. Extract archive
    3. Install kube-prometheus  initializer
    4. Wait installing initializer completely
    5. Install kube-prometheus 
    