## MetalLB installation through ansible

### Prerequisites 
    - Docker
    - Kubernetes
    - Helm

#### Version specifications: 
    0.8.1 (Helm Chart 0.12.0)

#### Variables:
    AWX:
        - host_helm_client ( specifies which hosts installed helm client connected with target Cluster ) 
        - addresses ( Provisioning IP Addresses for LoadBalancer ) 
    ENV:
        - ansible_mount_path_metallb
        - metallb_namespace

#### Tasks explained
    
metallb_installation.yml:

    1. Ensure ansible mount directory exists
    2. Ensure Helm is installed
    3. Create namespace {{ metallb_namespace }}
    4. Copy MetalLB helm chart
    5. Configure Provisioning IP Addresses
    6. Install MetalLB