## kiali installation through ansible

### Prerequisites 
    kubectl (k8s cluster version 1.14+)
    kube-prometheus

#### Version specifications: 
| Istio     | Kiali      | Notes                                                   |
|-----------|------------|---------------------------------------------------------|
| 1\.5\.1\+ | 1\.15\.2\+ | Avoid Istio v1\.5\.0 for security reasons\.             |
| 1\.4\.7\+ | 1\.15\.2\+ | Avoid Istio v1\.4\.0 \- v1\.4\.6 for security reasons\. |
| ⇐ 1\.3    | n/a        | Out of support, users should use Istio >= 1\.4          |


#### Variables:
    Required:
        - hosts_kubectl  ( specifies which hosts to run on ) [kubectl should be targeted cluster what you want]
        - credentials_username ( Login ID )
        - credentials_passphrase ( Login Password )
  
    Selective:
        - kiali_version (Default 1.18.0)
        
#### Tasks explained
    
kiali_installation.yml:

    1. Check Istio/Kiali Version
     - Get istio version
     - Check version specifications
    2. Download kiali installation script
    3. Download & Install Kiali
