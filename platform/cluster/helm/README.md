## Helm installation through ansible

### Prerequisites 
    - Docker
    - Kubernetes

#### Version specifications: 
    v3.0.2-linux-amd64

#### Variables:
    AWX:
        - hosts_helm ( specifies which hosts to run on ) [should be master]
    ENV:
        - ansible_mount_path_helm
        - helm_version
        - helm_os
        - helm_architecture

#### Tasks explained
    
helm_installation.yml:

    1. Ensure mount path exists
    2. Download helm tar ball
    3. Unzip helm tar ball
    4. Copy and paste helm binary for use