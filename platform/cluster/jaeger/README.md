## Jaeger installation through ansible

### Prerequisites 
    kubectl
    helm (v3.0.2+)
    kube-prometheus

#### Version specifications: 
        App Version: 1.17.1
        jaeger_chart_version: 0.28.0
        jaeger_operator_chart_version: 2.14.2

#### Variables:
    Required:
        - hosts_kubectl  ( specifies which hosts to run on ) [kubectl should be targeted cluster what you want]

    Selective:
        - jaeger_chart_version
        - jaeger_operator_chart_version
        
#### Tasks explained
    
jaeger-installation.yml:

    1. Ensure kube-prometheus installed
    2. Add jaegertracing repo to helm client
    3. Create observability namespace
    4. Install Jaeger chart
    5. Install Jarger Operator chart