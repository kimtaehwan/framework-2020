## Docker installation through ansible

#### Version specifications: 
        
        yum_docker_package_version: 19.03.5
        yum_docker_package_release: 3.el7
        yum_containerd_io_package_version: 1.2.10-3.2.el7

#### Variables:

	AWX:
        - hosts_docker ( specifies which hosts to run on)

    ENV:
        - yum_docker_package_version ( docker package version)
        - yum_docker_package_release ( docker package release version)
        - yum_containerd_io_package_version ( containerio package version)
        - packages ( install required packages:
                              - firewalld
                              - yum-utils
                              - device-mapper-persistent-data
                              - lvm2)
        - services ( ensure services are running:
                              - docker
                              - firewalld)


#### Tasks explained
    
    1. Create file docker-ce.repo to be used by yum
    2. Write specifications for docker-ce installation
    3. Install docker-ce through yum
    4. Install containerd.io through yum
    5. Install required packages through yum
    6. Ensure services are running
    7. Enable bridge rule for Docker