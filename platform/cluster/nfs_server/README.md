## NFS-Server installation through ansible

#### Variables:
    AWX:
        - hosts_master  ( specifies which hosts to run on ) [should be master]
        - nfs_mount_path ( specfies which path to mount on host)
        - mount_only ( set to true if nfs/nfs_services is already installed, false if already run before and want to mount another path)
        - install_only ( set to true if you want to only install nfs and allow firewall for nfs services, will not create a mount )
    ENV:
        - firewalld_services_nfs
        - nfs_services
        
#### Tasks explained
    
nfs_server_installation.yml:

    1. Install nfs-utils using yum
    2. Add nfs_services to firewalld
    3. Restart firewalld
    4. Start and enable services that NFS requires

nfs_server_addition.yml:

    1. Create directory for nfs
    2. Delete existing line in /etc/exports
    3. (Re)Add directory to exports 
    4. Restart nfs-server