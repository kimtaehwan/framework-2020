## ReferenceApp for keyoperator K8s cluster connection

#### Requirements:
    1. K8s cluster
    2. namespaces: reference
    
##### 0. Ensure database and data exists in platform database 
###### Values in key pair values are encrypted using the platform key arn except encryption_mode and platform_key_arn if KEY_MODE is envelope

![Encrypting key-pair values with KMS onto secrets](./img/KeyOp-init-Step1.png)

##### 1. Create kubernetes secrets
Before deploying the keyoperator, you must deploy secrets in directory ./requiredSecrets
    
    $ kubectl create -f ./requiredSecrets/ -n reference
    

##### 2. Deploy helm charts

    $ make helm-install
    
The helm charts is set up so that it mounts the secret reference-keyop-init-db to init container for it to access platform database and use KEY_OP_ID as environment, defined in values.yaml

![Access platform database](./img/KeyOp-init-Step2.png)

##### 3. Init container then mounts the values from platform database

![Mounting secrets](./img/KeyOp-init-Step3.png)


##### 4. From Java application, read the mounted secrets and decrypt the following encrypted values to use:

- application key arn
- db_url
- db_username
- db_password

This example is shown in 
- /src/main/java/com/ondot/referenceapp/keyoperator/config/DataSourceConfiguration
- /src/main/java/com/ondot/referenceapp/keyoperator/config/KeyOperatorConfigurations

![Using the mounts](./img/KeyOp-init-Step4.png)


##### 5. Encrypt/Decrypt sensitive data

as shown in /src/main/java/com/ondot/referenceapp/keyoperator/utils/ObjectConverter when saving and getting from database


##### Example of encrypted and decrypted subscriber model: 
    2020-02-01 00:01:18.106  INFO [-,d838346e9b40e4dc,d838346e9b40e4dc,false] 1 --- [ XNIO-2 task-11] c.o.r.k.resource.SubscriberController    : ===================Get Begin=======================
    2020-02-01 00:01:18.106  INFO [-,d838346e9b40e4dc,d838346e9b40e4dc,false] 1 --- [ XNIO-2 task-11] c.o.r.k.resource.SubscriberController    : Encrypted Model :: EncryptedModel(1) :
    {
    "firstName":"Jason",
    "lastName":"Harris",
    "encryptedCardNumber":AYADeCUbKvD9CtLwnAtyA8jYbiYAXwABABVhd3MtY3J5cHRvLXB1YmxpYy1rZXkAREE4UVBuNGwvcW8zUklXd1FxTEJwVVlIUEY2UGlBVmw2WUw3M1lEU0trMXc2eDl2a0pRR1llS1RZZ0dNV0pzVDkwdz09AAEAB2F3cy1rbXMAS2Fybjphd3M6a21zOnVzLWVhc3QtMTo4ODI1Mzg3MjMyMjM6a2V5LzZhMDdkZTVkLTc5NWMtNDRkOS04YWJjLTgyZTM4MTk4MjRjNQC4AQIBAHgGfm5FjJPXWINTPze1smC5WQsDkoVpqmG+32YUrGPkWQG/nGxHvyIhNl1qwlMMC6d6AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMexNBJgwZQiE1+YpFAgEQgDtcUFkN0vmBO4026lPoS4cOoWIs0MKY3ROozSeP0GIQRv5QlL4ntaZiBbrGt70ns/EE1/XbK7rz/DgkJAIAAAAADAAAEAAAAAAAAAAAAAAAAADNRYAw1zcn7oBS8KZwMbc2/////wAAAAEAAAAAAAAAAAAAAAEAAAATHKl+Ly3XPXmNcdYizICIt+Wh8ouywlnFid5RCUaOwQcgOjoAZzBlAjBFepRrbwOxrB5sqxhEIRfHa0daY33xSusGN2+7HoKiOHdsD2XDBkJi8gZKbDSyZcwCMQCotGiLwWWc57p1sPCRwRA57BQ6z79Mb0xaR7B/dyCp2LXj1CNN4xRhAarf/WoKZzU=,
    "encryptedSsn":AYADePj8WDpxLOcDsQhBcQj7kVAAXwABABVhd3MtY3J5cHRvLXB1YmxpYy1rZXkAREE4UVBuNGwvcW8zUklXd1FxTEJwVVlIUEY2UGlBVmw2WUw3M1lEU0trMXc2eDl2a0pRR1llS1RZZ0dNV0pzVDkwdz09AAEAB2F3cy1rbXMAS2Fybjphd3M6a21zOnVzLWVhc3QtMTo4ODI1Mzg3MjMyMjM6a2V5LzZhMDdkZTVkLTc5NWMtNDRkOS04YWJjLTgyZTM4MTk4MjRjNQC4AQIBAHgGfm5FjJPXWINTPze1smC5WQsDkoVpqmG+32YUrGPkWQG/nGxHvyIhNl1qwlMMC6d6AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMexNBJgwZQiE1+YpFAgEQgDtcUFkN0vmBO4026lPoS4cOoWIs0MKY3ROozSeP0GIQRv5QlL4ntaZiBbrGt70ns/EE1/XbK7rz/DgkJAIAAAAADAAAEAAAAAAAAAAAAAAAAABq9sbEB71wCMSy38cCsYZc/////wAAAAEAAAAAAAAAAAAAAAEAAAAJSiJdrYc6i/PAx/mCw1fwJ3MTyaqlGnsY5wBnMGUCMAbbCJ5dueVoqHM/YkxrvIOKsPcMSIkM7N4IithdLalZfPaULkm4t5NP/qDB/qedCwIxAPNZ/8RJ9JhZ4TLlBPQOTvu5fLUgh2Jz1zx9uFBdvmu9QdHEkGxITXbkLoluwnGzoA==,
    }
    2020-02-01 00:01:18.106  INFO [-,d838346e9b40e4dc,d838346e9b40e4dc,false] 1 --- [ XNIO-2 task-11] c.o.r.k.resource.SubscriberController    : Plaintext model :: PlaintextModel(1) :
    {
    "firstName":"Jason",
    "lastName":"Harris",
    "plaintextCardNumber":8878-9828-8364-0566,
    "plaintextSsn":736002457,
    }
    2020-02-01 00:01:18.107  INFO [-,d838346e9b40e4dc,d838346e9b40e4dc,false] 1 --- [ XNIO-2 task-11] c.o.r.k.resource.SubscriberController    : ===================Get End=======================



