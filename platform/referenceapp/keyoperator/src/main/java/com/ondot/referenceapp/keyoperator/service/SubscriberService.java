package com.ondot.referenceapp.keyoperator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ondot.dcs.security.crypto.Crypto;
import com.ondot.referenceapp.keyoperator.config.EncryptionConfiguration;
import com.ondot.referenceapp.keyoperator.entity.SubscriberEntity;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelEncrypted;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelPlaintext;
import com.ondot.referenceapp.keyoperator.repository.SubscriberRepository;
import com.ondot.referenceapp.keyoperator.utils.ObjectConverter;
import com.ondot.referenceapp.keyoperator.utils.RandomSubscriberGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.swing.text.html.Option;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class SubscriberService {

    private static final String ENVELOPE = "envelope";

    private Crypto crypto;

    private ObjectMapper objectMapper;

    @Autowired
    EncryptionConfiguration encryptionConfiguration;

    @Autowired
    SubscriberRepository subscriberRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    @PostConstruct
    public void doPostConstruct() {
        log.info("doPostConstruct called");
        crypto = Crypto.getInstance();
        initializeObjectMapper(this.mappingJackson2HttpMessageConverter.getObjectMapper());
        log.info("encryption mode = "+encryptionConfiguration.getEncryptionMode()+", encryptionConfiguration.applicationKeyId = " + encryptionConfiguration.getApplicationKeyId());
        if(ENVELOPE.equalsIgnoreCase(encryptionConfiguration.getEncryptionMode()))
            crypto.init(encryptionConfiguration.getApplicationKeyId(), new HashMap<>(), true);
    }

    public void initializeObjectMapper(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    public void saveOrUpdateSubscriber(SubscriberModelPlaintext model)throws UnknownHostException {
        Optional<SubscriberEntity> optionalSubscriberEntity = subscriberRepository.findById(model.getReferenceId());
        SubscriberModelEncrypted encryptedModel;
        if("envelope".equalsIgnoreCase(encryptionConfiguration.getEncryptionMode())){
            encryptedModel = ObjectConverter.encrypt(crypto,model);
        }else{
            encryptedModel = ObjectConverter.dontEncrypt(model);
        }
        if(optionalSubscriberEntity.isPresent()){
            SubscriberEntity entity = optionalSubscriberEntity.get();
            entity = ObjectConverter.updateEntity(entity,encryptedModel);
            subscriberRepository.save(entity);
        }else{
            SubscriberEntity entity = ObjectConverter.modelToEntity(encryptedModel);
            subscriberRepository.save(entity);
        }
    }

    public Map<String,Optional> getSubscriber(String referenceId)throws UnknownHostException{
        Optional<SubscriberEntity> optionalSubscriberEntity = subscriberRepository.findById(referenceId);
        Map<String,Optional> map = new HashMap<>();
        final String keyEnc = "encryptedModel";
        final String keyPlaintext = "plaintextModel";
        if(optionalSubscriberEntity.isPresent()){
            log.info("Found subscriber :: referenceId = " + referenceId );
            SubscriberEntity entity = optionalSubscriberEntity.get();
            SubscriberModelEncrypted encryptedModel = ObjectConverter.entityToModel(entity);
            if("envelope".equalsIgnoreCase(encryptionConfiguration.getEncryptionMode())) {
                SubscriberModelPlaintext plaintextModel = ObjectConverter.decrypt(crypto, encryptedModel);
                map.put(keyPlaintext,Optional.of(plaintextModel));
            }else{
                map.put(keyPlaintext,Optional.empty());
            }
            map.put(keyEnc,Optional.of(encryptedModel));
        }else{
            log.error("Couldn't find subscriber :: referenceId = " + referenceId);
            map.put(keyEnc,Optional.empty());
            map.put(keyPlaintext,Optional.empty());
        }
        return map;
    }

    public void deleteAll(){
        subscriberRepository.deleteAll();
    }
}
