package com.ondot.referenceapp.keyoperator.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class OndotConfigurations {
    @Value("#{environment.SLEEP_TIME ?: '3'}")
    Long sleepTime;
}
