package com.ondot.referenceapp.keyoperator.config;


import com.ondot.dcs.security.crypto.Crypto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

@Slf4j
public class KeyOperatorConfigurations {

    private final static String basePath = "/opt/ondot/etc";

    @Bean
    @Profile("!local")
    public static Properties k8sProperties() throws IOException {
        log.info("==========================================");
        log.info("Updating Properties with mounted secrets");
        log.info("==========================================");

        String encryptionMode = Files.readAllLines(Paths.get(basePath+"/enc/encryption_mode").toAbsolutePath()).get(0);

        if("envelope".equalsIgnoreCase(encryptionMode)) {
            String platformKeyArn = Files.readAllLines(Paths.get(basePath+"/enc/platform_key_arn").toAbsolutePath()).get(0);
            Crypto crypto = Crypto.getInstance();

            crypto.init(platformKeyArn, null, true);

            //Get the encrypted Application Key id from mount
            String encryptedApplicationKeyARN = Files.readAllLines(Paths.get(basePath+"/enc/application_key_arn").toAbsolutePath()).get(0);
            String decryptedApplicationKeyARN = crypto.cipher(encryptedApplicationKeyARN, Crypto.DECRYPT_MODE);


            Properties properties =  new Properties();
            properties.put("ondot.encryption.mode","envelope");
            properties.put("ondot.application.key.id", decryptedApplicationKeyARN);
            return properties;
        }else {
            Properties properties = new Properties();
            properties.put("ondot.encryption.mode", "none");
            return properties;
        }
    }
}
