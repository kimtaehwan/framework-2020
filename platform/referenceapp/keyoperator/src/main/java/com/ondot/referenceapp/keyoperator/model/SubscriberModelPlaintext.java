package com.ondot.referenceapp.keyoperator.model;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
public class SubscriberModelPlaintext {

    String referenceId;

    String firstName;

    String lastName;

    String cardNo;

    String ssn;

    @Override
    public String toString(){
        return "PlaintextModel("+referenceId+") : \n" +
                "{\n" +
                "\"firstName\":\""+firstName + "\", \n"+
                "\"lastName\":\""+lastName+ "\", \n"+
                "\"plaintextCardNumber\":"+cardNo + ", \n"+
                "\"plaintextSsn\":"+ssn+ ", \n"+
                "}";
    }

}
