package com.ondot.referenceapp.keyoperator.config;

import com.ondot.dcs.security.crypto.Crypto;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class DataSourceConfiguration {

    final DataSourceProperties dataSourceProperties;

    private final String basePath = "/opt/ondot/etc";

    @Bean
    @Profile("local")
    public DataSource dataSource(Environment environment) {
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url(dataSourceProperties.getUrl())
                .username(dataSourceProperties.getUsername())
                .password(dataSourceProperties.getPassword())
                .build();
    }

    @Bean
    @Profile("!local")
    public DataSource k8sDataSource(Environment environment) throws IOException {
        String encryptionModePath = basePath + "/enc/encryption_mode";
        String dbUrlPath = basePath + "/db/url";
        String dbUsernamePath = basePath + "/db/username";
        String dbPasswordPath = basePath + "/db/password";
        String platformKeyARNPath = basePath + "/enc/platform_key_arn";
        //Get the encryption mode from mount
        String encryptionMode = Files.readAllLines(Paths.get(encryptionModePath).toAbsolutePath()).get(0);

        String springDatasourceUrl = Files.readAllLines(Paths.get(dbUrlPath).toAbsolutePath()).get(0);
        String springDatasourceUsername = Files.readAllLines(Paths.get(dbUsernamePath).toAbsolutePath()).get(0);
        String springDatasourcePassword  = Files.readAllLines(Paths.get(dbPasswordPath).toAbsolutePath()).get(0);

        if("envelope".equalsIgnoreCase(encryptionMode)) {
            String platformKeyArn = Files.readAllLines(Paths.get(platformKeyARNPath).toAbsolutePath()).get(0);
            Crypto crypto = Crypto.getInstance();

            HashMap<String,String> mp = new HashMap<>();
            mp.put("KeyString", "ValueString");
            crypto.init(platformKeyArn,mp,true);

            springDatasourceUrl = crypto.cipher(springDatasourceUrl,Crypto.DECRYPT_MODE);
            springDatasourceUsername = crypto.cipher(springDatasourceUsername,Crypto.DECRYPT_MODE);
            springDatasourcePassword = crypto.cipher(springDatasourcePassword,Crypto.DECRYPT_MODE);
        }
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .url(springDatasourceUrl)
                .username(springDatasourceUsername)
                .password(springDatasourcePassword)
                .build();
    }
}