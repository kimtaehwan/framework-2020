package com.ondot.referenceapp.keyoperator.utils;

import com.ondot.dcs.security.crypto.Crypto;
import com.ondot.referenceapp.keyoperator.entity.SubscriberEntity;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelEncrypted;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelPlaintext;

import java.net.UnknownHostException;

public class ObjectConverter {

    public static SubscriberEntity modelToEntity(SubscriberModelEncrypted model){
        return SubscriberEntity.builder()
                .referenceId(model.getReferenceId())
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .cardNo(model.getCardNo())
                .ssn(model.getSsn())
                .build();
    }

    public static SubscriberEntity updateEntity(SubscriberEntity entity, SubscriberModelEncrypted model){
        entity.setFirstName(model.getFirstName());
        entity.setLastName(model.getLastName());
        entity.setCardNo(model.getCardNo());
        entity.setSsn(model.getSsn());
        return entity;
    }

    public static SubscriberModelEncrypted entityToModel(SubscriberEntity entity){
        return SubscriberModelEncrypted.builder()
                .referenceId(entity.getReferenceId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .cardNo(entity.getCardNo())
                .ssn(entity.getSsn())
                .build();
    }
    public static SubscriberModelEncrypted dontEncrypt(SubscriberModelPlaintext plaintext){
        return SubscriberModelEncrypted.builder()
                .referenceId(plaintext.getReferenceId())
                .firstName(plaintext.getFirstName())
                .lastName(plaintext.getLastName())
                .cardNo(plaintext.getCardNo())
                .ssn(plaintext.getSsn())
                .build();
    }

    public static SubscriberModelEncrypted encrypt(Crypto crypto, SubscriberModelPlaintext plaintext) throws UnknownHostException {
        return SubscriberModelEncrypted.builder()
                .referenceId(plaintext.getReferenceId())
                .firstName(plaintext.getFirstName())
                .lastName(plaintext.getLastName())
                .cardNo(crypto.cipher(plaintext.getCardNo(),Crypto.ENCRYPT_MODE))
                .ssn(crypto.cipher(plaintext.getSsn(),Crypto.ENCRYPT_MODE))
                .build();
    }

    public static SubscriberModelPlaintext decrypt(Crypto crypto, SubscriberModelEncrypted encrypted) throws UnknownHostException{
        return SubscriberModelPlaintext.builder()
                .referenceId(encrypted.getReferenceId())
                .firstName(encrypted.getFirstName())
                .lastName(encrypted.getLastName())
                .cardNo(crypto.cipher(encrypted.getCardNo(),Crypto.DECRYPT_MODE))
                .ssn(crypto.cipher(encrypted.getSsn(),Crypto.DECRYPT_MODE))
                .build();
    }

}
