package com.ondot.referenceapp.keyoperator.repository;

import com.ondot.referenceapp.keyoperator.entity.SubscriberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberRepository extends JpaRepository<SubscriberEntity,String> {
}
