package com.ondot.referenceapp.keyoperator.resource;


import com.ondot.referenceapp.keyoperator.config.OndotConfigurations;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelEncrypted;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelPlaintext;
import com.ondot.referenceapp.keyoperator.service.SubscriberService;
import com.ondot.referenceapp.keyoperator.utils.RandomSubscriberGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping(value = "/v1")
public class SubscriberController {

    @Autowired
    OndotConfigurations ondotConfigurations;

    @Autowired
    SubscriberService subscriberService;

    private final String keyEnc = "encryptedModel";
    private final String keyPlaintext = "plaintextModel";


    private final String delimitter(String s){
        return "===================" + s + "=======================";
    }

    @GetMapping(value = "/sub/{id}")
    public void get(@PathVariable(value = "id")String referenceId)throws UnknownHostException{
        Map<String, Optional> mp = subscriberService.getSubscriber(referenceId);
        Optional<SubscriberModelEncrypted> encryptedOptional = mp.get(keyEnc);
        Optional<SubscriberModelPlaintext> plaintextOptional = mp.get(keyPlaintext);

        log.info(delimitter("Get Begin"));
        if(encryptedOptional.isPresent()){
            log.info("Encrypted Model :: " + encryptedOptional.get());
        }else{
            log.error("Encrypted model is not available");
        }
        if(plaintextOptional.isPresent()){
            log.info("Plaintext model :: " +plaintextOptional.get());
        }else{
            log.error("Plaintext model is not available");
        }
        log.info(delimitter("Get End"));
    }

    @GetMapping(value = "/rand/{id}")
    public void saveOrUpdate(@PathVariable(value = "id")String referenceId) throws UnknownHostException{
        SubscriberModelPlaintext plaintext = RandomSubscriberGenerator.randomSubscriber(referenceId);
        subscriberService.saveOrUpdateSubscriber(plaintext);
    }

    @GetMapping(value = "/times/{times}")
    public void saveAndGetTimes(@PathVariable(value = "times")String time) throws InterruptedException {
        Long sleepTime = ondotConfigurations.getSleepTime();
        int errCount = 0;
        try{
            int times = Integer.parseInt(time);
            for(int i = 0 ; i <= times ; i++ ){
                String iAsString = i+"";
                try {
                    saveOrUpdate(iAsString);
                    get(iAsString);
                }catch (UnknownHostException e){
                    errCount++;
                    log.error(delimitter("Error Begin"));
                    log.error("Got and UnknownHostException errCount @ "+errCount);
                    log.error(delimitter("Error End"));
                }finally {
                    TimeUnit.SECONDS.sleep(sleepTime);
                }
            }
            log.info(delimitter("Save and get times done start"));
            log.info("Got errCount = "+ errCount);
            log.info(delimitter("Save and get times done end"));

        }catch (NumberFormatException e){
            log.error("Times is not a number..");
        }
    }

    @GetMapping(value = "/deleteall")
    public void deleteDb(){
        subscriberService.deleteAll();
    }
}
