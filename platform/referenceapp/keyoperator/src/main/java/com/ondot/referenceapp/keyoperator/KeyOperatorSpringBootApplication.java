package com.ondot.referenceapp.keyoperator;

import com.ondot.referenceapp.keyoperator.config.KeyOperatorConfigurations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;
import java.util.Properties;

@Slf4j
@SpringBootApplication(scanBasePackages = "com.ondot")
@ComponentScan(basePackages = "com.ondot")
@EnableJpaRepositories
@ConfigurationProperties
public class KeyOperatorSpringBootApplication {
    public static void main(String[] args) {
        try {
            Properties prop = KeyOperatorConfigurations.k8sProperties();
            new SpringApplicationBuilder()
                    .sources(KeyOperatorSpringBootApplication.class)
                    .listeners(new ExternalPropertiesListener(prop))
                    .run(args);
        } catch (IOException e) {
            log.info("====================================================================================");
            log.info("Failed to update Properties with mounted secrets " + e);
            log.info("====================================================================================");
            SpringApplication.run(KeyOperatorSpringBootApplication.class, args);
        }
    }

    public static class ExternalPropertiesListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {
        Properties props;

        ExternalPropertiesListener(Properties props) {
            this.props = props;
        }

        @Override
        public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
            if (this.props == null || this.props.isEmpty()) {
                return;
            }
            ConfigurableEnvironment environment = event.getEnvironment();
            environment.getPropertySources().addFirst(new PropertiesPropertySource("externalProps", props));
        }
    }
}
