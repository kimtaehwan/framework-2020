package com.ondot.referenceapp.keyoperator.utils;

import com.ondot.referenceapp.keyoperator.model.SubscriberModelEncrypted;
import com.ondot.referenceapp.keyoperator.model.SubscriberModelPlaintext;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class RandomSubscriberGenerator {
    private static final Random rand = new Random();

    public static SubscriberModelPlaintext randomSubscriber(String referenceId){
        return SubscriberModelPlaintext.builder()
                .referenceId(referenceId)
                .firstName(randomFirstName())
                .lastName(randomLastName())
                .cardNo(randomCardNumber())
                .ssn(randomSsn())
                .build();
    }





    public static String randomFirstName(){
        return listOfFirstNames.get(rand.nextInt(listOfFirstNames.size()));
    }

    public static String randomLastName(){
        return listOfLastNames.get(rand.nextInt(listOfLastNames.size()));
    }

    private static int generateRandomDigits(int n) {
        int m = (int) Math.pow(10, n - 1);
        return m + rand.nextInt(9 * m);
    }

    public static String randomCardNumber(){
        return String.format((Locale)null,
                "%04d-%04d-%04d-%04d",
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000));
    }

    public static String randomSsn(){
        int ssn = generateRandomDigits(9);
        return Integer.toString(ssn);
    }

    private static List<String> listOfFirstNames = new ArrayList<String>(){{
        add("Bob");
        add("Liam");
        add("Emma");
        add("Noah");
        add("Olivia");
        add("William");
        add("Ava");
        add("James");
        add("Isabella");
        add("Oliver");
        add("Sophia");
        add("Benjamin");
        add("Charlotte");
        add("Elijah");
        add("Mia");
        add("Lucas");
        add("Amelia");
        add("Mason");
        add("Harper");
        add("Logan");
        add("Evelyn");
        add("Robin");
        add("Kevin");
        add("Joe");
        add("Jason");
        add("Peter");
        add("Justin");
        add("Patrick");
        add("Joseph");
        add("Otis");
        add("Mandy");
        add("Keiv");
        add("Lannister");
        add("Tywin");
    }};

    private static List<String> listOfLastNames = new ArrayList<String>(){{
        add("Smith");
        add("Johnson");
        add("Williams");
        add("Jones");
        add("Brown");
        add("Davis");
        add("Miller");
        add("Wilson");
        add("Moore");
        add("Taylor");
        add("Anderson");
        add("Thomas");
        add("Jackson");
        add("White");
        add("Harris");
        add("Martin");
        add("Thompson");
        add("Garcia");
        add("Martinez");
        add("Robinson");
        add("Clark");
        add("Rodriguez");
        add("Lewis");
        add("Lee");
        add("Walker");
    }};

}
