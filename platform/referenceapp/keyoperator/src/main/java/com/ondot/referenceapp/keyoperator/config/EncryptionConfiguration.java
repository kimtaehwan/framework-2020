package com.ondot.referenceapp.keyoperator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EncryptionConfiguration {

    @Value("${ondot.application.key.id}")
    private String applicationKeyId;

    public String getApplicationKeyId(){
        return applicationKeyId;
    }

    @Value("${ondot.encryption.mode}")
    private String encryptionMode;

    public String getEncryptionMode(){
        return encryptionMode;
    }
}
