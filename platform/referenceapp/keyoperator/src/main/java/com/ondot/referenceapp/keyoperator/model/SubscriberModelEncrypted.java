package com.ondot.referenceapp.keyoperator.model;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriberModelEncrypted {
    String referenceId;

    String firstName;

    String lastName;

    String cardNo;

    String ssn;

    @Override
    public String toString(){
        return "EncryptedModel("+referenceId+") : \n" +
                "{\n" +
                "\"firstName\":\""+firstName + "\", \n"+
                "\"lastName\":\""+lastName+ "\", \n"+
                "\"encryptedCardNumber\":"+cardNo + ", \n"+
                "\"encryptedSsn\":"+ssn+ ", \n"+
                "}";
    }
}
