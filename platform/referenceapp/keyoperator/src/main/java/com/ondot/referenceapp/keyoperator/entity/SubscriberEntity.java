package com.ondot.referenceapp.keyoperator.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "KEY_OP_SUBSCRIBER")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class SubscriberEntity {

    @Id
    @Column(name = "REFERENCE_ID",nullable = false)
    String referenceId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    @Column(name = "CARD_NUMBER")
    String cardNo;

    @Column(name = "SSN")
    String ssn;
}
