package com.ondot.referenceapp.keyoperator.initcontainer.file;

import com.ondot.referenceapp.keyoperator.initcontainer.common.CommonFileNames;
import com.ondot.referenceapp.keyoperator.initcontainer.model.KeyOp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Writer {

    private static void writeToFile(String fileName, String data) throws IOException {
        FileWriter myWriter = new FileWriter(fileName);
        myWriter.write(data);
        myWriter.close();
    }

    public static void writeKeyOpToFiles(KeyOp keyOp) throws IOException{
        String pathOut = "out/";
        File outDir = new File(pathOut);
        outDir.mkdir();

        String pathDb ="db/";
        File dbDir = new File(pathOut+pathDb);
        dbDir.mkdir();

        writeToFile(pathOut+pathDb+ CommonFileNames.databaseUrl,keyOp.getDatabaseUrl());
        writeToFile(pathOut+pathDb+ CommonFileNames.databaseUsername,keyOp.getDatabaseUsername());
        writeToFile(pathOut+pathDb+ CommonFileNames.databasePassword,keyOp.getDatabasePassword());

        String pathEnc = "enc/";
        File encDir = new File(pathOut+pathEnc);
        encDir.mkdir();

        writeToFile(pathOut+pathEnc+ CommonFileNames.encryptionMode,keyOp.getMode());
        writeToFile(pathOut+pathEnc+ CommonFileNames.applicationKeyArn,keyOp.getApplicationKeyArn());
        writeToFile(pathOut+pathEnc+ CommonFileNames.platformKeyArn,keyOp.getPlatformKeyArn());

    }
}
