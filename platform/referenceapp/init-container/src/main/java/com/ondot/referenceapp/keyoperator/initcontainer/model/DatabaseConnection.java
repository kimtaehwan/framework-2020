package com.ondot.referenceapp.keyoperator.initcontainer.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DatabaseConnection {

    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;


}
