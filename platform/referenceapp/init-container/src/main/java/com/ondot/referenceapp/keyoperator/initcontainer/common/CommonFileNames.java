package com.ondot.referenceapp.keyoperator.initcontainer.common;

public class CommonFileNames {
    public static final String databaseUrl = "url";
    public static final String databaseUsername = "username";
    public static final String databasePassword = "password";
    public static final String encryptionMode = "encryption_mode";
    public static final String applicationKeyArn = "application_key_arn";
    public static final String platformKeyArn = "platform_key_arn";
}
