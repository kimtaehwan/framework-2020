package com.ondot.referenceapp.keyoperator.initcontainer.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KeyOp {
    private Integer keyOpId;
    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;
    private String applicationKeyArn;
    private String platformKeyArn;

    private String mode;

    public boolean isEmpty(){
        if(null == keyOpId || databaseUrl.isEmpty())
            return true;
        else
            return false;
    }
}
