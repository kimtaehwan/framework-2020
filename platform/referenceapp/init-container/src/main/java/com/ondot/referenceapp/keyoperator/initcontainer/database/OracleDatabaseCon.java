package com.ondot.referenceapp.keyoperator.initcontainer.database;

import com.ondot.referenceapp.keyoperator.initcontainer.model.KeyOp;

import java.sql.*;
import java.util.Optional;
import static com.ondot.referenceapp.keyoperator.initcontainer.common.CommonStrings.*;


public class OracleDatabaseCon {
    public static Optional<KeyOp> getKeyOp(String databaseUrl, String username, String password, String id) throws ClassNotFoundException, SQLException {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection(databaseUrl,username,password);
            Statement stmt = con.createStatement();

            ResultSet rs = stmt.executeQuery("select * from " + tableName + " where "+ columnLabelKeyOpId + "=" + id);

            KeyOp keyOp = new KeyOp();

            while(rs.next()) {
                keyOp.setKeyOpId(rs.getInt(columnLabelKeyOpId));
                keyOp.setDatabaseUrl(rs.getString(columnLabelDatabaseUrl));
                keyOp.setDatabaseUsername(rs.getString(columnLabelDatabaseUsername));
                keyOp.setDatabasePassword(rs.getString(columnLabelDatabasePassword));
                keyOp.setApplicationKeyArn(rs.getString(columnLabelApplicationKeyArn));
                keyOp.setMode(rs.getString(columnLabelMode));
                keyOp.setPlatformKeyArn(rs.getString(columnLabelPlatformKeyArn));
            }

            if(keyOp.isEmpty()){
                return Optional.empty();
            }
            con.close();
            return Optional.of(keyOp);
    }
}
