package com.ondot.referenceapp.keyoperator.initcontainer.common;

public class CommonStrings {

    public static final String keyOpIdEnvKey = "KEY_OP_ID";


    public static final String tableName = "KEY_OP";
    public static final String columnLabelKeyOpId = "KEY_OP_ID";
    public static final String columnLabelDatabaseUrl = "DB_URL";
    public static final String columnLabelDatabaseUsername = "DB_USERNAME";
    public static final String columnLabelDatabasePassword = "DB_PASSWORD";
    public static final String columnLabelApplicationKeyArn = "APP_KEY_ARN";
    public static final String columnLabelPlatformKeyArn = "PLAT_KEY_ARN";
    public static final String columnLabelMode = "KEY_MODE";









}
