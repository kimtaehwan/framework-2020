package com.ondot.referenceapp.keyoperator.initcontainer;

import com.ondot.referenceapp.keyoperator.initcontainer.database.OracleDatabaseCon;
import com.ondot.referenceapp.keyoperator.initcontainer.file.Reader;
import com.ondot.referenceapp.keyoperator.initcontainer.file.Writer;
import com.ondot.referenceapp.keyoperator.initcontainer.model.DatabaseConnection;
import com.ondot.referenceapp.keyoperator.initcontainer.model.KeyOp;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class Runner {


    public static void main(String[] args) {
        try {
            DatabaseConnection connectionDetails = Reader.readConnectionDetails();

            String keyOpId = Reader.getIdFromEnvironmentVar();

            System.out.println("Getting credentials for keyOpId :: " + keyOpId);

            Optional<KeyOp> optionalKeyOp = OracleDatabaseCon.getKeyOp(connectionDetails.getDatabaseUrl(), connectionDetails.getDatabaseUsername(), connectionDetails.getDatabasePassword(),keyOpId);
            if(optionalKeyOp.isPresent()){
                Writer.writeKeyOpToFiles(optionalKeyOp.get());
                System.out.println("Successfully mounted credentials.");
            }else{
                throw new NullPointerException("KeyOp not found in database");
            }
        }catch (Exception ex){
            System.err.println(ex);
            System.exit(-1);
        }
    }
}
