package com.ondot.referenceapp.keyoperator.initcontainer.file;

import com.ondot.referenceapp.keyoperator.initcontainer.common.CommonStrings;
import com.ondot.referenceapp.keyoperator.initcontainer.model.DatabaseConnection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Reader {

    public static String getIdFromEnvironmentVar() throws NullPointerException{

        String keyOpId = System.getenv(CommonStrings.keyOpIdEnvKey);
        if(null == keyOpId)
            throw new NullPointerException("Unable to ENV value for :: " + CommonStrings.keyOpIdEnvKey);
        return keyOpId;
    }


    public static DatabaseConnection readConnectionDetails() throws IOException {
        String inPath = "in/";
        //Get the encryption mode from mount
        String dbUrlPath = inPath + "/db/url";
        String dbUsernamePath = inPath + "/db/username";
        String dbPasswordPath = inPath + "/db/password";

        String springDatasourceUrl = Files.readAllLines(Paths.get(dbUrlPath).toAbsolutePath()).get(0);
        String springDatasourceUsername = Files.readAllLines(Paths.get(dbUsernamePath).toAbsolutePath()).get(0);
        String springDatasourcePassword = Files.readAllLines(Paths.get(dbPasswordPath).toAbsolutePath()).get(0);

        return DatabaseConnection.builder()
                .databaseUrl(springDatasourceUrl)
                .databaseUsername(springDatasourceUsername)
                .databasePassword(springDatasourcePassword)
                .build();
    }
}
