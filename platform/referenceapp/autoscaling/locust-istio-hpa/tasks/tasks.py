from locust import HttpLocust, TaskSet, task
import json

class UserBehavior(TaskSet):

	def on_start(self):
		print('Test Ready')

	@task(1)
	def generatePodInfoRequests(self):
		url = "http://podinfo:9898"
		response = self.client.get(url, name='generatePodInfoRequests')
		# print('Response status code:', response.status_code)
		# print('Response content:', response.content)

class WebsiteUser(HttpLocust):
	task_set = UserBehavior
	min_wait = 1000
	max_wait = 5000