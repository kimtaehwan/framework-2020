## CPU Loader for HPA Test

##### This source is forked from [kubernetes/kubernetes]

#### Build image: 
    docker build -t {docker_id}/{docker_repo}:{tag} .

#### Run CPU loader:
    kubectl run cpu-loader --image={docker_id}/{docker_repo} --requests=cpu=200m --expose --port=80