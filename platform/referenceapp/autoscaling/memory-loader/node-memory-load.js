const express = require('express')
const app = express()
const port = 80

let gcWait = false;

app.get('*', function(req,res){

   if(gcWait){
      res.json({'result':'already loaded'});
      return;
   }

   gcWait = true;

   var arr = [...Array(1e7*2).keys()];
   arr = [...Array(1e7*2).keys()];
   arr = [...Array(1e7*2).keys()];

   arr = null;
   
   console.log('Added dummy data to memory...');

   setTimeout(function(){
      global.gc();
      gcWait = false;
      console.log('flushed in-memory data!');
   },10000);
   
   res.json({'result':'loaded'});
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))