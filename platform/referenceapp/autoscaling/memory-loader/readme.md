## Memory Loader for HPA Test

##### This image is already pushed to [arinpang/memory-loader:latest]

#### Build image: 
    docker build -t {docker_id}/{docker_repo}:{tag} .

#### Run memory loader:
    kubectl run memory-loader --image={docker_id}/{docker_repo} --requests=memory=300m --expose --port=80