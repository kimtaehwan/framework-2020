from locust import HttpLocust, TaskSet, task
import json

class UserBehavior(TaskSet):

	def on_start(self):
		print('Test Ready')

	@task(1)
	def generateCPULoad(self):
		url = "http://cpu-loader"
		response = self.client.get(url, name='generateCPULoad')
		# print('Response status code:', response.status_code)
		# print('Response content:', response.content)

	@task(2)
	def generateMemoryLoad(self):
		url = "http://memory-loader"
		response = self.client.get(url, name='generateMemoryLoad')
		# print('Response status code:', response.status_code)
		# print('Response content:', response.content)


class WebsiteUser(HttpLocust):
	task_set = UserBehavior
	min_wait = 1000
	max_wait = 5000