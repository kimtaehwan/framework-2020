# Kubernetes HPA(Horizontal Pod Autoscaler) Test

### Install: 
     make helm-install

### Uninstall: 
     make helm-delete


## only use Kubernetes Metrics

### 1. Test with load-generator

#### CPU Test:

1. Run HPA status monitor
```
kubectl get hpa cpu-loader -w
```
2. Open new SSH window
3. Run load-generator in new window
```
cd load-generator
make load-gen-cpu
```
4. If you want stop load generation, press Ctrl+C


#### Memory Test:

1. Run HPA status monitor in Terminal
```
kubectl get hpa memory-loader -w
```
2. Open new terminal window
3. Run load-generator in new window
```
cd load-generator
make load-gen-memory
```
4. If you want stop load generation, press Ctrl+C



### 2. Test with locust

#### CPU/Memory Test:

1. Run HPA status monitor in Terminal
```
kubectl get hpa cpu-loader -w
```
2. Open http://installed-cluster:locust-autoscaling-master-svc-NodePort with web browser
3. Type next inputs: Number of users to simulate - 10000000, Hatch rate - 20
4. Click 'Start Swarming' button.
5. If you want stop load generation, click 'Stop' button.



## with Istio Custom Metrics


### Test with locust

#### (Istio Requests Total / Pod Count) Test:


##### Caution: If you never sent any request to podinfo, you can't get 'istio-requests-total' metric. therefore, It can be displayed 'Unknown' value from HPA status.


1. Run HPA status monitor in Terminal
```
kubectl get hpa podinfo -w
```
2. Open http://installed-cluster:locust-istio-hpa-master-svc-NodePort with web browser
3. Type next inputs: Number of users to simulate - 10000000, Hatch rate - 20
4. Click 'Start Swarming' button.
5. If you want stop load generation, click 'Stop' button.