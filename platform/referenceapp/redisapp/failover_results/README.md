# RedisApp testing for failover

#### Notes:
###### RedisApp always gets cache right after pushing to cache

##### Step 0:
###### Check master pod where master pod resides:
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL get-master-addr-by-name master

![Redis master ip output 71](./img/redis-master-ip-71.png)

###### Get kubernetes services to see ip of redis services 
    $ kubectl get services -n system-tools
    
![Redis service 71 highlighted](./img/redis-services-71.png)

###### Verified that the master pod is in redis-ha-1 because ip corresponds to redis-ha-announce-1 with 10.107.135.71

##### Step 1:
###### Check the connection by caching a message
![Step 1](./img/redis-failover-1.png) 

##### Step 2:
###### Force a failover manually (SENTINEL failover master)
![Step 2](./img/redis-failover-2.png)

##### Step 3:
###### Force a failover by deleting pod that master node is in
![Step 3](./img/redis-failover-3.png) 
  
##### Step 4:
###### Verify successful failover 
![Step 4](./img/redis-failover-4.png) 

#### Results:
##### Step 0: Run the redisapp on cluster and verify
    $ make helm-install
    $ kubectl get pods -n reference
    
![Pods Running in reference namespace](./img/test_pods.png)

##### Step 1: Expose redisapp and start caching 20 messages
    $ kubectl port-forward svc/redisapp 8080:8080 -n reference
    $ curl -s localhost:8080/redis/20


##### Step 2: Force a failover manually and verify:
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL failover master
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL get-master-addr-by-name master
    
![Redis Manual failover](./img/redis-fail-over-to-42.png)

##### Step 3: Kill pod containing master node 
    $ kubectl delete pod redis-ha-1 -n system-tools  

##### Step 4: Verify successful failover to existing pod 
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL get-master-addr-by-name master

![Redis Kill to failover](./img/redis-kill-to-failover-71.png)

#### Results of logs:
###### exported the logs using cmd:
    $ kubectl logs {redisapp-podname} -n reference > redisapp.out
    
##### stored in ./logs/redisapp.out

Notice:
- Between Cached Data (3) and Cached Data (5), (manual failover) a sentinel was downed, and a new sentinel was added, and line 87, shows master has changed 
- Between Cached Data (17) to Queued Data (19), (deleted pod redis-ha-1) there is an error, until existing pod is recognized as new master  
