package com.ondot.referenceapp.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "com.ondot")
@ComponentScan(basePackages = "com.ondot")
public class RedisAppSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisAppSpringBootApplication.class, args);
    }
}
