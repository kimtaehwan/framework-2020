package com.ondot.referenceapp.redis.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
public class OndotConfigurations {

    @Value("${spring.redis.host}")
    String redisHost;

    @Value("${spring.redis.port}")
    Integer redisPort;

    @Value("#{environment.SLEEP_TIME ?: '3'}")
    Long sleepTime;

    @Value("${redis.master.name}")
    String masterName;

    @Value("${redis.sentinel.port}")
    Integer sentinelPort;

    @Value("${spring.redis.password}")
    String redisPassword;

}
