package com.ondot.referenceapp.redis.resource;


import com.ondot.referenceapp.redis.config.OndotConfigurations;
import com.ondot.referenceapp.redis.utils.TimerUtils;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReadMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping(value = "/redis")
public class RedisResource {

    private static void header(String header){
      log.info("=============================== REDIS : " + header +"====================================");
    }

    @Autowired
    OndotConfigurations ondotConfigurations;

    @GetMapping(value = "/info")
    public String getRedisInfo(){
        String out = "Redis server info :: " + ondotConfigurations.getRedisHost() +":" +ondotConfigurations.getRedisPort();
        log.info(out);
        return out;
    }

    @GetMapping(value = "/{times}")
    public String continousSaveGetUpdateAndGetAndDelete(@PathVariable(value = "times")Long times) throws InterruptedException{
        Long sleepTime = ondotConfigurations.getSleepTime();
        Long totalStartTime = System.nanoTime();
        boolean failed = false;
        header("Testing Cache save and get "+times+" times every "+sleepTime +" seconds");
        Config config = new Config();
        config.useSentinelServers()
                .addSentinelAddress("redis://"+ondotConfigurations.getRedisHost()+":26379")
                .setPassword("ondot1234")
                .setMasterName("master")
                .setReadMode(ReadMode.MASTER_SLAVE);

        RedissonClient redissonClient = Redisson.create(config);

        Long startTime = System.nanoTime();
        for(int i = 1 ; i<= times ; i++){
            try{
                RMap<String, String> map = redissonClient.getMap("simpleMap");
                String message ="Cached Data ("+ i + ")";
                log.info("Adding to cache : " + message);
                map.put("mapKey", message);

                String mapValue = map.get("mapKey");
                log.info("Getting Cached: " + mapValue);
                if(mapValue == null){
                    failed = true;
                    startTime = System.nanoTime();
                }else if(failed){
                    Long endTime = System.nanoTime();
                    failed = false;
                    Long timeElapsed = TimerUtils.findExecutedTime(startTime,endTime)-sleepTime*1000;
                    log.info("===== Down time = " + timeElapsed + "ms");
                }

            }catch (Exception e){
                log.error("Error: " +e);
                failed = true;
                startTime = System.nanoTime();
            }finally {
                TimeUnit.SECONDS.sleep(sleepTime);
            }
        }
        redissonClient.shutdown();
        header("Testing Done");
        Long totalEndTime = System.nanoTime();
        Long totalTimeElapsed = TimerUtils.findExecutedTime(totalStartTime, totalEndTime) - sleepTime*times;
        log.info("===== Total Time Elapsed= " + totalTimeElapsed + "ms");
        return "done";
    }

}
