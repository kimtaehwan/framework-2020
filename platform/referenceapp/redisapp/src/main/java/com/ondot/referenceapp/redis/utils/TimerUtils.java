package com.ondot.referenceapp.redis.utils;

public class TimerUtils {

    public static Long findExecutedTime(Long startTime,Long endTime){
        return (endTime - startTime)/1000000;
    }
}
