## ReferenceApp for Redis-HA K8s cluster connection

#### Requirements:
    1. K8s cluster
    2. namespaces: system-tools, reference
    3. in system-tools namespace: redis-ha installed. See platform/base_services/redis_ha for installation
    
##### To check which pod is master:
###### Get the ip of master using cmd:
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL get-master-addr-by-name master 
    
![Redis master ip output 71](./faileover_results/img/redis-master-ip-71.png)

###### Get kubernetes services to see ip of redis services 
    $ kubectl get services -n system-tools
    
![Redis service 71 highlighted](./faileover_results/img/redis-services-71.png)

###### Force manual failover and check master ip:
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL failover master
    $ kubectl exec -it redis-ha-0 -n system-tools -c redis -- redis-cli -p 26379 SENTINEL get-master-addr-by-name master

![Redis force failover and check master](./faileover_results/img/redis-fail-over-to-42.png)

###### Check services again 
    $ kubectl get services -n system-tools
    
![Redis service 71 highlighted](./faileover_results/img/redis-services-42.png)



###### After requirements system-tools namespace will look like:
![System-tools namespace](./img/system-tools.png) 

###### and cluster will look like:  
![Redis Diagram](./img/redis.png)

#### The redisapp exposes resources:
    1. /redis/info
        shows the redis connection info (host,port,etc)
    2. /redis/{times}
        Caches static string to map and gets the same map from cache