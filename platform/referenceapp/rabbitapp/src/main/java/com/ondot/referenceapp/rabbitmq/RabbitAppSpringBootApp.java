package com.ondot.referenceapp.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "com.ondot")
@ComponentScan(basePackages = "com.ondot")
public class RabbitAppSpringBootApp {
    public static void main(String[] args) {
        SpringApplication.run(RabbitAppSpringBootApp.class, args);
    }
}
