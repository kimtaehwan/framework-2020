package com.ondot.referenceapp.rabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RabbitMQListener implements MessageListener {

	public void onMessage(Message message) {
        log.info("RabbitMQListener :: Consuming Message - " + new String(message.getBody()));
	}

}