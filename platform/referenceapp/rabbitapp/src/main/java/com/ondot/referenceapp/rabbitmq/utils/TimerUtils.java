package com.ondot.referenceapp.rabbitmq.utils;

public class TimerUtils {

    public static Long findExecutedTime(Long startTime,Long endTime){
        return (endTime - startTime)/1000000;
    }
}
