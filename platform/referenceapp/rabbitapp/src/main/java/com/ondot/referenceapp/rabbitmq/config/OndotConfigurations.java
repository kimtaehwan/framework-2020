package com.ondot.referenceapp.rabbitmq.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
public class OndotConfigurations {

    @Value("${spring.rabbitmq.host}")
    String rabbitmqHost;

    @Value("${spring.rabbitmq.port}")
    Integer rabbitmqPort;

    @Value("${rabbitmq.queue}")
    String rabbitmqQueueName;

    @Value("${spring.rabbitmq.username}")
    String rabbitmqUsername;

    @Value("${spring.rabbitmq.password}")
    String rabbitmqPassword;

    @Value("#{environment.SLEEP_TIME ?: '3'}")
    Long sleepTime;

}
