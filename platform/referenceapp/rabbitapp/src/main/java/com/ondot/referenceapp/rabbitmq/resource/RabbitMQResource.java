package com.ondot.referenceapp.rabbitmq.resource;

import com.ondot.referenceapp.rabbitmq.config.OndotConfigurations;
import com.ondot.referenceapp.rabbitmq.utils.TimerUtils;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


@Slf4j
@RestController
@RequestMapping(value = "/mq")
public class RabbitMQResource {

    private static void header(String header){
        log.info("============================ RABBITMQ : " + header +"====================================");
    }

    @Autowired
    OndotConfigurations ondotConfigurations;


    @GetMapping(value = "/info")
    public String getRabbitMQInfo(){
        String out = "RabbitMQ server info :: " + ondotConfigurations.getRabbitmqHost() +":" +ondotConfigurations.getRabbitmqPort() + ", queue name :" + ondotConfigurations.getRabbitmqQueueName();
        log.info(out);
        return out;
    }

    private CachingConnectionFactory getConnectionFactory(){
        CachingConnectionFactory factory = new CachingConnectionFactory();
        factory.setUsername(ondotConfigurations.getRabbitmqUsername());
        factory.setPassword(ondotConfigurations.getRabbitmqPassword());
        factory.setHost(ondotConfigurations.getRabbitmqHost());
        factory.setPort(ondotConfigurations.getRabbitmqPort());
        return factory;
    }

    @GetMapping(value = "/{times}")
    public String continousSendAndGet(@PathVariable(value = "times")Long times){
        Long sleepTime = ondotConfigurations.getSleepTime();
        Long totalStartTime = System.nanoTime();
        String queueName = "test.queue";
        boolean failed = false;
        CachingConnectionFactory connectionFactory = getConnectionFactory();
        header("Testing message queue save and get "+ times+ " times every "+ sleepTime +" seconds");
        try{
            Connection connection = connectionFactory.createConnection();
            Channel channel = connection.createChannel(false);
            channel.queueDeclare(queueName, false, false, false, null);
            int timesFailed = 0;
            Long startTime = System.nanoTime();
            for(int i = 1 ; i <= times ; i++){
                try {
                    if(channel.isOpen()) {
                        log.info("======= Channel OPEN ========");
                        String message = "Queued Data (" + i + ")";
                        log.info("Adding to queue : " + message);
                        channel.basicPublish("", queueName, null, message.getBytes());
                        if (failed) {
                            Long endTime = System.nanoTime();
                            failed = false;
                            Long timeElapsed = TimerUtils.findExecutedTime(startTime, endTime) - TimeUnit.SECONDS.convert(sleepTime,TimeUnit.NANOSECONDS) * timesFailed;
                            log.info("===== Down time = " + timeElapsed + "ms");
                            timesFailed = 0;
                        }
                    }else{
                        log.info("======= Channel CLOSED ========");
                        channel = restartConnection(channel,connection);
                        failed = true;
                        if(timesFailed == 0)
                            startTime = System.nanoTime();
                        timesFailed += 1;
                    }
                }catch (Exception e){
                    log.error("Error(1) : " + e + "; Starting new connection");
                    channel = restartConnection(channel,connection);
                    failed = true;
                    if(timesFailed == 0)
                        startTime = System.nanoTime();
                    timesFailed += 1;
                }finally {
                    TimeUnit.SECONDS.sleep(sleepTime);
                }
            }
        }catch (Exception e){
            log.error("Error(2) : "+e);
        }
        Long totalEndTime = System.nanoTime();
        Long totalTimeElapsed = TimerUtils.findExecutedTime(totalStartTime, totalEndTime) - sleepTime*times;
        log.info("===== Total Time Elapsed= " + totalTimeElapsed + "ms");
        header("Testing Done");
        return "done";
    }

    private Channel restartConnection(Channel channel, Connection connection) throws TimeoutException , IOException {
        log.info("New Connection");
        CachingConnectionFactory connectionFactory = getConnectionFactory();
        channel.close();
        connection.close();
        connection = connectionFactory.createConnection();
        return connection.createChannel(false);
    }

}
