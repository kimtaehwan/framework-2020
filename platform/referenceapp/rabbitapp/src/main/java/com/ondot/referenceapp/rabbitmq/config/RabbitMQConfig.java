package com.ondot.referenceapp.rabbitmq.config;


import com.ondot.referenceapp.rabbitmq.service.RabbitMQListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {
    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.host}")
    String rabbitmqHost;

    @Value("${spring.rabbitmq.port}")
    Integer rabbitmqPort;

    @Value("${spring.rabbitmq.ssl.key-store}")
    String keyStore;

    @Value("${spring.rabbitmq.ssl.key-store-password}")
    String keyStorePassword;

    @Value("${spring.rabbitmq.ssl.trust-store}")
    String trustStore;

    @Value("${spring.rabbitmq.ssl.trust-store-password}")
    String trustStorePassword;

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    //create custom connection factory
	@Bean
	ConnectionFactory connectionFactory() throws Exception{
        RabbitConnectionFactoryBean rabbitConnectionFactoryBean = new RabbitConnectionFactoryBean();
        rabbitConnectionFactoryBean.setHost(rabbitmqHost);
        rabbitConnectionFactoryBean.setPort(rabbitmqPort);
        rabbitConnectionFactoryBean.setUsername(username);
        rabbitConnectionFactoryBean.setPassword(password);
        rabbitConnectionFactoryBean.setUseSSL(true);
        rabbitConnectionFactoryBean.setKeyStore(keyStore);
        rabbitConnectionFactoryBean.setKeyStorePassphrase(keyStorePassword);
        rabbitConnectionFactoryBean.setTrustStore(trustStore);
        rabbitConnectionFactoryBean.setTrustStorePassphrase(trustStorePassword);
        rabbitConnectionFactoryBean.afterPropertiesSet();
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(
                rabbitConnectionFactoryBean.getObject());
        connectionFactory.setAddresses(rabbitmqHost);
        connectionFactory.setPort(rabbitmqPort);
        return connectionFactory;
	}

    //create MessageListenerContainer using custom connection factory
	@Bean
	MessageListenerContainer messageListenerContainer() throws Exception {
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
		simpleMessageListenerContainer.setQueues(queue());
		simpleMessageListenerContainer.setMessageListener(new RabbitMQListener());
		return simpleMessageListenerContainer;
	}

}