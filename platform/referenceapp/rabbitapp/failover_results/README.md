# RabbitApp testing for failover

#### Notes:
###### RabbitApp always output from test.queue whenever message is published

##### Step 0:
###### Check the master pod where queue resides:
    $ kubectl exec -it rabbitmq-ha-0 rabbitmqctl list_queues name pid -c rabbitmq-ha -n system-tools
![Pod to queue](./img/queue_pid.png)
###### Queue exists in rabbitmq-ha-0

##### Step 1:
###### Check the connection by publishing a message to queue
![Step 1](./img/Step1.png) 

##### Step 2:
###### Delete pod where queue resides
![Step 2](./img/Step2.png)

##### Step 3:
###### Observe failover to a existing pod
![Step 3](./img/Step3.png)  

##### Step 4:
###### Delete the new pod where queue resides 
![Step 4](./img/Step4.png) 

##### Step 5:
###### Repeat
![Step 5](./img/Step5.png) 

#### Results:
##### Step 0: Run the rabbitapp on cluster and verify
    $ make helm-install
    $ kubectl get pods -n reference
    
![Pods Running in reference namespace](./img/test_pods.png)

##### Step 1: Expose rabbitapp and start publishing 100 messages
    $ kubectl port-forward svc/rabbitapp 8080:8080 -n reference
    $ curl -s localhost:8080/mq/100
    
##### Step 2: While rabbitapp is publishing 100 messages, delete the pod that the queue is in
    $ kubectl delete pod rabbitmq-ha-0 -n system-tools
    
##### Step 3: Observe failover to an existing pod
    $ kubectl get pods -n system-tools
    
![RabbitMQ-HA pods in system-tools](./img/first_kill_pods.png)

    $ kubectl exec -it rabbitmq-ha-1 rabbitmqctl list_queues name pid -c rabbitmq-ha -n system-tools
    
![Checking failover using rabbitmqctl](./img/first_kill_failover.png)

##### Step 4-5: Delete the existing pod where queue failed-over to
    $ kubectl get pods -n system-tools
    
![RabbitMQ-HA pods in system-tools](./img/second_kill_pods.png)

    $ kubectl exec -it rabbitmq-ha-2 rabbitmqctl list_queues name pid -c rabbitmq-ha -n system-tools
    
![Checking failover using rabbitmqctl](./img/second_kill_failover.png)


#### Results of logs:
###### exported the logs using cmd:
    $ kubectl logs {rabbitmq-podname} -n reference > rabbitapp.out
    
##### stored in ./logs/rabbitapp.out

Notice:
- After each Queued Data, RabbitMQListener outputs each queued message
- Between Queued Data (12) and Queued Data (14), there was a Channel shutdown, (deleted pod rabbitmq-ha-0) 
- Queued Data (32) to Queued Data (34), (deleted pod rabbitmq-ha-1) 
- and again Queued Data (71) to Queued Data (73), (deleted pod rabbitmq-ha-2)
