## RabbitApp for RabbitMQ-HA K8s cluster connection

#### Requirements:
    1. K8s cluster
    2. namespaces: system-tools, test
    3. in system-tools namespace: rabbitmq-ha installed. See platform/base_services/rabbitmq_ha for installation
    
    
###### To check which pod each queue is residing:
    kubectl exec -it rabbitmq-ha-0 rabbitmqctl list_queues name pid -c rabbitmq-ha -n system-tools
    or change rabbitmq-ha-[0,1,2] 
    
![Pod to queue](./failover_results/img/queue_pid.png)

###### After requirements system-tools namespace will look like:
![System-tools namespace](./img/system-tools.png) 

###### and cluster will look like:  
![RabbitMQ Diagram](./img/rabbitmq.png)

#### The rabbitapp exposes resources:
    1. /mq/info
        shows the rabbitmq connection info (host,port,etc)
    2. /mq/{times}
        performs publish to queue (test.queue) ${times}
        
###### Check directory ./failover_results for results of test for failover