## Reference Apps


#### Included apps:
- autoscaling/cpu-loader
- autoscaling/memory-loader
- autoscaling/load-generator
- autoscaling/locust-autoscaling
- autoscaling/podinfo
- autoscaling/locust-istio-hpa

#### Requirements:
1. docker
2. kubectl
3. helm
4. helm plugin diff
	> helm plugin install https://github.com/databus23/helm-diff

5. put helmfile executable in PATH
	> https://github.com/roboll/helmfile/releases


#### Helm install ( must have helmfile in PATH )

> make helm-install

#### Helm upgrade ( must have helmfile in PATH )

> make helm-upgrade

#### Helm delete ( must have helmfile in PATH )

> make helm-delete