## Datadog installation through ansible

### Prerequisites 
    Docker
    Kubernetes
    Helm

#### Variables:
    AWX:
        - hosts_master  ( specifies which hosts to run on ) [should be master]
  
    ENV:
        - ansible_mount_services_datadog
        
#### Tasks explained
    
datadog_installation.yml:

    1. Ensure metrics directory exists
    2. Copy metrics zip file from files directory
    3. Ensure metrics extraction directory exists
    4. Unarchive zip file
    5. Use kubectl to install metrics server