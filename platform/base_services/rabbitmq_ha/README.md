## RabbitMQ-HA installation through ansible

### Prerequisites 
    Docker
    Kubernetes
    Helm

#### Variables:
    AWX:
        - hosts_master  ( specifies which hosts to run on ) [should be master]      
        - nfs_mount_path_rabbitmq ( path of NFS )
        - nfs_mount_server ( IP of NFS )
  
    ENV:
        - ansible_mount_services_rabbitmq
        - kubernetes_namespace
        - k8_resource_pv_name
        - k8_resource_pvc_name
        
#### Tasks explained
    
rabbitmq_installation.yml:

    1. Ensure helm is installed
    2. Create kubernetes namespace given in environment_variables
    3. Copy templated rabbitmq-persistent-volume which includes information for PersistentVolume and PersistentVolumeClaim
    4. Delete existing PersistentVolume and PersistentVolumeClaim
    5. Add PersistentVolume and PersistentVolumeClaim using kubectl
    6. Ensure directory exists for exporting helm charts
    7. Export contents of file/pod/
    8. Export contents of file/pod/templates
    9. Install RabbitMQ-HA using helm