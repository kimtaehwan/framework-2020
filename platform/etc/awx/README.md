## Reference for initalizing AWX tower

#### Requirements:
    1. AWX installed
![AWX Installation guide](https://github.com/ansible/awx/blob/devel/INSTALL.md#docker-compose)
    
    2. Multiple VMs to run playbooks with python 2.7.5

#### 1. Set up Inventories:
###### List of VMs that the playbooks will run on
![Inventories](./img/Inv.png)

##### Inventory Details
![Inventory Details](./img/InvDetails.png)

##### Inventory Groups 
###### Some Playbooks are configured to only run on specific groups (EX: master/workers)
![Inventory Groups](./img/InvGroups.png)

##### Inventory Hosts
###### List of VMs given by IP
![Inventory Hosts](./img/InvHosts.png)

###### Host Details
![Inventory Host Details](./img/InvHostDetail.png)

#### 2. Set up Credentials:

##### List of credentials
###### You can set up multiple credentials with diffferent kind of credentials, but for now we only need Bit Bucket SSH Key and Ondot VM
![Credentials](./img/Creds.png)

##### Ondot VM
###### This will be of credential type/kind of Machine and will use username and password
![Credential Details](./img/CredsDetail.png)

##### Source Control
###### You can use either username/password or ssh private key, I used ssh private key. (~/.ssh/id_rsa)

![Credential Source Control](./img/CredSC.png)


#### 3. Projects

##### Projects is where you will pull the playbooks from (git repository)
###### Most of the playbooks are in the develop branch currently so use develop branch like so, with SCM link being ssh://git@git.internal.ondotsystems.com:7999/cp/platform.git
![Project Details](./img/ProjectDetails.png)

##### Make sure to update after
![Proejct update](img/ProjectRefresh.png)
#### 4. Templates
###### Templates are what defines the jobs when run.

##### Docker upgrade
![Template Docker upgrade](./img/TempDocker.png)

##### K8s upgrade
![Template K8s upgrade](./img/TempK8s.png)

