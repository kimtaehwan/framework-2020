#!/bin/bash

GLOBAL_VALUES_YAML=$1

echo ${GLOBAL_VALUES_YAML}

DATADOG_FLAG="$(awk '/ddEnabled:/' ${GLOBAL_VALUES_YAML} | cut -d':' -f2 | tr -d " ")"
DATADOG_NAMESPACE="$(awk '/datadogns:/' ${GLOBAL_VALUES_YAML} | cut -d':' -f2 | tr -d " ")"

if [[ "x${DATADOG_FLAG}" != "x" ]] && [[ "${DATADOG_FLAG}" = "true" ]] ; then

   kubectl get namespace "${DATADOG_NAMESPACE}" 2>/dev/null

  exit_status=$?

   if [ $exit_status -eq 0 ]; then
      echo "Namespace $DATADOG_NAMESPACE already exists"

   else
      echo "Creating Namespace $DATADOG_NAMESPACE before running datadog helm install"
      kubectl create namespace "${DATADOG_NAMESPACE}"
  	exit_status=$?
   	if [ $exit_status -eq 0 ]; then
      		echo "Namespace created ${DATADOG_NAMESPACE}"
	else 
      		echo "Namespace creation failed ${DATADOG_NAMESPACE}"
	fi

   fi

else
   echo "Datadog flag is not enabled thus no action will be taken"
fi
