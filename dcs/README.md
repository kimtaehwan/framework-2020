## DCS MonoRepo

#### Requirements:
1. java
2. docker
3. kubectl
4. helm
5. helm plugin diff
	> helm plugin install https://github.com/databus23/helm-diff

	**if Error: plugin home "$HELM_HOME/plugins" does not exist**

	> helm init --client-only

	**Rerun helm plugin install**

6. put helmfile executable in PATH
	> https://github.com/roboll/helmfile/releases

#### Build dcs-common for local access for microservices that use dcs-common

> make build-common


#### Docker build:

> make docker-build

#### Docker push ( must have access to ECR (aws ecr get-login)):

> make docker-push

#### Helm install ( must have helmfile in PATH )

> make helm-install

#### Helm upgrade ( must have helmfile in PATH )

> make helm-upgrade

#### Helm delete ( must have helmfile in PATH )

> make helm-delete

### Jira
* https://www.atlassian.com/software/jira/pricing
* Jira 활용
    * https://medium.com/hgmin/devops-jira%EB%A5%BC-%ED%99%9C%EC%9A%A9%ED%95%9C-%ED%98%91%EC%97%85-4f4049a36a56
* Scrum
    * https://medium.com/hgmin/scrum-363a6bfd7a60

### Encryption
* http://blog.securekim.com/2017/09/kisa.html
* https://wariua.github.io/facility/aes-gcm-and-vaes-instruction.html
* https://blog.naver.com/PostView.nhn?blogId=vjhh0712v&logNo=221533578238
* https://cloud.google.com/kms/docs/additional-authenticated-data
* https://svcministry.org/ko/dictionary/what-is-the-difference-between-aes-gcm-and-aes-cbc/

### HSM
* https://m.blog.naver.com/PostView.nhn?blogId=epicman1&logNo=10179700150&proxyReferer=https:%2F%2Fwww.google.com%2F
* http://blog.naver.com/PostView.nhn?blogId=aepkoreanet&logNo=221011181275&parentCategoryNo=&categoryNo=8&viewDate=&isShowPopularPosts=true&from=search

### image
* https://okky.kr/article/342165

### Spring
* ApplicationContext, ServletContext
    * https://jaehun2841.github.io/2018/10/21/2018-10-21-spring-context/#web-application-context

### brave Tracer 
* https://velog.io/@hanblueblue/%EB%B2%88%EC%97%AD-Spring-Cloud-Sleuth-2-Introduction
