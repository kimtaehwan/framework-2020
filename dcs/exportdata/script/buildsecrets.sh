#!/usr/bin/env bash
#$1 = MICROSERVICENAME
#$2 = METADATA_NAMESPACE
#$3 = DB_URL
#$4 = DB_USERNAME
#$5 = DB_PASSWORD
mkdir ./secret
cat <<EOF >./secret/secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: $1-config
  namespace: $2
type: Opaque
stringData:
  cmbasicauthusername: "SOWDcl720FexTzKYdNyDxTM6I8WYT3nh"
  cmbasicauthpassword: "R7M7JZ3IGdstApE285solY1jiKpmEeYE"
  cmdeploymenttoken: "FISERV2014"
  cmfitoken: "NewtonFi"
  cmapptoken: "NewtonApp"
  cmurl: "https://cmqa1.ondotsystems.com/api/v3/notification"
EOF
