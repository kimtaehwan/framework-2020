## Exportdata

### Stream
* https://futurecreator.github.io/2018/08/26/java-8-streams/

### TestCode
* https://brunch.co.kr/@springboot/207
* https://brunch.co.kr/@springboot/292
* https://jojoldu.tistory.com/320
* https://velog.io/@swchoi0329/Spring-Boot-%ED%85%8C%EC%8A%A4%ED%8A%B8-%EC%BD%94%EB%93%9C-%EC%9E%91%EC%84%B1
#### BDDMockito
* https://riptutorial.com/ko/mockito/example/12243/bddmockito-%EC%8A%A4%ED%83%80%EC%9D%BC

### ZonedDateTime
* https://stackoverflow.com/questions/45043236/what-min-max-values-will-work-with-both-zoneddatetime-and-instant-toepochmilli
* https://www.daleseo.com/java8-zoned-date-time/

### Lombok
* https://hyoj.github.io/blog/java/basic/lombok/#builder-builder-default-singular

#### builder
* https://cobbybb.tistory.com/14

### CSV
* https://hellogk.tistory.com/52
* http://opencsv.sourceforge.net/
* https://jeong-pro.tistory.com/70

### Suver CSV
* http://super-csv.github.io/super-csv/examples_writing.html

### commons csv
* https://mvnrepository.com/artifact/org.apache.commons/commons-csv/1.8
   
### Excel
* https://woowabros.github.io/experience/2020/10/08/excel-download.html

### Image
* https://javaconceptoftheday.com/read-and-write-images-in-java/
* https://jang8584.tistory.com/154

## Trouble Shooting
### maven-surefire-plugin
* install 실패와 initialization Error 발생시 maven-surefire-plugin 을 plugin 에 추가해줘야 함.
####
    [ERROR] initializationError(com.ondot.dcs.exportdata.DcsExportdataApplicationTests)  Time elapsed: 0 s  <<< ERROR!
    java.lang.Exception: Test class should have exactly one public constructor
    
    2020-10-30 19:37:30.323  INFO 81632 --- [       Thread-2] o.s.s.concurrent.ThreadPoolTaskExecutor  : Shutting down ExecutorService 'applicationTaskExecutor'
####

### SSL Exception
* http://www.javased.com/index.php?api=javax.net.ssl.X509TrustManager  

### Mocking
* https://bestalign.github.io/2016/07/10/intro-mockito-2/
* http://wonwoo.ml/index.php/post/1453
* https://cornswrold.tistory.com/369

### Sftp
* https://penthegom.tistory.com/6

## Date 
### LocalDate
* https://jeong-pro.tistory.com/163

### ZonedDateTime
* https://www.daleseo.com/java8-zoned-date-time/

### CronJob
* https://kubernetes.io/ko/docs/tasks/job/automated-tasks-with-cron-jobs/

### start of day, end of day
* https://stackoverflow.com/questions/10308356/how-to-obtain-the-start-time-and-end-time-of-a-day/20536041
* https://stackoverflow.com/questions/60539840/how-to-get-the-end-of-day-as-a-offsetdatetime-in-the-format-2019-12-29-055959
