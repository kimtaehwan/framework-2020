package com.ondot.dcs.exportdata.service.client.useridentification;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        UserIdentificationClientService.class,
        UserIdentificationClient.class
})
public class UserIdentificationClientServiceTest {

    @Autowired
    UserIdentificationClientService userIdentificationClientService;

    @MockBean
    UserIdentificationClient userIdentificationClient;

    @Test
    public void getUserIdentificationV09() {

        // given
        String applicationReferenceId = "applicationReferenceId";
        BDDMockito
                .given(this.userIdentificationClient.getUserIdentificationV09(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("UserIdentification Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        UserIdentificationLog actual = this.userIdentificationClientService.getUserIdentificationV09(applicationReferenceId);

        log.info("actual: {}", actual);

        // then
        assertNull(actual);
    }
}
