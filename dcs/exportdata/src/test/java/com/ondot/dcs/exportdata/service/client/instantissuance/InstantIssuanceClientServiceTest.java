package com.ondot.dcs.exportdata.service.client.instantissuance;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class InstantIssuanceClientServiceTest {

    @Autowired
    InstantIssuanceClientService instantIssuanceClientService;

    @MockBean
    InstantIssuanceClient instantIssuanceClient;

    @Test
    public void getInstantIssuanceInfo() {
        // given
        String applicationReferenceId = "applicationReferenceId";
        BDDMockito
                .given(this.instantIssuanceClient.getInstantIssuanceInfo(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("InstantIssuance Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        SingleResult<InstantIssuanceInfo> actual = this.instantIssuanceClientService.getInstantIssuanceInfo(applicationReferenceId);

        log.info("instantIssuance: {}", actual);

        // then
        assertThat(actual.getStatus()).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
