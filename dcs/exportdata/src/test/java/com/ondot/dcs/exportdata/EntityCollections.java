package com.ondot.dcs.exportdata;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EntityCollections {
    public static final int DEFAULT_PAGE = 0;
    public static final int DEFAULT_SIZE = 10;

    public static Pageable createPageable() {
        return PageRequest.of(DEFAULT_PAGE, DEFAULT_SIZE);
    }
}
