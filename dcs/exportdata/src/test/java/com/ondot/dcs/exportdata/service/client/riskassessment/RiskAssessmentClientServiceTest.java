package com.ondot.dcs.exportdata.service.client.riskassessment;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        RiskAssessmentClientService.class,
        RiskAssessmentClient.class
})
@Slf4j
public class RiskAssessmentClientServiceTest {

    @Autowired
    RiskAssessmentClientService riskAssessmentClientService;

    @MockBean
    RiskAssessmentClient riskAssessmentClient;

    @Test
    public void getRiskReportLog() {
        // given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.riskAssessmentClient.getLog(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("riskAssessment Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        SingleResult<RiskReportLog> actual = this.riskAssessmentClientService.getRiskReportLog(applicationReferenceId);

        log.info("actual: {}", actual);

        // then
        assertNotNull(actual);
    }
}
