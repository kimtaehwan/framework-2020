package com.ondot.dcs.exportdata.service.client.journey;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JourneyClientServiceTest {

    @Autowired
    JourneyClientService journeyClientService;

    @MockBean
    JourneyClient journeyClient;

    @Test
    public void getReferenceList() {
        // given
    }

    @Test
    public void getReferenceJourneyProcessLogInfo() {
    }
}
