package com.ondot.dcs.exportdata.service.client.idv;

import com.ondot.dcs.exportdata.dto.core.ListResult;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class IdentityVerificationClientServiceTest {

    @Autowired
    IdentityVerificationClientService identityVerificationClientService;

    @MockBean
    IdentityVerificationClient identityVerificationClient;

    @Test
    public void getDocumentResultLogList() {
        //given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.identityVerificationClient.getDocumentResultLogList(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("test exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        final ListResult<DocumentResultLog> actual = this.identityVerificationClientService.getDocumentResultLogList(applicationReferenceId);

        log.info("actual result: {}", actual);

        // then
        assertThat(actual).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
