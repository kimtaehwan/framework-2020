package com.ondot.dcs.exportdata.service.client.eligibility;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.eligibility.dto.EligibilityLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EligibilityClientServiceTest {

    @Autowired
    EligibilityClientService eligibilityClientService;

    @MockBean
    EligibilityClient eligibilityClient;

    @Test
    public void getEligibilityLog() {
        // given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.eligibilityClient.getEligibilityLog(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("test exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null), null)
                );

        // when
        final SingleResult<EligibilityLog> actual = this.eligibilityClientService.getEligibilityLog(applicationReferenceId);

        log.info("actual result: {}", actual);

        // then
        assertThat(actual).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
