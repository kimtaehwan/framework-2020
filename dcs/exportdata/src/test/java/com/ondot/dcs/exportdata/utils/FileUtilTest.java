package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.core.StorageConfiguration;
import com.ondot.dcs.exportdata.core.StoragePathConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@Slf4j
@RunWith(SpringRunner.class)
public class FileUtilTest {

    @MockBean
    StorageConfiguration storageConfiguration;

    @MockBean
    StoragePathConfiguration storagePathConfiguration;

    @Test
    public void getDirectoryName() {
        // given
        String fileName = "onebank_20201030144611";
        String fiToken = "onebank";
        ZonedDateTime currentDatetime = ZonedDateTime.now();

        // when
        String actual = FileUtil.getDirectoryName(fiToken, currentDatetime);

        // then
        assertNotEquals(actual, fileName);
    }

    public void saveImageTest1() {
        String downloadUrl = "https://www.gstatic.com/webp/gallery/1.sm.jpg";

        try {
            URL url = new URL(downloadUrl);
            BufferedImage bufferedImage = ImageIO.read(url);

            File file = new File("./test.gif");
            ImageIO.write(bufferedImage, "gif", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveImageTest2() {
        String downloadUrl = "https://www.gstatic.com/webp/gallery/1.sm.jpg";
        String filename = "D:\\framework-2020\\framework-2020-0615\\target\\opt\\ondot\\volume\\export-data\\onebank_20201106163215\\r02khgh9vbfv\\selfie2.jpg";
        try {
            URL url = new URL(downloadUrl);
            BufferedImage bufferedImage = ImageIO.read(url);

            File file = new File(filename);
            ImageIO.write(bufferedImage, "gif", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
