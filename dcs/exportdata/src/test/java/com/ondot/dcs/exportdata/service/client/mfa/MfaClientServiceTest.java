package com.ondot.dcs.exportdata.service.client.mfa;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.mfa.dto.MFAOTPMapping;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MfaClientServiceTest {

    @Autowired
    MfaClientService mfaClientService;

    @MockBean
    MfaClient mfaClient;

    @Test
    public void getMFAOTPMapping() {
        // given
        String applicationReferenceId = "applicationReferenceId";
        BDDMockito
                .given(this.mfaClient.getMFAOTPMappingList(null, null, null, null, null, null, applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("mfa exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        SingleResult<MFAOTPMapping> actual = this.mfaClientService.getMFAOTPMapping(applicationReferenceId);

        log.info("actual: {}", actual);

        // then
        assertThat(actual).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
