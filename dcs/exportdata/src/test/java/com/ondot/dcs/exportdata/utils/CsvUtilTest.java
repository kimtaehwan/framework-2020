package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.dto.ApplicationData;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.LMinMax;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class CsvUtilTest {

    public void csvWriteTest() throws Exception {
        // given
        List<ApplicationData.Csv> list = getList();

        // when
        ICsvBeanWriter beanWriter = null;

        // then
        try {
            beanWriter = new CsvBeanWriter(new FileWriter("test.csv"),
                    CsvPreference.STANDARD_PREFERENCE);

            final String[] header = getHeader();
            final CellProcessor[] processors = getProcessors();

            beanWriter.writeHeader(header);

            for (final ApplicationData.Csv data : list) {
                beanWriter.write(data, header, processors);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (beanWriter != null) {
                beanWriter.close();
            }
        }
    }

    private String[] getHeader() {
        return new String[] {
                "applicationReferenceId",
                "lastModifiedTime",
                "productName",
                "applicationStatus",
                "applicantName",
                "dateOfBirth",
                "ssn",
                "address",
                "phoneNumber",
                "email",
                "cardType",
                "cardNumber",
                "userVerificationStatus",
                "userVerificationMethod",
                "productDuplicateDetectionStatus",
                "consentAcceptanceStatus",
                "userAcceptedConsentDocuments",
                "consentAcceptanceDate",
                "eSignatureDate",
                "riskAssessmentStatus",
                "riskScore",
                "thresholdRiskScore",
                "creditInquiry",
                "creditScore",
                "thresholdCreditScore",
                "creditClassCode",
                "userSelectedEligibilityCriteria",
                "idVerificationStatus",
                "homeAddressVerificationStatus",
                "idType",
                "digitalWallet",
                "verificationPhoneNumber",
                "emailAddressForOTP",
                "metaData"
        };
    }

    private List<ApplicationData.Csv> getList() {
        ApplicationData.Csv csv = ApplicationData.Csv.builder()
                .applicationReferenceId("test1")
                .lastModifiedTime(new Date())
                .productName("productName2")
                .applicationStatus(ApplicationData.ApplicationStatus.APPROVED_ISSUED)
                .applicantName("address3")
                .dateOfBirth("")
                .ssn("")
                .address("")
                .phoneNumber("")
                .email("")
                .cardType("")
                .cardNumber("")
                .userVerificationStatus("")
                .userVerificationMethod("")
                .productDuplicateDetectionStatus("")
                .consentAcceptanceStatus("")
                .userAcceptedConsentDocuments("")
                .eSignatureDate(new Date())
                .riskAssessmentStatus("")
                .riskScore("")
                .thresholdRiskScore("")
                .creditInquiry("")
                .creditScore("")
                .thresholdCreditScore("")
                .creditClassCode("")
                .userSelectedEligibilityCriteria("")
                .idVerificationStatus("")
                .homeAddressVerificationStatus("")
                .idType("")
                .digitalWallet("")
                .verificationPhoneNumber("")
                .emailAddressForOTP("")
                .metaData("")
                .build();

        List<ApplicationData.Csv> list = new ArrayList<>();
        list.add(csv);
        list.add(csv);

        return list;
    }

    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[] {
                new NotNull(), // applicationReferenceId (must be unique)
                new Optional(new FmtDate("dd/MM/yyyy")),    // lastModifiedTime
                new Optional(), // productName
                new Optional(), // applicationStatus
                new Optional(), // applicantName
                new Optional(), // dateOfBirth
                new Optional(), // ssn
                new Optional(), // address
                new Optional(), // phoneNumber
                new Optional(), // email
                new Optional(), // cardType
                new Optional(), // cardNumber
                new Optional(), // userVerificationStatus
                new Optional(), // userVerificationMethod
                new Optional(), // productDuplicateDetectionStatus
                new Optional(), // consentAcceptanceStatus
                new Optional(), // userAcceptedConsentDocuments
                new Optional(), // consentAcceptanceDate
                new Optional(), // eSignatureDate
                new Optional(), // riskAssessmentStatus
                new Optional(), // riskScore
                new Optional(), // thresholdRiskScore
                new Optional(), // creditInquiry
                new Optional(), // creditScore
                new Optional(), // thresholdCreditScore
                new Optional(), // creditClassCode
                new Optional(), // userSelectedEligibilityCriteria
                new Optional(), // idVerificationStatus
                new Optional(), // homeAddressVerificationStatus
                new Optional(), // idType
                new Optional(), // digitalWallet
                new Optional(), // verificationPhoneNumber
                new Optional(), // emailAddressForOTP
                new Optional()  // metaData
        };

        return processors;
    }
}
