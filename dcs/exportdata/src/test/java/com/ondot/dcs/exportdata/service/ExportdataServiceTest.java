package com.ondot.dcs.exportdata.service;

import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ScheduleExport;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExportdataServiceTest {

    @Autowired
    private ExportdataService exportdataService;

    @Test
    public void checkPassMonthlyTest() {
        log.info("checkPassMonthlyTesty");
        // given


        // when
        ZonedDateTime now = ZonedDateTime.now();
        boolean actual = exportdataService.checkPassMonthly(now, ScheduleExport.MONTHLY);

        // then
        assertEquals(actual, true);
    }
}