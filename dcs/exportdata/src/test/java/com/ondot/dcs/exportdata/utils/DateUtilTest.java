package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.model.enums.DateType;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ScheduleExport;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.threeten.extra.Interval;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import static org.junit.Assert.*;

@Slf4j
public class DateUtilTest {

    @Test
    public void getMinZonedDateTimeTest() {
        // given
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String startDate = "2020-10-20";
        ZonedDateTime minTime = ZonedDateTime.parse(
                ZonedDateTime.of(2020, 10, 20, 0, 0, 0, 0, ZoneId.of("UTC"))
                .format(dateTimeFormatter),
                dateTimeFormatter
        );

        // when
        ZonedDateTime actual = DateUtil.getMinZonedDateTime(startDate);

        // then
        assertEquals(minTime, actual);
    }

    @Test
    public void getMaxZonedDateTimeTest() {
        // given
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String date = "2020-10-20";
        ZonedDateTime maxtime = ZonedDateTime.parse(
                ZonedDateTime.of(2020, 10, 20, 23, 59, 59, 999999999, ZoneId.of("UTC"))
                .format(dateTimeFormatter),
                dateTimeFormatter
        );

        // when
        ZonedDateTime actual = DateUtil.getMaxZonedDateTime(date);

        // then
        assertEquals(maxtime, actual);
    }

    @Test
    public void getDate() {
        // given
        String date = "2020-10-20";

        Integer year = 2020;
        Integer month = 10;
        Integer day = 20;

        // when
        Integer actualYear = DateUtil.getIntegerDate(date, DateType.YEAR);
        Integer actualMonth = DateUtil.getIntegerDate(date, DateType.MONTH);
        Integer actualDay = DateUtil.getIntegerDate(date, DateType.DAY);

        // then
        assertEquals(year, actualYear);
        assertEquals(month, actualMonth);
        assertEquals(day, actualDay);
    }

    @Test
    public void atStartOfDayTest() {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime minusDays = now.minusDays(1);

        Instant instant = LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())
                .atStartOfDay(ZoneId.systemDefault())
                .toInstant();

        Date startDate = Date.from(instant);

        DateTimeFormatter myFormatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd'T'HH:mm:ss.nnnnnnnnn xxx");

        log.info("startDate: {}", startDate);


        log.info("now: {}", now);
        log.info("ZoneId.systemDefault(): {}", ZoneId.systemDefault());
        log.info("minusDays: {}", minusDays);
    }

    @Test
    public void dateTest() {
        OffsetDateTime beforeDay = OffsetDateTime.now().minusDays(1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.nnnnnnnnn xxx");
//        OffsetDateTime startDay = OffsetDateTime.of(beforeDay.getYear(), beforeDay.getMonthValue(), beforeDay.getDayOfMonth(), 0, 0, 0, 0,);
        log.info("beforeDay.getYear(): {}", beforeDay.getYear());
        log.info("beforeDay.getMonth(): {}", beforeDay.getMonth());
        log.info("beforeDay.getMonthValue(): {}", beforeDay.getMonthValue());
        log.info("beforeDay.getDayOfMonth(): {}", beforeDay.getDayOfMonth());


        LocalDate today = LocalDate.now(ZoneId.systemDefault());

        log.info("ZoneId.systemDefault(): {}", ZoneId.systemDefault());
        log.info("today {}", today);
        log.info("today.minusDays: {}", today.minusDays(1));
        LocalDate minusDay = today.minusDays(1);

        OffsetDateTime startDate = minusDay.atTime(OffsetTime.MIN);
        log.info("startDate: {}", startDate);

        OffsetDateTime endDate = minusDay.atTime(OffsetTime.MAX);
        log.info("endDate: {}", endDate);

        Instant start = startDate.toInstant();
        Instant stop = endDate.toInstant();

        Interval interval = Interval.of(start, stop);
        log.info("interval: {}", interval);
    }

    @Test
    public void dateTest2() {
        OffsetDateTime offsetDateTime = OffsetDateTime.now().minusDays(1);

        LocalDateTime localDateTime = atStartOfDay(offsetDateTime.toLocalDateTime());

        log.info("startOfDay: {}", atStartOfDay(offsetDateTime.toLocalDateTime()));
        log.info("offset: {}", convertToOffsetDateTime(localDateTime));
    }

    @Test
    public void dateEndTest() {
        OffsetDateTime offsetDateTime = OffsetDateTime.now().minusDays(1);

        LocalDateTime localDateTime = atEndOfDay(offsetDateTime.toLocalDateTime());

        log.info("endOfDay: {}", localDateTime);
        log.info("offset: {}", convertToOffsetDateTime(localDateTime));
    }

    @Test
    public void checkFirstDayTest() {
        // given
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime first = OffsetDateTime.of(2021, 01, 01, 0, 0, 0, 0, ZoneOffset.UTC);

        log.info("days: {}", now.getDayOfMonth());
        log.info("first: {}", first.getDayOfMonth());
        // when
        boolean actual = DateUtil.checkFirstDay(first);

        // then
        assertEquals(true, actual);
    }

    @Test
    public void dateMonthTest() {
        // given
        OffsetDateTime first = OffsetDateTime.of(2021, 01, 01, 0, 0, 0, 0, ZoneOffset.UTC);
        OffsetDateTime minusMonthDay = first.minusMonths(1);

        // when
        LocalDateTime startDate = DateUtil.atStartOfDay(minusMonthDay.toLocalDateTime());
        OffsetDateTime startOfDay = DateUtil.convertToOffsetDateTime(startDate);

        log.info("endOfDay: {}", startOfDay);

    }

    @Test
    public void startDateMonthTest2() {
        OffsetDateTime first = OffsetDateTime.of(2021, 01, 02, 0, 0, 0, 0, ZoneOffset.UTC);
        LocalDate initial = first.minusMonths(1).toLocalDate();
        LocalDate start = initial.withDayOfMonth(1);
        LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());

        LocalDateTime startLocalTime = start.atStartOfDay();
        OffsetDateTime startTime = DateUtil.convertToOffsetDateTime(startLocalTime);

        log.info("initial : {}", initial);
        log.info("start: {}", start);
        log.info("end: {}", end);

        log.info("startLocalTime: {}", startLocalTime);
        log.info("startTime: {}", startTime);
    }

    @Test
    public void endDateMonthTest() {
        OffsetDateTime first = OffsetDateTime.of(2021, 01, 02, 0, 0, 0, 0, ZoneOffset.UTC);
        LocalDate initial = first.minusMonths(1).toLocalDate();
        LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());

        log.info("initial : {}", initial);
        log.info("end: {}", end);

        LocalDateTime endLocalTime = end.atStartOfDay();
        LocalDateTime endOfDay = DateUtil.atEndOfDay(endLocalTime);
        OffsetDateTime endTime = DateUtil.convertToOffsetDateTime(endOfDay);

        log.info("endOfDay: {}", endOfDay);
        log.info("endTime: {}", endTime);
    }

    public static LocalDateTime atStartOfDay(LocalDateTime date) {
//        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = date.with(LocalTime.MIN);
        return startOfDay;
    }

    public static LocalDateTime atEndOfDay(LocalDateTime date) {
        LocalDateTime endOfDay = date.with(LocalTime.MAX);
        return endOfDay;
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    OffsetDateTime convertToOffsetDateTime(LocalDateTime localDateTime) {
        ZoneOffset offset = OffsetDateTime.now().getOffset();
        OffsetDateTime offsetDateTime = localDateTime.atOffset(offset);
        return offsetDateTime;
    }
}
