package com.ondot.dcs.exportdata.service.client.cinfo;

import brave.Tracer;
import brave.Tracing;
import brave.propagation.StrictScopeDecorator;
import brave.propagation.ThreadLocalCurrentTraceContext;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.cinfo.dto.VerifiedApplicationForm;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.sleuth.util.ArrayListSpanReporter;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CustomerInfoClientServiceTest {

    @Autowired
    CustomerInfoClientService customerInfoClientService;

    @MockBean
    CustomerInfoClient customerInfoClient;

    @MockBean
    Tracer tracer;

    Tracer testTracer;

    @Before
    public void setUp() throws Exception {
        ArrayListSpanReporter reporter = new ArrayListSpanReporter();
        Tracing tracing = Tracing.newBuilder()
                .currentTraceContext(ThreadLocalCurrentTraceContext.newBuilder()
                        .addScopeDecorator(StrictScopeDecorator.create()).build())
                .spanReporter(reporter)
                .build();

        testTracer = tracing.currentTracer();

        Mockito.when(this.tracer.currentSpan())
                .thenReturn(testTracer.newTrace().name("dcs-idv").start());

        log.info("setUp()");
    }

    @After
    public void tearDown() throws Exception {
        log.info("tearDown()");
    }

    @Test
    public void getCustomerVerifiedApplicationForm() {

        //given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.customerInfoClient.getCustomerVerifiedApplicationForm(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("test exception", Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null), null)
                );
        //when
        final SingleResult<VerifiedApplicationForm> actual = this.customerInfoClientService.getCustomerVerifiedApplicationForm(applicationReferenceId);

        log.info("actual result: {}", actual);

        //then
        assertThat(actual).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
