package com.ondot.dcs.exportdata.model.enums;

import com.ondot.dcs.exportdata.dto.ApplicationData;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationStatusTest {

    @Test
    public void statusTest() {
        // given
        ApplicationData.ApplicationStatus status = ApplicationData.ApplicationStatus.APPROVED_ISSUED;

        // when
        ApplicationData.ApplicationStatus actual = ApplicationData.ApplicationStatus.APPROVED_ISSUED;

        // then
        assertEquals(actual.getMessage(), status.getMessage());
    }
}
