package com.ondot.dcs.exportdata.service.client.product;

import brave.Tracer;
import brave.Tracing;
import brave.propagation.StrictScopeDecorator;
import brave.propagation.ThreadLocalCurrentTraceContext;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.sleuth.util.ArrayListSpanReporter;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductInfoClientServiceTest {

    @Autowired
    ProductInfoClientService productInfoClientService;

    @MockBean
    ProductInfoClient productInfoClient;

    @MockBean
    Tracer tracer;

    Tracer testTracer;

    @Before
    public void setUp() throws Exception {
        ArrayListSpanReporter reporter = new ArrayListSpanReporter();
        Tracing tracing = Tracing.newBuilder()
                .currentTraceContext(ThreadLocalCurrentTraceContext.newBuilder()
                        .addScopeDecorator(StrictScopeDecorator.create()).build())
                .spanReporter(reporter).build();
        testTracer = tracing.currentTracer();

        Mockito.when(this.tracer.currentSpan())
                .thenReturn(testTracer.newTrace().name("dcs-idv").start());
        log.info("setUp()");
    }

    @After
    public void tearDown() throws Exception {
        log.info("tearDown()");
    }

    @Test
    public void getCustomerProduct() {
        //given
        String applicationReferenceId = "applicationReferenceId";
        String imageType = "url";

        BDDMockito
                .given(this.productInfoClient.getCustomerProduct(applicationReferenceId, imageType))
                .willThrow(
                        new FeignException.BadRequest("test exception", Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null), null)
                );
        //when
        final SingleResult<Product> actual = this.productInfoClientService.getCustomerProduct(applicationReferenceId, imageType);

        log.info("actual result: {}", actual);
    }
}
