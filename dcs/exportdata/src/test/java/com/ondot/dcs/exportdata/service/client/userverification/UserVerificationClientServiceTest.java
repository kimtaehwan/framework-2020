package com.ondot.dcs.exportdata.service.client.userverification;

import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        UserVerificationClientService.class,
        UserVerificationClient.class
})
public class UserVerificationClientServiceTest {

    @Autowired
    UserVerificationClientService userVerificationClientService;

    @MockBean
    UserVerificationClient userVerificationClient;

    @Test
    public void getUserVerificationV09() {
        // given
        String applicationReferenceId = "applicationReferenceId";
        BDDMockito
                .given(this.userVerificationClient.getUserVerificationV09(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("UserVerification Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        UserVerificationLog actual = this.userVerificationClientService.getUserVerificationV09(applicationReferenceId);

        log.info("actual: {}", actual);

        // then
        assertNull(actual);
    }
}
