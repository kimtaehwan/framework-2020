package com.ondot.dcs.exportdata.service.client.esignature;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ESignatureClientServiceTest {

    @Autowired
    ESignatureClientService eSignatureClientService;

    @MockBean
    ESignatureClient eSignatureClient;

    @Test
    public void getSignature() {
        // given
        String applicationReferenceId = "applicationReferenceId";
        String fileType = "base64";

        BDDMockito
                .given(this.eSignatureClient.getSignature(applicationReferenceId, fileType))
                .willThrow(
                        new FeignException.BadRequest("test exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        final SingleResult<ESigDocModel> actual = this.eSignatureClientService.getSignature(applicationReferenceId);

        log.info("actual result: {}", actual);

        // then
        assertThat(actual).isNotNull();
        assertThat(actual.getStatus().isSuccess()).isEqualTo(Boolean.FALSE);
    }
}
