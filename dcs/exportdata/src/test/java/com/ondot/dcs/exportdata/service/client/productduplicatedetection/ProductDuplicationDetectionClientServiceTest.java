package com.ondot.dcs.exportdata.service.client.productduplicatedetection;

import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductDuplicationDetectionClientServiceTest {

    @Autowired
    ProductDuplicationDetectionClientService productDuplicationDetectionClientService;

    @MockBean
    ProductDuplicationDetectionClient productDuplicationDetectionClient;

    @Test
    public void getSavedDuplicateProductResultV09() {
        // given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.productDuplicationDetectionClient.getSavedDuplicateProductResultV09(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("PDD Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        ProductDuplicationDetectionLog actual = this.productDuplicationDetectionClientService.getSavedDuplicateProductResultV09(applicationReferenceId);

        log.info("actual: {}", actual);
        assertEquals(null, actual);
    }
}
