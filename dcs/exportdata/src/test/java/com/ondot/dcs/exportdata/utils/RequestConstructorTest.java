package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.config.OndotConfig;
import com.ondot.dcs.exportdata.model.cm.request.email.CMRequestEmail;
import com.ondot.dcs.exportdata.model.cm.request.email.EmailContent;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static com.ondot.dcs.exportdata.testUtils.MockStaticObjects.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class RequestConstructorTest {

    @InjectMocks
    RequestConstructor requestConstructor;

    @Mock
    OndotConfig ondotConfig;

    @Test
    public void requestConstructorForEmail() {
        // given
        String toEmail = "taehwan.kim@ondotsystems.com";
        EmailContent emailContent = EmailContent.builder()
                .fileNames(Arrays.asList("file1", "file2"))
                .executedTime(ZonedDateTime.now())
                .exportedRecordsCount(100)
                .failureRecordsCount(0)
                .exportAttemptsCount(1)
                .build();

        log.info("# emailContent: {}", emailContent);

        List<CMRequestEmail> requests = RequestConstructor.requestConstructorForEmail(appToken, fiToken, toEmail, emailContent);

        log.info("# requests: {}", requests);
    }
}
