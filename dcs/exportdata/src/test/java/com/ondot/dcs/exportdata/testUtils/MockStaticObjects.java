package com.ondot.dcs.exportdata.testUtils;

public class MockStaticObjects {
    public static String traceId = "TraceId";

    public static String applicationReferenceId = "MockApplicationReferenceId";
    public static String appToken = "MockAppToken";
    public static String fiToken = "MockFiToken";
    public static String serviceType = "MockServiceType";
    public static String uniqueDeviceId = "MockUniqueDeviceId";
    public static String phoneNumberInvalid = "MockUniquePhoneNumber";
    public static String phoneNumberValid = "4083211234";
    public static Boolean updateJourney = false;

    public static String emailAddressInvalid = "MockEmailAddress";
    public static String emailAddressValid = "MockEmailAddress@ondotsystems.com";
    public static String remoteAddr = "MockRemoteAddr";

    public static int baseResultCode = 1;
    public static Integer baseResultHttpCode = 100;
    public static String baseResultMessage = "baseResultMessage";

    public static boolean OTP_NUMERIC_TRUE = true;
    public static boolean OTP_NUMERIC_FALSE = false;
    public static int OTP_LENGTH = 6;
    public static int OTP_EXPIRY = 3;
    public static String OTP_EXPIRY_STRING = "3";
    public static int OTP_ATTEMPTS =2;
    public static boolean OTP_MOBILE = true;
    public static boolean OTP_EMAIL_TRUE = true;
    public static boolean OTP_EMAIL_FALSE = false;
    public static String otpCode = "111111";
    public static String cmNumber = "123456";
    public static String cmMessage = "MockCMMessage";
    public static String cmURL = "MockCMUrl";
    public static String cmDeploymentTokenKey = "MockCMDeploymentTokenKey";
    public static String cmDeploymentToken = "MockCMDeploymentToken";
    public static final String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String basePath = "MockBasePath";
    public static String cmBasicAuthUsername = "MockCmBasicAuthUsername";
    public static String cmBasicAuthPassword = "MockCmBasicAuthPassword";
    public static String deploymentToken = "MockDeploymentToken";
    public static String cmFiToken = "MockCmFiToken";
    public static String cmAppToken = "MockCmAppToken";
    public static String cmUrl = "MockCmUrl";

    public static String cmBasicAuthUsernameKey = "cm.required.basic.auth.username";
    public static String cmBasicAuthPasswordKey = "cm.required.basic.auth.password";
    public static String deploymentTokenKey = "cm.required.deployment.token";
    public static String cmFiTokenKey = "cm.body.fitoken";
    public static String cmAppTokenKey = "cm.body.apptoken";
    public static String cmUrlKey = "cm.url";

    public static String encryptionModeNone = "none";
    public static String encryptionModeEnvelope = "envelope";
    public static String dbUrl = "jdbc:h2:mem:inmemdb";
    public static String dbUsername = "MockDbUsername";
    public static String dbPassword = "MockDbPassword";
    public static String platformKeyARN = "MockPlatformKeyARN";
}
