package com.ondot.dcs.exportdata.service.client.terms;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import feign.FeignException;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        TermsClientService.class,
        TermsClient.class
})
@Slf4j
public class TermsClientServiceTest {

    @Autowired
    TermsClientService termsClientService;

    @MockBean
    TermsClient termsClient;

    @Test
    public void getCustomerTerm() {
        // given
        String applicationReferenceId = "applicationReferenceId";

        BDDMockito
                .given(this.termsClient.getCustomerTerm(applicationReferenceId))
                .willThrow(
                        new FeignException.BadRequest("Terms Exception",
                                Request.create(Request.HttpMethod.GET, "url", Collections.emptyMap(), null),
                                null)
                );

        // when
        SingleResult<TermsStatusLog> actual = this.termsClientService.getCustomerTerm(applicationReferenceId);

        log.info("actual: {}", actual);

        // then
        assertNotNull(actual);
    }
}
