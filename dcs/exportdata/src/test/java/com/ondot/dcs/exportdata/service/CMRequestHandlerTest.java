package com.ondot.dcs.exportdata.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ondot.dcs.exportdata.config.OndotConfig;
import com.ondot.dcs.exportdata.model.cm.request.email.EmailContent;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplateHandler;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Map;

import static com.ondot.dcs.exportdata.testUtils.MockStaticObjects.*;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class CMRequestHandlerTest {

    @InjectMocks
    CMRequestHandler cmRequestHandler;

    @Mock
    OndotConfig ondotConfig;

    @Mock
    ObjectMapper objectMapper;

    @Mock
    RestTemplate restTemplate;

    @Mock
    UriTemplateHandler uriTemplateHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    /**
     * TODO: CMRequestHandler.java - makeEmailRequest
     */
    @Test
    public void makeEmailRequestTest() {
        // given
        URI uri = URI.create(cmURL);

        when(ondotConfig.getCmURL()).thenReturn(cmURL);
        when(ondotConfig.getDeploymentTokenKey()).thenReturn(cmDeploymentTokenKey);
        when(ondotConfig.getDeploymentToken()).thenReturn(cmDeploymentToken);
        when(ondotConfig.getAppToken()).thenReturn(appToken);
        when(ondotConfig.getFiToken()).thenReturn(fiToken);

        when(restTemplate.getUriTemplateHandler()).thenReturn(uriTemplateHandler);
        when(uriTemplateHandler.expand(any(), any(Map.class))).thenReturn(uri);

        log.info("deploymentToken: {}", ondotConfig.getDeploymentToken());
        String toEmail = "taehwan.kim@ondotsystems.com";
        EmailContent emailContent = EmailContent.builder()
                .fileNames(Arrays.asList("file1", "file2"))
                .executedTime(ZonedDateTime.now())
                .exportedRecordsCount(100)
                .failureRecordsCount(0)
                .exportAttemptsCount(1)
                .build();

        cmRequestHandler.makeEmailRequest(toEmail, emailContent);
    }
}
