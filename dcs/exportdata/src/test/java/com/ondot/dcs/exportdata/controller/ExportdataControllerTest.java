package com.ondot.dcs.exportdata.controller;

import com.ondot.dcs.exportdata.service.ExportdataService;
import com.ondot.dcs.exportdata.service.TracerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ExportdataController.class)
public class ExportdataControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    ExportdataController exportdataController;

    @MockBean
    ExportdataService exportdataService;

    @MockBean
    TracerService tracerService;

    @Test
    public void getApplicationsCsvTest() throws Exception {
        // when
        ResultActions resultActions = mvc.perform(
                get("/api/v0.9/exportdata/applications/csv")
                        .param("fiTokens", "onebank")
                        .param("startDate", "2020-10-25")
                        .param("endDate", "2020-10-25")
        );

        // then
        resultActions.andExpect(status().isOk())
                .andDo(print());
    }
}
