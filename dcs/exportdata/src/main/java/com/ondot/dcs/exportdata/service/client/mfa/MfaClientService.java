package com.ondot.dcs.exportdata.service.client.mfa;

import com.ondot.dcs.exportdata.dto.core.PaginationResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.mfa.dto.MFAOTPMapping;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MfaClientService extends AbstractFeignClientService {

    private final MfaClient mfaClient;

    public SingleResult<MFAOTPMapping> getMFAOTPMapping(@Nullable String applicationReferenceId) {
        try {
            ResponseEntity<PaginationResult<MFAOTPMapping>> responseEntity = this.mfaClient.getMFAOTPMappingList(null, null, null, null, null, null, applicationReferenceId);

            SingleResult<MFAOTPMapping> singleResult = SingleResult.<MFAOTPMapping>singleResultBuilder()
                    .status(responseEntity.getBody().getStatus())
                    .build();

            if (!responseEntity.getBody().getContents().isEmpty()) {
                singleResult.setContent(responseEntity.getBody().getContents().get(0));
            }

            return singleResult;
        } catch (FeignException e) {
            log.error("getMFAOTPMapping error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<MFAOTPMapping>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
