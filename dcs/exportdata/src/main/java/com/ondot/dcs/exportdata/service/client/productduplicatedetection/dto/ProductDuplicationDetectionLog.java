package com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDuplicationDetectionLog {
    private String applicationReferenceId;
    private Boolean productDuplicationDetectedResult;
}
