package com.ondot.dcs.exportdata.service.client.journey.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Builder
@Getter
@Setter
@ToString
public class JourneyProcessTypeLog {
    String processType;
    JourneyProcessStatus processStatus;
    int processPosition;
    long processId;
    Date createdTime;
    Date lastModifiedTime;
}
