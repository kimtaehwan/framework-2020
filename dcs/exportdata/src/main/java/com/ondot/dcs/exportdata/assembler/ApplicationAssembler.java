package com.ondot.dcs.exportdata.assembler;

import com.ondot.dcs.exportdata.dto.ApplicationData;
import com.ondot.dcs.exportdata.service.client.casaccount.dto.LoginCreationResult;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.CreditReportLog;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.CreditUnderwritingLog;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.IssuanceResponse;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessLogInfo;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessTypeLog;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ApplicationAssembler {
    public List<ApplicationData.Csv> convertToCsvData(List<JourneyProcessLogInfo> journeyProcessLogInfoList) {
        if (journeyProcessLogInfoList == null) {
            return Collections.emptyList();
        }

        return journeyProcessLogInfoList.stream()
                .map(this::convertToListing)
                .collect(Collectors.toList());
    }

    private ApplicationData.Csv convertToListing(JourneyProcessLogInfo journeyProcessLogInfo) {
        Date lastModifiedTime = this.getLastModifiedTime(journeyProcessLogInfo);

        ApplicationData.ApplicationStatus applicationStatus = this.getApplicationStatus(journeyProcessLogInfo);

        Product product = journeyProcessLogInfo.getProductInfo();
        String productName = this.getProductName(product);

        UserVerificationLog userVerificationLog = journeyProcessLogInfo.getUserVerificationLog();
        String dob = this.getDob(userVerificationLog);
        String ssn = this.getSsn(userVerificationLog);
        String phoneNumber = this.getPhoneNumber(userVerificationLog);
        String userVerificationStatus = this.getUserVerificationStatus(userVerificationLog);
        String userVerificationMethod = this.getUserVerificationMethod(userVerificationLog);

        InstantIssuanceInfo instantIssuanceInfo = journeyProcessLogInfo.getInstantIssuanceInfo();
        String address = this.getAddress(instantIssuanceInfo);

        IssuanceResponse issuanceResponse = journeyProcessLogInfo.getIssuanceResponse();
        IssuanceResponse.CardDetails cardDetails = this.getCardDetatils(issuanceResponse);

        String cardType = "";
        String cardNumber = "";

        if (cardDetails != null) {
            cardType = cardDetails.getCardType();
            cardNumber = cardDetails.getCardPan();
        }

        LoginCreationResult loginCreationResult = journeyProcessLogInfo.getLoginCreationResult();
        String email = this.getEmail(loginCreationResult);

        ProductDuplicationDetectionLog productDuplicationDetectionLog = journeyProcessLogInfo.getProductDuplicationDetectionLog();
        String productDuplicateDetectionStatus = this.getProductDuplicationDetectedResult(productDuplicationDetectionLog);

        TermsStatusLog termsStatusLog = journeyProcessLogInfo.getTermsStatusLog();
        String consentAcceptanceStatus = this.getConsentAcceptanceStatus(termsStatusLog);
        Date consentAcceptanceDate = this.getConsentAcceptanceDate(termsStatusLog);

        ESigDocModel eSigDocModel = journeyProcessLogInfo.getESigDocModel();
        Date eSignatureDate = this.getESignatureDate(eSigDocModel);

        RiskReportLog riskReportLog = journeyProcessLogInfo.getRiskReportLog();
        String riskAssessmentStatus = this.getRiskAssessmentStatus(riskReportLog);
        String riskScore = this.getRiskScore(riskReportLog);
        String thresholdRiskScore = this.getThresholdRiskScore(riskReportLog);

        CreditReportLog creditReportLog = journeyProcessLogInfo.getCreditReportLog();
        String creditInquiry = this.getCreditInquiry(creditReportLog);
        String creditScore = this.getCreditScore(creditReportLog);

        UserIdentificationLog userIdentificationLog = journeyProcessLogInfo.getUserIdentificationLog();
        String idVerificationStatus = this.getIdentificationStatus(userIdentificationLog);

        CreditUnderwritingLog creditUnderwritingLog = journeyProcessLogInfo.getCreditUnderwritingLog();
        String creditClassCode = this.getCreditClassCode(creditUnderwritingLog);

        return ApplicationData.Csv.builder()
                .applicationReferenceId(journeyProcessLogInfo.getApplicationReferenceId())
                .lastModifiedTime(lastModifiedTime)
                .productName(productName)
                .applicationStatus(applicationStatus)
                .applicantName(null)
                .dateOfBirth(dob)
                .ssn(ssn)
                .address(address)
                .phoneNumber(phoneNumber)
                .email(email)
                .cardType(cardType)
                .cardNumber(cardNumber)
                .userVerificationStatus(userVerificationStatus)
                .userVerificationMethod(userVerificationMethod)
                .productDuplicateDetectionStatus(productDuplicateDetectionStatus)
                .consentAcceptanceStatus(consentAcceptanceStatus)
                .userAcceptedConsentDocuments("")
                .consentAcceptanceDate(consentAcceptanceDate)
                .eSignatureDate(new Date())
                .riskAssessmentStatus(riskAssessmentStatus)
                .riskScore(riskScore)
                .thresholdRiskScore(thresholdRiskScore)
                .creditInquiry(creditInquiry)
                .creditScore(creditScore)
                .thresholdCreditScore(creditScore)
                .creditClassCode(creditClassCode)
                .userSelectedEligibilityCriteria(null)
                .idVerificationStatus(idVerificationStatus)
                .homeAddressVerificationStatus("homeAddressVerificationStatus")
                .idType(cardType)
                .digitalWallet("digitalWallet")
                .verificationPhoneNumber(phoneNumber)
                .emailAddressForOTP(null)
                .metaData(null)
                .build();
    }

    private String getCreditClassCode(CreditUnderwritingLog creditUnderwritingLog) {
        if ( creditUnderwritingLog == null) {
            return null;
        }

        return creditUnderwritingLog.getCreditClassCode();
    }

    private String getIdentificationStatus(UserIdentificationLog userIdentificationLog) {
        if (userIdentificationLog == null) {
            return null;
        }

        return userIdentificationLog.getIdentificationStatus();
    }

    private String getCreditScore(CreditReportLog creditReportLog) {
        if (creditReportLog == null) {
            return null;
        }

        return creditReportLog.getCreditScore();
    }

    private String getCreditInquiry(CreditReportLog creditReportLog) {
        if (creditReportLog == null) {
            return null;
        }

        return "Approve";
    }

    private String getThresholdRiskScore(RiskReportLog riskReportLog) {
        if (riskReportLog == null) {
            return null;
        }

        // TODO: convert String to Json
        return "500";
    }

    private String getRiskScore(RiskReportLog riskReportLog) {
        if (riskReportLog == null) {
            return null;
        }

        // TODO: convert String to Json

        return "500";
    }

    private String getRiskAssessmentStatus(RiskReportLog riskReportLog) {
        if (riskReportLog == null) {
            return null;
        }

        if (riskReportLog.isAccept()) {
            return "SUCCESS";
        }

        return "FAIL";
    }

    private Date getESignatureDate(ESigDocModel eSigDocModel) {
        if (eSigDocModel == null) {
            return null;
        }
        ZonedDateTime created = eSigDocModel.getCreated();
        if (created == null) {
            return null;
        }
        return Date.from(created.toInstant());
    }

    private Date getConsentAcceptanceDate(TermsStatusLog termsStatusLog) {
        if (termsStatusLog == null) {
            return null;
        }

        return termsStatusLog.getCreatedTime();
    }

    private String getConsentAcceptanceStatus(TermsStatusLog termsStatusLog) {
        if (termsStatusLog == null) {
            return null;
        }

        if (termsStatusLog.getStatus() == 0) {
            return "Pending";
        }

        if (termsStatusLog.getStatus() == 1) {
            return "Accepted";
        }

        if (termsStatusLog.getStatus() == 2) {
            return "Rejected";
        }

        return null;
    }

    private String getProductDuplicationDetectedResult(ProductDuplicationDetectionLog productDuplicationDetectionLog) {
        if (productDuplicationDetectionLog == null || productDuplicationDetectionLog.getProductDuplicationDetectedResult() == null) {
            return "FAIL";
        }

        log.info("productDuplicationDetectionLog: {}", productDuplicationDetectionLog.toString());
        return "SUCCESS";
    }

    private String getUserVerificationMethod(UserVerificationLog userVerificationLog) {
        if (userVerificationLog == null) {
            return null;
        }

        return userVerificationLog.getVerificationFieldType();
    }

    private IssuanceResponse.CardDetails getCardDetatils(IssuanceResponse issuanceResponse) {
        if (issuanceResponse == null) {
            return null;
        }
        IssuanceResponse.CardDetails cardDetails = null;
        Iterator<IssuanceResponse.CardDetails> it = issuanceResponse.getCardDetails().iterator();

        while (it.hasNext()) {
            cardDetails = it.next();
        }

        return cardDetails;
    }

    private String getEmail(LoginCreationResult loginCreationResult) {
        if (loginCreationResult == null) {
            return null;
        }

        return loginCreationResult.getSubscriberUserEmail();
    }

    private String getPhoneNumber(UserVerificationLog userVerificationLog) {
        if (userVerificationLog == null) {
            return null;
        }

        return userVerificationLog.getUserPhoneNumber();
    }

    private String getUserVerificationStatus(UserVerificationLog userVerificationLog) {
        if (userVerificationLog == null) {
            return null;
        }

        return userVerificationLog.getStateStatus();
    }

    private String getAddress(InstantIssuanceInfo instantIssuanceInfo) {
        if (instantIssuanceInfo == null) {
            return null;
        }

        return instantIssuanceInfo.getAddress1();
    }

    private String getDob(UserVerificationLog userVerificationLog) {
        if (userVerificationLog == null) {
            return null;
        }

        return userVerificationLog.getUserDob();
    }

    private String getSsn(UserVerificationLog userVerificationLog) {
        if (userVerificationLog == null) {
            return null;
        }

        return userVerificationLog.getUserSsn();
    }

    private String getProductName(Product product) {
        if (product == null) {
            return null;
        }

        return product.getName();
    }

    private ApplicationData.ApplicationStatus getApplicationStatus(JourneyProcessLogInfo journeyProcessLogInfo) {
        return ApplicationData.ApplicationStatus.APPROVED_ISSUED;
    }

    private Date getLastModifiedTime(JourneyProcessLogInfo journeyProcessLogInfo) {
        if (journeyProcessLogInfo.getJourneyProcessTypeLogList() == null) {
            return null;
        }

        return journeyProcessLogInfo.getJourneyProcessTypeLogList().get(0).getLastModifiedTime();
    }
}
