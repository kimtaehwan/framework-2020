package com.ondot.dcs.exportdata.service.client.casaccount;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.casaccount.dto.LoginCreationResult;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CasAccountClientService extends AbstractFeignClientService {

    private final CasAccountClient casAccountClient;

    public SingleResult<LoginCreationResult> getLoginCreationResult(@NonNull String applicationReferenceId) {
        try {
            final ResponseEntity<SingleResult<LoginCreationResult>> responseEntity = this.casAccountClient.getLoginCreationResult(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getLoginCreationResult error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<LoginCreationResult>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
