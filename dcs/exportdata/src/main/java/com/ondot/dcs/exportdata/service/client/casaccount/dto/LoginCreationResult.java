package com.ondot.dcs.exportdata.service.client.casaccount.dto;

import lombok.*;

import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginCreationResult {
    private String applicationReferenceId;
    private String subscriberReferenceId;
    private String subscriberUserLogin;
    private String subscriberUserStatus;
    private String subscriberFailedReason;
    private String subscriberUserEmail;
    private String subscriberUserFullName;
    private ZonedDateTime created;
    private ZonedDateTime modified;
}
