package com.ondot.dcs.exportdata.service.client.productduplicatedetection;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service.client.dcs-productduplicatedetection.name}", url = "${service.client.dcs-productduplicatedetection.url}")
public interface ProductDuplicationDetectionClient {

    @GetMapping(path = "/mgmt-api/v0.9/productdetectduplicate")
    ResponseResult<ProductDuplicationDetectionLog> getSavedDuplicateProductResultV09(
            @RequestParam(value = "applicationReferenceId") String applicationReferenceId);
}
