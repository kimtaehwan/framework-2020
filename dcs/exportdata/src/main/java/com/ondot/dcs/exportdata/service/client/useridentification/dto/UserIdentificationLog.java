package com.ondot.dcs.exportdata.service.client.useridentification.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UserIdentificationLog {
    private String applicationReferenceId;
    private String subscriberReferenceId;
    private String externalCustomerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> externalIdToName;
    private String identificationStatus;
    private String userFullName;
}
