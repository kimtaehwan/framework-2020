// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.model.cm.request.sms;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class AccountMap {
    String subscriberReferenceId;
    String cardMask;
    String accountMask;
    String cardType;
}
