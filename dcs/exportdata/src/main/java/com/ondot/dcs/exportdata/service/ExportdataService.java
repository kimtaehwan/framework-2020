package com.ondot.dcs.exportdata.service;

import com.ondot.dcs.exportdata.assembler.ApplicationAssembler;
import com.ondot.dcs.exportdata.core.StoragePathConfiguration;
import com.ondot.dcs.exportdata.dto.ApplicationData;
import com.ondot.dcs.exportdata.model.ExportJobDetail;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ExportJobResponse;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ScheduleExport;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessLogInfo;
import com.ondot.dcs.exportdata.utils.CsvUtil;
import com.ondot.dcs.exportdata.utils.DateUtil;
import com.ondot.dcs.exportdata.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExportdataService {

    final StoragePathConfiguration storagePathConfiguration;
    final ApplicationService applicationService;
    final ApplicationAssembler applicationAssembler;
    final EmailService emailService;

//    public ApplicationResponse.Csv getApplicationsResponse(List<String> fiTokens, ZonedDateTime startTime, ZonedDateTime endTime, List<String> emails) {
//
//        for (String token : fiTokens) {
//            final List<String> applicationIdList = this.applicationService.getApplicationIdList(token, startTime, endTime);
//
//            String filename = this.getFilename(token);
//            Path directoryPath = this.getDirectoryPath(filename);
//
//            final List<JourneyProcessLogInfo> journeyProcessLogInfoList
//                    = this.applicationService.getJourneyProcessLogInfoList(directoryPath, applicationIdList);
//
//            List<ApplicationData.Csv> csvAssemberList = this.applicationAssembler.convertToCsvData(journeyProcessLogInfoList);
//
//            String csvFile = FileUtil.getFilename(directoryPath, filename, ".csv");
//            CsvUtil.csvWrite(csvFile, csvAssemberList);
//
//            /**
//             * executedTime
//             * exportedRecordsCount
//             * failureRecordsCount
//             * exportAttemptsCount
//             */
//
//            /**
//             * make zip file
//             * directoryPath
//              */
//
//            // send file using sftp
//        }
//
//        EmailContent emailContent = EmailContent.builder()
//                .fileNames(Arrays.asList("file1", "file2"))
//                .executedTime(ZonedDateTime.now())
//                .exportedRecordsCount(100)
//                .failureRecordsCount(0)
//                .exportAttemptsCount(1)
//                .build();
//
//        for (String toEmail : emails) {
//            emailService.sendMail(toEmail, emailContent);
//        }
//
//        return null;
//    }

    private String getFilename(String token) {
        ZonedDateTime now = ZonedDateTime.now();
        return FileUtil.getDirectoryName(token, now);
    }

    private Path getDirectoryPath(String fileName) {
        String rootPath = storagePathConfiguration.getExportData() + fileName;
        Path storagePath = FileUtil.getPath(rootPath);

        return FileUtil.createDirectory(storagePath);
    }

    public ExportJobResponse.Result executeJob(ExportJobDetail detail) {


        /**
         * fiToken
         * emailAddress1
         * emailAddress2
         * from
         * to
         */
        ZonedDateTime from = detail.getFrom();
        ZonedDateTime to = detail.getTo();
        String fiToken = detail.getFiToken();

        List<String> applicationIdList = applicationService.getApplicationIdList(fiToken, from, to);

        String filename = this.getFilename(fiToken);
        Path directoryPath = this.getDirectoryPath(filename);
        final List<JourneyProcessLogInfo> journeyProcessLogInfoList = this.applicationService.getJourneyProcessLogInfoList(directoryPath, applicationIdList);

        List<ApplicationData.Csv> csvAssemblerList = this.applicationAssembler.convertToCsvData(journeyProcessLogInfoList);

        // save csv file
        String csvFile = FileUtil.getFileName(directoryPath, filename, ".csv");
        CsvUtil.csvWrite(csvFile, csvAssemblerList);

        // csv 파일 압축

        // sftp 파일 전송

        // send email

        return null;
    }

    public ExportJobDetail convertExportJobDetail(ExportJobResponse.Retrieve retrieve) {

        /**
         * DAILY 일 경우, 전날의 data export
         * MONTHLY 일 경우, 매달 1일에만 실행함
         * CUSTOM 일 경우, dateRangeFrom ~ dateRangeTo 의 data export
         */
        ScheduleExport scheduleExport = retrieve.getScheduleExport();

        OffsetDateTime startDate = getStartDate(scheduleExport, retrieve.getDateRangeFrom(), retrieve.getDateRangeTo());
        log.info("schedule - startDate: {}", startDate);
        log.info("schedule - startDate.toZonedDateTime: {}", startDate.toZonedDateTime());
        OffsetDateTime endDate = getEndDate(scheduleExport, retrieve.getDateRangeFrom(), retrieve.getDateRangeTo());
        log.info("schedule - endDate: {}", endDate);
        log.info("schedule - endDate.toZonedDateTime: {}", endDate.toZonedDateTime());

        ExportJobDetail exportJobDetail = ExportJobDetail.builder()
                .fiToken(retrieve.getFiToken())
                .approvedAndCardIssued(retrieve.getApprovedAndCardIssued())
                .approvedAndCardNotIssued(retrieve.getApprovedAndCardNotIssued())
                .notApproved(retrieve.getNotApproved())
                .dataCollectionInprogress(retrieve.getDataCollectionInprogress())
                .dataCollectionFail(retrieve.getDataCollectionFail())
                .emailAddress1(retrieve.getEmailAddress1())
                .emailAddress2(retrieve.getEmailAddress2())
                .scheduleExport(scheduleExport)
                .dateRangeFrom(startDate)
                .dateRangeTo(endDate)
                .from(startDate.toZonedDateTime())
                .to(endDate.toZonedDateTime())
                .build();

        return exportJobDetail;
    }

    private OffsetDateTime getStartDate(ScheduleExport schedule, OffsetDateTime from, OffsetDateTime to) {
        // from: 오늘이 2021-01-14 라면, 2021-01-13 00:00:00 이다.
        // to: 오늘이 2021-01-14 라면, 2021-01-13 23:59:59 이다.
        if (ScheduleExport.DAILY.equals(schedule)) {
            return DateUtil.getStartDate();
        }

        /**
         * 매달 1일 이어야 실행됨. => 해당월이 아닌 전달 1일 ~ 말일 까지 실행.
         * 전달 1일 ~ 말일 까지 기간으로 설정
         * from: 오늘이 1일 인지 체크해서, 2021-01-01 이라면, 2020-12-01 00:00:00 이다.
         * to: 오늘이 1인인지 체크해서, 2021-01-01 이라면, 2020-12-31 23:59:59 이다.
          */
        if (ScheduleExport.MONTHLY.equals(schedule)) {
            return DateUtil.getStartDateOfMonth();
        }

        if (ScheduleExport.CUSTOM.equals(schedule)) {
            LocalDateTime startOfDay = DateUtil.atStartOfDay(from.toLocalDateTime());
            return DateUtil.convertToOffsetDateTime(startOfDay);
        }

        return null;
    }

    private OffsetDateTime getEndDate(ScheduleExport schedule, OffsetDateTime from, OffsetDateTime to) {
        if (ScheduleExport.DAILY.equals(schedule)) {
            return DateUtil.getEndDate();
        }

        if (ScheduleExport.MONTHLY.equals(schedule)) {
            return DateUtil.getEndDateOfMonth();
        }

        if (ScheduleExport.CUSTOM.equals(schedule)) {
            LocalDateTime endOfDay = DateUtil.atEndOfDay(to.toLocalDateTime());
            return DateUtil.convertToOffsetDateTime(endOfDay);
        }

        return null;

    }

    public boolean checkPassMonthly(ZonedDateTime now, ScheduleExport scheduleExport) {

        if (scheduleExport.equals(ScheduleExport.MONTHLY)) {
            log.info("now: {}", now);
            log.info("day: {}", now.getDayOfMonth());
        }

        return false;
    }
}
