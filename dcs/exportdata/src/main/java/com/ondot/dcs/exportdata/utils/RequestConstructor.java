package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.model.cm.request.email.*;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class RequestConstructor {

    private static final String MESSAGE_TYPE_EMAIL = "email";
    private static final String EMAIL_FROM = "ondot@ondotsystems.com";
    private static final String EMAIL_FROM_NAME = "ONDOT DCS";
    private static final String EMAIL_SUBJECT = "Data export from Ondot Digital Account Opening Services to Financial Institutions";

    public static List<CMRequestEmail> requestConstructorForEmail(String appToken, String fiToken, String toEmail, EmailContent emailContent) {
        List<CMRequestEmail> requests = new ArrayList<>();
        requests.add(cmRequestEmail(appToken, fiToken, toEmail, emailContent));

        return requests;
    }

    private static CMRequestEmail cmRequestEmail(String appToken, String fiToken, String toEmail, EmailContent emailContent) {
        return CMRequestEmail.builder()
                .appToken(appToken)
                .fiToken(fiToken)
                .retryCount(1)
                .alertCategory("transaction")
                .alertTypeCode("txn")
                .notificationReferenceId("147")
                .accountMap(
                        AccountMap.builder()
                                .subscriberReferenceId("9af0e2e2ondotaf5csub4721be4da54f113da2a6-5-3-0-0")
                                .cardMask("XXXX XXXX XXXX 1001")
                                .accountMask("XXXX XXXX 0003")
                                .cardType("Debit")
                                .build()
                )
                .transactionMap(TransactionMap.builder()
                        .notificationFlag("true")
                        .build())
                .channelPriority(ChannelPriority.builder()
                        .jsonMember1(createStringList(MESSAGE_TYPE_EMAIL))
                        .build())
                .messageMap(MessageMap.builder()
                        .email(createMessageListEmail(toEmail, emailContent))
                        .build())
                .build();
    }

    private static List<EmailItem> createMessageListEmail(String toEmail, EmailContent emailContent) {
        List<EmailItem> emailItems = new ArrayList<>();
        emailItems.add(EmailItem.builder()
                .from(EMAIL_FROM)
                .fromName(EMAIL_FROM_NAME)
                .subject(EMAIL_SUBJECT)
                .to(toEmail)
                .message(constructEmailMessageBody(emailContent))
                .applyCloudTemplate(false)
                .build());

        return emailItems;
    }

    private static String constructEmailMessageBody(EmailContent emailContent) {
        // TODO: emailContent.getFileNames().get(0)
        String fileName = "onebank_" + ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String now = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd/ HH:mm:ss"));
        long exportedRecordsCount = 14;
        long failureRecordsCount = 0;
        int exportAttemptsCount = 1;

        return "<strong>File Name: " + fileName + "</strong>" +
                "<p>Export Job executed on:  " + now
                + "</p><p>Number of records exported: " + exportedRecordsCount + "</p>"
                + "<p>Number of failure records: " + failureRecordsCount + "</p>"
                + "<p>Number of Export attempts: " + exportAttemptsCount + "</p>"
                ;
    }

    private static List<String> createStringList(String notificationType) {
        List<String> stringList = new ArrayList<>();
        stringList.add(notificationType);
        return stringList;
    }
}
