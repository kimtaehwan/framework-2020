package com.ondot.dcs.exportdata.model.cm.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response{

	@JsonProperty("requestId")
	private String requestId;

	@JsonProperty("messageCode")
	private String messageCode;

	@JsonProperty("additionalData")
	private AdditionalData additionalData;

	@JsonProperty("message")
	private String message;

	@JsonProperty("notificationReferenceId")
	private String notificationReferenceId;

	@JsonProperty("statusCode")
	private int statusCode;
}
