package com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.data;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author jared (Sangwon.Park@ondotsystems.com)
 * @version 1.0 2020/10/23
 */
@Data
@Builder
@ToString
public class HttpReqData {

	String url;
	String method;
	String contentType;
	String body;

}
