package com.ondot.dcs.exportdata.service.client.eligibility;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.eligibility.dto.EligibilityLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-eligibility.name}", url = "${service.client.dcs-eligibility.url}")
public interface EligibilityClient {
    @GetMapping(value = "/v2/eligible", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<EligibilityLog>> getEligibilityLog(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
