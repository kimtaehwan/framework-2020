package com.ondot.dcs.exportdata.service.client.journey.dto;

import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class ApplicationReference {
    @NonNull
    String applicationReferenceId;

    @NonNull
    String appToken;

    @NonNull
    String fiToken;

    String journeyPointId;
    Long journeyId;
    JourneyStatus journeyStatus;

    Long processId;
    String processType;
    JourneyProcessStatus processStatus;
    Integer processPosition;
    Integer processLastPosition;

    @NonNull
    String deviceUniqueId;
    String subscriberId;
    String phoneNumber;

    @NonNull Date createdTime;
    Date lastModifiedTime;

    CustomerInfo customerInfo;
    Product productInfo;
}
