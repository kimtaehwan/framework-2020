package com.ondot.dcs.exportdata.service.client.esignature;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-esignature.name}", url = "${service.client.dcs-esignature.url}")
public interface ESignatureClient {

    @GetMapping(value = "/v2/signature")
    ResponseEntity<SingleResult<ESigDocModel>> getSignature(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId,
            @RequestHeader(value = "x-ondotsystems-fileType", required = false, defaultValue = "path") String fileType);
}
