package com.ondot.dcs.exportdata.model.cm.request.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class TransactionMap{

	@JsonProperty("notificationFlag")
	private String notificationFlag;
}
