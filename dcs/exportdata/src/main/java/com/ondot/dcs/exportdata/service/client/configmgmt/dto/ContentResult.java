package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import com.ondot.dcs.exportdata.dto.core.BaseResult;
import com.ondot.dcs.exportdata.dto.core.ResultStatus;
import lombok.*;
import org.springframework.util.MultiValueMap;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ContentResult<T> extends BaseResult {

    List<T> content;

    @Builder(builderMethodName = "listResultBuilder")
    public ContentResult(ResultStatus status, MultiValueMap<String, String> httpHeaders, List<T> content) {
        super(status, httpHeaders);
        this.content = content;
    }

}
