package com.ondot.dcs.exportdata.model.cm.request.email;


import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class EmailContent {
    private List<String> fileNames;
    private ZonedDateTime executedTime;
    private long exportedRecordsCount;
    private long failureRecordsCount;
    private int exportAttemptsCount;
}
