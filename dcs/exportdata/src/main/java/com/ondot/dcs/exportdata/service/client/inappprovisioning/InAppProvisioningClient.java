package com.ondot.dcs.exportdata.service.client.inappprovisioning;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.inappprovisioning.dto.PushProvisioningResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-inappprovisioning.name}", url = "${service.client.dcs-inappprovisioning.url}")
public interface InAppProvisioningClient {

    @GetMapping(value = "/v1/iap/result")
    ResponseEntity<SingleResult<PushProvisioningResult>> getPushProvisioningResult(
            @RequestHeader(value = "x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
