package com.ondot.dcs.exportdata.service.client.riskassessment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RiskReportLog {
    String applicationId;
    String fiToken;
    String journeyId;
    boolean accept;
    Date reqTime;
    Date respTime;
    HttpReqData requestData;
    HttpRespData responseData;
}
