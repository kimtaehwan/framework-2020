package com.ondot.dcs.exportdata.model.cm.request.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class CMRequestEmail {

	@JsonProperty("accountMap")
	private AccountMap accountMap;

	@JsonProperty("transactionMap")
	private TransactionMap transactionMap;

	@JsonProperty("appToken")
	private String appToken;

	@JsonProperty("retryCount")
	private int retryCount;

	@JsonProperty("alertTypeCode")
	private String alertTypeCode;

	@JsonProperty("channelPriority")
	private ChannelPriority channelPriority;

	@JsonProperty("fiToken")
	private String fiToken;

	@JsonProperty("messageMap")
	private MessageMap messageMap;

	@JsonProperty("notificationReferenceId")
	private String notificationReferenceId;

	@JsonProperty("alertCategory")
	private String alertCategory;
}
