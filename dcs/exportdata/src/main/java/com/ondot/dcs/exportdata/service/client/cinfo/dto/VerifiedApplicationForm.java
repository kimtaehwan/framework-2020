package com.ondot.dcs.exportdata.service.client.cinfo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class VerifiedApplicationForm {
    String applicationReferenceId;
    Long applicationFormId;
    @JsonProperty(value = "applicationForm")
    List<VerifiedForm> verifiedFormList;
}
