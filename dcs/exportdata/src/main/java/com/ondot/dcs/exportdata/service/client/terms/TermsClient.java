package com.ondot.dcs.exportdata.service.client.terms;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-terms.name}", url = "${service.client.dcs-terms.url}")
public interface TermsClient {

    @GetMapping(value = "/v2/term", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<TermsStatusLog>> getCustomerTerm(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
