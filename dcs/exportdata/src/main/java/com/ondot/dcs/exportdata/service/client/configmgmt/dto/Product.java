// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import lombok.Getter;
import lombok.ToString;

import java.time.ZonedDateTime;

public class Product {

    private Product() {
    }

    @Getter
    @ToString
    public static class Retrieve {
        private Long id;
        private ProductType type;
        private String name;
        private ImageFile image;
        private String description;
        private ZonedDateTime created;
        private ZonedDateTime lastModified;
        private String status;
        private String creator;
    }

    @Getter
    @ToString
    public static class Listing {
        private Long id;
        private ProductType type;
        private String name;
        private ImageFile image;
        private String description;
        private ZonedDateTime created;
        private ZonedDateTime lastModified;
        private String status;
        private String creator;
    }

}
