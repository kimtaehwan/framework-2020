package com.ondot.dcs.exportdata.service.client.creditunderwriting;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.CreditUnderwritingLog;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CreditUnderwritingClientService extends AbstractFeignClientService {

    private final CreditUnderwritingClient creditUnderwritingClient;

    public CreditUnderwritingLog getLogs(List<String> applicationReferenceIds) {
        try {
            final ResponseResult<CreditUnderwritingLog> responseResult = this.creditUnderwritingClient.getLogs(applicationReferenceIds);
            return Optional.ofNullable(responseResult)
                    .map(ResponseResult::getData)
                    .orElse(null);
        } catch (FeignException e) {
            log.error("getLogs error. applicationReferenceId={}, ex={}", applicationReferenceIds, e.toString());
            return null;
        }
    }
}
