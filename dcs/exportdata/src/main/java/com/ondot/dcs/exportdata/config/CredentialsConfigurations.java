package com.ondot.dcs.exportdata.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

@Slf4j
@DependsOn
public class CredentialsConfigurations {

    private static String basePath = "/opt/ondot/etc/exportdata";

    public void injectStringForUnitTesting(String basePath){
        this.basePath = basePath;
    }

    @Bean
    @Profile("!local")
    public static Properties updatePropertiesWithMountedSecrets() throws IOException {
        log.info("==========================================");
        log.info("Updating Properties with mounted secrets");
        log.info("==========================================");

        String cmBasicAuthUsername = Files.readAllLines(Paths.get(basePath+ "/cmbasicauthusername").toAbsolutePath()).get(0);
        String cmBasicAuthPassword = Files.readAllLines(Paths.get(basePath+"/cmbasicauthpassword").toAbsolutePath()).get(0);
        String deploymentToken = Files.readAllLines(Paths.get(basePath+"/cmdeploymenttoken").toAbsolutePath()).get(0);
        String cmFiToken = Files.readAllLines(Paths.get(basePath+"/cmfitoken").toAbsolutePath()).get(0);
        String cmAppToken = Files.readAllLines(Paths.get(basePath+"/cmapptoken").toAbsolutePath()).get(0);
        String cmURL = Files.readAllLines(Paths.get(basePath+"/cmurl").toAbsolutePath()).get(0);

        log.info("# cmBasicAuthUsername: {}", cmBasicAuthUsername);
        log.info("# cmBasicAuthPassword: {}", cmBasicAuthPassword);
        log.info("# deploymentToken: {}", deploymentToken);
        log.info("# cmFiToken: {}", cmFiToken);
        log.info("# cmAppToken: {}", cmAppToken);
        log.info("# cmURL: {}", cmURL);

        Properties properties = new Properties();
        properties.put("cm.required.basic.auth.username", cmBasicAuthUsername);
        properties.put("cm.required.basic.auth.password", cmBasicAuthPassword);
        properties.put("cm.required.deployment.token",deploymentToken);
        properties.put("cm.body.fitoken",cmFiToken);
        properties.put("cm.body.apptoken", cmAppToken);
        properties.put("cm.url",cmURL);
        return properties;
    }
}
