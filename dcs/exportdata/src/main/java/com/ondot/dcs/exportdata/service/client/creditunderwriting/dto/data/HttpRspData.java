package com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.data;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author jared (Sangwon.Park@ondotsystems.com)
 * @version 1.0 2020/10/23
 */
@Data
@ToString
@Builder
public class HttpRspData
{

	Integer httpCode;
	String httpStatus;
	String httpDescription;
	String contentType;
	String body;
}
