package com.ondot.dcs.exportdata.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class OndotConfig {

    private String deploymentTokenKey= "deploymentToken";

    public String getDeploymentTokenKey(){
        return deploymentTokenKey;
    }

    @Value("${cm.required.deployment.token}")
    private String deploymentToken;

    public String getDeploymentToken(){
        return deploymentToken;
    }

    @Value("${cm.required.basic.auth.username}")
    private String basicAuthUsername;

    public String getBasicAuthUsername(){
        return basicAuthUsername;
    }

    @Value("${cm.required.basic.auth.password}")
    private String basicAuthPassword;

    public String getBasicAuthPassword(){
        return basicAuthPassword;
    }

    @Value("${cm.url}")
    private String cmURL;

    public String getCmURL(){
        return cmURL;
    }

    @Value("${cm.body.apptoken}")
    private String appToken;

    public String getAppToken(){
        return appToken;
    }

    @Value("${cm.body.fitoken}")
    private String fiToken;

    public String getFiToken(){
        return fiToken;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
