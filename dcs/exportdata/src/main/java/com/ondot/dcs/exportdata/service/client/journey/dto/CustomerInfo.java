package com.ondot.dcs.exportdata.service.client.journey.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@EqualsAndHashCode
@ToString
public class CustomerInfo {
    private String firstName;
    private String lastName;
}
