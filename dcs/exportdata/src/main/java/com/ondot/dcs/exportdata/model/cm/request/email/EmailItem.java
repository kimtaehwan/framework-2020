package com.ondot.dcs.exportdata.model.cm.request.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class EmailItem{

    @JsonProperty("from")
    private String from;

	@JsonProperty("applyCloudTemplate")
	private boolean applyCloudTemplate;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("fromName")
	private String fromName;

	@JsonProperty("to")
	private String to;

	@JsonProperty("message")
	private String message;
}
