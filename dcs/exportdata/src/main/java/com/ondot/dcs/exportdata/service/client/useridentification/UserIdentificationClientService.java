package com.ondot.dcs.exportdata.service.client.useridentification;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserIdentificationClientService extends AbstractFeignClientService {

    private final UserIdentificationClient userIdentificationClient;

    public UserIdentificationLog getUserIdentificationV09(@NonNull String applicationReferenceId) {
        try {
            final ResponseResult<UserIdentificationLog> responseResult = this.userIdentificationClient.getUserIdentificationV09(applicationReferenceId);
            return Optional.ofNullable(responseResult).map(ResponseResult::getData).orElse(null);
        } catch (FeignException e) {
            log.error("getUserIdentificationV09 error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return null;
        }
    }
}
