package com.ondot.dcs.exportdata.service.client.creditunderwriting;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.CreditUnderwritingLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${service.client.dcs-creditunderwriting.name}", url = "${service.client.dcs-creditunderwriting.url}")
public interface CreditUnderwritingClient {
    @GetMapping(value = "/api/v0.9/logs")
    ResponseResult<CreditUnderwritingLog> getLogs(
            @RequestParam("applicationReferenceId") List<String> applicationReferenceIds);
}
