package com.ondot.dcs.exportdata.service.client.creditcheck.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class HttpRespData {
    Integer httpCode;
    String httpStatus;
    String httpDescription;
    String contentType;
    String body;
}
