package com.ondot.dcs.exportdata.service.client.idv.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@EqualsAndHashCode
@ToString
public class Footmark {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String deviceUniqueId;
    String clientIp;
    Double latitude;
    Double longitude;
}
