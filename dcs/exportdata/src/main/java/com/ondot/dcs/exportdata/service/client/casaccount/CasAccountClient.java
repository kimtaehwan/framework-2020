package com.ondot.dcs.exportdata.service.client.casaccount;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.casaccount.dto.LoginCreationResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-casaccount.name}", url = "${service.client.dcs-casaccount.url}")
public interface CasAccountClient {

    @GetMapping(value = "/v1/user", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<LoginCreationResult>> getLoginCreationResult(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
