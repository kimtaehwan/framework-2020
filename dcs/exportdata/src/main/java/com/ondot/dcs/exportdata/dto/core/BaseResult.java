// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.dto.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.util.MultiValueMap;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class BaseResult {
    ResultStatus status;

    @JsonIgnore
    MultiValueMap<String, String> httpHeaders;

    @Builder(builderMethodName = "baseResultBuilder")
    public BaseResult(ResultStatus status, MultiValueMap<String, String> httpHeaders) {
        this.status = status;
        this.httpHeaders = httpHeaders;
    }

}
