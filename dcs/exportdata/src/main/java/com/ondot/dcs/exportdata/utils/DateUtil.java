package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.model.enums.DateType;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ScheduleExport;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static Integer getIntegerDate(String date, DateType dateType) {
        String[] dates = date.split("-");

        if ( DateType.YEAR.equals(dateType) ) {
            return Integer.parseInt(dates[0]);
        }

        if ( DateType.MONTH.equals(dateType) ) {
            return Integer.parseInt(dates[1]);
        }

        if ( DateType.DAY.equals(dateType) ) {
            return Integer.parseInt(dates[2]);
        }

        return null;
    }

    public static ZonedDateTime getMinZonedDateTime(String date) {
        Integer year = DateUtil.getIntegerDate(date, DateType.YEAR);
        Integer month = DateUtil.getIntegerDate(date, DateType.MONTH);
        Integer day = DateUtil.getIntegerDate(date, DateType.DAY);

        return ZonedDateTime.parse(
                ZonedDateTime.of(year, month, day, 0, 0, 0, 0, ZoneId.of("UTC"))
                .format(dateTimeFormatter),
                dateTimeFormatter
        );
    }

    public static ZonedDateTime getMaxZonedDateTime(String date) {
        Integer year = DateUtil.getIntegerDate(date, DateType.YEAR);
        Integer month = DateUtil.getIntegerDate(date, DateType.MONTH);
        Integer day = DateUtil.getIntegerDate(date, DateType.DAY);

        return ZonedDateTime.parse(
                ZonedDateTime.of(year, month, day, 23, 59, 59, 999999999, ZoneId.of("UTC"))
                        .format(dateTimeFormatter),
                dateTimeFormatter
        );
    }

    public static LocalDateTime atStartOfDay(LocalDateTime dateTime) {
        return dateTime.with(LocalTime.MIN);
    }

    public static LocalDateTime atEndOfDay(LocalDateTime dateTime) {
        return dateTime.with(LocalTime.MAX);
    }

    public static OffsetDateTime convertToOffsetDateTime(LocalDateTime dateTime) {
        ZoneOffset offset = OffsetDateTime.now().getOffset();
        return dateTime.atOffset(offset);
    }


    public static boolean checkFirstDay(OffsetDateTime dateTime) {
        int day = dateTime.getDayOfMonth();

        if ( day == 1 ) {
            return true;
        }

        return false;
    }

    public static OffsetDateTime getStartDate() {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime minusDay = now.minusDays(1);
        LocalDateTime startOfDay = DateUtil.atStartOfDay(minusDay.toLocalDateTime());
        return DateUtil.convertToOffsetDateTime(startOfDay);
    }

    public static OffsetDateTime getStartDateOfMonth() {
        OffsetDateTime now = OffsetDateTime.now();
        LocalDate initial = now.minusMonths(1).toLocalDate();
        LocalDate start = initial.withDayOfMonth(1);

        LocalDateTime startLocalTime = start.atStartOfDay();
        return DateUtil.convertToOffsetDateTime(startLocalTime);
    }

    public static OffsetDateTime getEndDate() {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime minusDay = now.minusDays(1);
        LocalDateTime endOfDay = DateUtil.atEndOfDay(minusDay.toLocalDateTime());
        return DateUtil.convertToOffsetDateTime(endOfDay);
    }

    public static OffsetDateTime getEndDateOfMonth() {
        OffsetDateTime now = OffsetDateTime.now();
        LocalDate initial = now.minusMonths(1).toLocalDate();
        LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());

        LocalDateTime endLocalTime = end.atStartOfDay();
        LocalDateTime endOfDay = DateUtil.atEndOfDay(endLocalTime);
        return DateUtil.convertToOffsetDateTime(endOfDay);
    }
}
