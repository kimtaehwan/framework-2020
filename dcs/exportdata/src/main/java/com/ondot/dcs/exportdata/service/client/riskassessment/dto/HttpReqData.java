// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.riskassessment.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class HttpReqData {

    String url;
    String method;
    String contentType;
    String body;

}
