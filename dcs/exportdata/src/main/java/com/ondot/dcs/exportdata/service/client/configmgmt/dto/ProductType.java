// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import lombok.ToString;

@ToString
public enum ProductType {
    CREDIT_CARD,
    DEBIT_CARD,
    LOAN;
}
