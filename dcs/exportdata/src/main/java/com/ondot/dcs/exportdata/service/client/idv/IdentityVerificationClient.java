package com.ondot.dcs.exportdata.service.client.idv;

import com.ondot.dcs.exportdata.dto.core.ListResult;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${service.client.dcs-idv.name}", url = "${service.client.dcs-idv.url}")
public interface IdentityVerificationClient {

    @GetMapping(value = "/api/v1/documents/{ticketId}/logs", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    ResponseEntity<ListResult<DocumentResultLog>> getDocumentResultLogList(@PathVariable("ticketId") String ticketId);
}
