package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

public enum ScheduleExport {
    DAILY, MONTHLY, CUSTOM
}
