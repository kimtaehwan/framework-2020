package com.ondot.dcs.exportdata.service.client.eligibility;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.eligibility.dto.EligibilityLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EligibilityClientService extends AbstractFeignClientService {

    private final EligibilityClient eligibilityClient;

    public SingleResult<EligibilityLog> getEligibilityLog(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<EligibilityLog>> responseEntity = this.eligibilityClient.getEligibilityLog(applicationReferenceId);

            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getEligibilityLog error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<EligibilityLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
