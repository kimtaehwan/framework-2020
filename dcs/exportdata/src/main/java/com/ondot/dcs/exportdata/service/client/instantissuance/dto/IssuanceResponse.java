package com.ondot.dcs.exportdata.service.client.instantissuance.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Set;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class IssuanceResponse {
    private String applicationReferenceId;
    private String subscriberReferenceId;
    private String externalReferenceId;
    private String cardIssuancePolicy;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cardProvisionStatus;
    private String statusDisplay;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserDetails userDetails;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<CardDetails> cardDetails;

    @Data
    @Builder
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserDetails{
        private String firstName;
        private String lastName;
        private String address;
        private String city;
        private String state;
        private String zip;
        private String country;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CardDetails{
        private Long cardReferenceId;
        private String cardType;
        private String cardPan;
        private String cardExpDate;
        private String casAddCardStatus;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String casMessageCode;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String cardImage;

        @Override
        public String toString() {
            if(null != cardImage && !cardImage.isEmpty() && cardImage.length()>25) {
                String cardImageConcatenated = cardImage.substring(0,25);
                return "CardDetails{" +
                        "cardReferenceId=" + cardReferenceId +
                        ", cardType='" + cardType + '\'' +
                        ", cardPan='" + cardPan + '\'' +
                        ", cardExpDate='" + cardExpDate + '\'' +
                        ", casAddCardStatus='" + casAddCardStatus + '\'' +
                        ", casMessageCode='" + casMessageCode + '\'' +
                        ", cardImageConcatenated='" + cardImageConcatenated + '\'' +
                        '}';
            }else{
                return "CardDetails{" +
                        "cardReferenceId=" + cardReferenceId +
                        ", cardType='" + cardType + '\'' +
                        ", cardPan='" + cardPan + '\'' +
                        ", cardExpDate='" + cardExpDate + '\'' +
                        ", casAddCardStatus='" + casAddCardStatus + '\'' +
                        ", casMessageCode='" + casMessageCode + '\'' +
                        ", cardImage='" + cardImage + '\'' +
                        '}';
            }
        }
    }
}
