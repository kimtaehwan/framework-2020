package com.ondot.dcs.exportdata.dto;

import lombok.*;

import java.util.Date;

public class ApplicationData {

    @Getter
    @ToString
    @Builder
    public static class Csv {
        private String applicationReferenceId;
        private Date lastModifiedTime;
        private String productName;
        private ApplicationStatus applicationStatus;
        private String applicantName;
        private String dateOfBirth;
        private String ssn;
        private String address;
        private String phoneNumber;
        private String email;
        private String cardType;
        private String cardNumber;
        private String userVerificationStatus;
        private String userVerificationMethod; // OPT/SSN/DOB
        private String productDuplicateDetectionStatus;
        private String consentAcceptanceStatus;
        private String userAcceptedConsentDocuments;
        private Date consentAcceptanceDate;
        private Date eSignatureDate;
        private String riskAssessmentStatus;
        private String riskScore;
        private String thresholdRiskScore;
        private String creditInquiry;
        private String creditScore;
        private String thresholdCreditScore;
        private String creditClassCode;
        private String userSelectedEligibilityCriteria;
        private String idVerificationStatus;
        private String homeAddressVerificationStatus;
        private String idType;  // Driving License/ Passport
        private String digitalWallet;
        private String verificationPhoneNumber;
        private String emailAddressForOTP; // Email address used for sending OTP
        private String metaData;
    }

    public enum ApplicationStatus {
        APPROVED_ISSUED("Approved - Issued"),
        APPROVED_NOT_ISSUED("Approved - Not Issued"),
        CANCELLED("Cancelled"),
        IN_PROGRESS("In Progress"),
        FAIL("Fail"),
        ABANDONED("Abandoned");

        private String message;
        ApplicationStatus(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
    }
}
