package com.ondot.dcs.exportdata.service;

import brave.Tracer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TracerService {

    final Tracer tracer;

    public String getTraceId() {
        return tracer.currentSpan().context().traceIdString();
    }
}
