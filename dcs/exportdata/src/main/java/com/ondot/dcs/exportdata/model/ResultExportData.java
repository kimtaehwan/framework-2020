package com.ondot.dcs.exportdata.model;

import lombok.*;

@Getter
@ToString
@Builder
public class ResultExportData {
    private String fiToken;
}
