package com.ondot.dcs.exportdata.model.cm.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdditionalData{

	@JsonProperty("147")
	private JsonMember147 jsonMember147;
}
