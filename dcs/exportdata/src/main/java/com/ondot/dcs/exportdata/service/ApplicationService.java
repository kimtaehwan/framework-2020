package com.ondot.dcs.exportdata.service;

import com.ondot.dcs.exportdata.dto.core.ListResult;
import com.ondot.dcs.exportdata.dto.core.PaginationResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.casaccount.CasAccountClientService;
import com.ondot.dcs.exportdata.service.client.casaccount.dto.LoginCreationResult;
import com.ondot.dcs.exportdata.service.client.cinfo.CustomerInfoClientService;
import com.ondot.dcs.exportdata.service.client.cinfo.dto.VerifiedApplicationForm;
import com.ondot.dcs.exportdata.service.client.creditcheck.CreditCheckClientService;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.CreditReportLog;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.HttpReqData;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.CreditUnderwritingClientService;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.CreditUnderwritingLog;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.data.HttpRspData;
import com.ondot.dcs.exportdata.service.client.eligibility.EligibilityClientService;
import com.ondot.dcs.exportdata.service.client.eligibility.dto.EligibilityLog;
import com.ondot.dcs.exportdata.service.client.esignature.ESignatureClientService;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import com.ondot.dcs.exportdata.service.client.idv.IdentityVerificationClientService;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import com.ondot.dcs.exportdata.service.client.inappprovisioning.InAppProvisioningClientService;
import com.ondot.dcs.exportdata.service.client.inappprovisioning.dto.PushProvisioningResult;
import com.ondot.dcs.exportdata.service.client.instantissuance.InstantIssuanceClientService;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import com.ondot.dcs.exportdata.service.client.journey.JourneyClientService;
import com.ondot.dcs.exportdata.service.client.journey.dto.*;
import com.ondot.dcs.exportdata.service.client.mfa.MfaClientService;
import com.ondot.dcs.exportdata.service.client.mfa.dto.MFAOTPMapping;
import com.ondot.dcs.exportdata.service.client.product.ProductInfoClientService;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.ProductDuplicationDetectionClientService;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import com.ondot.dcs.exportdata.service.client.regionvalidator.RegionValidatorClientService;
import com.ondot.dcs.exportdata.service.client.regionvalidator.dto.GeolocationRef;
import com.ondot.dcs.exportdata.service.client.riskassessment.RiskAssessmentClientService;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.HttpRespData;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import com.ondot.dcs.exportdata.service.client.terms.TermsClientService;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import com.ondot.dcs.exportdata.service.client.useridentification.UserIdentificationClientService;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import com.ondot.dcs.exportdata.service.client.userverification.UserVerificationClientService;
import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import com.ondot.dcs.exportdata.utils.FileUtil;
import com.ondot.dcs.exportdata.utils.ImageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationService {

    private static final int PAGE = 0;
    private static final int SIZE = 100;

    private final JourneyClientService journeyClientService;
    private final MfaClientService mfaClientService;
    private final ProductInfoClientService productInfoClientService;
    private final IdentityVerificationClientService identityVerificationClientService;
    private final CustomerInfoClientService customerInfoClientService;
    private final CreditCheckClientService creditCheckClientService;
    private final RiskAssessmentClientService riskAssessmentClientService;
    private final RegionValidatorClientService regionValidatorClientService;
    private final TermsClientService termsClientService;
    private final EligibilityClientService eligibilityClientService;
    private final ESignatureClientService eSignatureClientService;
    private final InstantIssuanceClientService instantIssuanceClientService;
    private final InAppProvisioningClientService inAppProvisioningClientService;
    private final CasAccountClientService casAccountClientService;
    private final UserIdentificationClientService userIdentificationClientService;
    private final UserVerificationClientService userVerificationClientService;
    private final ProductDuplicationDetectionClientService productDuplicationDetectionClientService;
    private final CreditUnderwritingClientService creditUnderwritingClientService;

    public List<String> getApplicationIdList(String token, ZonedDateTime startTime, ZonedDateTime endTime) {
        int totalPages = this.getTotalPages(PAGE, SIZE, token, startTime, endTime);
        List<String> applicationIdList = new ArrayList<>();

        for ( int i = 0; i < totalPages; i++ ) {
            List<String> idList = this.getReferenceList(i, SIZE, startTime, endTime, token);
            applicationIdList = Stream.concat(applicationIdList.stream(), idList.stream())
                    .collect(Collectors.toList());
        }

        return applicationIdList;
    }

    private List<String> getReferenceList(int page, int size, ZonedDateTime startTime, ZonedDateTime endTime, String token) {
        final ResponseEntity<PaginationResult<ApplicationReference>> paginationResultResponseEntity
                = this.journeyClientService.getReferenceList(page, size, startTime, endTime, token);
        List<String> idList = new ArrayList<>();
        List<ApplicationReference> applicationReferenceList = paginationResultResponseEntity.getBody().getContents();
        for (ApplicationReference applicationReference : applicationReferenceList) {
            idList.add(applicationReference.getApplicationReferenceId());
        }

        return idList;
    }

    private int getTotalPages(int page, int size, String token, ZonedDateTime startTime, ZonedDateTime endTime) {
        final ResponseEntity<PaginationResult<ApplicationReference>> paginationResultResponseEntity
                = this.journeyClientService.getReferenceList(page, size, startTime, endTime, token);

        return paginationResultResponseEntity.getBody().getTotalPages();
    }

    public List<JourneyProcessLogInfo> getJourneyProcessLogInfoList(Path directoryPath, List<String> applicationIdList) {
        List<JourneyProcessLogInfo> journeyProcessLogInfoList = new ArrayList<>();

        for (String applicationReferenceId : applicationIdList) {
            Path referencePath = FileUtil.createDirectoryPath(directoryPath, applicationReferenceId);
            Path referenceDirectoryPath = FileUtil.createDirectory(referencePath);

            journeyProcessLogInfoList.add(this.getJourneyProcessLogInfo(applicationReferenceId, referenceDirectoryPath));
        }

        return journeyProcessLogInfoList;
    }

    private JourneyProcessLogInfo getJourneyProcessLogInfo(String applicationReferenceId, Path referenceDirectoryPath) {
        final ResponseEntity<SingleResult<JourneyProcessLogInfo>> processLogInfo = this.journeyClientService.getReferenceJourneyProcessLogInfo(applicationReferenceId);
        JourneyProcessLogInfo journeyProcessLogInfo = processLogInfo.getBody().getContent();
        journeyProcessLogInfo.setApplicationReferenceId(applicationReferenceId);
        Path processTypePath = null;

        for (JourneyProcessTypeLog journeyProcessTypeLog : journeyProcessLogInfo.getJourneyProcessTypeLogList()) {
            String journeyProcessType = journeyProcessTypeLog.getProcessType();
            JourneyProcessStatus journeyProcessStatus = journeyProcessTypeLog.getProcessStatus();

            if ( JourneyProcessStatus.NOT_STARTED.equals(journeyProcessStatus)
                    || JourneyProcessStatus.ABANDONED.equals(journeyProcessStatus) ) {
                continue;
            }

            processTypePath = Paths.get(referenceDirectoryPath.toAbsolutePath().toString(), journeyProcessType)
                    .toAbsolutePath()
                    .normalize();

            Path directoryPath = FileUtil.createDirectory(processTypePath);

            if (journeyProcessType.equals(JourneyProcessType.MFA.name()) && journeyProcessLogInfo.getMfaotpMapping() == null) {
                // MFA
                SingleResult<MFAOTPMapping> mfaotpMappingSingleResult = this.mfaClientService.getMFAOTPMapping(applicationReferenceId);
                journeyProcessLogInfo.setMfaotpMapping(mfaotpMappingSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.PRODUCT.name()) && journeyProcessLogInfo.getProductInfo() == null) {
                // PRODUCT
                SingleResult<Product> productSingleResult = this.productInfoClientService.getCustomerProduct(applicationReferenceId, "url");
                journeyProcessLogInfo.setProductInfo(productSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.IDV.name()) && journeyProcessLogInfo.getDocumentResultLogList() == null) {
                // IDV
                ListResult<DocumentResultLog> documentResultLogListResult = this.identityVerificationClientService.getDocumentResultLogList(applicationReferenceId);
                journeyProcessLogInfo.setDocumentResultLogList(documentResultLogListResult.getContents());

                // TODO: save file, filename, filetype parsing
                log.info("### documentResultLogListResult.getContents(): {}", documentResultLogListResult.getContents());
                DocumentResultLog documentResultLog = documentResultLogListResult.getContents().get(0);
                // selfieImagePath
                // docFrontImagePath

                if (documentResultLog.getSelfieImagePath() != null) {
                    String downloadUrl = this.getIdvDownloadUrl(documentResultLog.getSelfieImagePath());
                    String filename = this.getFilename(documentResultLog.getSelfieImagePath());
                    String fileType = this.getFileType(documentResultLog.getSelfieImagePath());
                }

//                String filename = FileUtil.getFilename(directoryPath, "serfie.jpg", fileType);
//                FileUtil.saveImage(downloadUrl, filename, fileType);

            } else if (journeyProcessType.equals(JourneyProcessType.CINFO.name()) && journeyProcessLogInfo.getCustomerVerifiedApplicationForm() == null) {
                // CINFO
                SingleResult<VerifiedApplicationForm> customerVerifiedApplicationFormSingleResult = this.customerInfoClientService.getCustomerVerifiedApplicationForm(applicationReferenceId);
                journeyProcessLogInfo.setCustomerVerifiedApplicationForm(customerVerifiedApplicationFormSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.CREDIT_CHECK.name()) && journeyProcessLogInfo.getCreditReportLog() == null) {
                // CREDIT_CHECK
                SingleResult<CreditReportLog> creditReportLogSingleResult = this.creditCheckClientService.getCreditReportLog(applicationReferenceId);
                journeyProcessLogInfo.setCreditReportLog(creditReportLogSingleResult.getContent());

                HttpReqData reqData = creditReportLogSingleResult.getContent().getRequestData();
                String data = reqData.getBody();
                FileUtil.createFile(data, directoryPath, JourneyProcessType.CREDIT_CHECK);
            } else if (journeyProcessType.equals(JourneyProcessType.RISK_ASSESSMENT.name()) && journeyProcessLogInfo.getRiskReportLog() == null) {
                // RISK_ASSESSMENT
                SingleResult<RiskReportLog> riskReportLogSingleResult = this.riskAssessmentClientService.getRiskReportLog(applicationReferenceId);
                journeyProcessLogInfo.setRiskReportLog(riskReportLogSingleResult.getContent());

                HttpRespData respData = riskReportLogSingleResult.getContent().getResponseData();
                String data = respData.getBody();
                FileUtil.createFile(data, directoryPath, JourneyProcessType.RISK_ASSESSMENT);

            } else if (journeyProcessType.equals(JourneyProcessType.HOME_ADDRESS_VERIFICATION.name()) && journeyProcessLogInfo.getGeolocationRef() == null) {
                // HOME_ADDRESS_VERIFICATION
                SingleResult<GeolocationRef> geolocationRefSingleResult = this.regionValidatorClientService.getGeolocationRef(applicationReferenceId);
                journeyProcessLogInfo.setGeolocationRef(geolocationRefSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.TERMS_AND_CONDITIONS.name()) && journeyProcessLogInfo.getTermsStatusLog() == null) {
                // TERMS_AND_CONDITIONS
                SingleResult<TermsStatusLog> termStatusModelSingleResult = this.termsClientService.getCustomerTerm(applicationReferenceId);
                journeyProcessLogInfo.setTermsStatusLog(termStatusModelSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.ELIGIBILITY_CHECK.name()) && journeyProcessLogInfo.getEligibilityLog() == null) {
                // ELIGIBILITY_CHECK
                SingleResult<EligibilityLog> eligibilityLogSingleResult = this.eligibilityClientService.getEligibilityLog(applicationReferenceId);
                journeyProcessLogInfo.setEligibilityLog(eligibilityLogSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.ESIGNATURE.name()) && journeyProcessLogInfo.getESigDocModel() == null) {
                // ESIGNATURE
                SingleResult<ESigDocModel> eSigDocModelSingleResult = this.eSignatureClientService.getSignature(applicationReferenceId);
                journeyProcessLogInfo.setESigDocModel(eSigDocModelSingleResult.getContent());

                ESigDocModel eSigDocModel = eSigDocModelSingleResult.getContent();
                if ( eSigDocModel != null) {
                    String esignatureFile = eSigDocModel.getEsignatureFile();
                    ImageUtil.base64ToImage(esignatureFile, directoryPath, JourneyProcessType.ESIGNATURE);
                }

            } else if (journeyProcessType.equals(JourneyProcessType.INSTANT_ISSUANCE.name()) && journeyProcessLogInfo.getInstantIssuanceInfo() == null) {
                // INSTANT_ISSUANCE
                SingleResult<InstantIssuanceInfo> instantIssuanceInfoSingleResult = this.instantIssuanceClientService.getInstantIssuanceInfo(applicationReferenceId);
                journeyProcessLogInfo.setInstantIssuanceInfo(instantIssuanceInfoSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.PUSH_PROVISIONING.name()) && journeyProcessLogInfo.getPushProvisioningResult() == null) {
                // PUSH_PROVISIONING
                final SingleResult<PushProvisioningResult> pushProvisioningResultSingleResult = this.inAppProvisioningClientService.getPushProvisioningResult(applicationReferenceId);
                journeyProcessLogInfo.setPushProvisioningResult(pushProvisioningResultSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.LOGIN_CREATION.name()) && journeyProcessLogInfo.getLoginCreationResult() == null) {
                // LOGIN_CREATION
                final SingleResult<LoginCreationResult> loginCreationResultSingleResult = this.casAccountClientService.getLoginCreationResult(applicationReferenceId);
                journeyProcessLogInfo.setLoginCreationResult(loginCreationResultSingleResult.getContent());
            } else if (journeyProcessType.equals(JourneyProcessType.USER_IDENTIFICATION.name()) && journeyProcessLogInfo.getUserIdentificationLog() == null) {
                // USER_IDENTIFICATION
                final UserIdentificationLog userIdentificationLog = this.userIdentificationClientService.getUserIdentificationV09(applicationReferenceId);
                journeyProcessLogInfo.setUserIdentificationLog(userIdentificationLog);
            } else if (journeyProcessType.equals(JourneyProcessType.USER_VERIFICATION.name()) && journeyProcessLogInfo.getUserVerificationLog() == null) {
                // USER_VERIFICATION
                final UserVerificationLog userVerificationLog = this.userVerificationClientService.getUserVerificationV09(applicationReferenceId);
                journeyProcessLogInfo.setUserVerificationLog(userVerificationLog);
            } else if (journeyProcessType.equals(JourneyProcessType.PRODUCT_DUPLICATE_DETECTION.name()) && journeyProcessLogInfo.getProductDuplicationDetectionLog() == null) {
                // PRODUCT_DUPLICATE_DETECTION
                final ProductDuplicationDetectionLog productDuplicationDetectionLog = this.productDuplicationDetectionClientService.getSavedDuplicateProductResultV09(applicationReferenceId);
                journeyProcessLogInfo.setProductDuplicationDetectionLog(productDuplicationDetectionLog);
            } else if (journeyProcessType.equals(JourneyProcessType.CREDIT_DECISIONING.name()) && journeyProcessLogInfo.getCreditUnderwritingLog() == null) {
                List<String> applicationReferenceIds = Arrays.asList(applicationReferenceId);
                final CreditUnderwritingLog creditUnderwritingLog = this.creditUnderwritingClientService.getLogs(applicationReferenceIds);
                journeyProcessLogInfo.setCreditUnderwritingLog(creditUnderwritingLog);

                HttpRspData httpRspData = creditUnderwritingLog.getResponseData();
                String data = httpRspData.getBody();
                FileUtil.createFile(data, directoryPath, JourneyProcessType.CREDIT_DECISIONING);
            }
        }

        return journeyProcessLogInfo;
    }

    private String getFileType(String path) {
        String[] fileSplit = path.split("/");

        String fileEncrypted = fileSplit[11];
        String[] filenames = fileEncrypted.split("\\.");

        return filenames[1];
    }

    private String getFilename(String path) {
        String[] fileSplit = path.split("/");

        String fileEncrypted = fileSplit[11];
        String[] filenames = fileEncrypted.split("\\.");

        return filenames[0];
    }

    private String getIdvDownloadUrl(String path) {
        String host = "https://dcs-dev2-52180915.us-east-1.elb.amazonaws.com:443/mobile-api/";
        return host + path;
    }
}
