package com.ondot.dcs.exportdata.service.client.creditcheck;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.CreditReportLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CreditCheckClientService extends AbstractFeignClientService {
    private final CreditCheckClient creditCheckClient;

    public SingleResult<CreditReportLog> getCreditReportLog(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<CreditReportLog>> responseEntity = this.creditCheckClient.getLog(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                log.warn("failed to get CreditReportLog. HttpStatus is NOT_FOUND");
            } else {
                log.error(String.format("failed to get CreditReportLog. ex=%s", e.toString()), e);
            }
            return SingleResult.<CreditReportLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        } catch (Exception e) {
            log.error(String.format("failed to get CreditReportLog. ex=%s", e.toString()), e);
            return SingleResult.<CreditReportLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }

    public CreditReportLog getCreditReportLogV09(
            @org.springframework.lang.NonNull String applicationReferenceId) {

        try {
            final ResponseResult<CreditReportLog> responseResult = this.creditCheckClient.getLogV09(applicationReferenceId);

            return Optional.ofNullable(responseResult).map(ResponseResult::getData).orElse(null);
        } catch (FeignException e) {
            log.error("getEligibilityLogV09 error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return null;
        }
    }
}
