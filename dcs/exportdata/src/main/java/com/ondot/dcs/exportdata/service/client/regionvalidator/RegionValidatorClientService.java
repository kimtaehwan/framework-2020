package com.ondot.dcs.exportdata.service.client.regionvalidator;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.regionvalidator.dto.GeolocationRef;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegionValidatorClientService extends AbstractFeignClientService {

    private final RegionValidatorClient regionValidatorClient;

    public SingleResult<GeolocationRef> getGeolocationRef(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<GeolocationRef>> responseEntity = this.regionValidatorClient.find(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getGeolocationRef error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<GeolocationRef>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
