package com.ondot.dcs.exportdata.service.client.journey.dto;

import com.ondot.dcs.exportdata.service.client.casaccount.dto.LoginCreationResult;
import com.ondot.dcs.exportdata.service.client.cinfo.dto.VerifiedApplicationForm;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.CreditReportLog;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.CreditUnderwritingLog;
import com.ondot.dcs.exportdata.service.client.eligibility.dto.EligibilityLog;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import com.ondot.dcs.exportdata.service.client.inappprovisioning.dto.PushProvisioningResult;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.IssuanceResponse;
import com.ondot.dcs.exportdata.service.client.mfa.dto.MFAOTPMapping;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import com.ondot.dcs.exportdata.service.client.regionvalidator.dto.GeolocationRef;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@Setter
@ToString
public class JourneyProcessLogInfo {

    String applicationReferenceId;

    List<JourneyProcessTypeLog> journeyProcessTypeLogList;

    MobileDevice mobileDevice;

    MFAOTPMapping mfaotpMapping;

    List<DocumentResultLog> documentResultLogList;

    Product productInfo;

    VerifiedApplicationForm customerVerifiedApplicationForm;

    CreditReportLog creditReportLog;

    RiskReportLog riskReportLog;

    GeolocationRef geolocationRef;

    TermsStatusLog termsStatusLog;

    EligibilityLog eligibilityLog;

    ESigDocModel eSigDocModel;

    InstantIssuanceInfo instantIssuanceInfo;

    IssuanceResponse issuanceResponse;

    PushProvisioningResult pushProvisioningResult;

    LoginCreationResult loginCreationResult;

    UserIdentificationLog userIdentificationLog;

    UserVerificationLog userVerificationLog;

    ProductDuplicationDetectionLog productDuplicationDetectionLog;

    CreditUnderwritingLog creditUnderwritingLog;
}
