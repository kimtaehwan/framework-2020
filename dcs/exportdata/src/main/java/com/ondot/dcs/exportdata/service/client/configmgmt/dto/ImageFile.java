// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ImageFile {
    private String name;
    private String uri;
    private String type;
    private long size;
    private String originalName;
}
