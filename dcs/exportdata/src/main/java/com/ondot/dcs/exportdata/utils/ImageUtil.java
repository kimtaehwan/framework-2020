package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.exception.FileStorageException;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessType;
import io.micrometer.core.instrument.util.StringUtils;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageUtil {
    public static void base64ToImage(String base64, Path directoryPath, JourneyProcessType processType) {
        if (StringUtils.isBlank(base64)) {
            return;
        }

        byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64);
        String target = ImageUtil.createTarget(directoryPath, processType.name());
        try {
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageBytes));
            ImageIO.write(bufferedImage, "jpg", new File(target + ".jpg"));
        } catch (IOException e) {
            throw new FileStorageException("Could not create file.", e);
        }
    }

    private static String createTarget(Path directoryPath, String name) {
        return Paths.get(directoryPath.toString(), name).toString();
    }
}
