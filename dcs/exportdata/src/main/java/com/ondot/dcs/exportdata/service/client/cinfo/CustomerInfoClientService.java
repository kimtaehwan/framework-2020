package com.ondot.dcs.exportdata.service.client.cinfo;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.cinfo.dto.VerifiedApplicationForm;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerInfoClientService extends AbstractFeignClientService {
    private final CustomerInfoClient customerInfoClient;

    public SingleResult<VerifiedApplicationForm> getCustomerVerifiedApplicationForm(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<VerifiedApplicationForm>> responseEntity = this.customerInfoClient.getCustomerVerifiedApplicationForm(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getCustomerVerifiedApplicationForm error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<VerifiedApplicationForm>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
