// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.dto.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ResponseResult<T> {

    private static final String EXCEPTION_MESSAGE_NO_MATCHING_CONSTANT = "No matching constant for [%s]";

    @JsonProperty("apiVersion")
    private String apiVersion;

    @JsonProperty("responseDataList")
    @Builder.Default
    private List<T> dataList = Collections.emptyList();

    @JsonProperty("responseStatus")
    @Builder.Default
    private ResponseStatus status = ResponseStatus.SUCCESS;

    @JsonProperty("operationTraceId")
    private String traceId;

    @JsonIgnore
    public boolean isStatusSuccess() {
        return Optional.ofNullable(status)
                .map(ResponseStatus::getCode)
                .map(code -> code.equals(ResponseCode.SUCCESS.code()))
                .orElse(Boolean.FALSE);
    }

    @JsonIgnore
    public T getData() {
        return Optional.ofNullable(dataList)
                .flatMap(list -> list.stream().limit(1).filter(Objects::nonNull).findFirst())
                .orElse(null);
    }

    @JsonIgnore
    public ResponseResult<T> data(T data) {
        this.dataList = Optional.ofNullable(data).map(Arrays::asList).orElse(Collections.emptyList());
        return this;
    }

    @JsonIgnore
    public ResponseResult<T> dataList(List<T> dataList) {
        this.dataList = Optional.ofNullable(dataList).orElse(Collections.emptyList());
        return this;
    }

    public enum ResponseCode {
        SUCCESS(0),
        BAD_REQUEST(10),
        SERVER_ERROR(90);

        private final int code;

        private ResponseCode(int code) {
            this.code = code;
        }

        public static ResponseCode codeOf(int code) {
            return Arrays.stream(values())
                    .filter(v -> v.code == code)
                    .findAny()
                    .orElseThrow(() ->
                            new IllegalArgumentException(String.format(EXCEPTION_MESSAGE_NO_MATCHING_CONSTANT, code))
                    );
        }

        public int code() {
            return this.code;
        }

    }

    public enum ErrorDetailReason {
        MISSING_PARAMETER("missingParameter"),
        INVALID_PARAMETER("invalidParameter"),
        INTERNAL_ERROR("internalError"),
        EXTERNAL_ERROR("externalError");

        private final String code;

        private ErrorDetailReason(String code) {
            this.code = code;
        }

        public static ErrorDetailReason codeOf(String code) {
            return Arrays.stream(values())
                    .filter(v -> v.code.equals(code))
                    .findAny()
                    .orElseThrow(() ->
                            new IllegalArgumentException(String.format(EXCEPTION_MESSAGE_NO_MATCHING_CONSTANT, code))
                    );
        }

        public String code() {
            return this.code;
        }

    }

    public enum ErrorDetailLocationType {
        REQUEST_URL("requestUrl"),
        HEADER("header"),
        PARAMETER("parameter");

        private final String code;

        private ErrorDetailLocationType(String code) {
            this.code = code;
        }

        public static ErrorDetailLocationType codeOf(String code) {
            return Arrays.stream(values())
                    .filter(v -> v.code.equals(code))
                    .findAny()
                    .orElseThrow(() ->
                            new IllegalArgumentException(String.format(EXCEPTION_MESSAGE_NO_MATCHING_CONSTANT, code))
                    );
        }

        public String code() {
            return this.code;
        }

    }

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class ErrorDetail {
        String reason;
        String locationType;
        String location;

    }

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class ResponseStatus {

        public static final ResponseStatus SUCCESS = ResponseStatus.builder().code(ResponseCode.SUCCESS.code()).build();

        @JsonProperty("responseCode")
        private Integer code;
        private List<ErrorDetail> errorDetailList;
        private String userMessage;

        @JsonIgnore
        public ResponseStatus errorDetail(ErrorDetail errorDetail) {
            this.errorDetailList = Optional.ofNullable(errorDetail).map(Arrays::asList).orElse(Collections.emptyList());
            return this;
        }

    }

}
