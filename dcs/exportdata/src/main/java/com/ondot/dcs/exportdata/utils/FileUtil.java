package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.core.StoragePathConfiguration;
import com.ondot.dcs.exportdata.exception.FileStorageException;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessType;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FileUtil {

    @Autowired
    private StoragePathConfiguration storagePathConfiguration;

    private static final String DATE_PATTERN = "yyyyMMddHHmmss";

    public static String getDirectoryName(String fiToken, ZonedDateTime currentDate) {
        String date = currentDate.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
        return fiToken + "_" + date;
    }

    public static Path getPath(String path) {
        return Paths.get(path).toAbsolutePath().normalize();
    }

    public static Path createDirectory(Path storagePath) {
        try {
            return Files.createDirectories(storagePath);
        } catch (IOException e) {
            throw new FileStorageException("Could not create directory.", e);
        }
    }

    public static Path createDirectoryPath(Path directoryPath, String applicationReferenceId) {
        Path createPath = Paths.get(directoryPath.toString(), applicationReferenceId);
        return createPath.toAbsolutePath().normalize();
    }

    public static void createFile(String data, Path directoryPath, JourneyProcessType processType) {
        if (StringUtils.isBlank(data)) {
            return;
        }

        byte[] bytes = data.getBytes();
        Path path = Paths.get(directoryPath.toString(), processType.name()+ ".txt");

        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            throw new FileStorageException("Could not create file.", e);
        }
    }

    public static String getFileName(Path directoryPath, String filename, String fileType) {
        return directoryPath.toAbsolutePath().normalize() + File.separator + filename + fileType;
    }
}
