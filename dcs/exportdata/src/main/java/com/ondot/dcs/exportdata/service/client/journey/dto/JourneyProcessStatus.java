package com.ondot.dcs.exportdata.service.client.journey.dto;

public enum JourneyProcessStatus {
    NOT_STARTED,
    IN_PROGRESS,
    SUCCESS,
    FAIL,
    UNCONFIRMED_SUCCESS,
    ABANDONED
}
