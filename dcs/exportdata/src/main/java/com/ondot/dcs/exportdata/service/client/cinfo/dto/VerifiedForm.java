// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.cinfo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class VerifiedForm {
    String pageTitle;
    @JsonProperty(value = "forms")
    List<VerifiedPage> pages;
}
