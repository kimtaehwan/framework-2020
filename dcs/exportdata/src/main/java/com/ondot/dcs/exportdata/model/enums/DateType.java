package com.ondot.dcs.exportdata.model.enums;

public enum DateType {
    YEAR, MONTH, DAY
}
