package com.ondot.dcs.exportdata.core;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "storage.path")
public class StoragePathConfiguration {
    private String exportData;
}
