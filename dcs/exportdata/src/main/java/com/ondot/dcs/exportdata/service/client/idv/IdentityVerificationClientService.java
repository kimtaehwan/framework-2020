package com.ondot.dcs.exportdata.service.client.idv;

import com.ondot.dcs.exportdata.dto.core.ListResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.idv.dto.DocumentResultLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class IdentityVerificationClientService extends AbstractFeignClientService {
    private final IdentityVerificationClient identityVerificationClient;

    public ListResult<DocumentResultLog> getDocumentResultLogList(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<ListResult<DocumentResultLog>> responseEntity = this.identityVerificationClient.getDocumentResultLogList(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getDocumentResultLogList error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return ListResult.<DocumentResultLog>listResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
