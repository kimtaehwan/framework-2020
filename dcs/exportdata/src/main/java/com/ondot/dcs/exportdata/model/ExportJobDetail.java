package com.ondot.dcs.exportdata.model;

import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ScheduleExport;
import lombok.*;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

@Getter
@ToString
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class ExportJobDetail {
    private String fiToken;
    private String exportJobName;

    private Boolean approvedAndCardIssued;
    private Boolean approvedAndCardNotIssued;
    private Boolean notApproved;
    private Boolean dataCollectionInprogress;
    private Boolean dataCollectionFail;

    private String emailAddress1;
    private String emailAddress2;

    private ScheduleExport scheduleExport;

    private OffsetDateTime dateRangeFrom;
    private OffsetDateTime dateRangeTo;

    private ZonedDateTime from;
    private ZonedDateTime to;
}
