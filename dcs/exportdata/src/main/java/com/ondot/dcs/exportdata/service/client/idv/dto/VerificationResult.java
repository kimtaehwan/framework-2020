package com.ondot.dcs.exportdata.service.client.idv.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class VerificationResult {
    String ticketId;
    int ticketUseCount;
    int ticketUseLimit;

    String traceId;

    String docInstanceId;

    ImageMetric frontImageMetric;
    ImageMetric backImageMetric;

    AuthExtractData authExtractData;

    FaceMatch faceMatch;
    String faceMatchTraceId;

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    @ToString
    public static class ImageMetric {
        Boolean isValid;
        Integer glareMetric; // 0-49: Too much glare, image quality NOK
        Integer sharpnessMetric; // 0-49: Too blurry, image quality NOK
        Boolean isCropped; // If the image was cropped
        Boolean isTampered; // If the image was tampered with
    }

    public enum DocAuthResult {
        // Document Authentication Result
        UNKNOWN(0, "Unknown"),
        PASSED(1, "Passed"),
        FAILED(2, "Failed"),
        SKIPPED(3, "Skipped"),
        CAUTION(4, "Caution"),
        ATTENTION(5, "Attention"),
        ;

        private int code;
        private String jsonValue;

        DocAuthResult(int code, String jsonValue) {
            this.code = code;
            this.jsonValue = jsonValue;
        }

        public int getCode() {
            return this.code;
        }

        public static DocAuthResult fromCode(int code) {
            for(DocAuthResult v : values()) {
                if(code == v.code) {
                    return v;
                }
            }
            throw new IllegalArgumentException("unknown code. code=" + code);
        }

        @JsonCreator
        public static DocAuthResult fromJsonValue(String jsonValue) {
            for(DocAuthResult v: values()) {
                if (v.jsonValue.equals(jsonValue)) {
                    return v;
                }
            }
            throw new IllegalArgumentException("unknown jsonValue. jsonValue=" + jsonValue);
        }

        @JsonValue
        public String toJsonValue() {
            return this.jsonValue;
        }

    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    @ToString
    public static class AuthExtractData {
        DocAuthResult authResult;  // Consolidated overall authentication result.
        Date expirationDate;
        String firstName;
        String lastName;
        Integer age;
        Date birthDate;
        String zipCode;
        String state;
        String city;
        String addressLine1;
        String addressLine2;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    @ToString
    public static class FaceMatch {
        Boolean isMatch;
        Integer score;
    }

}
