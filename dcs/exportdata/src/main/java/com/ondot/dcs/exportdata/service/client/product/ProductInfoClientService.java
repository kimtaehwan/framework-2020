package com.ondot.dcs.exportdata.service.client.product;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductInfoClientService extends AbstractFeignClientService {
    private final ProductInfoClient productInfoClient;

    public SingleResult<Product> getCustomerProduct(
            @NonNull String applicationReferenceId,
            String imageType) {
        try {
            ResponseEntity<SingleResult<Product>> responseEntity = productInfoClient.getCustomerProduct(applicationReferenceId, imageType);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getCustomerProduct error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return null;
        }
    }
}
