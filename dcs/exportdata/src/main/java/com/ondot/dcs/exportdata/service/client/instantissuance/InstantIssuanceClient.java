package com.ondot.dcs.exportdata.service.client.instantissuance;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.IssuanceResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service.client.dcs-instantissuance.name}", url = "${service.client.dcs-instantissuance.url}")
public interface InstantIssuanceClient {
    @GetMapping(value = "/v1/issuance")
    ResponseEntity<SingleResult<InstantIssuanceInfo>> getInstantIssuanceInfo(
            @RequestHeader(value = "x-ondotsystems-applicationReferenceId") String applicationReferenceId);

    @GetMapping(value = "/mgmt-api/v0.9/issuance")
    ResponseResult<IssuanceResponse> getIssuedCards(
            @RequestParam(value = "applicationReferenceId") String applicationReferenceId,
            @RequestParam(value = "inclCardImage", required = false, defaultValue = "false") boolean includeCardImage,
            @RequestParam(value = "masked", required = false, defaultValue = "false") boolean masked);
}
