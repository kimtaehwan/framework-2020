package com.ondot.dcs.exportdata.utils;

import com.ondot.dcs.exportdata.dto.ApplicationData;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.nio.file.Path;
import java.util.List;

public class CsvUtil {
    public static void csvWrite(String filename, List<ApplicationData.Csv> csvAssemberList) {
        ICsvBeanWriter beanWriter = null;

        try {
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
            beanWriter = new CsvBeanWriter(writer, CsvPreference.STANDARD_PREFERENCE);

            final String[] header = getHeader();
            final CellProcessor[] processors = getProcessors();

            beanWriter.writeHeader(header);

            for (final ApplicationData.Csv data : csvAssemberList) {
                beanWriter.write(data, header, processors);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[] {
                new NotNull(), // applicationReferenceId (must be unique)
                new Optional(new FmtDate("dd/MM/yyyy")),    // lastModifiedTime
                new Optional(), // productName
                new Optional(), // applicationStatus
                new Optional(), // applicantName
                new Optional(), // dateOfBirth
                new Optional(), // ssn
                new Optional(), // address
                new Optional(), // phoneNumber
                new Optional(), // email
                new Optional(), // cardType
                new Optional(), // cardNumber
                new Optional(), // userVerificationStatus
                new Optional(), // userVerificationMethod
                new Optional(), // productDuplicateDetectionStatus
                new Optional(), // consentAcceptanceStatus
                new Optional(), // userAcceptedConsentDocuments
                new Optional(), // consentAcceptanceDate
                new Optional(), // eSignatureDate
                new Optional(), // riskAssessmentStatus
                new Optional(), // riskScore
                new Optional(), // thresholdRiskScore
                new Optional(), // creditInquiry
                new Optional(), // creditScore
                new Optional(), // thresholdCreditScore
                new Optional(), // creditClassCode
                new Optional(), // userSelectedEligibilityCriteria
                new Optional(), // idVerificationStatus
                new Optional(), // homeAddressVerificationStatus
                new Optional(), // idType
                new Optional(), // digitalWallet
                new Optional(), // verificationPhoneNumber
                new Optional(), // emailAddressForOTP
                new Optional()  // metaData
        };
    }

    private static String[] getHeader() {
        return new String[] {
                "applicationReferenceId",
                "lastModifiedTime",
                "productName",
                "applicationStatus",
                "applicantName",
                "dateOfBirth",
                "ssn",
                "address",
                "phoneNumber",
                "email",
                "cardType",
                "cardNumber",
                "userVerificationStatus",
                "userVerificationMethod",
                "productDuplicateDetectionStatus",
                "consentAcceptanceStatus",
                "userAcceptedConsentDocuments",
                "consentAcceptanceDate",
                "eSignatureDate",
                "riskAssessmentStatus",
                "riskScore",
                "thresholdRiskScore",
                "creditInquiry",
                "creditScore",
                "thresholdCreditScore",
                "creditClassCode",
                "userSelectedEligibilityCriteria",
                "idVerificationStatus",
                "homeAddressVerificationStatus",
                "idType",
                "digitalWallet",
                "verificationPhoneNumber",
                "emailAddressForOTP",
                "metaData"
        };
    }
}
