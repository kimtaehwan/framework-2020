package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import com.ondot.dcs.exportdata.dto.core.ResultStatus;
import lombok.*;
import org.springframework.util.MultiValueMap;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PaginationResultContent<T> extends ContentResult<T> {

    int totalPages;
    long totalElements;
    int size;

    @Builder(builderMethodName = "paginationResultContentBuilder")
    public PaginationResultContent(ResultStatus status, MultiValueMap<String, String> httpHeaders, List<T> content, int totalPages, long totalElements, int size) {
        super(status, httpHeaders, content);
        this.totalPages = totalPages;
        this.totalElements = totalElements;
        this.size = size;
    }
}
