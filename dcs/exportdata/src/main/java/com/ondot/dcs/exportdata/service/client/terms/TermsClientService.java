package com.ondot.dcs.exportdata.service.client.terms;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.terms.dto.TermsStatusLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TermsClientService extends AbstractFeignClientService {

    private final TermsClient termsClient;

    public SingleResult<TermsStatusLog> getCustomerTerm(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<TermsStatusLog>> responseEntity = this.termsClient.getCustomerTerm(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getCustomerTerm error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<TermsStatusLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
