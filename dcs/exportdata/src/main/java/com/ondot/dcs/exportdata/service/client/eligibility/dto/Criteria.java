package com.ondot.dcs.exportdata.service.client.eligibility.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Criteria {
    private Long criteriaId;

    private String criteriaDetails;
}
