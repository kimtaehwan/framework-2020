package com.ondot.dcs.exportdata.core;

import com.ondot.dcs.exportdata.exception.FileStorageException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Getter
@Configuration
public class StorageConfiguration {

    final StoragePathConfiguration storagePathConfiguration;
    final Path exportdataFileLocation;

    @Autowired
    public StorageConfiguration(StoragePathConfiguration storagePathConfiguration) {
        this.storagePathConfiguration = storagePathConfiguration;
        this.exportdataFileLocation = Paths.get(storagePathConfiguration.getExportData()).toAbsolutePath().normalize();

        this.createDirectories();
    }

    private void createDirectories() {
        try {
            Files.createDirectories(this.exportdataFileLocation);
        } catch (IOException e) {
            throw new FileStorageException("Could not create directory.", e);
        }
    }
}
