package com.ondot.dcs.exportdata.service.client.productduplicatedetection;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.productduplicatedetection.dto.ProductDuplicationDetectionLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductDuplicationDetectionClientService extends AbstractFeignClientService {

    private final ProductDuplicationDetectionClient productDuplicationDetectionClient;


    public ProductDuplicationDetectionLog getSavedDuplicateProductResultV09(@NonNull String applicationReferenceId) {
        try {
            final ResponseResult<ProductDuplicationDetectionLog> responseResult = this.productDuplicationDetectionClient.getSavedDuplicateProductResultV09(applicationReferenceId);
            return Optional.ofNullable(responseResult).map(ResponseResult::getData).orElse(null);
        } catch (FeignException e) {
            log.error("getSavedDuplicateProductResultV09 error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return null;
        }
    }
}
