package com.ondot.dcs.exportdata.service.client.configmgmt;

import com.ondot.dcs.exportdata.dto.core.ListResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ExportJobResponse;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.PaginationResultContent;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.Product;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.Status;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConfigMgmtClientService extends AbstractFeignClientService {

    private final ConfigMgmtClient configMgmtClient;

    public List<ExportJobResponse.Listing> getList(Pageable pageable1,
                                                                      List<Long> fiIdList,
                                                                      String q,
                                                                      OffsetDateTime from,
                                                                      OffsetDateTime to,
                                                                      Status status) {
        try {
            log.info("configmgmt-getList");
            List<ExportJobResponse.Listing> resultList = new ArrayList<>();
            Pageable pageable = PageRequest.of(0, 2);
            ResponseEntity<PaginationResultContent<ExportJobResponse.Listing>> listings = this.configMgmtClient.listing(pageable);

            int totalPages = listings.getBody().getTotalPages();

            if ( totalPages == 1) {
                resultList.addAll(listings.getBody().getContent());
            } else {
                for (int i = 0; i < totalPages; i++) {
                    Pageable pageableIndex = PageRequest.of(i, 2);
                    ResponseEntity<PaginationResultContent<ExportJobResponse.Listing>> temps = this.configMgmtClient.listing(pageableIndex);
                    log.info("temps: {}", temps);
                    resultList.addAll(temps.getBody().getContent());
                }
            }
//            ResponseEntity responseEntity = this.configMgmtClient.retrieve(120L);

            return resultList;
        } catch (FeignException e) {
            log.error("error: {}", e);
            return null;
        }
    }

    public ResponseEntity<List<Product.Listing>> listingProducts(String fiToken) {
        try {
            ResponseEntity<List<Product.Listing>> responseEntity = this.configMgmtClient.listingProducts(fiToken);
            return responseEntity;
        } catch (FeignException e) {
            log.error("error: {}", e);
            return null;
        }
    }

    public ExportJobResponse.Retrieve retrieve(Long id) {
        ResponseEntity<ExportJobResponse.Retrieve> responseEntity = configMgmtClient.retrieve(id);
        return responseEntity.getBody();
    }
}

