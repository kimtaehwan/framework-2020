package com.ondot.dcs.exportdata.model.cm.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonMember147{

	@JsonProperty("ackId")
	private String ackId;

	@JsonProperty("statusCode")
	private int statusCode;
}
