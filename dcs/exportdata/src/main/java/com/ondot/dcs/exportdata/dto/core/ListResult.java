// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.dto.core;

import lombok.*;
import org.springframework.util.MultiValueMap;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ListResult<T> extends BaseResult {

    List<T> contents;

    @Builder(builderMethodName = "listResultBuilder")
    public ListResult(ResultStatus status, MultiValueMap<String, String> httpHeaders, List<T> contents) {
        super(status, httpHeaders);
        this.contents = contents;
    }

}
