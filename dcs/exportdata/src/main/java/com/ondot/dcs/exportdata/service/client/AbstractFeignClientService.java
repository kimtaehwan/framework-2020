package com.ondot.dcs.exportdata.service.client;

import com.ondot.dcs.exportdata.dto.core.ResultStatus;
import feign.FeignException;
import org.springframework.http.HttpStatus;

public abstract class AbstractFeignClientService {

    protected ResultStatus toResultStatus(FeignException e) {
        return ResultStatus.builder()
                .code(800)
                .httpStatus(HttpStatus.resolve(e.status()))
                .message(e.getMessage())
                .build();
    }

    protected ResultStatus toResultStatus(Exception e) {
        return ResultStatus.builder()
                .code(999)
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message(e.toString())
                .build();
    }
}
