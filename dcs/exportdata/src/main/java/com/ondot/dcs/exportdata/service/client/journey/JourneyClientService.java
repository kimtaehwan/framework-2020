package com.ondot.dcs.exportdata.service.client.journey;

import com.ondot.dcs.exportdata.dto.core.PaginationResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.journey.dto.ApplicationReference;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessLogInfo;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class JourneyClientService extends AbstractFeignClientService {
    private final JourneyClient journeyClient;

    public ResponseEntity<PaginationResult<ApplicationReference>> getReferenceList(int page, int size, ZonedDateTime startTime, ZonedDateTime endTime, String fiToken) {
        try {
            ResponseEntity<PaginationResult<ApplicationReference>> responseEntity = journeyClient.getReferenceList(
                    page,
                    size,
                    startTime,
                    endTime,
                    null,
                    fiToken);
            return responseEntity;
        } catch (FeignException e) {
            log.error("getReferenceList error. startTime={}, endTime={}, ex={}", startTime, endTime, e.toString());
            return null;
        }
    }

    public ResponseEntity<SingleResult<JourneyProcessLogInfo>> getReferenceJourneyProcessLogInfo(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<JourneyProcessLogInfo>> responseEntity = this.journeyClient.getApplicationJourneyProcessLogInfo(applicationReferenceId);
            return responseEntity;
        } catch (FeignException e) {
            log.error("getReferenceJourneyProcessLogInfo error. ex={}", e.toString());
            return null;
        }
    }
}
