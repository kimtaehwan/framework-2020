package com.ondot.dcs.exportdata.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ondot.dcs.exportdata.config.OndotConfig;
import com.ondot.dcs.exportdata.exception.ExternalApiException;
import com.ondot.dcs.exportdata.model.cm.request.email.CMRequestEmail;
import com.ondot.dcs.exportdata.model.cm.request.email.EmailContent;
import com.ondot.dcs.exportdata.utils.RequestConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@EnableAutoConfiguration
public class CMRequestHandler {

    @Autowired
    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter; //springMvcJacksonConverter

    @Autowired
    private RestTemplate restTemplate;

    private String authorizationHeaderValue;

    private ObjectMapper objectMapper;

    @Autowired
    OndotConfig ondotConfig;

    @PostConstruct
    public void doPostConstruct() {
        this.restTemplate = this.newRestTemplate();
        log.info("URL :  " + ondotConfig.getCmURL());
        this.authorizationHeaderValue = this.getAuthorizationHeaderValue(ondotConfig.getBasicAuthUsername(), ondotConfig.getBasicAuthPassword());
        this.objectMapper = this.mappingJackson2HttpMessageConverter.getObjectMapper();
        log.info("doPostConstruct called");
    }

    @Bean
    public RestTemplate newRestTemplate() {
        SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        return new RestTemplate(new BufferingClientHttpRequestFactory(clientHttpRequestFactory));
    }

    public void makeEmailRequest(String toEmail, EmailContent emailContent) {
        String methodName = "makeEmailRequest :: ";

        try {
            List<CMRequestEmail> requests = RequestConstructor.requestConstructorForEmail(ondotConfig.getAppToken(), ondotConfig.getFiToken(), toEmail, emailContent);
            makeRestCall(objectToJson(requests));
        } catch(JsonProcessingException e) {
            String errMsg = "Email :: unsuccessful before request sent.";
            log.error(methodName + errMsg);
        } catch(Exception e){
            String errMsg = "Email :: unknown Exception :: " + e + " :: " + e.getMessage() ;
            log.error(methodName+errMsg);
        }
    }

    private void makeRestCall(String jsonRequest) {
        String methodName = "makeRestCall :: ";
        URI uri = restTemplate.getUriTemplateHandler().expand(ondotConfig.getCmURL(), Collections.emptyMap());
        HttpHeaders headers = httpHeaders();
        HttpEntity<String> requestEntity = new HttpEntity<>(jsonRequest,headers);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);
            log.info(methodName + responseEntity.getBody());
            if (!responseEntity.getStatusCode().is2xxSuccessful()) {
                String errorMsg = "makeRestCall unsuccessful";
                log.error("HttpStatus is not successful :: StatusCode={}, ReasonPhrase={}", responseEntity.getStatusCode(), responseEntity.getStatusCode().getReasonPhrase());
                throw new ExternalApiException(errorMsg);
            }
        } catch (Exception e){
            log.error(methodName + e + e.getMessage());
        }

    }

    private String getAuthorizationHeaderValue(String username, String password) {
        String auth = username + ":" + password;
        return new StringBuilder()
                .append("Basic ")
                .append(Base64Utils.encodeToString(auth.getBytes(StandardCharsets.US_ASCII))).toString();
    }

    private HttpHeaders httpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, this.authorizationHeaderValue);
        headers.set(ondotConfig.getDeploymentTokenKey(), ondotConfig.getDeploymentToken());
        MediaType accept = MediaType.APPLICATION_JSON;
        MediaType contentType = MediaType.APPLICATION_JSON;

        if (accept != null) {
            headers.setAccept(Arrays.asList(accept));
        }
        if (contentType != null) {
            headers.setContentType(contentType);
        }
        return headers;
    }

    private String objectToJson(Object value) throws JsonProcessingException {
        return objectMapper.writeValueAsString(value);
    }
}
