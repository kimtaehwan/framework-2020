package com.ondot.dcs.exportdata.service.client.creditcheck.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class HttpReqData {
    String url;
    String method;
    String contentType;
    String body;
}
