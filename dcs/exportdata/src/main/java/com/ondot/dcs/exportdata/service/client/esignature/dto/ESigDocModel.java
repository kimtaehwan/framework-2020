package com.ondot.dcs.exportdata.service.client.esignature.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
@Builder
@ToString
public class ESigDocModel {
    private String applicationId;

    private Long journeyId;

    private String fiId;

    private String esignatureFile;

    private Set<ESigItemModel> esigItem;

    private Long esignatureId;

    private String esignatureContentTitle;

    private String esignatureTitle;

    private String esignatureMessage;

    private ZonedDateTime created;

    private ZonedDateTime lastModified;
}
