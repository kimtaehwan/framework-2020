package com.ondot.dcs.exportdata.service.client.journey.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class MobileDevice {

    String deviceUniqueId;
    String deviceType;
    String deviceManufacturer;
    String deviceModel;
    String deviceNotificationToken;
    String osVersion;
    String mobileNetwork;
    String countryCode;
    Date createdTime;
    Date lastModifiedTime;
}
