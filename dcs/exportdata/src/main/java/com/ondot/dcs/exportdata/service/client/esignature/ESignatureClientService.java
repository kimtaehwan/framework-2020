package com.ondot.dcs.exportdata.service.client.esignature;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.esignature.dto.ESigDocModel;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ESignatureClientService extends AbstractFeignClientService {
    private final ESignatureClient eSignatureClient;

    public SingleResult<ESigDocModel> getSignature(@Nullable String applicationReferenceId) {
        try {
            final String fileType = "base64";
            ResponseEntity<SingleResult<ESigDocModel>> responseEntity = this.eSignatureClient.getSignature(applicationReferenceId, fileType);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getSignature error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<ESigDocModel>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
