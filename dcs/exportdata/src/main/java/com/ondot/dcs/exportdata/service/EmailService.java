package com.ondot.dcs.exportdata.service;

import com.ondot.dcs.exportdata.model.cm.request.email.EmailContent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    final CMRequestHandler cmRequestHandler;

    public void sendMail(String toEmail, EmailContent emailContent) {
        cmRequestHandler.makeEmailRequest(toEmail, emailContent);
    }
}
