package com.ondot.dcs.exportdata.dto;

import lombok.*;

import java.time.OffsetDateTime;
import java.util.List;

public class ApplicationResponse {

    @Getter
    @Builder
    @ToString
    @RequiredArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Csv {
        private OffsetDateTime executedTime;
    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Email {
        private String subject;
        private Content content;
    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(staticName = "of")
    public static class Content {
        private List<String> fileNames;
        private OffsetDateTime executedTime;
        private int successRecordCount;
        private int failedRecordCount;
        private int attemptCount;
    }
}
