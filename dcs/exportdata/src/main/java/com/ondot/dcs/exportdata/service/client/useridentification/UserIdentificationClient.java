package com.ondot.dcs.exportdata.service.client.useridentification;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.useridentification.dto.UserIdentificationLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "${service.client.dcs-useridentification.name}", url = "${service.client.dcs-useridentification.url}")
public interface UserIdentificationClient {

    @GetMapping(value = "/mgmt-api/v0.9/useridentification")
    ResponseResult<UserIdentificationLog> getUserIdentificationV09(
            @RequestParam(value = "applicationReferenceId") String applicationReferenceId);
}
