package com.ondot.dcs.exportdata.service.client.regionvalidator.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class GeolocationRef {
    String applicationReferenceId;
    String userLatitude;
    String userLongitude;
    String zipCentralLatitude;
    String zipCentralLongitude;
    String address;
    boolean isInRange;
    String userDistance;
    String allowedDistance;
    Date createdTime;
    String unit;
}
