package com.ondot.dcs.exportdata.service.client.eligibility.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Data
@ToString
@Builder
public class EligibilityLog {
    private String applicationReferenceId;

    private String fiToken;

    private Set<Criteria> criterias;

    private Date createdTime;

    private String eligibilityContentTitle;

    private String eligibilityTitle;

    private String eligibilityMessage;

    private String fiName;

    private Long selectedCriteria;
}
