package com.ondot.dcs.exportdata.service.client.riskassessment;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${service.client.dcs-riskassessment.name}", url = "${service.client.dcs-riskassessment.url}")
public interface RiskAssessmentClient {

    @GetMapping(path = "/v1/logs/{applicationReferenceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<RiskReportLog>> getLog(
            @PathVariable(name = "applicationReferenceId") String applicationReferenceId);
}
