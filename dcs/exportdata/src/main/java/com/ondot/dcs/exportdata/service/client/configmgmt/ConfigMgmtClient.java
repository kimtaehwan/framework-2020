package com.ondot.dcs.exportdata.service.client.configmgmt;

import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ExportJobResponse;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.PaginationResultContent;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${service.client.dcs-configmgmt.name}", url = "${service.client.dcs-configmgmt.url}")
public interface ConfigMgmtClient {

    @GetMapping(path = "/exportJobList")
    ResponseEntity<PaginationResultContent<ExportJobResponse.Listing>> listing(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable);



    @GetMapping(path = "/exportJobList/{id}")
    ResponseEntity<ExportJobResponse.Retrieve> retrieve(@PathVariable(value = "id") Long id);

    @GetMapping(path = "/fi/{fiToken}/products",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<Product.Listing>> listingProducts(@PathVariable(value = "fiToken") String fiToken);

}
