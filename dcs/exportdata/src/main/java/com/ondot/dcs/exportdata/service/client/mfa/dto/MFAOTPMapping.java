package com.ondot.dcs.exportdata.service.client.mfa.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class MFAOTPMapping {
    String uniqueDeviceId;

    String otpCode;

    Integer index;

    Date createdTime;

    String serviceType;

    String appToken;

    String fiToken;

    int status;

    int attempts;

    String applicationReferenceId;

    String phoneNumber;
}
