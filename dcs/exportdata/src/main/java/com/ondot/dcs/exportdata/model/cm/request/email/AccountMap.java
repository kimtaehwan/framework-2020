package com.ondot.dcs.exportdata.model.cm.request.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class AccountMap{

	@JsonProperty("cardMask")
	private String cardMask;

	@JsonProperty("subscriberReferenceId")
	private String subscriberReferenceId;

	@JsonProperty("accountMask")
	private String accountMask;

	@JsonProperty("cardType")
	private String cardType;
}
