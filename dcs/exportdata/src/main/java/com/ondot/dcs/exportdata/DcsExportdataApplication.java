package com.ondot.dcs.exportdata;

import com.ondot.dcs.exportdata.config.OndotConfig;
import com.ondot.dcs.exportdata.core.StoragePathConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties({StoragePathConfiguration.class})
public class DcsExportdataApplication {

    public static void main(String[] args) {
        SpringApplication.run(DcsExportdataApplication.class, args);
    }

}
