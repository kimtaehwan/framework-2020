package com.ondot.dcs.exportdata.service.client.userverification;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.userverification.dto.UserVerificationLog;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserVerificationClientService extends AbstractFeignClientService {
    private final UserVerificationClient userVerificationClient;


    public UserVerificationLog getUserVerificationV09(@NotNull String applicationReferenceId) {
        try {
            final ResponseResult<UserVerificationLog> responseResult = this.userVerificationClient.getUserVerificationV09(applicationReferenceId);
            return Optional.ofNullable(responseResult).map(ResponseResult::getData).orElse(null);
        } catch (FeignException e) {
            log.error("getUserVerificationV09 error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return null;
        }
    }
}
