// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.dto.core;


import lombok.*;
import org.springframework.util.MultiValueMap;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PaginationResult<T> extends ListResult<T> {

    int totalPages;
    long totalCount;

    @Builder(builderMethodName = "paginationResultBuilder")
    public PaginationResult(ResultStatus status, MultiValueMap<String, String> httpHeaders, List<T> contents, int totalPages, long totalCount) {
        super(status, httpHeaders, contents);
        this.totalPages = totalPages;
        this.totalCount = totalCount;
    }

}
