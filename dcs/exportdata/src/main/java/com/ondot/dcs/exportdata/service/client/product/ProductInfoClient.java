package com.ondot.dcs.exportdata.service.client.product;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.product.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-productinfo.name}", url = "${service.client.dcs-productinfo.url}")
public interface ProductInfoClient {

    @GetMapping(value = "/v2/product", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<Product>> getCustomerProduct(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId,
            @RequestHeader(name = "image-type", defaultValue = "url") String imageType);
}
