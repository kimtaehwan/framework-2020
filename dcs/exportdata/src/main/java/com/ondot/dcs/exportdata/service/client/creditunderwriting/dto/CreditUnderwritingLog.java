package com.ondot.dcs.exportdata.service.client.creditunderwriting.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.data.HttpReqData;
import com.ondot.dcs.exportdata.service.client.creditunderwriting.dto.data.HttpRspData;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditUnderwritingLog {
    @EqualsAndHashCode.Include
    String      applicationId;
    String      platformType;
    @EqualsAndHashCode.Include
    String      creditClassCode;
    @EqualsAndHashCode.Include
    String      creditLimit;
    @EqualsAndHashCode.Include
    String      apr;
    String      approvalSegmentation;
    String      standardPolicy;
    Date reqTime;
    Date        respTime;
    HttpReqData requestData;
    HttpRspData responseData;
}
