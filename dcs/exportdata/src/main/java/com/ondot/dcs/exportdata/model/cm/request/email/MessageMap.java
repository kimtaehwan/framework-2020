package com.ondot.dcs.exportdata.model.cm.request.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class MessageMap{

	@JsonProperty("email")
	private List<EmailItem> email;
}
