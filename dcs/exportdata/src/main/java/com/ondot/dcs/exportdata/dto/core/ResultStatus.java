package com.ondot.dcs.exportdata.dto.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class ResultStatus {

    public static final int RESULT_STATUS_CODE_SUCCESS = 0;
    public static final ResultStatus OK = new ResultStatus(RESULT_STATUS_CODE_SUCCESS, HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
    public static final ResultStatus CREATED = new ResultStatus(RESULT_STATUS_CODE_SUCCESS, HttpStatus.CREATED, HttpStatus.CREATED.getReasonPhrase());

    @EqualsAndHashCode.Include
    int code;
    @JsonIgnore
    @EqualsAndHashCode.Include
    HttpStatus httpStatus;
    String message;

    @JsonIgnore
    public boolean isSuccess() {
        return this.code == RESULT_STATUS_CODE_SUCCESS;
    }
}
