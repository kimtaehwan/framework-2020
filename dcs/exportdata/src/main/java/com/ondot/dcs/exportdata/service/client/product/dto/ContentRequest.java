// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.product.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class ContentRequest {

    Integer fiId;
    Integer journeyId;

}
