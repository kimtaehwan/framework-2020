package com.ondot.dcs.exportdata.service.client.terms.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@Builder
public class TermsStatusLog {
    private String applicationReferenceId;

    private String fiToken;

    private Long termId;

    // [0=Pending,1=Accepted,2=Rejected]
    private Integer status;

    private String htmlFileLocation;

    private Date createdTime;

    private String contentTitle;
}
