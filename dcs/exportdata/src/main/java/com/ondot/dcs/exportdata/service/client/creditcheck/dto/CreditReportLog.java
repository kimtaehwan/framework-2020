package com.ondot.dcs.exportdata.service.client.creditcheck.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditReportLog {
    String applicationId;
    String creditScore;
    Date reqTime;
    Date respTime;
    HttpReqData requestData;
    HttpRespData responseData;
}
