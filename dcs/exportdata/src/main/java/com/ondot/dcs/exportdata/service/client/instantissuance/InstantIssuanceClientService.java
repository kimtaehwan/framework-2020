package com.ondot.dcs.exportdata.service.client.instantissuance;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.InstantIssuanceInfo;
import com.ondot.dcs.exportdata.service.client.instantissuance.dto.IssuanceResponse;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InstantIssuanceClientService extends AbstractFeignClientService {

    private final InstantIssuanceClient instantIssuanceClient;

    public SingleResult<InstantIssuanceInfo> getInstantIssuanceInfo(@NonNull String applicationReferenceId) {
        try {
            final ResponseEntity<SingleResult<InstantIssuanceInfo>> responseEntity = this.instantIssuanceClient.getInstantIssuanceInfo(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            return SingleResult.<InstantIssuanceInfo>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
