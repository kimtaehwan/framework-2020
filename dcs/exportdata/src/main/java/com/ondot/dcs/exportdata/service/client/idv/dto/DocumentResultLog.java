package com.ondot.dcs.exportdata.service.client.idv.dto;

import com.ondot.dcs.exportdata.dto.core.ResultStatus;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class DocumentResultLog {
    String ticketId;
    Integer ticketUsageCount;
    Integer ticketUsageLimit;
    String traceId;
    ResultStatus status;
    Date reqTime;
    Date respTime;
    Integer docType;
    String docFrontImagePath;
    String docBackImagePath;
    String docPhotoImagePath;
    String selfieImagePath;
    VerificationResult docVerificationResult;
    Footmark footmark;
    Date logStartTime;
    Date logEndTime;
}
