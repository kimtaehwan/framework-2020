package com.ondot.dcs.exportdata.exception;

public class ExternalApiException extends Exception{
    public ExternalApiException(String msg){
        super(msg);
    }
}
