package com.ondot.dcs.exportdata.service.client.instantissuance.dto;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InstantIssuanceInfo {
    String applicationReferenceId;

    String firstName;

    String lastName;

    String pan;

    String expiryDate;

    String cvv;

    String address1;

    String address2;

    String city;

    String state;
}
