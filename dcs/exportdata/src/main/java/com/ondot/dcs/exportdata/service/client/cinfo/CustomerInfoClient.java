package com.ondot.dcs.exportdata.service.client.cinfo;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.cinfo.dto.VerifiedApplicationForm;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-cinfo.name}", url = "${service.client.dcs-cinfo.url}")
public interface CustomerInfoClient {
    @GetMapping(value = "/v1/cinfo",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<VerifiedApplicationForm>> getCustomerVerifiedApplicationForm(
            @RequestHeader(value = "x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
