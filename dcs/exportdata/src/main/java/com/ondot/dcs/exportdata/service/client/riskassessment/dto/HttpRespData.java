// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.riskassessment.dto;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class HttpRespData {

    Integer httpCode;
    String httpStatus;
    String httpDescription;
    String contentType;
    String body;
}
