package com.ondot.dcs.exportdata.service.client.journey;

import com.ondot.dcs.exportdata.dto.core.PaginationResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.journey.dto.ApplicationReference;
import com.ondot.dcs.exportdata.service.client.journey.dto.JourneyProcessLogInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@FeignClient(name = "${service.client.dcs-journey.name}", url = "${service.client.dcs-journey.url}")
public interface JourneyClient {

    @GetMapping(value = "/api/v2/applications/references", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<PaginationResult<ApplicationReference>> getReferenceList(
            @RequestParam(name = "page", required = false, defaultValue = "0") @Min(0) int page,
            @RequestParam(name = "size", required = false, defaultValue = "20") @Min(1) @Max(100) int size,
            @RequestParam(name = "startTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ZonedDateTime startTime,
            @RequestParam(name = "endTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ZonedDateTime endTime,
            @RequestParam(name = "appToken", required = false) @Size(min = 1, max = 40) String appToken,
            @RequestParam(name = "fiToken", required = false) @Size(min = 1, max = 40) String fiToken);

    @GetMapping(path = "/api/v2/applications/references/{applicationReferenceId}/process-log")
    ResponseEntity<SingleResult<JourneyProcessLogInfo>> getApplicationJourneyProcessLogInfo(@PathVariable(name = "applicationReferenceId") String applicationReferenceId);
}
