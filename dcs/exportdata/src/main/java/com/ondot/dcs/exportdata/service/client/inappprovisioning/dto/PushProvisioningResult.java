package com.ondot.dcs.exportdata.service.client.inappprovisioning.dto;

import lombok.*;

import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PushProvisioningResult {
    private String applicationReferenceId;
    private String paymentNetworkType;
    private String walletType;
    private String userWalletId;
    private String resultCode;
    private String results;
    private String provisioningToken;
    private ZonedDateTime created;
    private ZonedDateTime lastModified;
}
