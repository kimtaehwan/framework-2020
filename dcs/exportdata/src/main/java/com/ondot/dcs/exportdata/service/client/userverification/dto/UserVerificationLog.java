package com.ondot.dcs.exportdata.service.client.userverification.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserVerificationLog {
    private String applicationReferenceId;
    private String externalCustomerId;
    private String stateStatus;
    private String verificationFieldType;
    @JsonIgnore
    private String verificationFieldValue;
    private String userSsn;
    private String userPhoneNumber;
    private String userDob;
}
