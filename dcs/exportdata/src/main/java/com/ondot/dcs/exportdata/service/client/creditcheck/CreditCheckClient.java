package com.ondot.dcs.exportdata.service.client.creditcheck;

import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.creditcheck.dto.CreditReportLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${service.client.dcs-creditcheck.name}", url = "${service.client.dcs-creditcheck.url}")
public interface CreditCheckClient {

    @GetMapping(path = "/v1/logs/{applicationReferenceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<SingleResult<CreditReportLog>> getLog(@PathVariable(name = "applicationReferenceId") String applicationReferenceId);

    @GetMapping(path = "/v1/logs/{applicationReferenceId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseResult<CreditReportLog> getLogV09(
            @PathVariable(name = "applicationReferenceId") String applicationReferenceId);
}
