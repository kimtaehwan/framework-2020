// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.model.cm.request.sms;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class Message {
    String to;
    String message;
}
