// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.service.client.product.dto;

import lombok.*;

import java.time.ZonedDateTime;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@ToString
public class Product {
    private Long id;
    private String type;
    private String status;
    private String name;
    private String description;
    private String image;
    private ZonedDateTime created;
    private ZonedDateTime lastModified;
}
