// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.model.cm.request.sms;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class CMRequestSMS {

    String appToken;
    String fiToken;
    Integer retryCount;
    String alertCategory;
    String alertTypeCode;
    String notificationReferenceId;
    AccountMap accountMap;
    TransactionMap transactionMap;
    ChannelPriority channelPriority;
    MessageMap messageMap;


}
