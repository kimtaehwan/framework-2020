package com.ondot.dcs.exportdata.service.client.mfa;

import com.ondot.dcs.exportdata.dto.core.PaginationResult;
import com.ondot.dcs.exportdata.service.client.mfa.dto.MFAOTPMapping;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.ZonedDateTime;

@FeignClient(name = "${service.client.dcs-mfa.name}", url = "${service.client.dcs-mfa.url}")
public interface MfaClient {
    @GetMapping(path = "/v1/list")
    ResponseEntity<PaginationResult<MFAOTPMapping>> getMFAOTPMappingList(
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "startTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ZonedDateTime startTime,
            @RequestParam(name = "endTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ZonedDateTime endTime,
            @RequestParam(name = "appToken", required = false) String appToken,
            @RequestParam(name = "fiToken", required = false) String fiToken,
            @RequestParam(name = "applicationReferenceId", required = false) String applicationReferenceId);
}
