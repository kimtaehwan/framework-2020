package com.ondot.dcs.exportdata.service.client.riskassessment;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.riskassessment.dto.RiskReportLog;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RiskAssessmentClientService extends AbstractFeignClientService {

    private final RiskAssessmentClient riskAssessmentClient;

    public SingleResult<RiskReportLog> getRiskReportLog(@NonNull String applicationReferenceId) {
        try {
            ResponseEntity<SingleResult<RiskReportLog>> responseEntity = this.riskAssessmentClient.getLog(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                log.warn("failed to get RiskReportLog. HttpStatus is NOT_FOUND");
            } else {
                log.error(String.format("failed to get RiskReportLog. ex=%s", e.toString()));
            }

            return SingleResult.<RiskReportLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        } catch (Exception e) {
            log.error(String.format("failed to get RiskReportLog. ex=%s", e.toString()), e);
            return SingleResult.<RiskReportLog>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
