// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.model.cm.request.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class ChannelPriority {

    @JsonProperty(value = "1")
    List<String> one;
}
