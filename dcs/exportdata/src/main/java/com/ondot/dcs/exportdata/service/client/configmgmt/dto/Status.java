package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

public enum Status {
    PUBLISHED, DRAFT
}
