// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.model.cm.request.sms;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class MessageMap {

    List<Message> sms;
}
