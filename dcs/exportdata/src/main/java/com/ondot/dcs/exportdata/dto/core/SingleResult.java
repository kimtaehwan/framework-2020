// Copyright 2011-2020 Ondot Systems, Inc.
package com.ondot.dcs.exportdata.dto.core;

import lombok.*;
import org.springframework.util.MultiValueMap;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SingleResult<T> extends BaseResult {

    T content;

    @Builder(builderMethodName = "singleResultBuilder")
    public SingleResult(ResultStatus status, MultiValueMap<String, String> httpHeaders, T content) {
        super(status, httpHeaders);
        this.content = content;
    }

}
