package com.ondot.dcs.exportdata.controller;

import com.ondot.dcs.exportdata.dto.ApplicationResponse;
import com.ondot.dcs.exportdata.dto.core.ResponseResult;
import com.ondot.dcs.exportdata.model.ExportJobDetail;
import com.ondot.dcs.exportdata.service.ExportdataService;
import com.ondot.dcs.exportdata.service.TracerService;
import com.ondot.dcs.exportdata.service.client.configmgmt.ConfigMgmtClientService;
import com.ondot.dcs.exportdata.service.client.configmgmt.dto.ExportJobResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/v0.9/exportdata")
@RequiredArgsConstructor
@Slf4j
public class ExportdataController {

    final TracerService tracerService;
    final ExportdataService exportdataService;
    final ConfigMgmtClientService configMgmtClientService;

    @GetMapping(path = "/applications/csv")
    public ResponseEntity<ResponseResult<ApplicationResponse.Csv>> getApplicationsCsv() {

        // dcs-configmgmt 에서 exportJobList 를 읽어오기.
        // 매일 밤 00시 에, export data list 를 가져온다.
        // 모든 List 정보를 가져온다.
        OffsetDateTime from = OffsetDateTime.now();
        OffsetDateTime to = OffsetDateTime.now();
        Pageable pageable = PageRequest.of(0, 10);
        List<ExportJobResponse.Listing> exportJobList = configMgmtClientService.getList(pageable, null, null, from, to, null);

        // int
        List<Long> exportJobIdList = exportJobList.stream()
                .map(ExportJobResponse.Listing::getId)
                .collect(Collectors.toList());

        log.info("# exportJob idList: {}", exportJobIdList);
        for (Long exportId : exportJobIdList) {
            ExportJobResponse.Retrieve retrieve = configMgmtClientService.retrieve(exportId);

            log.info("retrieve: ", retrieve.toString());

            /**
             * 1. ExportdataService 에 던져서, applicationResponse 관련 작업을 처리해야 함.
             * 2. csv 및 application 파일 만들기
             * 3. 파일 압축
             * 4. sftp 파일 전송
             * 5. mail 보내기
             */
            ExportJobDetail detail = exportdataService.convertExportJobDetail(retrieve);

            // monthly 인데, 1일이 아닐 경우는 실행 안함.
            ZonedDateTime now = ZonedDateTime.now();
            boolean passMonthly = exportdataService.checkPassMonthly(now, detail.getScheduleExport());

            ExportJobResponse.Result result = exportdataService.executeJob(detail);
        }

//        ResponseEntity<List<Product.Listing>> listResponseEntity = configMgmtClientService.listingProducts("onebank");

//        ZonedDateTime startTime = DateUtil.getMinZonedDateTime(startDate);
//        ZonedDateTime endTime = DateUtil.getMaxZonedDateTime(endDate);
//
//        log.info("### startTime: {}", startTime);
//        log.info("### endTime: {}", endTime);
//
//        ApplicationResponse.Csv applicationsResponse = this.exportdataService.getApplicationsResponse(fiTokens, startTime, endTime, emails);
//        List<ApplicationResponse.Csv> dataList = new ArrayList<>();
//        dataList.add(applicationsResponse);
//
//        ResponseResult<ApplicationResponse.Csv> responseResult = ResponseResult.<ApplicationResponse.Csv>builder()
//                .apiVersion("v0.9")
//                .status(ResponseResult.ResponseStatus.SUCCESS)
//                .dataList(dataList)
//                .traceId(tracerService.getTraceId())
//                .build();

        return ResponseEntity.ok(null);
    }
}
