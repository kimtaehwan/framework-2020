package com.ondot.dcs.exportdata.service.client.regionvalidator;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.regionvalidator.dto.GeolocationRef;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${service.client.dcs-regionvalidator.name}", url = "${service.client.dcs-regionvalidator.url}")
public interface RegionValidatorClient {
    @GetMapping(path = "/v1/find")
    ResponseEntity<SingleResult<GeolocationRef>> find(
            @RequestHeader("x-ondotsystems-applicationReferenceId") String applicationReferenceId);
}
