package com.ondot.dcs.exportdata.service.client.esignature.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ESigItemModel {
    private Long esignatureItemId;

    private Long esignatureId;

    private String content;

    private Long termsId;

    private boolean accepted;
}
