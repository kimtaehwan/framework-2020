package com.ondot.dcs.exportdata.service.client.journey.dto;

public enum JourneyProcessType {
    START,
    PRODUCT,                    // 1) Product Type / Product
    MFA,                        // 2) Mobile Number Verification
    IDV,                        // 3) ID&V
    CREDIT_CHECK,               // 4) Credit Check
    INSTANT_ISSUANCE,           // 5) Instant Issuance
    ACTIVATE_CARD,              // 6) Activate Card
    PUSH_PROVISIONING,          // 7) Push Provisioning
    //DEVICE_ACTIVATION,          //  ?) Device Activation
    EMAIL_CHECKING,             // 8) Email Checking
    CARDHOLDER_VERIFICATION,    // 9) Cardholder Verification (yodlee)
    HOME_ADDRESS_VERIFICATION,  // 10) Home Address Verification
    TERMS_AND_CONDITIONS,       // 11) Terms & Condition
    ESIGNATURE,                 // 12) E-Signature
    CINFO,                      // 13) Customer Information
    RISK_ASSESSMENT,            // 14) Risk Assessment
    ELIGIBILITY_CHECK,          // 15) Eligibility Check
    LOGIN_CREATION,             // 16) Login Creation
    USER_IDENTIFICATION,        // 17) User Identification
    USER_VERIFICATION,          // 18) User Verification
    PRODUCT_DUPLICATE_DETECTION,// 19) Product Duplicate Detection
    CREDIT_DECISIONING,
    END
}
