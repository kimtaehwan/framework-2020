package com.ondot.dcs.exportdata.service.client.inappprovisioning;

import com.ondot.dcs.exportdata.dto.core.SingleResult;
import com.ondot.dcs.exportdata.service.client.AbstractFeignClientService;
import com.ondot.dcs.exportdata.service.client.inappprovisioning.dto.PushProvisioningResult;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InAppProvisioningClientService extends AbstractFeignClientService {

    private final InAppProvisioningClient inAppProvisioningClient;

    public SingleResult<PushProvisioningResult> getPushProvisioningResult(@NonNull String applicationReferenceId) {
        try {
            final ResponseEntity<SingleResult<PushProvisioningResult>> responseEntity = this.inAppProvisioningClient.getPushProvisioningResult(applicationReferenceId);
            return responseEntity.getBody();
        } catch (FeignException e) {
            log.error("getPushProvisioningResult error. applicationReferenceId={}, ex={}", applicationReferenceId, e.toString());
            return SingleResult.<PushProvisioningResult>singleResultBuilder().status(this.toResultStatus(e)).build();
        }
    }
}
