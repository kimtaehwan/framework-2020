package com.ondot.dcs.exportdata.service.client.configmgmt.dto;

import lombok.*;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExportJobResponse {

    @Getter
    @Setter
    @ToString
    public static class Listing{
        private Long id;
        private OffsetDateTime lastExecutedOn;
        private String fiName;
        private String exportJobName;
        private OffsetDateTime nextExecutionDate;
        private ScheduleExport jobExecutionSchedule;
        private Status status;
    }

    @Getter
    @Setter
    @ToString
    @Builder
    public static class Retrieve {
        private Long id;
        private String fiName;
        private String fiToken;
        private String exportJobName;

        private Boolean approvedAndCardIssued;
        private Boolean approvedAndCardNotIssued;
        private Boolean notApproved;
        private Boolean dataCollectionInprogress;
        private Boolean dataCollectionFail;

        private String emailAddress1;
        private String emailAddress2;

        private ScheduleExport scheduleExport;

        private OffsetDateTime dateRangeFrom;
        private OffsetDateTime dateRangeTo;
    }

    @Getter
    @Setter
    @ToString
    public static class Result {
        private Boolean isSuccess;
        private OffsetDateTime execute;
    }
}
