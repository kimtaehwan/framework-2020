package com.ondot.dcs.gateway;

import com.ondot.dcs.gateway.config.RbacProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({RbacProperties.class})
public class DcsGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DcsGatewayApplication.class, args);
    }

}
