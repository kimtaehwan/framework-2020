package com.ondot.dcs.gateway.vo;

import java.util.HashMap;
import java.util.Map;

public enum Menus {
    APPLICATION_LIST(9, "application-list"),
    JOURNEY(10, "journey"),
    JOURNEY_POINT(11, "journey-point"),
    PROCESS(12, "process"),
    PRODUCT(13, "product"),
    CONSENT(14, "consent"),
    ELIGIBILITY(15, "eligibility"),
    E_SIGNATURE(16, "esignature"),
    APPLICATION_FORM(17, "application-form"),
    POLICY_CONTENT(26, "policy-contents"),
    CLOSING_PAGE(28, "closing-page"),
    CREADIT_UNDERWRITING(25, "credit-underwriting"),
    FI_ONBOARDING(20, "financialinstitutions"),
    GLOBAL(21, "global"),
    EXPORT_DATA(27, "export-data"),
    DEFAULT_LIST(100, "default-list")
    ;

    private int id;
    private String name;

    Menus(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private static final Map<String, Menus> byName = new HashMap<>();
    static {
        for (Menus menus : Menus.values()) {
            if (byName.put(menus.getName(), menus) != null) {
                throw new IllegalArgumentException("duplicate name: " + menus.getName());
            }
        }
    }

    public static Menus valueOfName(String name) {
        return byName.get(name);
    }
}
