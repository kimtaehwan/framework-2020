package com.ondot.dcs.gateway.vo;

import java.util.regex.Pattern;

public class RbacConstants {
    public static final String AUTHORIZATION = "Authorization";
    public static final String CUSTOM_DCS_PRIVILEGES = "custom:dcs:privileges";
    public static final String CUSTOM_DCS_DATA = "custom:dcsData";

    public static final Pattern BEARER = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE);
}
