package com.ondot.dcs.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ondot.dcs.gateway.config.RbacProperties;
import com.ondot.dcs.gateway.vo.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.impl.DefaultJwtParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.ondot.dcs.gateway.vo.Menus.APPLICATION_LIST;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class RbacService {

    @Autowired
    private RbacProperties rbacProperties;

    public boolean checkAuthority(RbacRequest request) {
        log.info("rbacRequest: {}", request.toString());
        String authorizationToken = obtainAuthorizationToken(request.getToken());
        if (authorizationToken != null) {
            try {
                Claims claims = decodeTokenClaims(authorizationToken);
                String dcsData = claims.get(RbacConstants.CUSTOM_DCS_DATA, String.class);
                List<String> authorityList = obtainDcsData(dcsData);
                return isValidAuthority(request.getMethodName(), request.getApiPath(), authorityList);
            } catch (Exception e) {
                log.warn("Jwt processing failed: {}", e.getMessage());
            }
        }

        return false;
    }

    public boolean getContainsUrl(List<String> paths, String apiPath) {
        return paths.stream()
                .anyMatch(apiPath::contains);
    }

    public String obtainAuthorizationToken(String token) {
        try {
            token = URLDecoder.decode(token, "UTF-8");
            String[] parts = token.split(" ");
            if (parts.length == 2) {
                String scheme = parts[0];
                String credentials = parts[1];
                return RbacConstants.BEARER.matcher(scheme).matches() ? credentials : null;
            }
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Claims decodeTokenClaims(String token) {
        String[] splitToken = token.split("\\.");
        String unsignedToken = splitToken[0] + "." + splitToken[1] + ".";

        DefaultJwtParser parser = new DefaultJwtParser();
        Jwt<?, ?> jwt = parser.parse(unsignedToken);
        return (Claims) jwt.getBody();
    }

    public List<String> obtainPrivileges(Claims claims) {
        String privileges = claims.get(RbacConstants.CUSTOM_DCS_PRIVILEGES, String.class);
        List<String> roles = convertToList(privileges);

        log.info("roles: {}", roles);

        if (roles.isEmpty()) {
            return Collections.emptyList();
        }

        return roles.stream()
                .map(String::new)
                .collect(toList());
    }

    public List<String> obtainDcsData(String dcsData) {
        log.info("obtainDcsData: {}", dcsData);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            DcsData dcsDataObj = objectMapper.readValue(dcsData, DcsData.class);
            log.info("permissions: {}", dcsDataObj);
            List<String> authorityList = convertAuthorityList(dcsDataObj);
            return authorityList;
        } catch (Exception e) {
            log.error("convert error: {}", e);
        }

        return Collections.emptyList();
    }

    private List<String> convertAuthorityList(DcsData dcsData) {
        List<String> authorityList = new ArrayList<>();
        Permissions permissions = dcsData.getPermissions();

        if (!permissions.getDcsCustomerSupport().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsCustomerSupport(), Menus.APPLICATION_LIST));
        }

        if (!permissions.getDcsJourney().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsJourney(), Menus.JOURNEY));
        }

        if (!permissions.getDcsJourneyPoint().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsJourneyPoint(), Menus.JOURNEY_POINT));
        }

        if (!permissions.getDcsProcess().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsProcess(), Menus.PROCESS));
        }

        if (!permissions.getDcsProduct().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsProduct(), Menus.PRODUCT));
        }

        if (!permissions.getDcsConsent().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsConsent(), Menus.CONSENT));
        }

        if (!permissions.getDcsEligibility().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsEligibility(), Menus.ELIGIBILITY));
        }

        if (!permissions.getDcsESignature().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsESignature(), Menus.E_SIGNATURE));
        }

        if (!permissions.getDcsApplicationForm().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsApplicationForm(), Menus.APPLICATION_FORM));
        }

        if (!permissions.getDcsPolicyContent().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsPolicyContent(), Menus.POLICY_CONTENT));
        }

        if (!permissions.getDcsClosingPage().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsClosingPage(), Menus.CLOSING_PAGE));
        }

        if (!permissions.getDcsCreditUnderwriting().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsCreditUnderwriting(), Menus.CREADIT_UNDERWRITING));
        }

        if (!permissions.getDcsFIOnboarding().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsFIOnboarding(), Menus.FI_ONBOARDING));
        }

        if (!permissions.getDcsGlobal().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsGlobal(), Menus.GLOBAL));
        }

        if (!permissions.getDcsExportData().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsExportData(), Menus.EXPORT_DATA));
        }

        if (!permissions.getDcsDefaultList().isEmpty()) {
            authorityList.addAll(getAuthorityList(permissions.getDcsDefaultList(), Menus.DEFAULT_LIST));
        }

        return authorityList;
    }

    private List<String> getAuthorityList(List<String> authList, Menus menus) {
        List<String> resultList = new ArrayList<>();

        for (String auth : authList) {
            resultList.add(getAuthorityNameToCas(auth, menus));
        }

        return resultList;
    }

    private String getAuthorityNameToCas(String auth, Menus menus) {
        return "ag." + menus.getId() + "." +auth;
    }

    public List<String> convertToList(String privileges) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(privileges, List.class);
        } catch (Exception e) {
            log.error("convert error: {}", e);
        }

        return Collections.emptyList();
    }

    public boolean isValidAuthority(String methodName, String apiPath, List<String> authorityList) {
        Menus menus = getMenus(apiPath);
        if (menus == null) {
            return false;
        }

        if (Menus.DEFAULT_LIST.equals(menus)) {
            return true;
        }

        String authorityName = getAuthorityName(methodName, menus);
        if (authorityList.contains(authorityName)) {
            return true;
        }

        return false;
    }

    public Menus getMenus(String apiPath) {

        if (getContainsUrl(rbacProperties.getApplicationList(), apiPath)) {
            return APPLICATION_LIST;
        }

        if ( getContainsUrl(rbacProperties.getFinancialInstitutions(), apiPath)) {
            return Menus.FI_ONBOARDING;
        }

        if ( getContainsUrl(rbacProperties.getJourney(), apiPath)) {
            return Menus.JOURNEY;
        }

        if ( getContainsUrl(rbacProperties.getJourneyPoint(), apiPath)) {
            return Menus.JOURNEY_POINT;
        }

        if ( getContainsUrl(rbacProperties.getProcess(), apiPath)) {
            return Menus.PROCESS;
        }

        if ( getContainsUrl(rbacProperties.getProduct(), apiPath)) {
            return Menus.PRODUCT;
        }

        if ( getContainsUrl(rbacProperties.getConsent(), apiPath)) {
            return Menus.CONSENT;
        }

        if ( getContainsUrl(rbacProperties.getEligibility(), apiPath)) {
            return Menus.ELIGIBILITY;
        }

        if (getContainsUrl(rbacProperties.getEsignature(), apiPath))  {
            return Menus.E_SIGNATURE;
        }

        if (getContainsUrl(rbacProperties.getApplicationForm(), apiPath)) {
            return Menus.APPLICATION_FORM;
        }

        if (getContainsUrl(rbacProperties.getPolicyContents(), apiPath)) {
            return Menus.POLICY_CONTENT;
        }

        if (getContainsUrl(rbacProperties.getClosingPage(), apiPath)) {
            return Menus.CLOSING_PAGE;
        }

        if (getContainsUrl(rbacProperties.getCreditUnderwriting(), apiPath)) {
            return Menus.CREADIT_UNDERWRITING;
        }

        if (getContainsUrl(rbacProperties.getGlobal(), apiPath)) {
            return Menus.GLOBAL;
        }

        if (getContainsUrl(rbacProperties.getExportData(), apiPath)) {
            return Menus.EXPORT_DATA;
        }

        if ( getContainsUrl(rbacProperties.getDefaultList(), apiPath)) {
            return Menus.DEFAULT_LIST;
        }

        return null;
    }

    public String getAuthorityName(String methodName, Menus menus) {
        if (StringUtils.equalsAny(methodName, "GET")) {
            return "ag." + menus.getId() + ".view";
        }

        return "ag." + menus.getId() + ".edit";
    }

    public RbacRequest convertToRbacRequest(ServerHttpRequest request) {
        List<String> tokens = request.getHeaders().get(RbacConstants.AUTHORIZATION);
        if (tokens == null || tokens.isEmpty()) {
            return null;
        }

        HttpMethod method = request.getMethod();
        if (method == null) {
            return null;
        }

        RequestPath path = request.getPath();

        return RbacRequest.builder()
                .token(tokens.get(0))
                .methodName(method.name())
                .apiPath(path.toString())
                .build();
    }

    public boolean checkDefaultPass(ServerHttpRequest request) {
        RequestPath path = request.getPath();
        String apiPath = path.toString();
        return getContainsUrl(rbacProperties.getImages(), apiPath);
    }
}
