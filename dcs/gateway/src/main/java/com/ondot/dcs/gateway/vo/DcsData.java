package com.ondot.dcs.gateway.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DcsData {

    @JsonProperty("permissions")
    private Permissions permissions;

    @JsonProperty("fiList")
    private List<String> fiList;

    @JsonProperty("JSESSIONID")
    private String jsessionId;

    @JsonProperty("OndotSessionId")
    private String ondotSessionId;
}
