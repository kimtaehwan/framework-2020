package com.ondot.dcs.gateway.filter;

import com.ondot.dcs.gateway.service.RbacService;
import com.ondot.dcs.gateway.vo.RbacRequest;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class JwtAuthenticationTokenFilter extends AbstractGatewayFilterFactory<JwtAuthenticationTokenFilter.Config> {

    @Autowired
    private RbacService rbacService;

    public JwtAuthenticationTokenFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            log.info("JwtAuthenticationTokenFilter baseMessage >>> {}", config.getBaseMessage());

            if (config.isPreLogger()) {
                log.info("JwtAuthenticationTokenFilter Start >>> {}", exchange.getRequest());
                boolean isDefaultPass = rbacService.checkDefaultPass(exchange.getRequest());

                if (!isDefaultPass) {
                    RbacRequest request = rbacService.convertToRbacRequest(exchange.getRequest());
                    if (request == null) {
                        log.error("request Invalid");
                        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                        return exchange.getResponse().setComplete();
                    }

                    log.info("RbacRequest.toString(): {}", request.toString());
                    if (!rbacService.checkAuthority(request)) {
                        log.info("JwtAuthenticationTokenFilter - InValid Authority");
                        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                        return exchange.getResponse().setComplete();
                    }
                }
            }

            return chain.filter(exchange)
                    .then(Mono.fromRunnable(() -> {
                        if (config.isPostLogger()) {
                            log.info("JwtAuthenticationTokenFilter End >>> {}", exchange.getResponse());
                        }
                    }));
        });
    }

    @Data
    public static class Config {
        private String baseMessage;
        private boolean preLogger;
        private boolean postLogger;
    }
}
