package com.ondot.dcs.gateway.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Permissions {
    @JsonProperty("DCS-CustomerSupport")
    private List<String> dcsCustomerSupport;

    @JsonProperty("DCS-FIOnboarding")
    private List<String> dcsFIOnboarding;

    @JsonProperty("DCS-Journey")
    private List<String> dcsJourney;

    @JsonProperty("DCS-JourneyPoint")
    private List<String> dcsJourneyPoint;

    @JsonProperty("DCS-Process")
    private List<String> dcsProcess;

    @JsonProperty("DCS-Product")
    private List<String> dcsProduct;

    @JsonProperty("DCS-Consent")
    private List<String> dcsConsent;

    @JsonProperty("DCS-Eligibility")
    private List<String> dcsEligibility;

    @JsonProperty("DCS-E-Signature")
    private List<String> dcsESignature;

    @JsonProperty("DCS-ApplicationForm")
    private List<String> dcsApplicationForm;

    @JsonProperty("DCS-PolicyContent")
    private List<String> dcsPolicyContent;

    @JsonProperty("DCS-ClosingPage")
    private List<String> dcsClosingPage;

    @JsonProperty("DCS-CreditUnderwriting")
    private List<String> dcsCreditUnderwriting;

    @JsonProperty("DCS-GLOBAL")
    private List<String> dcsGlobal;

    @JsonProperty("DCS-EXPORT_DATA")
    private List<String> dcsExportData;

    @JsonProperty("DCS-DEFAULT_LIST")
    private List<String> dcsDefaultList;


}
