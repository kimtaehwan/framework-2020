package com.ondotsystems.dcsconfigmgmt.entity;

import lombok.*;

@Getter
@ToString
@Builder
public class FinancialInstitution {
    private Long id;

    private String name;

    private String contactEmailAddress;

    private String contactPhoneNumber;
}
