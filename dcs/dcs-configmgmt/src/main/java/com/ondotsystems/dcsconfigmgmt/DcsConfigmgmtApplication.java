package com.ondotsystems.dcsconfigmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DcsConfigmgmtApplication {

    public static void main(String[] args) {
        SpringApplication.run(DcsConfigmgmtApplication.class, args);
    }

}
