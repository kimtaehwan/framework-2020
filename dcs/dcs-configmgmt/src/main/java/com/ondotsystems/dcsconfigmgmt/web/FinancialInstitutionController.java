package com.ondotsystems.dcsconfigmgmt.web;

import com.ondotsystems.dcsconfigmgmt.entity.FinancialInstitution;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class FinancialInstitutionController {

    @GetMapping(value = "/financialInstitutions")
    public ResponseEntity listingFinancialInstitutions() {
        FinancialInstitution financialInstitution = FinancialInstitution.builder()
                .id(1L)
                .name("ondot")
                .contactEmailAddress("ondot@systems.com")
                .contactPhoneNumber("123456")
                .build();

        return ResponseEntity.ok(financialInstitution);
    }

    @GetMapping(value = "/financialInstitutions/{id}")
    public ResponseEntity retrieveAFinancialInstitution(@PathVariable(value = "id") Long id) {
        FinancialInstitution financialInstitution = FinancialInstitution.builder()
                .id(2L)
                .name("id - ondot")
                .contactEmailAddress("ondot@systems.com")
                .contactPhoneNumber("123456")
                .build();

        return ResponseEntity.ok(financialInstitution);
    }

    @GetMapping(value = "/api/customer-support/idv/{id}")
    public ResponseEntity retriveAIdv(@PathVariable(value = "id") Long id) {
        FinancialInstitution financialInstitution = FinancialInstitution.builder()
                .id(id)
                .name("id - ondot")
                .contactEmailAddress("ondot@systems.com")
                .contactPhoneNumber("123456")
                .build();

        return ResponseEntity.ok(financialInstitution);
    }

    @GetMapping(value = "/mfa/v1/list")
    public ResponseEntity retriveAMfa(@RequestParam(value = "applicationReferenceId") String applicationReferenceId) {
        FinancialInstitution financialInstitution = FinancialInstitution.builder()
                .id(1L)
                .name(applicationReferenceId)
                .contactEmailAddress("ondot@systems.com")
                .contactPhoneNumber("123456")
                .build();

        return ResponseEntity.ok(financialInstitution);
    }
}
