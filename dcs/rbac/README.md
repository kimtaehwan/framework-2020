# Spring Cloud Gateway

## Reference
### Spring Cloud Gateway
* https://twofootdog.tistory.com/64
* https://cheese10yun.github.io/spring-cloud-gateway/
* https://www.popit.kr/spring-cloud-gateway/
* https://springboot.cloud/26

#### JWT Parsing
* https://velog.io/@tlatldms/API-Gateway-Refresh-JWT-%EC%9D%B8%EC%A6%9D%EC%84%9C%EB%B2%84-%EA%B5%AC%EC%B6%95%ED%95%98%EA%B8%B0-Spring-boot-Spring-Cloud-Gateway-Redis-mysql-JPA-3%ED%8E%B8

### Auto Configuration
* https://engkimbs.tistory.com/754?category=767865

### Properties
* https://velog.io/@lsb156/Spring-Boot-Properties-Usage

### Cors
* https://twofootdog.tistory.com/63
* https://dev-pengun.tistory.com/entry/Spring-Boot-CORS-%EC%84%A4%EC%A0%95%ED%95%98%EA%B8%B0

### Response Setting
* https://stackoverflow.com/questions/53853345/forbid-unauthenticated-requests-in-spring-cloud-gateway
