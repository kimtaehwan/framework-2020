package com.ondot.dcs.rbac.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Builder
public class Privilege {
    List<String> authorityList;
}
