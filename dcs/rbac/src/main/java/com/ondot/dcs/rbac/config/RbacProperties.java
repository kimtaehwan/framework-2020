package com.ondot.dcs.rbac.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Getter
@Setter
@ConfigurationProperties(prefix = "spring.mvc.cas-rbac-filter.user-group.allow")
public class RbacProperties {

    private List<String> applicationList;

    private List<String> financialInstitutions;

    private List<String> journey;

    private List<String> journeyPoint;

    private List<String> process;

    private List<String> product;

    private List<String> consent;

    private List<String> eligibility;

    private List<String> esignature;

    private List<String> applicationForm;

    private List<String> policyContents;

    private List<String> closingPage;

    private List<String> creditUnderwriting;

    private List<String> global;

    private List<String> exportData;

    private List<String> images;

    private List<String> defaultList;
}
