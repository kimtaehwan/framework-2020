package com.ondot.dcs.rbac;

import com.ondot.dcs.rbac.config.RbacProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@EnableConfigurationProperties({RbacProperties.class})
public class RbacApplication {

    
    public static void main(String[] args) {
        SpringApplication.run(RbacApplication.class, args);
    }

}
