package com.ondot.dcs.rbac.service;

import com.ondot.dcs.rbac.config.RbacProperties;
import com.ondot.dcs.rbac.vo.Menus;
import com.ondot.dcs.rbac.vo.RbacRequest;
import io.jsonwebtoken.Claims;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RbacServiceTest {

    @InjectMocks
    RbacService rbacService;

    @MockBean
    RbacProperties rbacProperties;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    String token = "Bearer eyJraWQiOiJcL3NtWDRTYmZKU09IcVRITmdERWM0cWtCbFwvTkNDVG16QWFNbGU4S0dLVlU9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI4ODI5NjdkMC04MjliLTQ2M2ItYmM3Ny0zNTVkNjMyNzBiMmYiLCJhdWQiOiI3ajhnZWdxYTNtNGxsNXA5Y3RhcTBhc2VtbSIsImNvZ25pdG86Z3JvdXBzIjpbImRjcy1hZG1pbiJdLCJldmVudF9pZCI6IjFmNTI5MWIyLTg4ZTctNGQ0OC1hOTg2LTNmMjYyOTBiM2FhYyIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjA4MTk2MzM5LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9LQWo2VFdpWWIiLCJjdXN0b206ZGNzOnByaXZpbGVnZXMiOiJbXCJhZy4yLmVkaXRcIixcImFnLjIudmlld1wiLFwiYWcuOS5lZGl0XCIsXCJhZy45LnZpZXdcIixcImFnLjEwLmVkaXRcIixcImFnLjEwLnZpZXdcIixcImFnLjExLmVkaXRcIixcImFnLjExLnZpZXdcIixcImFnLjEyLmVkaXRcIixcImFnLjEyLnZpZXdcIixcImFnLjI5LmVkaXRcIixcImFnLjI5LnZpZXdcIixcImFnLjEzLmVkaXRcIixcImFnLjEzLnZpZXdcIixcImFnLjE0LmVkaXRcIixcImFnLjE0LnZpZXdcIixcImFnLjE1LmVkaXRcIixcImFnLjE1LnZpZXdcIixcImFnLjE2LmVkaXRcIixcImFnLjE2LnZpZXdcIixcImFnLjE3LmVkaXRcIixcImFnLjE3LnZpZXdcIixcImFnLjI2LmVkaXRcIixcImFnLjI2LnZpZXdcIixcImFnLjI4LmVkaXRcIixcImFnLjI4LnZpZXdcIixcImFnLjIwLmVkaXRcIixcImFnLjIwLnZpZXdcIixcImFnLjIxLmVkaXRcIixcImFnLjIxLnZpZXdcIixcImFnLjI3LmVkaXRcIixcImFnLjI3LnZpZXdcIixcImFnLjguZWRpdFwiLFwiYWcuOC52aWV3XCIsXCJhZy4yMy5lZGl0XCIsXCJhZy4yMy52aWV3XCIsXCJhZy4yNS5lZGl0XCIsXCJhZy4yNS52aWV3XCJdIiwiY29nbml0bzp1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjA4MTk5OTM5LCJjdXN0b206b3N0eXBlIjoiYWRtaW4iLCJpYXQiOjE2MDgxOTYzMzl9.Oi8hxYpbpYn4S54aX3OAGCcxHSDxEmtKqjee2f0YTsf5UZ036q-laC_FaqvdT4uMoQEsbmRN7yX1tnbMzMh9GTU-xPam19NNo4i1uiBOwk_H1QPA0Qgkf1OSveFPhZ41h6_J39c7eISJHeZtZH07l4B2a8Px06LqCa6GDBGN3IbyxuNIWZQr5IsrGztsgyZrFw008MP2BY7Vixiklnmea3PK5_PWc7-K0_AoKy7Q7-xVACZJca6yKoCQQ6BE7Ug6zZ-25ev_ReDW9du9cJ-7niKdGCuxfEWgDBxeq2IXBAqcvI5hn4qtO0QrLHMrAftmGt2NH3vc1L00-J5FxgrcXA";

    @Test
    public void checkAuthority() {
        // given
        RbacRequest rbacRequest = RbacRequest.builder()
                .token(token)
                .methodName("GET")
                .apiPath("/api/menuList")
                .build();
        boolean isAuthority = true;

        // when
        BDDMockito.given(rbacProperties.getDefaultList()).willReturn(Arrays.asList("/api/menuList"));
        boolean actual = rbacService.checkAuthority(rbacRequest);

        // then
        assertEquals(isAuthority, actual);
    }

    @Test
    public void decodeTokenClaimsTest() {
        // when
        Claims claims = rbacService.decodeTokenClaims(token);

        assertNotNull(claims);
    }

    @Test
    public void getContainsUrlTest() {
        // given
        List<String> paths = Arrays.asList("/api/fiOnboarding");
        boolean actual = rbacService.getContainsUrl(paths, "/api/fiOnboarding");

        assertEquals(true, actual);
    }

    @Test
    public void obtainAuthorizationTokenTest() {
        // given

        // when
        String actual = rbacService.obtainAuthorizationToken(token);

        // then
        assertNotNull(actual);
    }

    @Test
    public void convertToListTest() {
        // given
        String privileges = "[\"ag.2.edit\",\"ag.2.view\"]";

        // when
        List<String> actual = rbacService.convertToList(privileges);

        // then
        assertNotNull(actual);
    }

    @Test
    public void convertToListTest2() throws Exception {
        // given
        String privileges = "";

        // when
        List<String> actual = rbacService.convertToList(privileges);

        // then
        assertNotNull(actual);
    }

    @Test
    public void isValidAuthorityTest() {
        // given
        String methodName = "GET";
        List<String> authorityList = new ArrayList<>();
        authorityList.add("ag.20.view");
        String apiPath = "/api/fiOnboarding";

        // when
        BDDMockito
                .given(rbacProperties.getFinancialInstitutions())
                .willReturn(Arrays.asList("/api/fiOnboarding"));

        boolean actual = rbacService.isValidAuthority(methodName, apiPath, authorityList);

        assertEquals(true, actual);
    }

    @Test
    public void getMenusTest() {
        String apiPath = "/api/customer-support";
        BDDMockito
                .given(rbacProperties.getApplicationList())
                .willReturn(Arrays.asList("/api/customer-support"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.APPLICATION_LIST.getId(), actual.getId());
    }

    @Test
    public void getMenusTest1() {
        String apiPath = "/api/fiOnboarding";
        BDDMockito
                .given(rbacProperties.getFinancialInstitutions())
                .willReturn(Arrays.asList("/api/fiOnboarding"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.FI_ONBOARDING.getId(), actual.getId());
    }

    @Test
    public void getMenusTest2() {
        String apiPath = "/api/v1/journeys";
        BDDMockito
                .given(rbacProperties.getJourney())
                .willReturn(Arrays.asList("/api/v1/journeys"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.JOURNEY.getId(), actual.getId());
    }

    @Test
    public void getMenusTest3() {
        String apiPath = "/v2/journeys/point-maps";
        BDDMockito
                .given(rbacProperties.getJourneyPoint())
                .willReturn(Arrays.asList("/v2/journeys/point-maps"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.JOURNEY_POINT.getId(), actual.getId());
    }

    @Test
    public void getMenusTest4() {
        String apiPath = "/api/process";
        BDDMockito
                .given(rbacProperties.getProcess())
                .willReturn(Arrays.asList("/api/process"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.PROCESS.getId(), actual.getId());
    }

    @Test
    public void getMenusTest5() {
        String apiPath = "/api/v1/products";
        BDDMockito
                .given(rbacProperties.getProduct())
                .willReturn(Arrays.asList("/api/v1/products"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.PRODUCT.getId(), actual.getId());
    }

    @Test
    public void getMenusTest6() {
        String apiPath = "/api/terms";
        BDDMockito
                .given(rbacProperties.getConsent())
                .willReturn(Arrays.asList("/api/terms"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.CONSENT.getId(), actual.getId());
    }

    @Test
    public void getMenusTest7() {
        String apiPath = "/api/eligibility";
        BDDMockito
                .given(rbacProperties.getEligibility())
                .willReturn(Arrays.asList("/api/eligibility"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.ELIGIBILITY.getId(), actual.getId());
    }

    @Test
    public void getMenusTest8() {
        String apiPath = "/signature";
        BDDMockito
                .given(rbacProperties.getEsignature())
                .willReturn(Arrays.asList("/signature"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.E_SIGNATURE.getId(), actual.getId());
    }

    @Test
    public void getMenusTest9() {
        String apiPath = "/api/appForm";
        BDDMockito
                .given(rbacProperties.getApplicationForm())
                .willReturn(Arrays.asList("/api/appForm"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.APPLICATION_FORM.getId(), actual.getId());
    }

    @Test
    public void getMenusTest10() {
        String apiPath = "/api/policy-contents";
        BDDMockito
                .given(rbacProperties.getPolicyContents())
                .willReturn(Arrays.asList("/api/policy-contents"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.POLICY_CONTENT.getId(), actual.getId());
    }

    @Test
    public void getMenusTest11() {
        String apiPath = "/api/closingPages";
        BDDMockito
                .given(rbacProperties.getClosingPage())
                .willReturn(Arrays.asList("/api/closingPages"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.CLOSING_PAGE.getId(), actual.getId());
    }

    @Test
    public void getMenusTest12() {
        String apiPath = "/api/credit-underwriting";
        BDDMockito
                .given(rbacProperties.getCreditUnderwriting())
                .willReturn(Arrays.asList("/api/credit-underwriting"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.CREADIT_UNDERWRITING.getId(), actual.getId());
    }

    @Test
    public void getMenusTest13() {
        String apiPath = "/global";
        BDDMockito
                .given(rbacProperties.getGlobal())
                .willReturn(Arrays.asList("/global"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.GLOBAL.getId(), actual.getId());
    }

    @Test
    public void getMenusTest14() {
        String apiPath = "/exportJobList";
        BDDMockito
                .given(rbacProperties.getExportData())
                .willReturn(Arrays.asList("/exportJobList"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.EXPORT_DATA.getId(), actual.getId());
    }

    @Test
    public void getMenusTest15() {
        String apiPath = "/api/menuList";
        BDDMockito
                .given(rbacProperties.getDefaultList())
                .willReturn(Arrays.asList("/api/menuList"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.DEFAULT_LIST.getId(), actual.getId());
    }
}
