package com.example.api.paypal.dto.instruments;

import com.example.api.paypal.dto.AccountHolderName;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class CardAccount {
    private String referenceFinancialInstrumentId;
    private PanInfo pan;
    private String expiryDate;
    private BillingAddress billingAddress;
    private AccountHolderName accountHolderName;
}
