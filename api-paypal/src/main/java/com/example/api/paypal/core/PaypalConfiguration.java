package com.example.api.paypal.core;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "paypal.consumer")
public class PaypalConfiguration {
    private String key;
    private String secret;
    private String encodedValue;

}
