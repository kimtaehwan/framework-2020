package com.example.api.paypal.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class PaypalOnboardResponse {
    private String link;
    private String referralId;
}
