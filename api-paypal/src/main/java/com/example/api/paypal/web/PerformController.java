package com.example.api.paypal.web;

import com.example.api.paypal.dto.CoreCBESResponse;
import com.example.api.paypal.dto.onboard.PaypalRequest;
import com.example.api.paypal.service.PerformService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/fiserver/cbes")
public class PerformController {

    final PerformService performService;

    @PostMapping("/perform.do")
    public ResponseEntity createPaypalOnboarding(
            @RequestParam(value = "opId", required = false) String opId,
            @RequestParam(value = "apiVersion",required = false) String apiVersion,
            @RequestBody PaypalRequest.CreateReq createReq
            ) {

        CoreCBESResponse response = null;

        if ("GET_INAPP_PUSH_PAYPAL_ONBOARD".equalsIgnoreCase(opId)) {
            response = performService.createPaypalOnboarding(createReq);
        } else if("GET_INAPP_PUSH_PAYPAL_COMPLETION".equalsIgnoreCase(opId)){
            response = performService.createPaypalCompletion(createReq);
        }

        return ResponseEntity.ok(response);
    }
}
