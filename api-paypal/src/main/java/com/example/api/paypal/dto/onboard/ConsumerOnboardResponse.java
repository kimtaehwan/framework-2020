package com.example.api.paypal.dto.onboard;

import com.example.api.paypal.dto.PaypalEntity;
import com.example.api.paypal.dto.PaypalMetadata;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ConsumerOnboardResponse {
    private PaypalEntity entity;
    private PaypalMetadata metadata;
}
