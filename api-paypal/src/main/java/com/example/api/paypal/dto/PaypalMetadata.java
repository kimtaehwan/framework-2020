package com.example.api.paypal.dto;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class PaypalMetadata {
    private List<PaypalMessage> messages;
}
