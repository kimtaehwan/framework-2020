package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class AddressInfo {
    private AddressDetail address;
    private String addressType;
}
