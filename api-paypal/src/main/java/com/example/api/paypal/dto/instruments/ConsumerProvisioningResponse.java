package com.example.api.paypal.dto.instruments;

import com.example.api.paypal.dto.PaypalMetadata;
import com.example.api.paypal.dto.PaypalEntity;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ConsumerProvisioningResponse {
    private PaypalEntity entity;
    private PaypalMetadata metadata;
}
