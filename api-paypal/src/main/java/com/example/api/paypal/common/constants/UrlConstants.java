package com.example.api.paypal.common.constants;

import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class UrlConstants {

    public static URI getAccessTokenUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("token")
                .queryParam("grant_type","client_credentials")
                .build()
                .toUri();
    }

    public static URI getConsumerOnboardUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/consumer-onboard")
                .build()
                .toUri();
    }

    public static URI getInstrumentsUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/instruments")
                .build()
                .toUri();
    }

    public static URI getCompletionUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/provisioning-completion")
                .build()
                .toUri();
    }
}
