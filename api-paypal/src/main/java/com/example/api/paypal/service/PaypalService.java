package com.example.api.paypal.service;

import com.example.api.paypal.core.PaypalConfiguration;
import com.example.api.paypal.dto.CoreCBESResponse;
import com.example.api.paypal.dto.common.CommonResponse;
import com.example.api.paypal.dto.common.ContextualOperationInformation;
import com.example.api.paypal.dto.common.ResponseStatus;
import com.example.api.paypal.dto.onboard.PaypalRequest;
import com.example.api.paypal.vo.MessageCode;
import com.example.api.paypal.vo.PaypalOnboardData;
import com.example.api.paypal.vo.ResponseStatusCodeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class PaypalService {
    private static final String API_SPEC_CONTEXT = "While validating In-App Push Paypal data request.";

    final RestTemplate restTemplate;
    final PaypalConfiguration paypalConfiguration;

    public CoreCBESResponse createPaypalOnboard(PaypalOnboardData onboardData) {

        // check privilege with card/subscriber, and set pan
        boolean isValidCard = this.checkValidCard(onboardData);

        if (!isValidCard) {
            // return error
        }


        return null;
    }

    private boolean checkValidCard(PaypalOnboardData onboardData) {
        return true;
    }

    public CoreCBESResponse apiSpecificValidation(PaypalRequest.CreateReq request) {
        CoreCBESResponse coreCBESResponse = validateRequest(request);

        return coreCBESResponse;
    }

    private CoreCBESResponse validateRequest(PaypalRequest.CreateReq request) {
        CoreCBESResponse coreCBESResponse;

        coreCBESResponse = validateSpec(request.getCardReferenceId(), "card reference ID");

        return coreCBESResponse;
    }

    private CoreCBESResponse validateSpec(final Integer value, final String field) {
        if (value == null) {
            return generateErrorResponse(field);
        }

        return null;
    }

    private CoreCBESResponse generateErrorResponse(String field) {
        ContextualOperationInformation contextualOperationInformation = ContextualOperationInformation.builder()
            .cause("No" + field + " in the request.")
            .context(API_SPEC_CONTEXT)
            .build();

        ResponseStatus responseStatus = new ResponseStatus(ResponseStatusCodeEnum.FAILURE.code(), MessageCode.API_102.getMessageCode());
        return new CoreCBESResponse(CommonResponse.builder().responseStatus(responseStatus).build(), contextualOperationInformation);
    }
}
