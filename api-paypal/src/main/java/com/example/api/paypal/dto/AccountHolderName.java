package com.example.api.paypal.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class AccountHolderName {
    private String prefix;
    private String givenName;
    private String surname;
    private String middleName;
    private String suffix;
}
