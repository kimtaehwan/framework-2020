package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class AccountProperties {
    private String accountCountryCode;
}
