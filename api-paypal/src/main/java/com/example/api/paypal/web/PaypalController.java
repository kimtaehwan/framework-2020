package com.example.api.paypal.web;

import com.example.api.paypal.core.PaypalConfiguration;
import com.example.api.paypal.dto.CoreCBESResponse;
import com.example.api.paypal.dto.onboard.PaypalRequest;
import com.example.api.paypal.service.PaypalService;
import com.example.api.paypal.vo.PaypalOnboardData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PaypalController {

    final PaypalService paypalService;
    final PaypalConfiguration paypalConfiguration;

    @PostMapping(value = "/paypalOnboard")
    public ResponseEntity createPaypalOnboard(@RequestBody PaypalRequest.CreateReq request) {

        log.info("createOnboard 1: {}", request.toString());
        log.info("createOnboard 2: {}", request);

        log.info("### consumer key: {}", paypalConfiguration.getKey());
        log.info("### consumer secret: {}", paypalConfiguration.getSecret());
        log.info("### consumer encodedValue: {}", paypalConfiguration.getEncodedValue());

        /**
         * TODO: request validation - PaypalOnboardServiceExecutor
         */
        CoreCBESResponse response = paypalService.apiSpecificValidation(request);

        /**
         * TODO: transForm
          */
        // PaypalOnboardData paypalOnboardData = this.transFormToPaypalOnboardData(request);

        // CoreCBESResponse response = paypalService.createPaypalOnboard(paypalOnboardData);


        log.info("### response: {}", response.toString());

        return ResponseEntity.ok(response);
    }

    private PaypalOnboardData transFormToPaypalOnboardData(PaypalRequest.CreateReq request) {
        return PaypalOnboardData.builder()
                .clientAppId("")
                .clientDeviceId("")
                .cardReferenceId(request.getCardReferenceId())
                .subscriberId(request.getSubscriberId())
                .subscriberReferenceId(request.getSubscriberReferenceId())
                .appToken(request.getAppToken())
                .deviceUniqueId(request.getDeviceUniqueId())
                .language(request.getLanguage())
                .build();
    }
}
