package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class PhoneDetails {

    private Phone phone;
    private PhoneType phoneType;

    @Value
    @Builder(toBuilder = true)
    public static class Phone {
        private String countryCode;
        private String nationalNumber;
        private String extensionNumber;
    }

    public enum PhoneType {
        FAX, HOME, MOBILE, OTHER, PAGER
    }
}
