package com.example.api.paypal.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;
import java.util.Map;

@Getter
@Builder
@ToString
public class CommonResponseStatus {
    private String  message;
    private String  messageCode;
    private int     responseCode;
    private String  operationTraceId;
    private Map<String, Object> additionalInfo;
    private Date    responseTimeStamp;
    private boolean doNotOverrideMsgCode;

}
