package com.example.api.paypal.dto;

import com.example.api.paypal.dto.common.CommonResponse;
import com.example.api.paypal.dto.common.ContextualOperationInformation;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class CoreCBESResponse {
    private CommonResponse response;
    private ContextualOperationInformation contextualOperationInformation;

    public CoreCBESResponse(CommonResponse response, ContextualOperationInformation contextualOperationInformation) {
        this.response = response;
        this.contextualOperationInformation = contextualOperationInformation;
    }
}
