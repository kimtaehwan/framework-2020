package com.example.api.paypal.dto.onboard;

import com.example.api.paypal.dto.AccountHolderName;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString
public class PersonDetails {
    private List<AccountHolderName> names;
    private List<PhoneContact> phoneContacts;
    private List<AddressInfo> addresses;
    private List<EmailAddressInfo> emailAddresses;
    private String locale;
}
