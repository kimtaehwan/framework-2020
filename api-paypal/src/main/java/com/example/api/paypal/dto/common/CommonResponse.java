package com.example.api.paypal.dto.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString
public class CommonResponse {
    private String  apiVersion;
    private String  opId;
    private List<?> responseDataList;
    private ResponseStatus responseStatus;


}
