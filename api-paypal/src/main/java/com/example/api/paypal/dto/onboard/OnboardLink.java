package com.example.api.paypal.dto.onboard;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OnboardLink {
    private String href;
    private String rel;
    private String method;
    private String cofEntity;
    private String linkType;
    private String cofEntityReferralId;
    private String scope;
    private String redirectUri;
}
