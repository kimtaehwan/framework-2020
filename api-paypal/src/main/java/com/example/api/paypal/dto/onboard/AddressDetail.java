package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class AddressDetail {

    private String line1;
    private String line2;
    private String city;
    private String state;
    private String county;
    private String countryCode;
    private String postalCode;
    private String streetNumber;
    private String streetName;
}
