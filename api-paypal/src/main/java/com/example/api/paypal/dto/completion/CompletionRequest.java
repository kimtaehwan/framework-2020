package com.example.api.paypal.dto.completion;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

public class CompletionRequest {

    @Getter
    @Builder
    @ToString
    public static class Create {
        private String entityId;
        private String referralId;
        private String financialInstitutionId;
        private String code;
    }
}
