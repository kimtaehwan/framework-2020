package com.example.api.paypal.vo;

public enum MessageCode {

    API_101("API_101", "The API Version is not supported."),
    //Currently adding API_102 message code in ResponseMessageCode.java file of connector package, as Connector is independent of fi-server package
    API_102("API_102", "Invalid request.")
    ;

    private final String messageCode;
    private final String messageDescription;

    private MessageCode(String messageCode, String messageDescription) {
        this.messageCode = messageCode;
        this.messageDescription = messageDescription;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getMessageDescription() {
        return messageDescription;
    }
}
