package com.example.api.paypal.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class PaypalOnboardData {
    private String  clientAppId;
    private String  clientDeviceId;
    private int     cardReferenceId;

    @JsonIgnore
    private int     subscriberId;
    private String  subscriberReferenceId;

    private String  appToken;
    private String  deviceUniqueId;
    private String  language;



}
