package com.example.api.paypal.dto.onboard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

public class PaypalRequest {

    private PaypalRequest() {}

    @Getter
    @Builder
    @ToString
    public static class CreateReq {
        private String  appToken;
        private String  deviceUniqueId;
        private String  language;
        private String  fiToken;
        private Long    lastReadTimeStamp;
        private String  requesterHostName;

        @JsonIgnore
        private int subscriberId;
        private String subscriberReferenceId;
        private Integer cardReferenceId;
        private String email;
        private String code;
        private String referralId;
    }

}
