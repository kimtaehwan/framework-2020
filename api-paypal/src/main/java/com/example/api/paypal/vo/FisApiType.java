package com.example.api.paypal.vo;

public enum FisApiType {
    ACCESS_TOKEN, ONBOARD, INSTRUMENTS, COMPLETION
}
