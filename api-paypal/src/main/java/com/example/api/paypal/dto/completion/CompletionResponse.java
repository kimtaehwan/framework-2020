package com.example.api.paypal.dto.completion;

import com.example.api.paypal.dto.PaypalEntity;
import com.example.api.paypal.dto.PaypalMetadata;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CompletionResponse {
    private PaypalMetadata metadata;
}
