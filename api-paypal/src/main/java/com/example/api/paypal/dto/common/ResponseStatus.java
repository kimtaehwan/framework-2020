package com.example.api.paypal.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ResponseStatus {
    private String      message;
    private String      messageCode;
    private int         responseCode;
    private String      operationTraceId;
    private Map<String, Object> additionalInfo;
    private Date        responseTimeStamp;
    private boolean    doNotOverrideMsgCode;

    public ResponseStatus(int responseCode, String messageCode) {
        this.responseCode = responseCode;
        this.messageCode = messageCode;
    }
}
