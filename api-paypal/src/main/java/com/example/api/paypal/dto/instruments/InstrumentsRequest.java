package com.example.api.paypal.dto.instruments;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

public class InstrumentsRequest {

    @Getter
    @Builder
    @ToString
    public static class Create {

        private List<CardAccount> cardAccounts;

        private String financialInstituteId;
        private String commerceId;
        private String referralId;
    }
}
