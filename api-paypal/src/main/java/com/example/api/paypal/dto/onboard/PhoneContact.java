package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class PhoneContact {
    private PhoneInfo phone;
    private String phoneType;
}
