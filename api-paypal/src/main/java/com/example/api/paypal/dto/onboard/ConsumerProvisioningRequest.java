package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

public class ConsumerProvisioningRequest {

    @Getter
    @Builder
    @ToString
    public static class Create {
        private String financialInstituteId;
        private PersonDetails personDetails;
        private AccountProperties accountProperties;
    }


}
