package com.example.api.paypal.dto;

import com.example.api.paypal.dto.onboard.OnboardLink;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaypalEntity {
    private String commerceId;
    private List<OnboardLink> links;
}
