package com.example.api.paypal.common.utils;

import com.example.api.paypal.common.constants.ValueConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@Slf4j
public class PaypalUtil {

    public static HttpHeaders getPaypalHttpHeaders(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("uuid", PaypalUtil.getUuid());
        headers.add("entityId", ValueConstants.ENTITY_ID);
        headers.add("accept", MediaType.APPLICATION_JSON_VALUE);

        String authorization = getAuthorization(accessToken);
        headers.add("Authorization", authorization);

        return headers;
    }

    private static String getAuthorization(String accessToken) {
        return "Bearer " +accessToken;
    }

    private static String getUuid() {
        return "419593ff-0080-a8c6-30d7-d1f8351b61c4";
    }

    public static String getJsonData(ResponseEntity<?> response) {
        String result = "";

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            result = objectMapper.writeValueAsString(response);
        } catch (Exception e) {
            log.error("getJsonData: {}", e);
        }

        return result;
    }
}
