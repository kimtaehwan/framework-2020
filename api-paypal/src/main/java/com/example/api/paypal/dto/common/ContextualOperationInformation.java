package com.example.api.paypal.dto.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@Builder
@ToString
public class ContextualOperationInformation {
    private String cause;
    private String context;
    private Map<String, Object> logInformation = new HashMap<>();

}
