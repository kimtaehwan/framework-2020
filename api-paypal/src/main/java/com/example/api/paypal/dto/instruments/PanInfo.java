package com.example.api.paypal.dto.instruments;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class PanInfo {
    private String plainText;
    private String cipherText;
    private String alias;
}
