package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class PhoneInfo {
    private String countryCode;
    private String nationalNumber;
    private String extensionNumber;
}
