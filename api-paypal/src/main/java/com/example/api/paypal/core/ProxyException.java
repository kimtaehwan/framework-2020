package com.example.api.paypal.core;

import lombok.Getter;
import org.springframework.http.client.ClientHttpResponse;

@Getter
public class ProxyException extends RuntimeException {

    private ClientHttpResponse clientHttpResponse;

    public ProxyException(ClientHttpResponse clientHttpResponse) {
        this.clientHttpResponse = clientHttpResponse;
    }
}
