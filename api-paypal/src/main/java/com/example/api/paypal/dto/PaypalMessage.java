package com.example.api.paypal.dto;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PaypalMessage {
    private String code;
    private String text;
}
