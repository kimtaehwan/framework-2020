package com.example.api.paypal.service;

import com.example.api.paypal.common.constants.CommonConstants;
import com.example.api.paypal.common.constants.UrlConstants;
import com.example.api.paypal.common.constants.ValueConstants;
import com.example.api.paypal.common.utils.PaypalUtil;
import com.example.api.paypal.core.PaypalConfiguration;
import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.AccountHolderName;
import com.example.api.paypal.dto.CoreCBESResponse;
import com.example.api.paypal.dto.PaypalOnboardResponse;
import com.example.api.paypal.dto.common.CommonResponse;
import com.example.api.paypal.dto.common.ContextualOperationInformation;
import com.example.api.paypal.dto.completion.CompletionRequest;
import com.example.api.paypal.dto.completion.CompletionResponse;
import com.example.api.paypal.dto.instruments.*;
import com.example.api.paypal.dto.onboard.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PerformService {

    final RestTemplate restTemplate;
    final PaypalConfiguration paypalConfiguration;

    public CoreCBESResponse createPaypalOnboarding(PaypalRequest.CreateReq createReq) {
        CoreCBESResponse result;

        String email = createReq.getEmail();

        // access Token and create header
        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalUtil.getPaypalHttpHeaders(accessToken);

        // call consumer-onboard
        ConsumerProvisioningRequest.Create createOnboard = this.getConsumerOnboardRequest(email);
        HttpEntity<ConsumerProvisioningRequest.Create> onboardHttpEntity = new HttpEntity<>(createOnboard, headers);

        URI onboardUri = UrlConstants.getConsumerOnboardUri();
        ResponseEntity<ConsumerOnboardResponse> onboardResponseEntity = restTemplate.exchange(onboardUri, HttpMethod.POST, onboardHttpEntity, ConsumerOnboardResponse.class);

        String commerceId = onboardResponseEntity.getBody().getEntity().getCommerceId();
        String referralId = onboardResponseEntity.getBody().getEntity().getLinks().get(0).getCofEntityReferralId();

        String href = onboardResponseEntity.getBody().getEntity().getLinks().get(0).getHref();

        // call instruments
        InstrumentsRequest.Create instrumentsRequest = getInstrumentsRequest(commerceId, referralId);
        HttpEntity httpEntity = new HttpEntity(instrumentsRequest, headers);

        log.info("### headers: {}", headers.toString());

        try {
            URI instrumentsUri = UrlConstants.getInstrumentsUri();
            ResponseEntity<ConsumerProvisioningResponse> response = restTemplate.exchange(instrumentsUri, HttpMethod.POST, httpEntity, ConsumerProvisioningResponse.class);
            log.info("### instruments: {}", response.getBody());
            String jsonData = PaypalUtil.getJsonData(response);

            log.info("### instruemntes - json: {}", jsonData);
        } catch (Exception e) {
            log.error("instruments: {}", e);
        }

        PaypalOnboardResponse paypalOnboardResponse = PaypalOnboardResponse.builder()
                .link(href)
                .referralId(referralId)
                .build();

        List<PaypalOnboardResponse> paypalOnboardResponseList = new ArrayList<>();
        paypalOnboardResponseList.add(paypalOnboardResponse);

        ContextualOperationInformation contextualOperationInformation = ContextualOperationInformation.builder()
                .cause("")
                .context("")
                .build();

        CoreCBESResponse coreCBESResponse = CoreCBESResponse.builder()
                .response(CommonResponse.builder()
                        .apiVersion("v2.0")
                        .opId("GET_INAPP_PUSH_PAYPAL_ONBOARD")
                        .responseDataList(paypalOnboardResponseList).build())
                .contextualOperationInformation(contextualOperationInformation)
                .build();

        return  coreCBESResponse;
    }

    public CoreCBESResponse createPaypalCompletion(PaypalRequest.CreateReq createReq) {

        // access Token and create header
        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalUtil.getPaypalHttpHeaders(accessToken);

        // set completion
        CompletionRequest.Create create = CompletionRequest.Create.builder()
                .entityId(ValueConstants.ENTITY_ID)
                .financialInstitutionId(ValueConstants.FINANCIAL_INSTITUTION_ID)
                .code(createReq.getCode())
                .referralId(createReq.getReferralId())
                .build();

        // call completion
        HttpEntity<CompletionRequest.Create> httpEntity = new HttpEntity<>(create, headers);

        URI uri = UrlConstants.getCompletionUri();
        ResponseEntity<CompletionResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, CompletionResponse.class);

        log.info("### completion: {}", response.getBody());

        ContextualOperationInformation contextualOperationInformation = ContextualOperationInformation.builder()
                .cause("")
                .context("")
                .build();

        CoreCBESResponse coreCBESResponse = CoreCBESResponse.builder()
                .response(CommonResponse.builder()
                        .apiVersion("v2.0")
                        .opId("GET_INAPP_PUSH_PAYPAL_COMPLETION")
                        .build())
                .contextualOperationInformation(contextualOperationInformation)
                .build();

        return coreCBESResponse;
    }

    private InstrumentsRequest.Create getInstrumentsRequest(String commerceId, String referralId) {
        List<CardAccount> cardAccounts = getCardAccounts();

        return InstrumentsRequest.Create.builder()
                .cardAccounts(cardAccounts)
                .financialInstituteId(ValueConstants.FINANCIAL_INSTITUTION_ID)
                .commerceId(commerceId)
                .referralId(referralId)
                .build();
    }

    private ConsumerProvisioningRequest.Create getConsumerOnboardRequest(String email) {
        PersonDetails personDetails = getPersonDetail(email);

        ConsumerProvisioningRequest.Create create = ConsumerProvisioningRequest.Create.builder()
                .financialInstituteId(ValueConstants.FINANCIAL_INSTITUTION_ID)
                .personDetails(personDetails)
                .accountProperties(AccountProperties.builder()
                        .accountCountryCode("AT")
                        .build())
                .build();

        return create;
    }

    private static PersonDetails getPersonDetail(String email) {
        AccountHolderName accountHolderName = AccountHolderName.builder()
                .prefix("Mr.")
                .givenName("Niklas")
                .surname("Frangos")
                .middleName("Kobe")
                .suffix("Jr.")
                .build();

        PhoneContact phoneContact = PhoneContact.builder()
                .phone(PhoneInfo.builder()
                        .countryCode("82")
                        .nationalNumber("82")
                        .extensionNumber("1027171703")
                        .build())
                .phoneType("MOBILE")
                .build();

        AddressInfo addressInfo = AddressInfo.builder()
                .address(AddressDetail.builder()
                        .line1("20/Wolfengasse 3")
                        .line2("Fleischmarkt")
                        .city("Vienna")
                        .state("string")
                        .county("string")
                        .countryCode("AT")
                        .postalCode("A-1010")
                        .streetNumber("string")
                        .streetName("string")
                        .build())
                .addressType("HOME")
                .build();

        EmailAddressInfo emailAddressInfo = EmailAddressInfo.builder()
                .emailAddress(email)
                .primary(true)
                .confirmed(true)
                .build();

        List<AccountHolderName> names = new ArrayList<>();
        names.add(accountHolderName);

        List<PhoneContact> phoneContacts = new ArrayList<>();
        phoneContacts.add(phoneContact);

        List<AddressInfo> addresses = new ArrayList<>();
        addresses.add(addressInfo);

        List<EmailAddressInfo> emailAddresses = new ArrayList<>();
        emailAddresses.add(emailAddressInfo);

        PersonDetails personDetails = PersonDetails.builder()
                .names(names)
                .phoneContacts(phoneContacts)
                .addresses(addresses)
                .emailAddresses(emailAddresses)
                .locale("en_US")
                .build();

        return personDetails;
    }

    private static List<CardAccount> getCardAccounts() {
        AccountHolderName accountHolderName = AccountHolderName.builder()
                .prefix("Mr.")
                .givenName("Niklas")
                .surname("Frangos")
                .middleName("Kobe")
                .suffix("Jr.")
                .build();

        List<CardAccount> result = new ArrayList<>();
        CardAccount cardAccount = CardAccount.builder()
                .referenceFinancialInstrumentId("057b0af3-0904-4b99-8cef-2a79c452e0f1")
                .pan(PanInfo.builder()
                        .plainText("4006443717595043")
                        .cipherText("")
                        .alias("")
                        .build())
                .expiryDate("2019-12")
                .billingAddress(BillingAddress.builder()
                        .addressLine1("20/Wolfengasse 3")
                        .addressLine2("Fleischmarkt")
                        .addressLine3("string")
                        .adminArea1("string")
                        .adminArea2("Vienna")
                        .postalCode("A-1010")
                        .countryCode("AT")
                        .build())
                .accountHolderName(accountHolderName)
                .build();

        result.add(cardAccount);

        return result;
    }

    private String getAccessToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CommonConstants.AUTHORIZATION, "Basic " +paypalConfiguration.getEncodedValue());

        HttpEntity httpEntity = new HttpEntity(headers);

        URI uri = UrlConstants.getAccessTokenUri();
        ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, AccessTokenResponse.class);
        return response.getBody().getAccessToken();
    }
}
