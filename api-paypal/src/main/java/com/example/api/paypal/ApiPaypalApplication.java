package com.example.api.paypal;

import com.example.api.paypal.core.LoggingClientHttpRequestInterceptor;
import com.example.api.paypal.core.PaypalConfiguration;
import com.example.api.paypal.core.ProxyResponseErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@SpringBootApplication
@EnableConfigurationProperties({PaypalConfiguration.class})
public class ApiPaypalApplication {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofSeconds(3l))
                .setReadTimeout(Duration.ofSeconds(3l))
                .additionalInterceptors(new LoggingClientHttpRequestInterceptor())
                .errorHandler(new ProxyResponseErrorHandler())
                .build();
    }

    public static void main(String[] args) {
        SpringApplication.run(ApiPaypalApplication.class, args);
    }

}
