package com.example.api.paypal.dto.instruments;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class BillingAddress {
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String adminArea1;
    private String adminArea2;
    private String postalCode;
    private String countryCode;
}
