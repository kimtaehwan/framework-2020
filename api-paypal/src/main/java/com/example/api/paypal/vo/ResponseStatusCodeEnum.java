package com.example.api.paypal.vo;

import java.util.HashMap;
import java.util.Map;

public enum ResponseStatusCodeEnum {
    SUCCESS(0, "success"),
    FAILURE_INVALIDATE_SESSION(1, "Invalid Session"),
    FAILURE(2, "Error"),
    SUCCESS_SHOW_POPUP(3, ""),
    SUCCESS_BUT_DEPRECATED_API(50, "Success but API is deprecated"),
    FAILURE_INVALIDATE_SESSION_BUT_DEPRECATED_API(51, "Invalid Session but API is deprecated"),
    FAILURE_BUT_DEPRECATED_API(52, "Failure and API is deprecated"),
    SUCCESS_SHOW_POPUP_BUT_DEPRECATED_API(53, "Failure but API is deprecated"),
    SYSTEM_ERROR(100, "");

    private static Map<Integer, ResponseStatusCodeEnum> responseStatusCodeEnumMap = new HashMap<>();
    static {
        responseStatusCodeEnumMap.put(0, SUCCESS);
        responseStatusCodeEnumMap.put(1, FAILURE_INVALIDATE_SESSION);
        responseStatusCodeEnumMap.put(2, FAILURE);
        responseStatusCodeEnumMap.put(3, SUCCESS_SHOW_POPUP);
        responseStatusCodeEnumMap.put(50, SUCCESS_BUT_DEPRECATED_API);
        responseStatusCodeEnumMap.put(51, FAILURE_INVALIDATE_SESSION_BUT_DEPRECATED_API);
        responseStatusCodeEnumMap.put(52, FAILURE_BUT_DEPRECATED_API);
        responseStatusCodeEnumMap.put(53, SUCCESS_SHOW_POPUP_BUT_DEPRECATED_API);
        responseStatusCodeEnumMap.put(100, SYSTEM_ERROR);
    }

    public static ResponseStatusCodeEnum getResponseStatusCodeEnum(int key) {
        return responseStatusCodeEnumMap.get(key);
    }

    public static Map<Integer, ResponseStatusCodeEnum> getResponseStatusCodeEnumMap() {
        return responseStatusCodeEnumMap;
    }

    private Integer code;
    private String  description;

    private ResponseStatusCodeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer code() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSuccess() {
        return this.equals(SUCCESS) || this.equals(SUCCESS_BUT_DEPRECATED_API) || this.equals(SUCCESS_SHOW_POPUP);
    }

    public static ResponseStatusCodeEnum responseStatusCodeEnum(int code) {
        return responseStatusCodeEnumMap.get(code);
    }

    public static ResponseStatusCodeEnum deprecatedOf(ResponseStatusCodeEnum responseStatusCodeEnum) {
        if (responseStatusCodeEnum == SUCCESS) {
            return SUCCESS_BUT_DEPRECATED_API;
        }

        if (responseStatusCodeEnum == FAILURE_INVALIDATE_SESSION) {
            return FAILURE_INVALIDATE_SESSION_BUT_DEPRECATED_API;
        }

        if (responseStatusCodeEnum == FAILURE) {
            return FAILURE_BUT_DEPRECATED_API;
        }

        if (responseStatusCodeEnum == SUCCESS_SHOW_POPUP) {
            return SUCCESS_SHOW_POPUP_BUT_DEPRECATED_API;
        }

        return null;
    }
}
