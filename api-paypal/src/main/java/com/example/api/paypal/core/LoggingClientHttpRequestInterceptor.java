package com.example.api.paypal.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
public class LoggingClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes,
                                        ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {

        log.info("Request: URI={}, getAccept={}, getContentType={}",
                httpRequest.getURI(), httpRequest.getHeaders().getAccept(), httpRequest.getHeaders().getContentType());

        ClientHttpResponse response = new BufferingClientHttpResponseWrapper(clientHttpRequestExecution.execute(httpRequest, bytes));

        log.info("Response: getStatusCode={}", response.getStatusCode());
        log.info("Response: body={}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        return response;
    }
}
