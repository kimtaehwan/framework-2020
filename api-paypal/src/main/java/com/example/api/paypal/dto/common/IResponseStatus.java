package com.example.api.paypal.dto.common;

import java.util.Map;

public interface IResponseStatus {
    public int getResponseCode();
    public String getMessageCode();
    public void setResponseCode(int responseCode);
    public void setMessageCode(String messageCode);
    public String getOperationTraceId();
    public void setOperationTraceId(String operationTraceId);
    public Map<String, Object> getAdditionalInfo();
    public void setAdditionalInfo(Map<String, Object> additionalInfo);
    public boolean isSuccess();
}
