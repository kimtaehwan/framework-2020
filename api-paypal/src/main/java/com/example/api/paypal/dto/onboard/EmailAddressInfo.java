package com.example.api.paypal.dto.onboard;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class EmailAddressInfo {
    private String emailAddress;
    private boolean primary;
    private boolean confirmed;
}
