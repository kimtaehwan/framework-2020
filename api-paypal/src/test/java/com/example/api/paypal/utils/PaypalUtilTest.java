package com.example.api.paypal.utils;

import com.example.api.paypal.dto.AccountHolderName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.Calendar;

@Slf4j
public class PaypalUtilTest {
    final String NUMBER_REG_EXP = "[^0-9]";

    @Test
    public void expiryDateTest_01() {
        String temp1 = "12/22";
        String temp2 = temp1.replaceAll("/", "/20");

        log.debug("1. temp: {}", temp2);

        String[] array = temp2.split("/");
        String results = "";

        if ( array != null && array.length == 2) {
            log.debug("array[0]: {}", array[0]);
            log.debug("array[1]: {}", array[1]);

            results = new StringBuilder()
                    .append(array[1])
                    .append("-")
                    .append(array[0]).toString();
        }


        log.debug("results: {}", results.toString());
    }

    @Test
    public void setDataTest_02() {
        TestDomain testDomain = new TestDomain();
        testDomain.setGivenName("given");
        testDomain.setMiddleName("middle");
        testDomain.setPrefix("prefix");

        log.debug(testDomain.toString());

        testDomain = changeAccount(testDomain);

        log.debug(testDomain.toString());

    }

    private TestDomain changeAccount(TestDomain testDomain) {
        testDomain.setGivenName("givven");
        return testDomain;
    }
    
    @Test
    public void enum_find_test01() {
        String test = "133";
        
        FISProvisioningResponseCode findEnum = FISProvisioningResponseCode.of(test);

        log.debug("code: {}", findEnum.getCode());
        log.debug("message: {}", findEnum.getDescription());
    }

    @Test
    public void enumFindTest02() {
        PaypalStatusCode paypalStatusCode = PaypalStatusCode.COMPLETION_FAIL;

        log.debug(paypalStatusCode.getCode());
        log.debug(String.valueOf(PaypalStatusCode.INTRUMENTS_SUCCESS));
    }

    @Test
    public void getYear4Digits_test01() {
        String year = "21";
        log.debug("result: {}", getYear4Digits(year));

        String cardExpiryDate = "07/20";
        YearMonth yearMonth = convertExpiryDate(cardExpiryDate);
        log.debug("1. yearMonth: {}", yearMonth);

        cardExpiryDate = "/12/23/45";
        yearMonth = convertExpiryDate(cardExpiryDate);

        log.debug("2. yearMonth: {}", yearMonth);
    }

    private YearMonth convertExpiryDate(String cardExpiryDate) {
        if (StringUtils.isEmpty(cardExpiryDate)) {
            return null;
        }

        YearMonth result = null;
        String[] splitDates = cardExpiryDate.split("/");

        if (splitDates.length == 2) {
            int month = Integer.parseInt(splitDates[0]);
            int year = Integer.parseInt(getYear4Digits(splitDates[1]));

            result = YearMonth.of(year, month);
        }

        return result;
    }

    private String getYear4Digits(String year)
    {
        String result;
        Calendar calendar;
        int thisYear;
        int century;
        if (year.length() == 2)
        {
            calendar = Calendar.getInstance();
            thisYear = Integer.parseInt((new SimpleDateFormat("yyyy")).format(calendar.getTime()));
            century = thisYear / 100;
            result = century + year; // configure date with this century
            if (Integer.parseInt(result) > thisYear + 20) // maximum valid date is 20 years
            {
                result = "00" + year;
            }
        }
        else
        {
            result = year;
        }
        return result;
    }

    @Test
    public void phoneType_test01() {
        log.debug("phoneType: {} ", PhoneType.MOBILE.name());
        log.debug("phoneType: {} ", PhoneType.valueOf(PhoneType.MOBILE.name()));

    }

    public enum PhoneType {
        FAX, HOME, MOBILE, OTHER, PAGER
    }
}

@Getter
@Setter
@ToString
class TestDomain {
    private String prefix;
    private String givenName;
    private String surname;
    private String middleName;
    private String suffix;
}
