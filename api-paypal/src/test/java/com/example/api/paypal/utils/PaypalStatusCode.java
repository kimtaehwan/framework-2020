package com.example.api.paypal.utils;

public enum PaypalStatusCode {
    INTRUMENTS_SUCCESS("1"),
    COMPLETION_FAIL("2"),
    COMPLETION_SUCCESS("3");

    private String code;

    PaypalStatusCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
