package com.example.api.paypal.web;

import com.example.api.paypal.dto.onboard.PaypalRequest;
import com.example.api.paypal.service.PerformService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PerformController.class)
class PerformControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PerformService performService;

    @Test
    public void createPaypalOnboarding() throws Exception {
        // given
        PaypalRequest.CreateReq buildRequest = PaypalRequest.CreateReq.builder()
                .appToken("appToken")
                .deviceUniqueId("test")
                .language("en")
                .fiToken("fi-token")
                .lastReadTimeStamp(123L)
                .requesterHostName("host")
                .subscriberId(123)
                .subscriberReferenceId("enen")
                .cardReferenceId(123)
                .email("test@gmail.com")
                .build();

        // when
        MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("opId", "GET_INAPP_PUSH_PAYPAL_ONBOARD");
        paramMap.add("apiVersion", "v2.0");

        RequestBuilder request = MockMvcRequestBuilders.post("/fiserver/cbes/perform.do")
                .params(paramMap)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildRequest));

        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andDo(print());
    }
}