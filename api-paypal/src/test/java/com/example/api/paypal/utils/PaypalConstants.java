package com.example.api.paypal.utils;

public class PaypalConstants {

    public static final String ACCESS_TOKEN_URL = "https://api-gw-uat.fisglobal.com/token?grant_type=client_credentials";
    public static final String URL_PARAMETERS = "?grant_type=client_credentials" ;

    public static final String ONBOARD_URL = "https://api-gw-uat.fisglobal.com/api/provisioning/services/v1/consumer-onboard";
    public static final String INSTRUMENTS_URL = "https://api-gw-uat.fisglobal.com/api/provisioning/services/v1/instruments";
    public static final String COMPLETION_URL = "https://api-gw-uat.fisglobal.com/api/provisioning/services/v1/provisioning-completion";

    public static final String AUTHORIZATION = "Basic eVEzZ01tUUZUWDR3RmwzSHVsSVU2dE4xdWl3YTpiNTdtSkZKeEZ2TGdoTVlLYUpFWXE5aDZHbXNh";
    public static final String UUID = "419593ff-0080-a8c6-30d7-d1f8351b61c4";
    public static final String ENTITY_ID = "PAYPAL";
    public static final String FINANCIAL_INSTITUTION_ID = "516388";
    public static final String UTF_8 = "UTF-8";
}
