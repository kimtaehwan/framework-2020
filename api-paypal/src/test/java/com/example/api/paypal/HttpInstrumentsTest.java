package com.example.api.paypal;

import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.instruments.InstrumentsRequest;
import com.example.api.paypal.dto.instruments.ConsumerProvisioningResponse;
import com.example.api.paypal.dto.onboard.ConsumerProvisioningRequest;
import com.example.api.paypal.dto.onboard.ConsumerOnboardResponse;
import com.example.api.paypal.utils.PaypalCollection;
import com.example.api.paypal.utils.PaypalConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestTemplate.class})
public class HttpInstrumentsTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void postInstruments_test01() {
        String accessToken = getAccessToken();
        log.info("### accessToken: {}", accessToken);

        HttpHeaders headers = PaypalCollection.getPaypalHttpHeaders(accessToken);
        log.info("### headers: {}", headers.toString());

        // call ConsumerProvisioningRequest
        ConsumerProvisioningRequest.Create create = PaypalCollection.getConsumerOnboardRequest();
        HttpEntity<ConsumerProvisioningRequest.Create> onboardHttpEntity = new HttpEntity<>(create, headers);

        URI onboardUri = PaypalCollection.getConsumerOnboardUri();
        ResponseEntity<ConsumerOnboardResponse> onboardResponseEntity = restTemplate.exchange(onboardUri, HttpMethod.POST, onboardHttpEntity, ConsumerOnboardResponse.class);

        log.info("### commerceId: {}", onboardResponseEntity.getBody().getEntity().getCommerceId());
        log.info("### referralId: {}", onboardResponseEntity.getBody().getEntity().getLinks().get(0).getCofEntityReferralId());
        log.info("### link - href: {}", onboardResponseEntity.getBody().getEntity().getLinks().get(0).getHref());

        String commerceId = onboardResponseEntity.getBody().getEntity().getCommerceId();
        String referralId = onboardResponseEntity.getBody().getEntity().getLinks().get(0).getHref();

        // call instruments
        InstrumentsRequest.Create instrumentsRequest = PaypalCollection.getInstrumentsRequest(commerceId, referralId);
        HttpEntity httpEntity = new HttpEntity(instrumentsRequest, headers);

        log.info("$$$ json: {}", instrumentsRequest.toString());
        URI instrumentsUri = PaypalCollection.getInstrumentsUri();
        ResponseEntity<ConsumerProvisioningResponse> response = restTemplate.exchange(instrumentsUri, HttpMethod.POST, httpEntity, ConsumerProvisioningResponse.class);

        log.info("### instruments: {}", response.getBody());
        String jsonData = PaypalCollection.getJsonData(response);

        log.info("### instruemntes - json: {}", jsonData);
    }

    @Test
    public void checkJsonData_test02() {
        String commerceId = "ba4a094b-8ac7-4ba1-ad5f-04ca48c68241";
        String referralId = "41507610a5c21b8c2ca31c1ef0440b8dedfdcf5a3b2323353a6dc6da5fb9ee5a";

        InstrumentsRequest.Create instrumentsRequest = PaypalCollection.getInstrumentsRequest(commerceId, referralId);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonData = objectMapper.writeValueAsString(instrumentsRequest);

            log.info("### jsonData: {}", jsonData);
        } catch (Exception e) {

        }
    }

    @Test
    public void instruments_test03() {
        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalCollection.getPaypalHttpHeaders(accessToken);

        String commerceId = "ba4a094b-8ac7-4ba1-ad5f-04ca48c68241";
        String referralId = "41507610a5c21b8c2ca31c1ef0440b8dedfdcf5a3b2323353a6dc6da5fb9ee5a";
        InstrumentsRequest.Create instrumentsRequest = PaypalCollection.getInstrumentsRequest(commerceId, referralId);
        HttpEntity httpEntity = new HttpEntity(instrumentsRequest, headers);

        String uri = "https://api-gw-uat.fisglobal.com/api/provisioning/services/v1/instruments";
        ResponseEntity<ConsumerProvisioningResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, ConsumerProvisioningResponse.class);

        log.info("### instruments: {}", response.getBody());
    }

    private String getAccessToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", PaypalConstants.AUTHORIZATION);

        HttpEntity httpEntity = new HttpEntity(headers);

        ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(PaypalConstants.ACCESS_TOKEN_URL, HttpMethod.POST, httpEntity, AccessTokenResponse.class);
        return response.getBody().getAccessToken();
    }


}
