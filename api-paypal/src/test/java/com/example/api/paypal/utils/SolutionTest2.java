package com.example.api.paypal.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.*;

@Slf4j
public class SolutionTest2 {

    @Test
    public void solutionTest_01() {
        int number = 1210;

        boolean isValid = checkValid(number);
        if (isValid == false) {
//            return -1;
        }

        int digit = getDigit(number);
        log.debug("digit: {}", digit);

        int answer = -1;

        for (int i = 2; i <= 10; i++) {

            List<String> list;
            list = convertNumber(number, i);

            log.debug("list: {}", String.join("", list));

            answer = compareNumber(list, number, digit);
        }
    }

    private int getDigit(int number) {
        return (int) Math.log10(number) + 1;
    }

    private int compareNumber(List<String> list, int number, int digit) {
        for (int i = 0; i < digit; i++) {

        }

        return 0;
    }

    private boolean checkValid(int number) {
        if ( number < 1 || number > 100000000) {
            return false;
        }

        return true;
    }

    private List<String> convertNumber(int decimal, int n) {
        List<String> results = new ArrayList<>();

        do {
            results.add(Integer.toString(decimal % n));
            decimal = decimal / n;

            if ( decimal  <= n) {
                results.add(Integer.toString(decimal));
                break;
            }
        } while (decimal != 0);

        Collections.reverse(results);
        return results;
    }
}
