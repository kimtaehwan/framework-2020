package com.example.api.paypal.web;

import com.example.api.paypal.dto.CoreCBESResponse;
import com.example.api.paypal.dto.common.CommonResponse;
import com.example.api.paypal.dto.common.ContextualOperationInformation;
import com.example.api.paypal.dto.common.ResponseStatus;
import com.example.api.paypal.dto.onboard.PaypalRequest;
import com.example.api.paypal.service.PaypalService;
import com.example.api.paypal.vo.MessageCode;
import com.example.api.paypal.vo.ResponseStatusCodeEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PaypalController.class)
public class PaypalControllerTest {
    private static final String API_SPEC_CONTEXT = "While validating In-App Push Paypal data request.";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PaypalService paypalService;

    @Test
    public void createPaypalOnboard_test01() throws Exception {

        // Given
        PaypalRequest.CreateReq buildRequest = PaypalRequest.CreateReq.builder()
                .appToken("appToken")
                .deviceUniqueId("test")
                .language("en")
                .fiToken("fi-token")
                .lastReadTimeStamp(123L)
                .requesterHostName("host")
                .subscriberId(123)
                .subscriberReferenceId("enen")
                .cardReferenceId(null)
                .email("test@gmail.com")
                .build();

        ContextualOperationInformation contextualOperationInformation = ContextualOperationInformation.builder()
                .cause("No field in the request.")
                .context(API_SPEC_CONTEXT)
                .build();

        ResponseStatus responseStatus = new ResponseStatus(ResponseStatusCodeEnum.FAILURE.code(), MessageCode.API_102.getMessageCode());

        given(paypalService.apiSpecificValidation(any(PaypalRequest.CreateReq.class))).willReturn(new CoreCBESResponse(CommonResponse.builder().responseStatus(responseStatus).build(), contextualOperationInformation));

        // when
        RequestBuilder request = MockMvcRequestBuilders.post("/createPaypalOnboard")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildRequest));

        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andDo(print());
    }

}