package com.example.api.paypal.utils;

import com.example.api.paypal.dto.AccountHolderName;
import com.example.api.paypal.dto.completion.CompletionRequest;
import com.example.api.paypal.dto.instruments.*;
import com.example.api.paypal.dto.onboard.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PaypalCollection {

    private static RestTemplate restTemplate;

    public static HttpHeaders getPaypalHttpHeaders(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("uuid", PaypalConstants.UUID);
        headers.add("entityId", PaypalConstants.ENTITY_ID);
        headers.add("accept", MediaType.APPLICATION_JSON_VALUE);

        String authorization = getAuthorization(accessToken);

        headers.add("Authorization", authorization);

        return headers;
    }

    private static String getAuthorization(String accessToken) {
        return "Bearer " +accessToken;
    }

    public static ConsumerProvisioningRequest.Create getConsumerOnboardRequest() {
        PersonDetails personDetails = getPersonDetail();

        ConsumerProvisioningRequest.Create create = ConsumerProvisioningRequest.Create.builder()
                .financialInstituteId(PaypalConstants.FINANCIAL_INSTITUTION_ID)
                .personDetails(personDetails)
                .accountProperties(AccountProperties.builder()
                        .accountCountryCode("AT")
                        .build())
                .build();

        return create;
    }

    private static PersonDetails getPersonDetail() {
        AccountHolderName accountHolderName = AccountHolderName.builder()
                .prefix("Mr.")
                .givenName("Niklas")
                .surname("Frangos")
                .middleName("Kobe")
                .suffix("Jr.")
                .build();

        PhoneContact phoneContact = PhoneContact.builder()
                .phone(PhoneInfo.builder()
                        .countryCode("82")
                        .nationalNumber("82")
                        .extensionNumber("1027171703")
                        .build())
                .phoneType("MOBILE")
                .build();

        AddressInfo addressInfo = AddressInfo.builder()
                .address(AddressDetail.builder()
                        .line1("20/Wolfengasse 3")
                        .line2("Fleischmarkt")
                        .city("Vienna")
                        .state("string")
                        .county("string")
                        .countryCode("AT")
                        .postalCode("A-1010")
                        .streetNumber("string")
                        .streetName("string")
                        .build())
                .addressType("HOME")
                .build();

        EmailAddressInfo emailAddressInfo = EmailAddressInfo.builder()
                .emailAddress("wih0202@gmail.com")
                .primary(true)
                .confirmed(true)
                .build();

        List<AccountHolderName> names = new ArrayList<>();
        names.add(accountHolderName);

        List<PhoneContact> phoneContacts = new ArrayList<>();
        phoneContacts.add(phoneContact);

        List<AddressInfo> addresses = new ArrayList<>();
        addresses.add(addressInfo);

        List<EmailAddressInfo> emailAddresses = new ArrayList<>();
        emailAddresses.add(emailAddressInfo);

        PersonDetails personDetails = PersonDetails.builder()
                .names(names)
                .phoneContacts(phoneContacts)
                .addresses(addresses)
                .emailAddresses(emailAddresses)
                .locale("en_US")
                .build();

        return personDetails;
    }

    public static InstrumentsRequest.Create getInstrumentsRequest(String commerceId, String referralId) {
        List<CardAccount> cardAccounts = getCardAccounts();

        return InstrumentsRequest.Create.builder()
                .cardAccounts(cardAccounts)
                .financialInstituteId(PaypalConstants.FINANCIAL_INSTITUTION_ID)
                .commerceId(commerceId)
                .referralId(referralId)
                .build();
    }

    private static List<CardAccount> getCardAccounts() {
        AccountHolderName accountHolderName = AccountHolderName.builder()
                .prefix("Mr.")
                .givenName("Niklas")
                .surname("Frangos")
                .middleName("Kobe")
                .suffix("Jr.")
                .build();

        String uuid = UUID.randomUUID().toString();

        List<CardAccount> result = new ArrayList<>();
        CardAccount cardAccount = CardAccount.builder()
                .referenceFinancialInstrumentId(uuid)
                .pan(PanInfo.builder()
                        .plainText("5163883431176032")
                        .cipherText("")
                        .alias("")
                        .build())
                .expiryDate("2019-12")
                .billingAddress(BillingAddress.builder()
                        .addressLine1("20/Wolfengasse 3")
                        .addressLine2("Fleischmarkt")
                        .addressLine3("string")
                        .adminArea1("string")
                        .adminArea2("Vienna")
                        .postalCode("A-1010")
                        .countryCode("AT")
                        .build())
                .accountHolderName(accountHolderName)
                .build();

        result.add(cardAccount);

        return result;
    }

    public static URI getConsumerOnboardUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/consumer-onboard")
                .build()
                .toUri();
    }

    public static URI getInstrumentsUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/instruments")
                .build()
                .toUri();
    }

    public static URI getCompletionUri() {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api-gw-uat.fisglobal.com")
                .path("/api/provisioning/services/v1/provisioning-completion")
                .build()
                .toUri();
    }

    public static String getJsonData(ResponseEntity<?> response) {
        String result = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            result = objectMapper.writeValueAsString(response);
        } catch (Exception e) {

        }

        return result;
    }


    public static CompletionRequest.Create getCompletionRequest(String code, String referralId) {
        return CompletionRequest.Create.builder()
                .referralId(referralId)
                .code(code)
                .financialInstitutionId(PaypalConstants.FINANCIAL_INSTITUTION_ID)
                .entityId(PaypalConstants.ENTITY_ID)
                .build();
    }
}
