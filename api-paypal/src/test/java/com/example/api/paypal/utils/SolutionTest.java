package com.example.api.paypal.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class SolutionTest {

    private String[] participant = {"leo", "kiki", "eden", ""};
    private String[] completion = {"eden", "kiki", ""};
    private static final int ERROR = 0;

    @Test
    public void solutionTest_01() {
        int number = 1210;

    }


    private boolean checkValidation(String[] participant, String[] completion) {
        boolean isValid = false;

        isValid = checkInput(participant);
        if (isValid == false) {
            return false;
        }

        isValid = checkInput(completion);
        if (isValid == false) {
            return false;
        }

        isValid = checkCompareLength(participant, completion);
        if (isValid == false) {
            return false;
        }

        isValid = checkLengthForName(participant);

        return true;
    }

    private boolean checkLengthForName(String[] inputs) {
        List<String> list = Arrays.stream(inputs)
                .filter(t -> t.length() < 1 || t.length() > 20)
                .collect(Collectors.toList());

        if (list.size() > 0) {
            return false;
        }

        return true;
    }

    private boolean checkCompareLength(String[] participant, String[] completion) {
        if (participant.length == completion.length + 1) {
            return true;
        }

        return false;
    }

    private boolean checkInput(String[] inputs) {
        if (inputs == null || inputs.length == 0 ) {
            return false;
        }

        return true;
    }

    @Test
    public void solution_test02() {
        int[] v = {20, 8, 10, 1, 4, 15};

        boolean isValid = checkValidation(v);
        if (isValid == false) {
//            return ERROR;
        }

        Integer[] sortedHeights = getSortedArray(v);
        int answer = 0;
        answer = getAnswer(sortedHeights);

        log.debug("{}", answer);
    }

    private int getAnswer(Integer[] sortedHeights) {
        int result = 0;

        for (int i = 0; i < sortedHeights.length - 1; i++) {
            int num1 = sortedHeights[i];
            int num2 = sortedHeights[i+1];

            result += getCalculatorABS(num1, num2);
        }

        return result;
    }

    private int getCalculatorABS(Integer num1, Integer num2) {
        return Math.abs(num1 - num2);
    }

    private Integer[] getSortedArray(final int[] heights) {
        Integer[] descSorted = Arrays.stream(heights)
                .boxed()
                .toArray(Integer[]::new);
        Arrays.sort(descSorted, Comparator.reverseOrder());

        return descSorted;
    }

    private boolean checkValidation(final int[] heights) {
        boolean isValid = false;

        if ( heights == null || heights.length < 2 || heights.length > 8) {
            return false;
        }

        isValid = checkHeight(heights);

        return isValid;
    }

    private boolean checkHeight(final int[] heights) {
        Long count =  Arrays.stream(heights)
                .filter(t -> t < 1 || t > 100)
                .count();

        return count == 0 ? true : false;
    }
}
