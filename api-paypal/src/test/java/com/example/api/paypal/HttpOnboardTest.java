package com.example.api.paypal;

import com.example.api.paypal.core.PaypalConfiguration;
import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.onboard.*;
import com.example.api.paypal.utils.PaypalCollection;
import com.example.api.paypal.utils.PaypalConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@RunWith(SpringRunner.class)
@EnableConfigurationProperties
@SpringBootTest(classes = {RestTemplate.class, PaypalConfiguration.class})
public class HttpOnboardTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void generateAccessToken_test01() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", PaypalConstants.AUTHORIZATION);

        HttpEntity httpEntity = new HttpEntity(headers);

        try {

            ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(PaypalConstants.ACCESS_TOKEN_URL, HttpMethod.POST, httpEntity, AccessTokenResponse.class);

            log.info("access token - getStatusCode: {}", response.getStatusCode());
            log.info("access token - getStatusCodeValue: {}", response.getStatusCodeValue());
            log.info("access token - headers: {}", response.getHeaders());
            log.info("access token - body: {}", response.getBody());
            log.info("access token - getBody - getAccessToken: {}", response.getBody().getAccessToken());
            log.info("access token - getBody - getExpiresIn: {}", response.getBody().getExpiresIn());
            log.info("access token - getBody - getScope: {}", response.getBody().getScope());
            log.info("access token - getBody - getTokenType: {}", response.getBody().getTokenType());
        } catch (Exception e) {
            log.error("### error: {}", e);
        }
    }

    @Test
    public void postConsumerOnboard_test01() {
        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalCollection.getPaypalHttpHeaders(accessToken);

        try {
            ConsumerProvisioningRequest.Create create = PaypalCollection.getConsumerOnboardRequest();
            log.info("create: {}", create.toString());

            HttpEntity httpEntity = new HttpEntity(create, headers);

            URI uri = PaypalCollection.getConsumerOnboardUri();
            ResponseEntity<ConsumerOnboardResponse> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, ConsumerOnboardResponse.class);

            log.info("response: {}", responseEntity.getBody());
            log.info("response: {}", responseEntity.getStatusCode());
            log.info("response: {}", responseEntity.getStatusCodeValue());
        }catch (Exception e) {
            log.error("{}", e);
        }
    }

    @Test
    public void postConsumerOnboard_test02() {
        String url = "https://api-gw-uat.fisglobal.com/api/provisioning/services/v1/consumer-onboard";

        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalCollection.getPaypalHttpHeaders(accessToken);

        ConsumerProvisioningRequest.Create create = PaypalCollection.getConsumerOnboardRequest();
        log.info("create.toString(): {}", create.toString());

        ObjectMapper mapper = new ObjectMapper();

        try {
            String jsonData = mapper.writeValueAsString(create);
            log.info("jsonData: {}", jsonData);

            HttpEntity httpEntity = new HttpEntity(create, headers);

            ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);

            log.info("test2: {}", response.getBody());
        } catch (Exception e) {
            log.error("{}", e);
        }
    }

    private String getAccessToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", PaypalConstants.AUTHORIZATION);

        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(PaypalConstants.ACCESS_TOKEN_URL, HttpMethod.POST, httpEntity, AccessTokenResponse.class);

        return response.getBody().getAccessToken();
    }
}
