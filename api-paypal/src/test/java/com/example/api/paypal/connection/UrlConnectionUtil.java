package com.example.api.paypal.connection;

import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.completion.CompletionResponse;
import com.example.api.paypal.dto.instruments.ConsumerProvisioningResponse;
import com.example.api.paypal.dto.onboard.ConsumerOnboardResponse;
import com.example.api.paypal.utils.PaypalConstants;
import com.example.api.paypal.vo.FisApiType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

@Slf4j
public class UrlConnectionUtil {
    public static String getAccessTokenResponse(HttpURLConnection connection) throws MalformedURLException, IOException {
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.flush();
        wr.close();

        Charset charset = Charset.forName(PaypalConstants.UTF_8);
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        log.info("response: {}", response.toString());
        return response.toString();
    }

    public static String getResponse(String jsonString, HttpURLConnection connection) throws IOException {

        OutputStream os = connection.getOutputStream();
        os.write(jsonString.getBytes(PaypalConstants.UTF_8));
        os.flush();

        String inputLine = null;
        StringBuffer outResult = new StringBuffer();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), PaypalConstants.UTF_8));
        while ((inputLine = in.readLine()) != null) {
            outResult.append(inputLine);
        }

        return outResult.toString();
    }

    public static HttpURLConnection getConnection(FisApiType type, String accessToken) {
        switch (type) {
            case ACCESS_TOKEN:
                return getAccessTokenConnection(PaypalConstants.ACCESS_TOKEN_URL, PaypalConstants.AUTHORIZATION);
            case ONBOARD:
                return getCommonConnection(PaypalConstants.ONBOARD_URL, accessToken);
            case INSTRUMENTS:
                return getCommonConnection(PaypalConstants.INSTRUMENTS_URL, accessToken);
            case COMPLETION:
                return getCommonConnection(PaypalConstants.COMPLETION_URL, accessToken);
            default:
                return null;
        }
    }

    private static HttpURLConnection getCommonConnection(String host, String accessToken) {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(host);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("uuid", PaypalConstants.UUID);
            connection.setRequestProperty("entityId", PaypalConstants.ENTITY_ID);
            connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_VALUE);
            connection.setRequestProperty("accept", MediaType.APPLICATION_JSON_VALUE);
            connection.setRequestProperty("Authorization", "Bearer "+accessToken);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
        } catch (Exception e) {
            log.error("connection: {}", e);
        }

        return connection;
    }


    public static HttpURLConnection getAccessTokenConnection(String host, String authorization) {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(host);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", authorization);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setDoOutput(true);
        } catch (Exception e) {
            log.error("connection: {}", e);
        }

        return connection;
    }

    public static AccessTokenResponse transformAccessTokenResponse(String response) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(response, AccessTokenResponse.class);
    }

    public static ConsumerOnboardResponse transformOnboardResponse(String strOnboardResponse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(strOnboardResponse, ConsumerOnboardResponse.class);
    }

    public static ConsumerProvisioningResponse transformInstrumentsResponse(String strInstrumentsResponse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(strInstrumentsResponse, ConsumerProvisioningResponse.class);
    }

    public static CompletionResponse transformCompletion(String strCompletionResponse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(strCompletionResponse, CompletionResponse.class);
    }
}
