package com.example.api.paypal;

import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.completion.CompletionRequest;
import com.example.api.paypal.dto.completion.CompletionResponse;
import com.example.api.paypal.utils.PaypalCollection;
import com.example.api.paypal.utils.PaypalConstants;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestTemplate.class})
public class HttpCompletionTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void postCompletion_test01() {
        String accessToken = getAccessToken();
        HttpHeaders headers = PaypalCollection.getPaypalHttpHeaders(accessToken);

        log.info("### headers: {}", headers.toString());

        // call CompletionRequest
        CompletionRequest.Create create = CompletionRequest.Create.builder()
                .entityId(PaypalConstants.ENTITY_ID)
                .referralId("4f95371fbc84bc1b34406ffdc73421bc0cc9b6a0d4ad2912bc90bf9e6360bc27")
                .financialInstitutionId(PaypalConstants.FINANCIAL_INSTITUTION_ID)
                .code("C21AAGDytk1ob2kUdJ5_utUs2k0EJVjPFLOslV13W5hEJuVpBpSXcyYkrVShC7feXVO3oHAvMWVSxZlA-WvMdscM9pyw6rm4A")
                .build();

        HttpEntity<CompletionRequest.Create> httpEntity = new HttpEntity<>(create, headers);

        URI uri = PaypalCollection.getCompletionUri();
        ResponseEntity<CompletionResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, CompletionResponse.class);

        log.info("### completion: {}", response.getBody());
    }

    private String getAccessToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", PaypalConstants.AUTHORIZATION);

        HttpEntity httpEntity = new HttpEntity(headers);

        ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(PaypalConstants.ACCESS_TOKEN_URL, HttpMethod.POST, httpEntity, AccessTokenResponse.class);
        return response.getBody().getAccessToken();
    }
}
