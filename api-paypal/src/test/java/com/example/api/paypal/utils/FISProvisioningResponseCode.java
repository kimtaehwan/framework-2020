package com.example.api.paypal.utils;

import java.util.Arrays;

public enum FISProvisioningResponseCode {
    SUCCESS("00", "Request Successfully Executed!"),
    AUTHORIZATION_FAILURE_01("01", "Bad Request"),
    AUTHORIZATION_FAILURE_02("02", "Payload decryption failed"),
    AUTHORIZATION_FAILURE_03("03", "Bad Encryption key"),
    AUTHORIZATION_FAILURE_04("04", "Token Verification failed"),
    AUTHORIZATION_FAILURE_05("05", "Unauthorized Bin Range. Please call customer support for assistance"),
    AUTHORIZATION_FAILURE_09("09", "Unauthorized Financial Institution. Please call customer support for assistance."),
    AUTHORIZATION_FAILURE_10("10", "Invalid card number"),
    AUTHORIZATION_FAILURE_11("11", "Multiple values for identifier, expected one"),

    VALIDATION_FAILED_13("13", "Image Format is not supported for Image"),
    VALIDATION_FAILED_14("14", "Account type is not supported for Account31- Platform specific error code will start from 31 for each API."),
    VALIDATION_FAILED_15("15", "Phone type is not supported for Phone 16- Billing address is missing"),
    VALIDATION_FAILED_17("17", "Commerce Id is missing"),
    VALIDATION_FAILED_18("18", "Address type is not supported for Address 19- Mandatory header is missing"),
    VALIDATION_FAILED_20("20", "Invalid commerceId. No matching details found"),
    VALIDATION_FAILED_21("21", "Instrument not provisioned"),
    VALIDATION_FAILED_23("23", "Mandatory field {0} is missing/invalid"),
    VALIDATION_FAILED_24("24", "Identifier type is not supported for Institution Identifier Type"),
    VALIDATION_FAILED_25("25", "Invalid Bin Range. Please call customer support for assistance."),
    VALIDATION_FAILED_26("26", "Bin not enrolled for the product provisioning hub."),
    VALIDATION_FAILED_27("27", "Reason type is not supported for remove identifier reason type"),
    VALIDATION_FAILED_28("28", "Invalid Email address"),
    VALIDATION_FAILED_29("29", "Invalid account Country Code. No matching details found"),
    VALIDATION_FAILED_30("30", "Invalid national number for phone."),
    VALIDATION_FAILED_31("31", "Invalid phone country code."),
    VALIDATION_FAILED_32("32", "Invalid extension number for phone."),
    VALIDATION_FAILED_33("33", "Account number type is not supported."),
    VALIDATION_FAILED_34("34", "Postal code is mandatory for the given country code under personal detail address."),
    VALIDATION_FAILED_35("35", "Referral Id doesn't match for the given financial institution"),
    VALIDATION_FAILED_36("36", "The entered financial institution is invalid"),
    VALIDATION_FAILED_37("37", "The consumer consent process in not completed"),
    VALIDATION_FAILED_38("38", "The authorization code is invalid or expired"),
    VALIDATION_FAILED_39("39", "Referral Id is missing"),
    VALIDATION_FAILED_40("40", "No matching details found for given commerceId and referralId"),
    VALIDATION_FAILED_41("41", "Either PAN or bin start range and bin end range must present"),
    VALIDATION_FAILED_42("42", "Mandatory field given name or surname is missing"),
    VALIDATION_FAILED_43("43", "Mandatory field foreground color is missing for metadata"),
    VALIDATION_FAILED_44("44", "FI for account number is not enrolled for the product provisioning hub"),

    INVALID_BIN_RANGE_80("80", "Invalid Bin Range. Please call customer support for assistance."),
    ERROR_FROM_PLATFORM_81("81", "Request could not be processed by platform. Please call customer support for assistance.")
    ;

    private final String code;
    private final String description;

    FISProvisioningResponseCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    static FISProvisioningResponseCode of(String code) {
        return Arrays.stream(values())
                .filter(v -> code.equals(v.code))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException (String.format("%s is not response Code", code)));
    }
}
