package com.example.api.paypal.connection;

import com.example.api.paypal.dto.AccessTokenResponse;
import com.example.api.paypal.dto.completion.CompletionRequest;
import com.example.api.paypal.dto.completion.CompletionResponse;
import com.example.api.paypal.dto.instruments.InstrumentsRequest;
import com.example.api.paypal.dto.instruments.ConsumerProvisioningResponse;
import com.example.api.paypal.dto.onboard.ConsumerProvisioningRequest;
import com.example.api.paypal.dto.onboard.ConsumerOnboardResponse;
import com.example.api.paypal.utils.PaypalCollection;
import com.example.api.paypal.vo.FisApiType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.UUID;

@Slf4j
public class UrlConnectionTest {

    @Test
    public void access_token_test01() {
        try {
            HttpURLConnection connection = UrlConnectionUtil.getConnection(FisApiType.ACCESS_TOKEN, "");

            String response = UrlConnectionUtil.getAccessTokenResponse(connection);
            AccessTokenResponse accessTokenResponse = UrlConnectionUtil.transformAccessTokenResponse(response);

            log.info("access_token: {}", accessTokenResponse.getAccessToken());
            log.info("expires_in: {}", accessTokenResponse.getExpiresIn());

        } catch (MalformedURLException e ) {
            log.error("The URL address is incorrect.");
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException: {}", e);
        } catch (IOException e) {
            log.error("It can't connect to the url.");
        }
    }

    @Test
    public void consumer_onboard_test02() {
        HttpURLConnection accessTokenConnection = null;
        HttpURLConnection onboardConnection = null;

        try {
            // call Access Token
            accessTokenConnection = UrlConnectionUtil.getConnection(FisApiType.ACCESS_TOKEN, "");

            String strTokenResponse = UrlConnectionUtil.getAccessTokenResponse(accessTokenConnection);
            AccessTokenResponse accessTokenResponse = UrlConnectionUtil.transformAccessTokenResponse(strTokenResponse);

            // call onboard
            String accessToken = accessTokenResponse.getAccessToken();
            onboardConnection = UrlConnectionUtil.getConnection(FisApiType.ONBOARD, accessToken);

            ConsumerProvisioningRequest.Create onboardRequest = PaypalCollection.getConsumerOnboardRequest();
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonString = objectMapper.writeValueAsString(onboardRequest);

            String strOnboardResponse = UrlConnectionUtil.getResponse(jsonString, onboardConnection);
            ConsumerOnboardResponse consumerOnboardResponse = UrlConnectionUtil.transformOnboardResponse(strOnboardResponse);

            log.info("consumer onboard: {}", consumerOnboardResponse.toString());
        } catch (MalformedURLException e ) {
            log.error("The URL address is incorrect: {}", e);
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException: {}", e);
        } catch (Exception e) {
            log.error("It can't connect to the url: {}", e);
        } finally {
            accessTokenConnection.disconnect();
            onboardConnection.disconnect();
        }
    }

    @Test
    public void instruments_test03() {
        HttpURLConnection accessTokenConnection = null;
        HttpURLConnection onboardConnection = null;
        HttpURLConnection instrumentsConnection = null;

        try {
            // call Access Token
            accessTokenConnection = UrlConnectionUtil.getConnection(FisApiType.ACCESS_TOKEN, "");

            String strTokenResponse = UrlConnectionUtil.getAccessTokenResponse(accessTokenConnection);
            AccessTokenResponse accessTokenResponse = UrlConnectionUtil.transformAccessTokenResponse(strTokenResponse);

            // call onboard
            String accessToken = accessTokenResponse.getAccessToken();
            onboardConnection = UrlConnectionUtil.getConnection(FisApiType.ONBOARD, accessToken);

            ConsumerProvisioningRequest.Create onboardRequest = PaypalCollection.getConsumerOnboardRequest();
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonString = objectMapper.writeValueAsString(onboardRequest);

            String strOnboardResponse = UrlConnectionUtil.getResponse(jsonString, onboardConnection);
            ConsumerOnboardResponse consumerOnboardResponse = UrlConnectionUtil.transformOnboardResponse(strOnboardResponse);

            log.info("commerceId: {}", consumerOnboardResponse.getEntity().getCommerceId());
            log.info("referralId: {}", consumerOnboardResponse.getEntity().getLinks().get(0).getCofEntityReferralId());
            log.info("link - href: {}", consumerOnboardResponse.getEntity().getLinks().get(0).getHref());

            // call instruments
            instrumentsConnection = UrlConnectionUtil.getConnection(FisApiType.INSTRUMENTS, accessToken);

            String commerceId = consumerOnboardResponse.getEntity().getCommerceId();
            String referralId = consumerOnboardResponse.getEntity().getLinks().get(0).getCofEntityReferralId();
            InstrumentsRequest.Create instrumentsRequest = PaypalCollection.getInstrumentsRequest(commerceId, referralId);

            jsonString = objectMapper.writeValueAsString(instrumentsRequest);
            String strInstrumentsResponse = UrlConnectionUtil.getResponse(jsonString, instrumentsConnection);

            log.info("strInstrumentsResponse: {}", strInstrumentsResponse);
            ConsumerProvisioningResponse consumerProvisioningResponse = UrlConnectionUtil.transformInstrumentsResponse(strInstrumentsResponse);

            log.info("instruments response: {}", consumerProvisioningResponse.toString());
        } catch (MalformedURLException e ) {
            log.error("The URL address is incorrect: {}", e);
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException: {}", e);
        } catch (Exception e) {
            log.error("It can't connect to the url: {}", e);
        } finally {
            accessTokenConnection.disconnect();
            onboardConnection.disconnect();
            instrumentsConnection.disconnect();
        }
    }

    @Test
    public void completion_test04() {
        HttpURLConnection accessTokenConnection = null;
        HttpURLConnection completionConnection = null;

        try {
            // call Access Token
            accessTokenConnection = UrlConnectionUtil.getConnection(FisApiType.ACCESS_TOKEN, "");

            String strTokenResponse = UrlConnectionUtil.getAccessTokenResponse(accessTokenConnection);
            AccessTokenResponse accessTokenResponse = UrlConnectionUtil.transformAccessTokenResponse(strTokenResponse);

            // call completion
            String accessToken = accessTokenResponse.getAccessToken();
            completionConnection = UrlConnectionUtil.getConnection(FisApiType.COMPLETION, accessToken);

            String code = "C21AAFz8kGBq6dcTp1qEvtnd18gR3IMO-DOYwx4F61DRhr0TPpXm9VeBdKXSN4YB8BKbhoKtZADPqZ1TYLnVTOud3wYcqF1LQ";
            String referralId = "5fa089828227e5ccebe591e71d813819313674a7c1b91f58db81e1bba722997e";
            CompletionRequest.Create request = PaypalCollection.getCompletionRequest(code, referralId);

            ObjectMapper objectMapper = new ObjectMapper();
            String jsonString = objectMapper.writeValueAsString(request);

            log.info("completion json: {}", jsonString);
            String strCompletionResponse = UrlConnectionUtil.getResponse(jsonString, completionConnection);
            CompletionResponse completionResponse = UrlConnectionUtil.transformCompletion(strCompletionResponse);

            log.info("code: {}", completionResponse.toString());
        } catch (MalformedURLException e ) {
            log.error("The URL address is incorrect: {}", e);
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException: {}", e);
        } catch (Exception e) {
            log.error("It can't connect to the url: {}", e);
        } finally {
            accessTokenConnection.disconnect();
            completionConnection.disconnect();
        }
    }

    @Test
    public void uuid_test05() {
        UUID uuid = UUID.randomUUID();

        log.info("uuid: {}", uuid.toString());
    }
}
