# Terraform
* https://github.com/terraform-providers/terraform-provider-aws
* https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy   

## Terraform 으로 AWS 관리하기 - Provider, VPC 구성
* [iam user 만들기](https://blog.outsider.ne.kr/1260)
* [IAM user 및 그룹 생성](https://medium.com/@jyson88/terraform-iam-%EC%82%AC%EC%9A%A9-%EB%B0%A9%EB%B2%95-6845d143e336)
* https://woowabros.github.io/tools/2019/09/20/terraform.html
* https://rampart81.github.io/post/iam_config_terraform/

## VPC
* https://blog.outsider.ne.kr/1260  

## AWS - KeyPair
* EC2 인스턴스에 SSH 로 접속
    * https://kin3303.tistory.com/101

## Reference
* https://woowabros.github.io/tools/2019/09/20/terraform.html
* https://www.44bits.io/ko/post/terraform_introduction_infrastrucute_as_code
