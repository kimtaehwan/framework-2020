resource "aws_instance" "test1" {
  ami = "ami-0a93a08544874b3b7"
  instance_type = "t2.micro"
  key_name = aws_key_pair.aws_key.key_name
  vpc_security_group_ids = [
    aws_vpc.example.id
  ]
}
