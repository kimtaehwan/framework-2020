resource "aws_vpc" "example" {
  cidr_block = "172.10.0.0/20"
  tags = {
    Name = "example"
  }
}

resource "aws_subnet" "example-a" {
  cidr_block = "172.10.0.0/24"
  vpc_id = aws_vpc.example.id
  availability_zone = var.aws_subnet_a
}

resource "aws_subnet" "example-b" {
  cidr_block = "172.10.1.0/24"
  vpc_id = aws_vpc.example.id
  availability_zone = var.aws_subnet_b
}
