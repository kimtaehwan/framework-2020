resource "aws_key_pair" "aws_key" {
  key_name = "aws_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+M13kNvI9+osFsuuvublKK8zW4NktCKTu5g1D9UWFmsouG9VUcQ5LhkpaO9Ag3PE7fk8pn+0qyYPLjnD0bkHDAFqBRvRmLD8S1sjkv6En47Yoj4BCyaJHofZRDjVxI0OtZn/NbwRQeYVHZhg4/CdhTwqUkEwwAX4yGP/sU+DpDU9S/2MD/RmEEdRzZJhLy1IXouqrd/sSYkJwGBaWg7RAOvf1aINRfKhwMNpAwweQfnrYi5y4j4CR8+DKPk+MCN9+RpEsxjGqoz0AMJZactV6Vl1b+SwyeWEdmU0nKAC6FajIOkn1flGH6KyPhaIxBbaNPUUfzBGUYs3h98akTNQR taehwan@ONDOT-CM"
  #public_key = "file(var.path_to_public_key)"
}

resource "aws_security_group" "ssh" {
  name = "allow_ssh_from_all"
  description = "Allow SSH port from all"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_security_group" "default" {
  name = "default"
}

resource "aws_instance" "web" {
  ami = "ami-0a93a08544874b3b7"
  instance_type = "t2.micro"
  key_name = aws_key_pair.aws_key.key_name
  vpc_security_group_ids = [
    aws_security_group.ssh.id,
    data.aws_security_group.default.id
  ]
}

