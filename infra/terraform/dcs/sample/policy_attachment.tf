resource "aws_iam_group_policy_attachment" "policy-attachment-eks-admin" {
  group = aws_iam_group.EKS-Group.name
  policy_arn = aws_iam_policy.EKS-Admin.name
}

resource "aws_iam_group_policy_attachment" "policy-attachment-eks-autoscaling" {
  group = aws_iam_group.EKS-Group.name
  policy_arn = aws_iam_policy.EKS-Admin.name
}
