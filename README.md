# Framework 2020

## Diagram
### Sequence Diagram
* https://brownbears.tistory.com/511


## Install [sonarqube](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/).
> docker pull sonarqube:community
> docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:community
> Log in to http://localhost:9000 with System Administrator credentials (login=admin, password=admin)    

## Install [SonarScanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/).

### Analyzing a Sonarqube
   * Click the Create new project button
   * When asked How do you want to create your project, select Manually.
   * Give your project a Project key and a Display name and click the Set Up button.
   * Under Provide a token, select Generate a token. Give your token a name, click the Generate button, and click Continue.
   * Select your project's main language under Run analysis on your project, and follow the instructions to analyze your project. Here you'll download and execute a Scanner on your code (if you're using Maven or Gradle, the Scanner is automatically downloaded).

Command to update
* ng update @angular/cdk @angular/core  @angular/common typescript @angular/flex-layout angular-in-memory-web-api @angular-devkit/build-angular --force
* ng update @angular/cli
* ng update @angular/material @ngrx/store rxjs
* ng update @swimlane/ngx-dnd
    
# Database Version control 
## [Flyway](https://flywaydb.org/)

# Git
## git 암호 저장
* Git은 몇 분 동안 입력한 사용자이름이나 암호를 저장해둔다. 이 기능을 활성화하려면 git config --global credential.helper cache 명령을 실행하여 환경설정을 추가한다.
