alter table TNC
    add FI_ID NUMBER
;

UPDATE TNC A
SET A.FI_ID = (SELECT B.FI_ID
               FROM FINANCIAL_INSTITUTION B
               WHERE B.FI_TOKEN = A.FI_TOKEN)
WHERE 1 = 1
;



alter table TNC drop column FI_TOKEN
;

create index TNC_FI_ID_INDEX
    on TNC (FI_ID)
;

alter table TNC modify FI_ID not null
;


create table TNC_AUD
(
    ID NUMBER not null,
    REV_ID  NUMBER(10) not null,
    REVTYPE NUMBER(3) not null,
    FI_ID NUMBER(10) not null,
    FILE_NAME VARCHAR2(500),
    STATUS VARCHAR2(50),
    CREATED DATE,
    CREATOR VARCHAR2(200),
    LAST_MODIFIED DATE,
    CONTENT_TITLE VARCHAR2(1000),
    ORG_FILE_NAME VARCHAR2(300),
    constraint TNC_AUD_PK primary key(ID, REV_ID)
)
;

insert
  into TNC_AUD(ID
              ,REV_ID
              ,REVTYPE
              ,FI_ID
              ,FILE_NAME
              ,STATUS
              ,CREATED
              ,CREATOR
              ,LAST_MODIFIED
              ,CONTENT_TITLE
              ,ORG_FILE_NAME)
select ID
      ,REVINFO_SEQ.nextval
      ,0
      ,FI_ID
      ,FILE_NAME
      ,STATUS
      ,CREATED
      ,CREATOR
      ,LAST_MODIFIED
      ,CONTENT_TITLE
      ,ORG_FILE_NAME
  from tnc a
;


insert
into REVINFO (REV, REVTSTMP)
select REV_ID
     ,(SYSDATE - TO_DATE('01/01/1970 00:00:00','DD/MM/YYYY HH24:MI:SS')) * 1000 * 60 * 60 * 24
from TNC_AUD
where 1 = 1
;