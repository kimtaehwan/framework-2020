Install Oracle Win
-----------------
* docker rm
    * docker rm oracle19.3.0-ee
* docker pull
    * docker pull banglamon/oracle193db:19.3.0-ee
    * docker image ls |grep oracle
* docker run
    * docker run -d --name oracle19.3.0-ee -p 1521:1521 -p 8081:8081 -e ORACLE_SID=CLOUDDB -e ORACLE_PDB=CLOUDPDB -e ORACLE_PWD=Oracle123 -v /u01/app/oracle/oradata:/opt/oracle/oradata banglamon/oracle193db:19.3.0-ee
    * docker ps -a |grep oracle
* docker logs
    * docker logs -f {container id}
* Reference
    * https://medium.com/oracledevs/devops-series-running-oracle-database-19c-in-a-docker-container-b41f142c19f6
    * https://github.com/oracle/docker-images/tree/master/OracleDatabase/SingleInstance

Create Table
-------------
~~~
CREATE TABLESPACE CLOUDDB DATAFILE 'CLOUDDB' SIZE 2048M AUTOEXTEND ON NEXT 4M MAXSIZE UNLIMITED LOGGING EXTENT MANAGEMENT LOCAL AUTOALLOCATE BLOCKSIZE 8K SEGMENT SPACE MANAGEMENT MANUAL FLASHBACK ON;

alter session set "_ORACLE_SCRIPT"=true;  
create user CLOUDDB identified by Oracle123 default tablespace CLOUDDB;

GRANT CREATE SESSION TO CLOUDDB;

ALTER USER CLOUDDB DEFAULT TABLESPACE USERS;
ALTER USER CLOUDDB TEMPORARY TABLESPACE TEMP;
ALTER USER CLOUDDB QUOTA UNLIMITED ON USERS;
GRANT CREATE ANY TABLE TO CLOUDDB;
GRANT CREATE ANY INDEX TO CLOUDDB;
GRANT CREATE ANY SEQUENCE TO CLOUDDB;
GRANT CREATE ANY SYNONYM TO CLOUDDB;
GRANT CREATE ANY TRIGGER TO CLOUDDB;
GRANT CREATE ANY TYPE TO CLOUDDB;
GRANT CREATE ANY VIEW TO CLOUDDB;
GRANT CREATE ANY MATERIALIZED VIEW TO CLOUDDB;
GRANT CREATE ANY PROCEDURE TO CLOUDDB;
GRANT CREATE PUBLIC SYNONYM TO CLOUDDB;
~~~

Contributing
------------
Here is the info on how you can contribute in various ways to the project: https://flywaydb.org/documentation/contribute/


License
-------
Copyright (C) 2010-2020 Boxfuse GmbH

Flyway Community Edition    : https://flywaydb.org/licenses/flyway-community
Flyway 30 day limited trial : https://flywaydb.org/licenses/flyway-trial
Flyway Pro Edition          : https://flywaydb.org/licenses/flyway-pro
Flyway Enterprise Edition   : https://flywaydb.org/licenses/flyway-enterprise

Flyway is a registered trademark of Boxfuse GmbH.