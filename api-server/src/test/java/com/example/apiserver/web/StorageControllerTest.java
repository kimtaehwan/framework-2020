package com.example.apiserver.web;

import com.example.apiserver.core.StorageConfiguration;
import com.example.apiserver.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = StorageController.class)
public class StorageControllerTest {

    private static final String TEMP_FILE_PREFIX = "tmp";
    private static final String IMAGE_SUFFIX = ".jpg";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StorageService storageService;

    @MockBean
    private StorageConfiguration storageConfiguration;

    @Test
    public void uploadFile_test() throws Exception {
        // Given - 준비
        MockMultipartFile multipartFile = new MockMultipartFile("file", "filename.txt", "text/plain", "some jpg text".getBytes());

        given(storageConfiguration.getOnboardingImageLocation()).willReturn(Paths.get(StringUtils.EMPTY));
        given(storageService.store(any(Resource.class), any(Path.class))).willReturn("newFilename.txt");

        // When - 실행
        RequestBuilder request = MockMvcRequestBuilders.multipart("/images")
                .file(multipartFile)
                .contentType(MediaType.MULTIPART_FORM_DATA);

        ResultActions result = mvc.perform(request);

        // Then - 검증
        result.andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("newFilename.txt"))
                .andExpect(jsonPath("$.uri").value("/images/newFilename.txt"))
                .andExpect(jsonPath("$.type").value("text/plain"))
                .andExpect(jsonPath("$.size").value(13))
                .andExpect(jsonPath("$.originalName").value("filename.txt"))
                .andDo(print());
    }

    @Test
    public void downloadFile_test() throws Exception {

        // Given
        File tempFile = File.createTempFile(TEMP_FILE_PREFIX, IMAGE_SUFFIX);
        tempFile.deleteOnExit();

        Resource resource = new FileSystemResource(tempFile);
        given(storageConfiguration.getOnboardingImageLocation()).willReturn(Paths.get(StringUtils.EMPTY));
        given(storageService.loadAsResource(anyString(), any(Path.class))).willReturn(resource);

        // when
        RequestBuilder request = MockMvcRequestBuilders.get("/images/{name}", tempFile.getName());
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andDo(print());
    }
}