package com.example.apiserver.web;

import com.example.apiserver.MockingUtils;
import com.example.apiserver.assembler.FinancialInstitutionAssembler;
import com.example.apiserver.dto.FinancialInstitutionRequest;
import com.example.apiserver.entity.Country;
import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.service.CountryService;
import com.example.apiserver.service.FinancialInstitutionService;
import com.example.apiserver.vo.EmailAddress;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.MockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FinancialInstitutionController.class)
@AutoConfigureJsonTesters
public class FinancialInstitutionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CountryService countryService;

    @MockBean
    private FinancialInstitutionService financialInstitutionService;

    @MockBean
    private FinancialInstitutionAssembler financialInstitutionAssembler;

    @Test
    public void createFinancialInstitution_test() throws Exception {
        // Given
        FinancialInstitutionRequest.Create buildRequest = FinancialInstitutionRequest.Create.builder()
                .name("test FI")
                .sourceEmailAddress(EmailAddress.of("source@ondotsystems.com"))
                .token("testToken")
                .defaultHomeCountryId(1L)
                .build();

        Country unitedStates = Country.builder()
                .id(1L)
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        given(countryService.getCountry(any(Long.class))).willReturn(unitedStates);
        given(financialInstitutionService.create(any(FinancialInstitution.class))).willReturn(1L);

        // When
        RequestBuilder request = MockMvcRequestBuilders.post("/financialInstitutions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildRequest)); // buildRequest Class 를 Json 형식의 String 으로 변환.

        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isCreated())
                .andExpect(header().string("Location", "/financialInstitutions/1"))
                .andDo(print());
//        Assert.assertEquals("test FI", buildRequest.getName());
//        Assert.assertEquals("source1", buildRequest.getSourceEmailAddress().getId());
    }

    @Test
    public void listingFinancialInstitutions_test() throws Exception {
        // Given
        FinancialInstitution usOndotBank = FinancialInstitution.builder()
                .id(1L)
                .name("US Ondot Bank")
                .sourceEmailAddress(EmailAddress.of("source@ondotsystems.com"))
                .token("Ondot")
                .build();

        Page<FinancialInstitution> page = MockingUtils.getPaginationMock(usOndotBank);
    }

}
