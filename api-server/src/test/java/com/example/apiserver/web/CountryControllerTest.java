package com.example.apiserver.web;

import com.example.apiserver.assembler.CountryAssembler;
import com.example.apiserver.dto.CountryResponse;
import com.example.apiserver.entity.Country;
import com.example.apiserver.service.CountryService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CountryController.class)
public class CountryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CountryService countryService;

    @MockBean
    private CountryAssembler countryAssembler;

    @Test
    public void listingCountries_test() throws Exception {

        // Given
        Country unitedStates = Country.builder()
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        given(countryService.getCountries()).willReturn(Lists.list(unitedStates));

        CountryResponse.Listing unitedStatesResponse = CountryResponse.Listing.builder()
                .id(1L)
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        given(countryAssembler.convertToListing(anyList())).willReturn(Lists.list(unitedStatesResponse));

        // when
        ResultActions result = mvc.perform(get("/countries"));

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.[0].code").value("US"))
                .andExpect(jsonPath("$.[0].threeLetterCode").value("USA"))
                .andExpect(jsonPath("$.[0].phoneCode").value("US"))
                .andExpect(jsonPath("$.[0].label").value("United States"))
                .andDo(print());
    }

    @Test
    public void retrieveACountry_test() throws Exception {
        // Given
        Country koreaStates = Country.builder()
                .code("KR")
                .threeLetterCode("KOR")
                .phoneCode("KR")
                .label("Korea")
                .build();

        given(countryService.getCountry(1L)).willReturn(koreaStates);

        CountryResponse.Retrieve unitedStatesResponse = CountryResponse.Retrieve.builder()
                .id(1L)
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        given(countryAssembler.convertToRetrieve(any(Country.class))).willReturn(unitedStatesResponse);

        // when
        ResultActions result = mvc.perform(get("/countries/1"));

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.code").value("US"))
                .andExpect(jsonPath("$.threeLetterCode").value("USA"))
                .andExpect(jsonPath("$.phoneCode").value("US"))
                .andExpect(jsonPath("$.label").value("United States"))
                .andDo(print());
    }
}