package com.example.apiserver;

import org.assertj.core.util.Lists;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class MockingUtils {

    public static final int DEFAULT_PAGE = 0;
    public static final int DEFAULT_SIZE = 20;

    private MockingUtils() {

    }

    public static <T> Page getPaginationMock(T t) {
        Pageable pageable = PageRequest.of(0, 20);
        List<T> content = Lists.list(t);
        return new PageImpl(content, pageable, content.size());
    }

    public static Pageable getPageable() {
        return PageRequest.of(DEFAULT_PAGE, DEFAULT_SIZE);
    }
}
