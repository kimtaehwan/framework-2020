package com.example.apiserver;

import com.example.apiserver.entity.Country;
import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.vo.EmailAddress;

import javax.validation.constraints.Email;

public class EntityCollections {

    public static FinancialInstitution createOndotBank() {
        Country unitedStates = createUnitedStates();
        return FinancialInstitution.builder()
                .id(1L)
                .name("Ondot Bank")
                .sourceEmailAddress(EmailAddress.of("source@ondotsystems.com"))
                .token("ONDOT12345")
                .defaultHomeCountry(unitedStates)
                .build();
    }

    public static Country createUnitedStates() {
        return Country.builder()
                .id(1L)
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();
    }
}
