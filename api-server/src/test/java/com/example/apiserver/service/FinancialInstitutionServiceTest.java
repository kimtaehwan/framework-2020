package com.example.apiserver.service;

import com.example.apiserver.EntityCollections;
import com.example.apiserver.entity.Country;
import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.repository.FinancialInstitutionRepository;
import com.example.apiserver.vo.EmailAddress;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;


@Slf4j
public class FinancialInstitutionServiceTest {

    @InjectMocks
    private FinancialInstitutionService financialInstitutionService;

    @Mock
    private FinancialInstitutionRepository repository;

//    @Rule
//    public ExpectedException expectedException = ExpectedException.none();
//
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create_test() {
        Country unitedStates = EntityCollections.createUnitedStates();

        FinancialInstitution ondotBank = FinancialInstitution.builder()
                .id(1L)
                .name("Ondot Bank")
                .sourceEmailAddress(EmailAddress.of("source@ondotsystems.com"))
                .token("ONDOT12345")
                .defaultHomeCountry(unitedStates)
                .build();

        given(repository.existsByToken(anyString())).willReturn(false);
        given(repository.save(any(FinancialInstitution.class))).willReturn(ondotBank);

        Long actual = financialInstitutionService.create(ondotBank);

        assertThat(actual).isEqualTo(1L);
    }
}