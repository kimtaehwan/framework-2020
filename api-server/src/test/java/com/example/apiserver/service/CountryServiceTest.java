package com.example.apiserver.service;

import com.example.apiserver.entity.Country;
import com.example.apiserver.repository.CountryRepository;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@Slf4j
public class CountryServiceTest {

    @InjectMocks
    private CountryService countryService;

    @Mock
    private CountryRepository countryRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCountry_test() {
        Country us = Country.builder()
                .code("KR")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        given(countryRepository.findById(1L)).willReturn(Optional.of(us));

        Country actual = countryService.getCountry(1L);

        assertThat(actual).isEqualTo(us);
    }

    @Test
    public void getCountries_test() {
        Country unitedStates = Country.builder()
                .code("US")
                .threeLetterCode("USA")
                .phoneCode("US")
                .label("United States")
                .build();

        Country southKorea = Country.builder()
                .code("KR")
                .threeLetterCode("KOR")
                .phoneCode("KR")
                .label("South Korea")
                .build();

        given(countryRepository.findAll()).willReturn(Lists.newArrayList(unitedStates, southKorea));

        List<Country> actual = countryService.getCountries();

        assertThat(actual).isNotNull();
        assertThat(actual.size()).isEqualTo(2);
        assertThat(actual).contains(unitedStates, southKorea);
    }
}