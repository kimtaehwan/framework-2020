package com.example.apiserver.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Java6Assertions.assertThat;

@Slf4j
public class FileStorageServiceTest {

    @InjectMocks
    private FileStorageService fileStorageService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void store_test() throws Exception {
        File tempFile = File.createTempFile("tmp", ".html");
        tempFile.deleteOnExit();

        Resource resource = new FileSystemResource(tempFile);

        String newFilename = fileStorageService.store(resource, Paths.get(Strings.EMPTY));

        assertThat(newFilename).isNotNull();
        assertThat(FilenameUtils.getBaseName(newFilename).length()).isEqualTo(50);

        Files.delete(Paths.get(newFilename));
    }

    @Test
    public void load_test() throws Exception {
        File tempFile = File.createTempFile("tmp", ".html");
        tempFile.deleteOnExit();

        String filename = FilenameUtils.getName(tempFile.getAbsolutePath());
        String location = FilenameUtils.getFullPath(tempFile.getAbsolutePath());

        Resource resource = fileStorageService.loadAsResource(filename, Paths.get(location));

        assertThat(resource).isNotNull();
        assertThat(resource.getFile()).isEqualTo(tempFile);
    }
}