package com.example.apiserver.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TermsFile {
    private String name;
    private String path;
    private String type;
    private long size;
    private String originalName;

    @Builder(toBuilder = true)
    public TermsFile(String name, String path, String type, long size, String originalName) {
        this.originalName = originalName;
        this.name = name;
        this.path = path;
        this.type = type;
        this.size = size;
    }
}
