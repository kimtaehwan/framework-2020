package com.example.apiserver.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "COUNTRY")
public class Country implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUNTRY_SEQ_GENERATOR")
    @SequenceGenerator(name = "COUNTRY_SEQ_GENERATOR", allocationSize = 1, sequenceName = "COUNTRY_SEQ")
    @Column(name = "COUNTRY_ID", nullable = false)
    private Long id;

    @Column(name = "COUNTRY_CODE", nullable = false)
    private String code;

    @Column(name = "THREE_LETTER_COUNTRY_CODE", nullable = false)
    private String threeLetterCode;

    @Column(name = "PHONE_CODE", nullable = false)
    private String phoneCode;

    @Column(name = "DISPLAY_LABEL", nullable = false)
    private String label;

    @CreatedDate
    @Column(name = "CREATED", nullable = false, updatable = false)
    private OffsetDateTime created;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    @Builder
    public Country(Long id, String code, String threeLetterCode, String phoneCode, String label) {
        this.id = id;
        this.code = code;
        this.threeLetterCode = threeLetterCode;
        this.phoneCode = phoneCode;
        this.label = label;
        this.created = OffsetDateTime.now();
    }
}
