package com.example.apiserver.entity;

import com.example.apiserver.dto.FinancialInstitutionRequest;
import com.example.apiserver.vo.EmailAddress;
import com.example.apiserver.vo.EmailAddressConverter;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "FINANCIAL_INSTITUTION")
public class FinancialInstitution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FINANCIAL_INSTITUTION_SEQ_GENERATOR")
    @SequenceGenerator(name = "FINANCIAL_INSTITUTION_SEQ_GENERATOR", allocationSize = 1, sequenceName = "FINANCIAL_INSTITUTION_SEQ")
    @Column(name = "FI_ID", nullable = false)
    private Long id;

    @Column(name = "FI_NAME", nullable = false)
    private String name;

    @Convert(converter = EmailAddressConverter.class)
    @Column(name = "CONTACT_EMAIL_ADDRESS")
    private EmailAddress contactEmailAddress;

    @Column(name = "CONTACT_PHONE_NUMBER")
    private String contactPhoneNumber;

    @Convert(converter = EmailAddressConverter.class)
    @Column(name = "BILLING_EMAIL_ADDRESS")
    private EmailAddress billingEmailAddress;

    @Convert(converter = EmailAddressConverter.class)
    @Column(name = "SOURCE_EMAIL_ADDRESS", nullable = false)
    private EmailAddress sourceEmailAddress;

    @Column(name = "FI_TOKEN", nullable = false)
    private String token;

    @Column(name = "FI_TOKEN_ALIAS1")
    private String tokenAlias1;

    @Column(name = "FI_TOKEN_ALIAS2")
    private String tokenAlias2;

    @ManyToOne
    @JoinColumn(name = "DEFAULT_HOME_COUNTRY_ID", nullable = false)
    private Country defaultHomeCountry;

    @CreatedDate
    @Column(name = "CREATED", nullable = false, updatable = false)
    private OffsetDateTime created;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    @Column(name = "DELETED")
    private ZonedDateTime deleted;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private Status status;

    @Builder
    @JsonCreator
    public FinancialInstitution(Long id, String name, EmailAddress contactEmailAddress, String contactPhoneNumber, EmailAddress billingEmailAddress, EmailAddress sourceEmailAddress, String token, String tokenAlias1, String tokenAlias2, Country defaultHomeCountry) {
        this.id = id;
        this.name = name;
        this.contactEmailAddress = contactEmailAddress;
        this.contactPhoneNumber = contactPhoneNumber;
        this.billingEmailAddress = billingEmailAddress;
        this.sourceEmailAddress = sourceEmailAddress;
        this.token = token;
        this.tokenAlias1 = tokenAlias1;
        this.tokenAlias2 = tokenAlias2;
        this.defaultHomeCountry = defaultHomeCountry;
        this.created = OffsetDateTime.now();
        this.status = Status.ENABLED;
    }

    public FinancialInstitution delete() {
        if (this.status == Status.DELETED)
            throw new IllegalStateException();

        this.status = Status.DELETED;
        this.deleted = ZonedDateTime.now();

        return this;
    }

    public FinancialInstitution enable() {
        if(this.status == Status.ENABLED)
            throw new IllegalStateException();

        this.status = Status.ENABLED;
        this.lastModified = OffsetDateTime.now();

        return this;
    }

    public FinancialInstitution disable() {
        if(this.status == Status.DISABLED)
            throw new IllegalStateException();

        this.status = Status.DISABLED;
        this.lastModified = OffsetDateTime.now();

        return this;
    }

    public FinancialInstitution modify(FinancialInstitutionRequest.ModifyReq req, Country defaultHomeCountry) {
        this.name = req.getName();
        this.contactEmailAddress = req.getContactEmailAddress();
        this.contactPhoneNumber = req.getContactPhoneNumber();
        this.billingEmailAddress = req.getBillingEmailAddress();
        this.sourceEmailAddress = req.getSourceEmailAddress();
        this.tokenAlias1 = req.getTokenAlias1();
        this.tokenAlias2 = req.getTokenAlias2();
        this.defaultHomeCountry = defaultHomeCountry;
        this.lastModified = OffsetDateTime.now();
        return this;
    }

    public enum Status {
        ENABLED, DISABLED, DELETED
    }
}
