package com.example.apiserver.entity;

import com.example.apiserver.vo.ImageFileConverter;
import com.example.apiserver.vo.ProductType;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Getter
@ToString
@Entity(name = "PRODUCT")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_SEQ_GENERATOR")
    @SequenceGenerator(name = "PRODUCT_SEQ_GENERATOR", allocationSize = 1, sequenceName = "PRODUCT_SEQ")
    @Column(name = "PRODUCT_ID", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FI_TOKEN", nullable = false, referencedColumnName = "FI_TOKEN")
    private FinancialInstitution financialInstitution;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRODUCT_TYPE", nullable = false)
    private ProductType type;

    @Column(name = "PRODUCT_NAME", nullable = false)
    private String name;

    @Column(name = "IMAGE_URI", nullable = false)
    @Convert(converter = ImageFileConverter.class)
    private ImageFile image;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @CreatedDate
    @Column(name = "CREATED", nullable = false, updatable = false)
    private OffsetDateTime created;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "CREATOR")
    private String creator;

    @Column(name = "CONTENT_TITLE")
    private String contentTitle;

    @Builder
    public Product(Long id, String name, ImageFile image, String description, String contentTitle, FinancialInstitution financialInstitution) {
        this.id = id;
        this.financialInstitution = financialInstitution;
        this.type = ProductType.CREDIT_CARD;
        this.name = name;
        this.image = image;
        this.description = description;
        this.created = OffsetDateTime.now(ZoneOffset.UTC);
        this.status = Status.PUBLISHED;
        this.creator = "Patricia Knight";
        this.contentTitle = contentTitle;
    }

    public enum Status {
        PUBLISHED
    }
}
