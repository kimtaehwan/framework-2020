package com.example.apiserver.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Getter
@ToString(exclude = {"financialInstitution", "journeyService"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "JOURNEYS")
public class Journey {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOURNEYS_SEQ_GENERATOR")
    @SequenceGenerator(name = "JOURNEYS_SEQ_GENERATOR", allocationSize = 1, sequenceName = "JOURNEYS_SEQ")
    @Column(name = "ID", nullable = false)
    private Long id;
}
