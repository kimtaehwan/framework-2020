package com.example.apiserver.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ImageFile {
    private String name;
    private String uri;
    private String type;
    private Long size;
    private String originalName;

    @Builder
    public ImageFile(String name, String uri, String type, Long size, String originalName) {
        this.name = name;
        this.uri = uri;
        this.type = type;
        this.size = size;
        this.originalName = originalName;
    }
}
