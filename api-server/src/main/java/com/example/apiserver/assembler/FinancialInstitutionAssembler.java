package com.example.apiserver.assembler;

import com.example.apiserver.dto.FinancialInstitutionResponse;
import com.example.apiserver.entity.FinancialInstitution;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FinancialInstitutionAssembler {

    final CountryAssembler countryAssembler;

    public FinancialInstitutionResponse.Listing convertToListing(FinancialInstitution financialInstitution) {
        return FinancialInstitutionResponse.Listing.builder()
                .id(financialInstitution.getId())
                .name(financialInstitution.getName())
                .token(financialInstitution.getToken())
                .created(financialInstitution.getCreated())
                .status(financialInstitution.getStatus().toString())
                .build();
    }

    public Page<FinancialInstitutionResponse.Listing> convertToListing(Page<FinancialInstitution> page) {
        return page.map(this::convertToListing);
    }

    public FinancialInstitutionResponse.Include convertToInclude(FinancialInstitution financialInstitution) {
        return FinancialInstitutionResponse.Include.builder()
                .id(financialInstitution.getId())
                .name(financialInstitution.getName())
                .token(financialInstitution.getToken())
                .build();
    }

    public FinancialInstitutionResponse.Retrieve convertToRetrieve(FinancialInstitution financialInstitution) {
        return FinancialInstitutionResponse.Retrieve.builder()
                .id(financialInstitution.getId())
                .name(financialInstitution.getName())
                .token(financialInstitution.getToken())
                .billingEmailAddress(financialInstitution.getBillingEmailAddress().getValue())
                .contactEmailAddress(financialInstitution.getContactEmailAddress().getValue())
                .contactPhoneNumber(financialInstitution.getContactPhoneNumber())
                .sourceEmailAddress(financialInstitution.getSourceEmailAddress().getValue())
                .created(financialInstitution.getCreated())
                .lastModified(financialInstitution.getLastModified())
                .status(financialInstitution.getStatus().toString())
                .defaultHomeCountry(countryAssembler.convertToListing(financialInstitution.getDefaultHomeCountry()))
                .build();
    }
}
