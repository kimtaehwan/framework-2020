package com.example.apiserver.assembler;

import com.example.apiserver.dto.ProductResponse;
import com.example.apiserver.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductAssembler {

    final FinancialInstitutionAssembler financialInstitutionAssembler;

    public Page<ProductResponse.Listing> convertToListing(Page<Product> products) {
        return products.map(this::convertToListing);
    }

    public ProductResponse.Listing convertToListing(Product product) {
        return ProductResponse.Listing.builder()
                .id(product.getId())
                .contentTitle(product.getContentTitle())
                .created(product.getCreated())
                .creator(product.getCreator())
                .status(product.getStatus().toString())
                .financialInstitution(financialInstitutionAssembler.convertToInclude(product.getFinancialInstitution()))
                .build();
    }


}
