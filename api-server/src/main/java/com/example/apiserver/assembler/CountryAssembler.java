package com.example.apiserver.assembler;

import com.example.apiserver.dto.CountryResponse;
import com.example.apiserver.entity.Country;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CountryAssembler {

    public CountryResponse.Listing convertToListing(Country country) {
        return CountryResponse.Listing.builder()
                .id(country.getId())
                .code(country.getCode())
                .label(country.getLabel())
                .phoneCode(country.getPhoneCode())
                .threeLetterCode(country.getThreeLetterCode())
                .build();
    }

    public List<CountryResponse.Listing> convertToListing(List<Country> countries) {
        if (countries == null) {
            return Collections.emptyList();
        }

        return countries.stream()
                .map(this::convertToListing)
                .collect(Collectors.toList());
    }

    public CountryResponse.Retrieve convertToRetrieve(Country country) {
        return CountryResponse.Retrieve.builder()
                .id(country.getId())
                .code(country.getCode())
                .label(country.getLabel())
                .phoneCode(country.getPhoneCode())
                .threeLetterCode(country.getThreeLetterCode())
                .created(country.getCreated())
                .lastModified(country.getLastModified())
                .build();
    }
}
