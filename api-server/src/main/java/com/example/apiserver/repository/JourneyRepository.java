package com.example.apiserver.repository;

import com.example.apiserver.entity.Journey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Clob;

@Repository
public interface JourneyRepository extends JpaRepository<Journey, Long> {

    @Query(value = CONFIG_SQL, nativeQuery = true)
    Clob findAllJourneyConfig();

    String CONFIG_SQL = "SELECT JSON_OBJECT(\n" +
            "    'services' VALUE JSON_ARRAYAGG(\n" +
            "        JSON_OBJECT(\n" +
            "            'id' VALUE SVC.ID,\n" +
            "            'name' VALUE SVC.NAME,\n" +
            "            'type' VALUE SVC.TYPE,\n" +
            "            'position' VALUE SVC.POSITION,\n" +
            "            'bitPosition' VALUE SVC.BIT_POSITION,\n" +
            "            'vendors' VALUE (\n" +
            "                SELECT JSON_ARRAYAGG(\n" +
            "                    JSON_OBJECT(\n" +
            "                        'name' VALUE SVC_VDR.NAME,\n" +
            "                        'type' VALUE SVC_VDR.TYPE,\n" +
            "                        'position' VALUE SVC_VDR.POSITION,\n" +
            "                        'serviceVendorFields' VALUE (\n" +
            "                            SELECT JSON_ARRAYAGG(\n" +
            "                                JSON_OBJECT(\n" +
            "                                    'name' VALUE SVC_VDR_FLD.NAME,\n" +
            "                                    'key' VALUE SVC_VDR_FLD.KEY,\n" +
            "                                    'position' VALUE SVC_VDR_FLD.POSITION,\n" +
            "                                    'columns' VALUE SVC_VDR_FLD.COLUMNS,\n" +
            "                                    'serviceFields' VALUE (\n" +
            "                                        SELECT JSON_ARRAYAGG(\n" +
            "                                            JSON_OBJECT(\n" +
            "                                                'id' VALUE SVC_FLD.ID,\n" +
            "                                                'key' VALUE SVC_FLD.KEY,\n" +
            "                                                'name' VALUE SVC_FLD.NAME,\n" +
            "                                                'fieldFormat' VALUE SVC_FLD.FIELD_FORMAT,\n" +
            "                                                'possibleValues' VALUE (\n" +
            "                                                            SELECT JSON_ARRAYAGG(DECODE(DATA, CHR(10), DATA, REGEXP_REPLACE(DATA, '['||CHR(10)||'|'']', '')))\n" +
            "                                                            FROM (\n" +
            "                                                                SELECT REGEXP_SUBSTR((SELECT POSSIBLE_VALUES FROM SERVICE_FIELDS WHERE ID = SVC_FLD.ID), '[^- ]+', 1, ROWNUM) DATA\n" +
            "                                                                FROM DUAL\n" +
            "                                                                CONNECT BY ROWNUM <= LENGTH(REGEXP_REPLACE((SELECT POSSIBLE_VALUES FROM SERVICE_FIELDS WHERE ID = SVC_FLD.ID), '[^- ]+')) + 1\n" +
            "                                                            )\n" +
            "                                                            WHERE DATA IS NOT NULL\n" +
            "                                                        ),\n" +
            "                                                'regExp' VALUE SVC_FLD.REGEXP,\n" +
            "                                                'minLen' VALUE SVC_FLD.MIN_LEN,\n" +
            "                                                'maxLen' VALUE SVC_FLD.MAX_LEN,\n" +
            "                                                'required' VALUE SVC_FLD.IS_REQUIRED,\n" +
            "                                                'position' VALUE SVC_FLD.POSITION,\n" +
            "                                                'defaultValue' VALUE SVC_FLD.DEFAULT_VALUE,\n" +
            "                                                'addable' VALUE SVC_FLD.IS_ADDABLE,\n" +
            "                                                'visible' VALUE SVC_FLD.IS_VISIBLE,\n" +
            "                                                'multipleOption' VALUE SVC_FLD.IS_MULTIPLE_CHOICE,\n" +
            "                                                'formatStore' VALUE SVC_FLD.FORMAT_STORE,\n" +
            "                                                'description' VALUE SVC_FLD.DESCRIPTION,\n" +
            "                                                'serviceFieldEnumerations' VALUE (\n" +
            "                                                    SELECT JSON_ARRAYAGG(\n" +
            "                                                        JSON_OBJECT(\n" +
            "                                                            'name' VALUE SVC_FLD_ENUM.NAME,\n" +
            "                                                            'value' VALUE SVC_FLD_ENUM.ID,\n" +
            "                                                            'position' VALUE SVC_FLD_ENUM.POSITION\n" +
            "                                                       ABSENT ON NULL RETURNING CLOB)\n" +
            "                                                    ORDER BY SVC_FLD_ENUM.POSITION RETURNING CLOB)\n" +
            "                                                    FROM SERVICE_FIELD_ENUMERATIONS SVC_FLD_ENUM\n" +
            "                                                    WHERE SVC_FLD_ENUM.IS_ACTIVE = '1'\n" +
            "                                                    AND SVC_FLD_ENUM.SERVICE_FIELD_ID = SVC_FLD.ID\n" +
            "                                                )\n" +
            "                                            ABSENT ON NULL RETURNING CLOB)\n" +
            "                                        ORDER BY SVC_FLD.POSITION RETURNING CLOB)\n" +
            "                                        FROM SERVICE_FIELDS SVC_FLD\n" +
            "                                        WHERE SVC_FLD.SERVICE_VENDOR_FIELD_ID = SVC_VDR_FLD.ID\n" +
            "                                    )\n" +
            "                                ABSENT ON NULL RETURNING CLOB)\n" +
            "                            ORDER BY SVC_VDR_FLD.POSITION RETURNING CLOB)\n" +
            "                            FROM SERVICE_VENDOR_FIELDS SVC_VDR_FLD\n" +
            "                            WHERE SVC_VDR_FLD.SERVICE_VENDOR_ID = SVC_VDR.ID\n" +
            "                            AND SVC_VDR_FLD.SERVICE_ID = SVC.ID\n" +
            "                        )\n" +
            "                    ABSENT ON NULL RETURNING CLOB)\n" +
            "                RETURNING CLOB)\n" +
            "                FROM SERVICE_VENDORS SVC_VDR\n" +
            "                WHERE REGEXP_SUBSTR(SVC_VDR.SUPPORT_SERVICES, '[^,]+', 1, SVC.BIT_POSITION) = 1\n" +
            "            )\n" +
            "        ABSENT ON NULL RETURNING CLOB)\n" +
            "    ORDER BY SVC.POSITION RETURNING CLOB)\n" +
            "ABSENT ON NULL RETURNING CLOB) AS RESULT\n" +
            "FROM SERVICES SVC\n" +
            "WHERE SVC.STATUS = '1'";
}
