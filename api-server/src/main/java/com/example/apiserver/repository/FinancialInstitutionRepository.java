package com.example.apiserver.repository;

import com.example.apiserver.entity.FinancialInstitution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialInstitutionRepository extends JpaRepository<FinancialInstitution, Long> {

    boolean existsByToken(String token);

    Page<FinancialInstitution> findByStatusAndNameContainingIgnoreCase(Pageable pageable, FinancialInstitution.Status status, String q);

    Page<FinancialInstitution> findByStatus(Pageable pageable, FinancialInstitution.Status status);
}
