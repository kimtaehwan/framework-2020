package com.example.apiserver.dto;

import com.example.apiserver.entity.Country;
import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.vo.EmailAddress;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class FinancialInstitutionRequest {

    private FinancialInstitutionRequest() {}

    @Getter
    @Builder
    @ToString
    public static class Create {

        @NotBlank
        private String name;

        private EmailAddress contactEmailAddress;

        private String contactPhoneNumber;

        private EmailAddress billingEmailAddress;

        @Valid
        private EmailAddress sourceEmailAddress;

        @NotBlank
        private String token;

        private String tokenAlias1;

        private String tokenAlias2;

        @NotNull
        private Long defaultHomeCountryId;

        public FinancialInstitution toEntity(Country defaultHomeCountry) {
            return FinancialInstitution.builder()
                    .name(name)
                    .contactEmailAddress(contactEmailAddress)
                    .contactPhoneNumber(contactPhoneNumber)
                    .billingEmailAddress(billingEmailAddress)
                    .sourceEmailAddress(sourceEmailAddress)
                    .token(token)
                    .tokenAlias1(tokenAlias1)
                    .tokenAlias2(tokenAlias2)
                    .defaultHomeCountry(defaultHomeCountry)
                    .build();
        }

    }

    @Setter
    @Getter
    @Builder
    @ToString
    public static class ModifyReq {
        @NotBlank
        private String name;

        @Valid
        private EmailAddress contactEmailAddress;

        private String contactPhoneNumber;

        @Valid
        private EmailAddress billingEmailAddress;

        @Valid
        private EmailAddress sourceEmailAddress;

        private String tokenAlias1;

        private String tokenAlias2;

        @NotNull
        private Long defaultHomeCountryId;
    }
}
