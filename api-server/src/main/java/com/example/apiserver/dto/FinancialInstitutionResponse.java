package com.example.apiserver.dto;

import lombok.*;

import java.time.OffsetDateTime;
import java.util.List;

public class FinancialInstitutionResponse {

    private FinancialInstitutionResponse() {
    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Listing {
        private Long id;
        private String name;
        private String token;
        private OffsetDateTime created;
        private String status;
    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Include {
        private Long id;
        private String name;
        private String token;
    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Retrieve {
        private Long id;
        private String name;
        private String contactEmailAddress;
        private String contactPhoneNumber;
        private String billingEmailAddress;
        private String sourceEmailAddress;
        private String token;
        private String tokenAlias1;
        private String tokenAlias2;
        private CountryResponse.Listing defaultHomeCountry;
        private OffsetDateTime created;
        private OffsetDateTime lastModified;
        private String status;
        private List<Metadata> metadataList;
    }

    @Getter
    @ToString
    @AllArgsConstructor(staticName = "of")
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Metadata {
        private String key;
        private String value;
        private String description;
    }
}
