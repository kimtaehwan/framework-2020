package com.example.apiserver.dto;

import lombok.*;

import java.time.OffsetDateTime;

public class ProductResponse {

    private ProductResponse() {

    }

    @Getter
    @Builder
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Listing {
        private Long id;
        private String contentTitle;
        private FinancialInstitutionResponse.Include financialInstitution;
        private OffsetDateTime created;
        private String creator;
        private String status;
    }
}
