package com.example.apiserver.dto;

import lombok.*;

import java.time.OffsetDateTime;

public class CountryResponse {

    private CountryResponse() {

    }

    @Getter
    @ToString
    @Builder
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Listing {
        private Long id;
        private String code;
        private String threeLetterCode;
        private String phoneCode;
        private String label;
    }

    @Getter
    @ToString
    @Builder
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Retrieve {
        private Long id;
        private String code;
        private String threeLetterCode;
        private String phoneCode;
        private String label;
        private OffsetDateTime created;
        private OffsetDateTime lastModified;
    }

}
