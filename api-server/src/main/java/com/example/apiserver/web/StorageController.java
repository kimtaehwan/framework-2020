package com.example.apiserver.web;

import com.example.apiserver.core.StorageConfiguration;
import com.example.apiserver.entity.ImageFile;
import com.example.apiserver.entity.TermsFile;
import com.example.apiserver.service.StorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;

@Slf4j
@RestController
@RequiredArgsConstructor
public class StorageController {

    final StorageService storageService;
    final StorageConfiguration storageConfiguration;

    @PostMapping("/images")
    public ResponseEntity uploadFile(@RequestParam(name = "file") MultipartFile multipartFile) {
        String fileName = storageService.store(multipartFile.getResource(), storageConfiguration.getOnboardingImageLocation());
        return this.getImageResponseEntity(fileName, multipartFile);
    }

    @GetMapping("/images/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = storageService.loadAsResource(fileName, storageConfiguration.getOnboardingImageLocation());
        String contentType = this.getContentType(request, resource);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private String getContentType(HttpServletRequest request, Resource resource) {
        String defaultContentType = "application/octet-stream";

        try {
            String absolutePath = resource.getFile().getAbsolutePath();
            String mimeType = request.getServletContext().getMimeType(absolutePath);

            return StringUtils.defaultIfBlank(mimeType, defaultContentType);
        } catch (IOException ex) {
            log.info("Could not determine file type.");
            return defaultContentType;
        }
    }

    private ResponseEntity getImageResponseEntity(String fileName, MultipartFile multipartFile) {
        URI imageDownloadUri = ServletUriComponentsBuilder.newInstance()
                .path("/images/")
                .path(fileName)
                .build()
                .toUri();

        ImageFile imageFile = ImageFile.builder()
                .name(fileName)
                .uri(imageDownloadUri.toString())
                .type(multipartFile.getContentType())
                .size(multipartFile.getSize())
                .originalName(multipartFile.getOriginalFilename())
                .build();

        return ResponseEntity.created(imageDownloadUri).body(imageFile);
    }

    @PostMapping("/files")
    public ResponseEntity uploadHtml(@RequestParam(name = "file") MultipartFile multipartFile) {
        String fileName = storageService.store(multipartFile.getResource(), storageConfiguration.getTermsFileLocation());

        URI htmlDownloadUri = ServletUriComponentsBuilder.newInstance()
                .path(storageConfiguration.getTermsFileLocation().toString())
                .path("/")
                .path(fileName)
                .build()
                .toUri();

        TermsFile imageFile = TermsFile.builder()
                .name(fileName)
                .path(htmlDownloadUri.toString())
                .type(multipartFile.getContentType())
                .size(multipartFile.getSize())
                .originalName(multipartFile.getOriginalFilename())
                .build();

        return ResponseEntity.created(htmlDownloadUri).body(imageFile);
    }

}
