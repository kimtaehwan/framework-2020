package com.example.apiserver.web;

import com.example.apiserver.assembler.ProductAssembler;
import com.example.apiserver.entity.Product;
import com.example.apiserver.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ProductController {

    final ProductService productService;
    final ProductAssembler productAssembler;

    @GetMapping(value = "/products")
    public ResponseEntity listingProduct(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
            @RequestParam(value = "title", defaultValue = StringUtils.EMPTY) String title) {
        Page<Product> products = productService.getProducts(pageable, title);
        return ResponseEntity.ok(productAssembler.convertToListing(products));
    }
}
