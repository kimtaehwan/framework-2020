package com.example.apiserver.web;

import com.example.apiserver.assembler.CountryAssembler;
import com.example.apiserver.entity.Country;
import com.example.apiserver.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CountryController {

    final CountryService countryService;
    final CountryAssembler countryAssembler;

    @GetMapping(value = "/countries")
    public ResponseEntity listingCountries() {
        List<Country> countries = countryService.getCountries();
        return ResponseEntity.ok(countryAssembler.convertToListing(countries));
    }

    @GetMapping(value = "/countries/{id}")
    public ResponseEntity retrieveACountry(@PathVariable("id") Long id) {
        Country country = countryService.getCountry(id);
        return ResponseEntity.ok(countryAssembler.convertToRetrieve(country));
    }

}
