package com.example.apiserver.web;

import com.example.apiserver.assembler.FinancialInstitutionAssembler;
import com.example.apiserver.dto.FinancialInstitutionRequest;
import com.example.apiserver.entity.Country;
import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.service.CountryService;
import com.example.apiserver.service.FinancialInstitutionService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequiredArgsConstructor
public class FinancialInstitutionController {

    final CountryService countryService;
    final FinancialInstitutionService financialInstitutionService;
    final FinancialInstitutionAssembler financialInstitutionAssembler;

    @GetMapping(value = "/financialInstitutions")
    public ResponseEntity listingFinancialInstitutions(@PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
                                                       @RequestParam(value = "q", defaultValue = StringUtils.EMPTY) String q) {
        Page<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions(pageable, q);
        return ResponseEntity.ok(financialInstitutionAssembler.convertToListing(financialInstitutions));
    }

    @PostMapping(value = "/financialInstitutions", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createFinancialInstitution(@RequestBody @Valid FinancialInstitutionRequest.Create req) {
        Country defaultHomeCountry = countryService.getCountry(req.getDefaultHomeCountryId());
        FinancialInstitution financialInstitution = req.toEntity(defaultHomeCountry);

        Long id = financialInstitutionService.create(financialInstitution);
        URI financialInstitutionUri = buildCreatedUri(id);

        return ResponseEntity.created(financialInstitutionUri).build();
    }

    @GetMapping(value = "/financialInstitutions/{id}")
    public ResponseEntity retrieveAFinancialInstitution(@PathVariable(value = "id") Long id) {
        FinancialInstitution financialInstitution = financialInstitutionService.getFinancialInstitution(id);
        return ResponseEntity.ok(financialInstitutionAssembler.convertToRetrieve(financialInstitution));
    }

    private URI buildCreatedUri(Long id) {
        return UriComponentsBuilder.newInstance()
                .path("/financialInstitutions/{id}")
                .build(id);
    }
}
