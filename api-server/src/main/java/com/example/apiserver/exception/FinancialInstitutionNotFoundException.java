package com.example.apiserver.exception;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FinancialInstitutionNotFoundException extends RuntimeException {
    private Long id;
    private String token;

    public FinancialInstitutionNotFoundException(Long id) {
        this.id = id;
    }

    public FinancialInstitutionNotFoundException(Long id, String token) {
        this.id = id;
        this.token = token;
    }
}
