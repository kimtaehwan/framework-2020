package com.example.apiserver.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DuplicateTokenException extends RuntimeException {
    private final String token;
}
