package com.example.apiserver.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class CountryNotFoundException extends RuntimeException {
    private final Long id;
}
