package com.example.apiserver.vo;

public enum ProductType {
    CREDIT_CARD,
    DEBIT_CARD,
    LOAN;
}
