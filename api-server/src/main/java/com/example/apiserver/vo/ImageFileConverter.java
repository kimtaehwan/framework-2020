package com.example.apiserver.vo;

import com.example.apiserver.entity.ImageFile;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ImageFileConverter implements AttributeConverter<ImageFile, String> {
    @Override
    public String convertToDatabaseColumn(ImageFile attribute) {
        return attribute == null ? null : attribute.getUri();
    }

    @Override
    public ImageFile convertToEntityAttribute(String dbData) {
        return ImageFile.builder()
                .uri(dbData)
                .size(0L)
                .name("")
                .build();
    }
}
