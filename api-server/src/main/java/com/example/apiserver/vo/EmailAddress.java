package com.example.apiserver.vo;

import lombok.Value;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Email;
import java.io.Serializable;

@Value(staticConstructor = "of")
public class EmailAddress implements Serializable {

    public static final String AT = "@";

    @Email
    private String value;

    public String getId() {
        return StringUtils.split(this.value, AT)[0];
    }

    public String getDomain() {
        return StringUtils.split(this.value, AT)[1];
    }
}
