package com.example.apiserver.vo;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class EmailAddressJsonComponent {
    private EmailAddressJsonComponent() {

    }

    public static class EmailAddressJsonSerializer extends JsonSerializer<EmailAddress> {
        @Override
        public void serialize(EmailAddress emailAddress, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(emailAddress.getValue());
        }
    }

    public static class EmailAddressJsonDeserializer extends JsonDeserializer<EmailAddress> {
        @Override
        public EmailAddress deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            final String valueAsString = jsonParser.getValueAsString();
            return EmailAddress.of(valueAsString);
        }
    }
}
