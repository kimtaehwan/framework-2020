package com.example.apiserver.vo;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EmailAddressConverter implements AttributeConverter<EmailAddress, String> {
    @Override
    public String convertToDatabaseColumn(EmailAddress attribute) {
        return attribute == null ? null : attribute.getValue();
    }

    @Override
    public EmailAddress convertToEntityAttribute(String dbData) {
        return StringUtils.isEmpty(dbData) ? EmailAddress.of("") : EmailAddress.of(dbData);
    }
}
