package com.example.apiserver.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.sql.Clob;

@Slf4j
public class ClobUtil {

    public static String clobToStr(Clob clob) {
        StringBuilder out = new StringBuilder();

        try {
            BufferedReader contentReader = new BufferedReader(clob.getCharacterStream());
            String aux;

            while ((aux = contentReader.readLine()) != null) {
                out.append(aux);
            }
        } catch (Exception e) {
            log.error("{}", e);
        }

        return out.toString();
    }
}
