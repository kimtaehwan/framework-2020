package com.example.apiserver.service;

import com.example.apiserver.entity.Product;
import com.example.apiserver.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {

    final ProductRepository productRepository;

    public Page<Product> getProducts(Pageable pageable, String title) {
        return productRepository.findAllByContentTitleContainingIgnoreCase(pageable, title);
    }
}
