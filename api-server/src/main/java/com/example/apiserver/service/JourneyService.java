package com.example.apiserver.service;

import com.example.apiserver.repository.JourneyRepository;
import com.example.apiserver.utils.ClobUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Clob;

@Slf4j
@Service
@RequiredArgsConstructor
public class JourneyService {

    final JourneyRepository journeyRepository;

    public String getJourneyConfig() {
        Clob allJourneyConfig = journeyRepository.findAllJourneyConfig();
        return ClobUtil.clobToStr(allJourneyConfig);
    }


}
