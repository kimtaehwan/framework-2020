package com.example.apiserver.service;

import com.example.apiserver.entity.Country;
import com.example.apiserver.exception.CountryNotFoundException;
import com.example.apiserver.repository.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CountryService {

    final CountryRepository countryRepository;

    public Country getCountry(Long id) {
        return countryRepository.findById(id)
                .orElseThrow( () -> new CountryNotFoundException(id));
    }

    public List<Country> getCountries() {
        return countryRepository.findAll();
    }
}
