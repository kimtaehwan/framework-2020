package com.example.apiserver.service;

import com.example.apiserver.exception.FileStorageException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService implements StorageService {

    public static final String DELIMITER = ".";
    public static final int RANDOM_ALPHANUMERIC_COUNT = 50;

    @Override
    public String store(Resource resource, Path storageLocation) {
        String targetFilename = createTargetFilename(resource.getFilename());
        Path targetLocation = storageLocation.resolve(targetFilename);

        try {
            Files.copy(resource.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + targetFilename + ". Please try again!", ex);
        }

        return targetFilename;
    }

    private String createTargetFilename(String originalFilename) {
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(RANDOM_ALPHANUMERIC_COUNT);
        String extension = FilenameUtils.getExtension(originalFilename);
        return String.join(DELIMITER, randomAlphanumeric, extension);
    }

    @Override
    public Resource loadAsResource(String filename, Path storageLocation) {
        Path path = storageLocation.resolve(filename).normalize();

        try {
            Resource resource = new UrlResource(path.toUri());

            if (!resource.exists()) {
                throw new FileStorageException("File not found " + filename);
            }

            return resource;
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File not found " + filename, ex);
        }
    }
}
