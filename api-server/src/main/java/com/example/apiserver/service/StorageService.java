package com.example.apiserver.service;

import org.springframework.core.io.Resource;

import java.nio.file.Path;

public interface StorageService {

    String store(Resource resource, Path storageLocation);

    Resource loadAsResource(String fileName, Path storageLocation);
}
