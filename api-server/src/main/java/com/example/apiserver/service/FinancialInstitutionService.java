package com.example.apiserver.service;

import com.example.apiserver.entity.FinancialInstitution;
import com.example.apiserver.exception.DuplicateTokenException;
import com.example.apiserver.exception.FinancialInstitutionNotFoundException;
import com.example.apiserver.repository.FinancialInstitutionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class FinancialInstitutionService {

    final FinancialInstitutionRepository financialInstitutionRepository;

    @Transactional
    public Long create(FinancialInstitution financialInstitution) {
        if(financialInstitutionRepository.existsByToken(financialInstitution.getToken())) {
            throw new DuplicateTokenException(financialInstitution.getToken());
        }

        return financialInstitutionRepository.save(financialInstitution).getId();
    }

    public Page<FinancialInstitution> getFinancialInstitutions(Pageable pageable, String q) {
        return financialInstitutionRepository.findByStatusAndNameContainingIgnoreCase(pageable, FinancialInstitution.Status.ENABLED, q);
    }

    public FinancialInstitution getFinancialInstitution(Long id) {
        return financialInstitutionRepository.findById(id)
                .orElseThrow(() -> new FinancialInstitutionNotFoundException(id));
    }

    public Page<FinancialInstitution> getFinancialInstitutions(Pageable pageable) {
        return financialInstitutionRepository.findByStatus(pageable, FinancialInstitution.Status.ENABLED);
    }
}
