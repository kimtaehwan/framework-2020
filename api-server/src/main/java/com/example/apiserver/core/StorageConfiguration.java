package com.example.apiserver.core;

import com.example.apiserver.exception.FileStorageException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Getter
@Configuration
public class StorageConfiguration {

    final StoragePathConfiguration storagePathConfiguration;
    final Path onboardingImageLocation;
    final Path termsFileLocation;

    @Autowired
    public StorageConfiguration(StoragePathConfiguration storagePathConfiguration) {
        this.storagePathConfiguration = storagePathConfiguration;
        this.onboardingImageLocation = Paths.get(storagePathConfiguration.getOnboardingImages()).toAbsolutePath().normalize();
        this.termsFileLocation = Paths.get(storagePathConfiguration.getTermsFiles()).toAbsolutePath().normalize();

        this.createDirectories();
    }

    private void createDirectories() {
        try {
            Files.createDirectories(this.onboardingImageLocation);
            Files.createDirectories(this.termsFileLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
}
