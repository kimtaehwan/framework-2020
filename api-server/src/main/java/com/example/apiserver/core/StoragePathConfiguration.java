package com.example.apiserver.core;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "storage.path")
public class StoragePathConfiguration {

    private String onboardingImages;
    private String termsFiles;
}
