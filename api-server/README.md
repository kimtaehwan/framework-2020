# Requirements:
1. java SDK 1.8
2. docker
3. kubectl
4. heml
5. helm plugin diff
    > helm plugin install https://github.com/databus23/helm-diff
    
    **if Error: plugin home "$HELM_HOME/plugins" does not exist**
    
    > helm init --client-only
    
    **Return helm plugin install**
    
6. put helmfile executable in PATH
    > https://github.com/roboll/helmfile/releases    
    
#### Build dcs-common for local access for microservices that use dcs-common

> make build-common


#### Docker build:

> make docker-build

#### Docker push ( must have access to ECR (aws ecr get-login)):

> make docker-push

#### Helm install ( must have helmfile in PATH )

> make helm-install

#### Helm upgrade ( must have helmfile in PATH )

> make helm-upgrade

#### Helm delete ( must have helmfile in PATH )

> make helm-delete    
    
# kubernetes

## Utils
* https://k8slens.dev/
* https://k9scli.io/
* https://github.com/sighupio/permission-manager
* https://github.com/johanhaleby/kubetail

## info
* https://blog.kloia.com/kubernetes-hpa-externalmetrics-prometheus-acb1d8a4ed50