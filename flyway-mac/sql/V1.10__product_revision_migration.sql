insert
into PRODUCT_AUD (PRODUCT_ID
                 ,REV_ID
                 ,REVTYPE
                 ,PRODUCT_GROUP_ID
                 ,FI_ID
                 ,PRODUCT_NAME
                 ,IMAGE_URI
                 ,DESCRIPTION
                 ,CREATED
                 ,LAST_MODIFIED
                 ,PRODUCT_TYPE
                 ,CREATOR
                 ,STATUS
                 ,CONTENT_TITLE
                 ,ORG_FILE_NAME)
select PRODUCT_ID
     ,REVINFO_SEQ.nextval
     ,0
     ,PRODUCT_GROUP_ID
     ,FI_ID
     ,PRODUCT_NAME
     ,IMAGE_URI
     ,DESCRIPTION
     ,CREATED
     ,LAST_MODIFIED
     ,PRODUCT_TYPE
     ,CREATOR
     ,STATUS
     ,CONTENT_TITLE
     ,ORG_FILE_NAME
from product a
;

insert
into PRODUCT_META_AUD(PRODUCT_META_ID
                     ,REV_ID
                     ,REVTYPE
                     ,PRODUCT_ID
                     ,META_KEY
                     ,META_VALUE
                     ,META_DESC)
select b.PRODUCT_META_ID
     ,(select REV_ID from PRODUCT_AUD where PRODUCT_ID = b.PRODUCT_ID)
     ,0
     ,b.PRODUCT_ID
     ,b.META_KEY
     ,b.META_VALUE
     ,b.META_DESC
from PRODUCT_META B
;

insert
into REVINFO (REV, REVTSTMP)
select REV_ID
     ,(SYSDATE - TO_DATE('01/01/1970 00:00:00','DD/MM/YYYY HH24:MI:SS')) * 1000 * 60 * 60 * 24
from PRODUCT_AUD
where 1 = 1
;