alter table JOURNEYS
    add RESUME_TIMEOUT NUMBER
;

update JOURNEYS
   set RESUME_TIMEOUT = 30
 where 1 = 1
;

alter table JOURNEYS modify RESUME_TIMEOUT not null
;