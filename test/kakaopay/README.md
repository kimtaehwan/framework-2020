# Kakaopay/coupon
#### Requirements:
1. java 1.8.0_271
2. Spring boot 2.1.14.RELEASE
3. maven 3.6.3
4. docker 20.10.0
5. mac 에서 실행
6. flyway 5.2.4

## Running The App
Ensure you have Java 8 and docker

* cd kakaopay/coupon
* cd kakaopay/gateway

```
make docker-build
make docker-run
```

## Testing The Actuator endpoints:
* http://localhost:8080/actuator/health

## Design
* System Design 은 Gateway 는 public 이고, Coupon 서비스는 internal 입니다.
* 모든 요청은 Gateway 에서 Authentication 통해서 전달됩니다. 
* System Design 으로 구현 안된 부분도 포함되어 있습니다.

### Sequence diagram
    * LogIn 시에 AWS Cognito 를 통해 JWT 발급 (구현 안됨)
    * JWT 를 request header 에 포함해서 Gateway 를 호출함.
    * Gateway 에서 role 및 token 검사 후 coupon 서비스를 호출함.
![coupon sequence diagram](./doc/Coupon%20Sequence%20diagram.png)

### Database Schema
![database schema](./doc/database.png)

## 작업 설명
### 완료
1. 랜덤한 코드의 쿠폰을 N개 생성하여 데이터베이스에 보관하는 API를 구현하세요.
2. 생성된 쿠폰중 하나를 사용자에게 지급하는 API를 구현하세요.
3. 사용자에게 지급된 쿠폰을 조회하는 API를 구현하세요.
4. 지급된 쿠폰중 하나를 사용하는 API를 구현하세요. (쿠폰 재사용은 불가)
5. 지급된 쿠폰중 하나를 사용 취소하는 API를 구현하세요. (취소된 쿠폰 재사용 가능)
6. 발급된 쿠폰중 당일 만료된 전체 쿠폰 목록을 조회하는 API를 구현하세요.
* 성능 이슈를 고려해서 두 개의 API 로 나눠서 작성함.
    * 만료된 쿠폰을 가져온 후에 가져온 id 로 쿠폰 코드 목록을 조회 합니다.
        * GET http://localhost:8080/api/coupons/expired
        * GET http://localhost:8080/api/couponCodes/expired?couponId=1&page=0&size=10
        
### 부분 완료 
1. API 인증을 위한 사용.
* Gateway 서비스를 사용해서, Token 기반 인증을 구현.
* jwt token 을 검사하고, role 을 검사할 수 있는 기능을 추가함.
* signup, signin 기능 및 token 발급. (구현 안됨)
    
### ToDo 
1. Response 를 통일되게 줄 수 있도록 공통화. 
2. Gateway - singup, signin 기능

### 성능 테스트 결과 / 피드백
* 10만개 이상의 쿠폰 코드 생성시 속도가 많이 느린 결과가 나왔다.
* Local 의 In-Memory DB 로 인한 원인도 있겠지만, 성능 refactoring 이 필요하다.
![test](./doc/test_result.png)