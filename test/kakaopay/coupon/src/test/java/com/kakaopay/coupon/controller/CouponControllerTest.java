package com.kakaopay.coupon.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kakaopay.coupon.EntityCollections;
import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.service.CouponCodeService;
import com.kakaopay.coupon.service.CouponService;
import com.kakaopay.coupon.vo.CouponType;
import org.junit.Test;
import org.junit.runner.Request;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.OffsetDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CouponController.class)
public class CouponControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CouponService couponService;

    @MockBean
    private CouponCodeService couponCodeService;

    @Test
    public void create_test() throws Exception {
        CouponRequest.Create buildRequest = CouponRequest.Create.all()
                .count(100)
                .expired(OffsetDateTime.now())
                .build();

        Coupon coupon = Coupon.all()
                .id(1L)
                .build();
        int saveCount = 100;

        given(couponService.create(any(CouponRequest.Create.class)))
                .willReturn(coupon);

        given(couponCodeService.createCouponCode(
                any(CouponRequest.Create.class)
                , any(Coupon.class)))
                .willReturn(saveCount);

        // when
        RequestBuilder request = MockMvcRequestBuilders.post("/api/coupons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(buildRequest));

        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isCreated())
                .andExpect(header().string("Location", "/coupons/1"))
                .andDo(print());
    }

    @Test
    public void listing_test() throws Exception {
        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        Page<Coupon> listingPage = EntityCollections.createPagination(coupon);
        given(couponService.getCoupons(any(Pageable.class)))
                .willReturn(listingPage);

        // when
        RequestBuilder request = MockMvcRequestBuilders.get("/api/coupons");
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content.[0].id").value(1))
                .andDo(print());
    }

    @Test
    public void expired_test() throws Exception {
        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        Page<Coupon> listingPage = EntityCollections.createPagination(coupon);
        given(couponService.getExpiredCoupon(any(Pageable.class)))
                .willReturn(listingPage);

        // when
        RequestBuilder request = MockMvcRequestBuilders.get("/api/coupons/expired");
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content.[0].id").value(1))
                .andDo(print());
    }
}