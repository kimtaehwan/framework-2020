package com.kakaopay.coupon.controller;

import com.kakaopay.coupon.EntityCollections;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.entity.CouponCode;
import com.kakaopay.coupon.service.CouponCodeService;
import com.kakaopay.coupon.vo.CouponType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.OffsetDateTime;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CouponCodeController.class)
public class CouponCodeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CouponCodeService couponCodeService;

    @Test
    public void listing_test() throws Exception {
        CouponCode couponCode = getCouponCode();

        Page<CouponCode> listingPage = EntityCollections.createPagination(couponCode);
        given(couponCodeService.getCouponCodes(any(Pageable.class)))
                .willReturn(listingPage);

        // when
        RequestBuilder request = MockMvcRequestBuilders.get("/api/couponCodes");
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content.[0].id").value(1))
                .andDo(print());
    }

    @Test
    public void update_issue_test() throws Exception {
        CouponCode couponCode = getCouponCode();

        given(couponCodeService.publishCouponCode(anyLong()))
                .willReturn(couponCode);

        Long id = 1L;
        // when
        RequestBuilder request = MockMvcRequestBuilders.put("/api/couponCodes/issue")
                .param("userId", String.valueOf(id));

        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andDo(print());
    }

    @Test
    public void listing_published_test() throws Exception {
        CouponCode couponCode = getCouponCode();

        Page<CouponCode> listingPage = EntityCollections.createPagination(couponCode);
        given(couponCodeService.getPublishedCouponCodes(any(Pageable.class)))
                .willReturn(listingPage);

        // when
        RequestBuilder request = MockMvcRequestBuilders.get("/api/couponCodes/published");
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value(1L))
                .andDo(print());
    }

    @Test
    public void update_code_test() throws Exception {
        CouponCode couponCode = getCouponCode();

        given(couponCodeService.useCoupon(anyString(), anyLong()))
                .willReturn(couponCode);

        // when
        String code = "test";
        Long id = 1L;
        RequestBuilder request = MockMvcRequestBuilders.put("/api/couponCodes/{code}", code)
                .param("userId", String.valueOf(id));
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andDo(print());
    }

    @Test
    public void delete_test() throws Exception {
        CouponCode couponCode = getCouponCode();

        given(couponCodeService.cancelCoupon(anyString()))
                .willReturn(couponCode);

        // when
        String code = "test";
        RequestBuilder request = MockMvcRequestBuilders.delete("/api/couponCodes/{code}", code);
        ResultActions result = mvc.perform(request);

        // then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andDo(print());
    }

    private CouponCode getCouponCode() {
        String code = generateCode();

        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        return CouponCode.all()
                .id(1L)
                .coupon(coupon)
                .code(code)
                .isUsed(false)
                .build();
    }

    private String generateCode() {
        return UUID.randomUUID().toString();
    }
}