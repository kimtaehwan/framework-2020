package com.kakaopay.coupon;

import com.kakaopay.coupon.entity.Coupon;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.assertj.core.util.Lists;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EntityCollections {

    public static final int DEFAULT_PAGE = 0;
    public static final int DEFAULT_SIZE = 10;

    public static <T> Page createPagination(T entity) {
        Pageable pageable = createPageable();
        ArrayList<T> content = Lists.list(entity);
        return new PageImpl<>(content, pageable, content.size());
    }

    public static Pageable createPageable() {
        return PageRequest.of(DEFAULT_PAGE, DEFAULT_SIZE);
    }
}
