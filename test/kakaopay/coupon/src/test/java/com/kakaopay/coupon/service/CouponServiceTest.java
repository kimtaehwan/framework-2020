package com.kakaopay.coupon.service;

import com.kakaopay.coupon.EntityCollections;
import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.repository.CouponRepository;
import com.kakaopay.coupon.vo.CouponType;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class CouponServiceTest {

    @InjectMocks
    private CouponService couponService;

    @Mock
    private CouponRepository couponRepository;

    @Test
    public void create_test() {
        OffsetDateTime expired = OffsetDateTime.now();
        int count = 100;

        CouponRequest.Create buildRequest = CouponRequest.Create.all()
                .expired(expired)
                .count(count)
                .build();

        Coupon save = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(expired)
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(count)
                .build();

        given(couponRepository.save(any(Coupon.class)))
                .willReturn(save);

        // when
        Coupon actual = couponService.create(buildRequest);

        // then
        assertThat(actual).isNotNull();
        assertThat(actual.getId()).isEqualTo(1L);
    }

    @Test
    public void getCoupons_test() {
        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        Page<Coupon> listingPage = EntityCollections.createPagination(coupon);

        given(couponRepository.findAll(any(Pageable.class)))
                .willReturn(listingPage);

        // when
        Page<Coupon> actual = couponService.getCoupons(EntityCollections.createPageable());

        // then
        assertThat(actual.getContent()).size().isEqualTo(1);
        assertThat(actual.getContent()).contains(coupon);
    }

    @Test
    public void getExpiredCoupon_test() {
        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        Page<Coupon> listingPage = EntityCollections.createPagination(coupon);

        given(couponRepository.findAll(any(Specification.class), any(Pageable.class)))
                .willReturn(listingPage);

        // when
        Page<Coupon> actual = couponService.getExpiredCoupon(EntityCollections.createPageable());

        // then
        assertThat(actual.getContent()).size().isEqualTo(1);
        assertThat(actual.getContent()).contains(coupon);
    }

}