package com.kakaopay.coupon.service;

import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.entity.CouponCode;
import com.kakaopay.coupon.repository.CouponCodeRepository;
import com.kakaopay.coupon.repository.CouponRepository;
import com.kakaopay.coupon.vo.CouponType;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class CouponCodeServiceTest {
    @InjectMocks
    private CouponCodeService couponCodeService;

    @Mock
    private CouponRepository couponRepository;

    @Mock
    private CouponCodeRepository couponCodeRepository;

    @Test
    public void createCouponCode_test() {
        OffsetDateTime expired = OffsetDateTime.now();
        int count = 1;

        CouponRequest.Create buildRequest = CouponRequest.Create.all()
                .expired(expired)
                .count(count)
                .build();

        CouponCode couponCode = getCouponCode();
        List<CouponCode> codeList = new ArrayList<>();
        codeList.add(couponCode);

        given(couponCodeRepository.saveAll(any(List.class)))
                .willReturn(codeList);

        // when
        int actual = couponCodeService.createCouponCode(buildRequest, couponCode.getCoupon());

        // then
        assertEquals(count, actual);
    }

    @Test
    public void publishCouponCode_test() {
        Long userId = 1L;
        CouponCode couponCode = getCouponCode();

        given(couponCodeRepository.findFirstByUserIdNullAndIsUsedFalse())
                .willReturn(Optional.of(couponCode));

        // when
        CouponCode actual = couponCodeService.publishCouponCode(userId);

        // then
        assertEquals(actual.getIsPublished(), true);
    }

    @Test
    public void useCoupon_test() {
        Long userId = 1L;
        String code = "test";
        CouponCode couponCode = getCouponCode();

        given(couponCodeRepository.findFirstByUserIdIsNotNullAndIsPublishedIsTrueAndCode(code))
                .willReturn(Optional.of(couponCode));

        // when
        CouponCode actual = couponCodeService.useCoupon(code, userId);

        // then
        assertEquals(actual.getIsUsed(), true);
    }

    @Test
    public void cancelCoupon_test() {
        String code = "test";
        CouponCode couponCode = getCouponCode();

        given(couponCodeRepository.findFirstByUserIdIsNotNullAndIsPublishedIsTrueAndCode(code))
                .willReturn(Optional.of(couponCode));

        // when
        CouponCode actual = couponCodeService.cancelCoupon(code);

        // then
        assertEquals(actual.getIsUsed(), false);
    }

    private CouponCode getCouponCode() {
        String code = generateCode();

        Coupon coupon = Coupon.all()
                .id(1L)
                .type(CouponType.GIFT)
                .expired(OffsetDateTime.now())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(100)
                .build();

        return CouponCode.all()
                .id(1L)
                .coupon(coupon)
                .code(code)
                .isPublished(true)
                .isUsed(true)
                .build();
    }

    private String generateCode() {
        return UUID.randomUUID().toString();
    }

}