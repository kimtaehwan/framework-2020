package com.kakaopay.coupon.service;

import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.dto.CouponResponse;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.repository.CouponRepository;
import com.kakaopay.coupon.repository.CouponSpecs;
import com.kakaopay.coupon.vo.CouponType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;

@Slf4j
@Service
public class CouponService {

    private CouponRepository couponRepository;

    @Autowired
    public CouponService(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    public Coupon create(CouponRequest.Create request) {
        OffsetDateTime expired = request.getExpired();
        int count = request.getCount();

        Coupon save = Coupon.all()
                .type(CouponType.GIFT)
                .expired(expired)
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .isDelete(false)
                .count(count)
                .build();

        return couponRepository.save(save);
    }

    public Page<Coupon> getCoupons(Pageable pageable) {
        return couponRepository.findAll(pageable);
    }

    public Page<Coupon> getExpiredCoupon(Pageable pageable) {
        LocalDate today = LocalDate.now() ;
        OffsetDateTime from = today.atTime( OffsetTime.MIN ) ;
        OffsetDateTime to = OffsetDateTime.now();

        Specification<Coupon> search = Specification.where(CouponSpecs.hasBetween(from, to));
        return couponRepository.findAll(search, pageable);
    }
}
