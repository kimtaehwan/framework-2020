package com.kakaopay.coupon.core;

import com.kakaopay.coupon.exception.CouponCodeNotFoundException;
import com.kakaopay.coupon.exception.CouponException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CouponException.class)
    public ResponseEntity<Object> couponException(CouponException e) {
        String message = String.format("Coupon Runtime Exception: %s", e.getMessage());
        return ResponseEntity.ok(message);
    }

    @ExceptionHandler(CouponCodeNotFoundException.class)
    public ResponseEntity couponCodeNotFoundException(CouponCodeNotFoundException e) {
        String message = String.format("Coupon Code Runtime Exception: %s", e.getMessage());
        return ResponseEntity.ok(message);
    }
}
