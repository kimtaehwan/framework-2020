package com.kakaopay.coupon.vo;

public enum CouponType {
    DEFAULT, GIFT
}
