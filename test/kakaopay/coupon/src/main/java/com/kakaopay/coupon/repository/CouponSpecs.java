package com.kakaopay.coupon.repository;

import com.kakaopay.coupon.entity.Coupon;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import java.time.OffsetDateTime;

public class CouponSpecs {

    private CouponSpecs() {
    }

    public static Specification<Coupon> hasBetween(OffsetDateTime from, OffsetDateTime to) {
        if (ObjectUtils.isEmpty(from) || ObjectUtils.isEmpty(to))
            return null;

        return (root, query, builder) -> builder.between(root.get("expired"), from, to);
    }
}
