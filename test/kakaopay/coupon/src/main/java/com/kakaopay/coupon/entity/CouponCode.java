package com.kakaopay.coupon.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Getter
@Builder(builderClassName = "All", builderMethodName = "all")
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "COUPON_CODE")
public class CouponCode {
    @Id
    @SequenceGenerator(name = "COUPON_CODE_SEQ_GENERATOR", sequenceName = "COUPON_CODE_SEQ", initialValue = 1, allocationSize = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUPON_CODE_SEQ_GENERATOR")
    @Column(name = "COUPON_CODE_ID", nullable = false)
    private Long id;

    @Setter
    @ManyToOne
    @JoinColumn(name = "COUPON_ID", nullable = false)
    private Coupon coupon;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "IS_PUBLISHED")
    private Boolean isPublished;

    @Column(name = "IS_USED")
    private Boolean isUsed;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    public CouponCode published(Long userId) {
        this.userId = userId;
        this.isPublished = Boolean.TRUE;
        this.lastModified = OffsetDateTime.now(ZoneOffset.UTC);
        return this;
    }

    public CouponCode used(Boolean isUsed) {
        this.isUsed = isUsed;
        this.lastModified = OffsetDateTime.now(ZoneOffset.UTC);
        return this;
    }
}
