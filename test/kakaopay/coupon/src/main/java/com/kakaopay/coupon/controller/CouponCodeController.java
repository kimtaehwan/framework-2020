package com.kakaopay.coupon.controller;

import com.kakaopay.coupon.entity.CouponCode;
import com.kakaopay.coupon.service.CouponCodeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class CouponCodeController {
    private CouponCodeService couponCodeService;

    public CouponCodeController(CouponCodeService couponCodeService) {
        this.couponCodeService = couponCodeService;
    }

    @GetMapping(value = "/couponCodes")
    public ResponseEntity listing(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        Page<CouponCode> couponCodes = couponCodeService.getCouponCodes(pageable);
        return ResponseEntity.ok(couponCodes);
    }

    /**
     * 2. 생성된 쿠폰중 하나를 사용자에게 지급하는 API를 구현하세요.
     * @return
     */
    @PutMapping(value = "/couponCodes/issue")
    public ResponseEntity issue(@RequestParam(name = "userId") Long userId) {
        CouponCode update = couponCodeService.publishCouponCode(userId);
        return ResponseEntity.ok(update);
    }

    /**
     * 3. 사용자에게 지급된 쿠폰을 조회하는 API를 구현하세요.
     */
    @GetMapping(value = "/couponCodes/published")
    public ResponseEntity published(@PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        Page<CouponCode> couponCodes = couponCodeService.getPublishedCouponCodes(pageable);
        return ResponseEntity.ok(couponCodes);
    }

    /**
     * 4. 지급된 쿠폰중 하나를 사용하는 API를 구현하세요. (쿠폰 재사용은 불가)
     */
    @PutMapping(value = "/couponCodes/{code}")
    public ResponseEntity use(@PathVariable(value = "code") String code,
                              @RequestParam(name = "userId") Long userId) {
        CouponCode useCoupon = couponCodeService.useCoupon(code, userId);
        return ResponseEntity.ok(useCoupon);
    }

    /**
     * 5. 지급된 쿠폰중 하나를 사용 취소하는 API를 구현하세요. (취소된 쿠폰 재사용 가능)
     */
    @DeleteMapping(value = "/couponCodes/{code}")
    public ResponseEntity delete(@PathVariable(value = "code") String code) {
        CouponCode cancel = couponCodeService.cancelCoupon(code);
        return ResponseEntity.ok(cancel);
    }

    @GetMapping(value = "/couponCodes/expired")
    public ResponseEntity getExpired(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
            @RequestParam(name = "couponId") Long couponId) {
        Page<CouponCode> couponCodes = couponCodeService.getCouponCodes(pageable, couponId);
        return ResponseEntity.ok(couponCodes);
    }
}
