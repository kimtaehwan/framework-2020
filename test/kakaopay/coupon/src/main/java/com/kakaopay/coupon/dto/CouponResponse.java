package com.kakaopay.coupon.dto;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CouponResponse {

    @Getter
    @Builder(builderClassName = "All", builderMethodName = "all")
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class List {
        private Long id;
    }
}
