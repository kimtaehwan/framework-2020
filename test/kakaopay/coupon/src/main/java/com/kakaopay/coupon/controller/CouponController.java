package com.kakaopay.coupon.controller;

import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.dto.CouponResponse;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.exception.CouponException;
import com.kakaopay.coupon.service.CouponCodeService;
import com.kakaopay.coupon.service.CouponService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api")
public class CouponController {

    final CouponService couponService;
    final CouponCodeService couponCodeService;

    @PostMapping(value = "/coupons", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody @Valid CouponRequest.Create request) {
        Coupon coupon = couponService.create(request);
        if (coupon == null || coupon.getId() == null) {
            throw new CouponException("Create Coupon");
        }

        int saveCount = couponCodeService.createCouponCode(request, coupon);
        return ResponseEntity.created(buildCreatedUri(coupon.getId())).build();
    }

    private URI buildCreatedUri(Long id) {
        return UriComponentsBuilder.newInstance()
                .path("/coupons/{id}")
                .build(id);
    }

    @GetMapping(value = "/coupons")
    public ResponseEntity listing(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        Page<Coupon> coupons = couponService.getCoupons(pageable);
        return ResponseEntity.ok(coupons);
    }

    /**
     * 6. 발급된 쿠폰중 당일 만료된 전체 쿠폰 목록을 조회하는 API를 구현하세요.
     *  a. 당일 만료 쿠폰 조회
     *  b. coupon id 를 parameter 로 넘겨서, coupon code 조회.
     */
    @GetMapping(value = "/coupons/expired")
    public ResponseEntity expired(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        Page<Coupon> expiredList = couponService.getExpiredCoupon(pageable);
        return ResponseEntity.ok(expiredList);
    }
}
