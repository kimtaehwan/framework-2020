package com.kakaopay.coupon.repository;

import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.entity.CouponCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CouponCodeRepository extends JpaRepository<CouponCode, Long> {
    Optional<CouponCode> findFirstByUserIdNullAndIsUsedFalse();

    Optional<CouponCode> findFirstByUserIdIsNotNullAndIsPublishedIsTrueAndCode(String code);

    Page<CouponCode> findAllByUserIdIsNotNull(Pageable pageable);

    Page<CouponCode> findAllByCoupon(Pageable pageable, Coupon coupon);
}
