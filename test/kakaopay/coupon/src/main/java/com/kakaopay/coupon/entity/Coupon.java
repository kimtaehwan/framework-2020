package com.kakaopay.coupon.entity;

import com.kakaopay.coupon.vo.CouponType;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Builder(builderClassName = "All", builderMethodName = "all")
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "COUPON")
public class Coupon {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUPON_SEQ_GENERATOR")
    @SequenceGenerator(name = "COUPON_SEQ_GENERATOR", sequenceName = "COUPON_SEQ", allocationSize = 1)
    @Column(name = "COUPON_ID", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private CouponType type;

    @Column(name = "EXPIRED", nullable = false)
    private OffsetDateTime expired;

    @CreatedDate
    @Column(name = "CREATED", nullable = false, updatable = false)
    private OffsetDateTime created;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    @Column(name = "IS_DELETE")
    private Boolean isDelete;

    @Column(name = "COUNT")
    private Integer count;

}
