package com.kakaopay.coupon.service;

import com.kakaopay.coupon.dto.CouponRequest;
import com.kakaopay.coupon.entity.Coupon;
import com.kakaopay.coupon.entity.CouponCode;
import com.kakaopay.coupon.exception.CouponCodeNotFoundException;
import com.kakaopay.coupon.exception.CouponException;
import com.kakaopay.coupon.repository.CouponCodeRepository;
import com.kakaopay.coupon.repository.CouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CouponCodeService {

    private static final int CODE_SIZE = 1000;

    private CouponRepository couponRepository;
    private CouponCodeRepository couponCodeRepository;

    @Autowired
    public CouponCodeService(CouponRepository couponRepository,
                             CouponCodeRepository couponCodeRepository) {
        this.couponRepository = couponRepository;
        this.couponCodeRepository = couponCodeRepository;
    }

    public Integer createCouponCode(CouponRequest.Create request, Coupon coupon) {
        int count = request.getCount();
        int saveCount = 0;

        int loopCount = Math.toIntExact(count / CODE_SIZE);
        int remindCount = Math.toIntExact(count % CODE_SIZE);

        // CODE_SIZE 만큰 insert
        for (int i = 0; i < loopCount; i++) {
            List<CouponCode> codeList = getCodeList(coupon, CODE_SIZE);
            saveCount += saveAll(codeList);
        }

        // 남은 건수 insert
        List<CouponCode> codeList = getCodeList(coupon, remindCount);
        saveCount += saveAll(codeList);

        return saveCount;
    }

    @Transactional
    public int saveAll(List<CouponCode> codeList) {
        return couponCodeRepository.saveAll(codeList).size();
    }

    @Transactional
    public CouponCode publishCouponCode(Long userId) {
        CouponCode couponCode = this.getCouponCode();
        return couponCode.published(userId);
    }

    @Transactional
    public CouponCode useCoupon(String code, Long userId) {
        CouponCode couponCode = this.getCouponCode(code);
        return couponCode.used(Boolean.TRUE);
    }

    private CouponCode getCouponCode(String code) {
        Optional<CouponCode> couponCode = couponCodeRepository.findFirstByUserIdIsNotNullAndIsPublishedIsTrueAndCode(code);
        return couponCode.orElseThrow(() -> new CouponCodeNotFoundException("Coupon Code is not Exists."));
    }

    @Transactional
    public CouponCode cancelCoupon(String code) {
        CouponCode couponCode = this.getCouponCode(code);
        return couponCode.used(Boolean.FALSE);
    }

    private CouponCode getCouponCode() {
        return couponCodeRepository.findFirstByUserIdNullAndIsUsedFalse()
                .orElseThrow(() -> new CouponCodeNotFoundException("Coupon Code is not Exists."));
    }

    public Page<CouponCode> getCouponCodes(Pageable pageable) {
        return couponCodeRepository.findAll(pageable);
    }

    public Page<CouponCode> getPublishedCouponCodes(Pageable pageable) {
        return couponCodeRepository.findAllByUserIdIsNotNull(pageable);
    }

    private List<CouponCode> getCodeList(Coupon coupon, int count) {
        List<CouponCode> codeList = new ArrayList<>();
        for(int i = 0; i < count; i++) {
            String code = generateCode();
            CouponCode couponCode = CouponCode.all()
                    .coupon(coupon)
                    .code(code)
                    .isPublished(false)
                    .isUsed(false)
                    .build();

            codeList.add(couponCode);
        }

        return codeList;
    }

    private String generateCode() {
        return UUID.randomUUID().toString();
    }

    public Page<CouponCode> getCouponCodes(Pageable pageable, Long couponId) {
        Optional<Coupon> coupon = couponRepository.findById(couponId);

        if (coupon.isPresent() == false) {
            throw new CouponException("Coupon is not exists.");
        }

        return couponCodeRepository.findAllByCoupon(pageable, coupon.get());
    }
}
