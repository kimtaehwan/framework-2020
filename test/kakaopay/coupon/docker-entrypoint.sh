#!/bin/bash
set -ex # Abort script at first error
cd /usr/local/app
exec java -jar "bin/coupon-1.0.0.jar"
