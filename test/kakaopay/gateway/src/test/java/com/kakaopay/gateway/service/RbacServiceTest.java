package com.kakaopay.gateway.service;

import com.kakaopay.gateway.config.RbacProperties;
import com.kakaopay.gateway.vo.Menus;
import com.kakaopay.gateway.vo.RbacRequest;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RbacServiceTest {
    @InjectMocks
    RbacService rbacService;

    @MockBean
    RbacProperties rbacProperties;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    String token = "Bearer eyJraWQiOiJcL3NtWDRTYmZKU09IcVRITmdERWM0cWtCbFwvTkNDVG16QWFNbGU4S0dLVlU9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI4ODI5NjdkMC04MjliLTQ2M2ItYmM3Ny0zNTVkNjMyNzBiMmYiLCJhdWQiOiI3ajhnZWdxYTNtNGxsNXA5Y3RhcTBhc2VtbSIsImNvZ25pdG86Z3JvdXBzIjpbImRjcy1hZG1pbiJdLCJldmVudF9pZCI6IjFmNTI5MWIyLTg4ZTctNGQ0OC1hOTg2LTNmMjYyOTBiM2FhYyIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjA4MTk2MzM5LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9LQWo2VFdpWWIiLCJjdXN0b206ZGNzOnByaXZpbGVnZXMiOiJbXCJhZy4yLmVkaXRcIixcImFnLjIudmlld1wiLFwiYWcuOS5lZGl0XCIsXCJhZy45LnZpZXdcIixcImFnLjEwLmVkaXRcIixcImFnLjEwLnZpZXdcIixcImFnLjExLmVkaXRcIixcImFnLjExLnZpZXdcIixcImFnLjEyLmVkaXRcIixcImFnLjEyLnZpZXdcIixcImFnLjI5LmVkaXRcIixcImFnLjI5LnZpZXdcIixcImFnLjEzLmVkaXRcIixcImFnLjEzLnZpZXdcIixcImFnLjE0LmVkaXRcIixcImFnLjE0LnZpZXdcIixcImFnLjE1LmVkaXRcIixcImFnLjE1LnZpZXdcIixcImFnLjE2LmVkaXRcIixcImFnLjE2LnZpZXdcIixcImFnLjE3LmVkaXRcIixcImFnLjE3LnZpZXdcIixcImFnLjI2LmVkaXRcIixcImFnLjI2LnZpZXdcIixcImFnLjI4LmVkaXRcIixcImFnLjI4LnZpZXdcIixcImFnLjIwLmVkaXRcIixcImFnLjIwLnZpZXdcIixcImFnLjIxLmVkaXRcIixcImFnLjIxLnZpZXdcIixcImFnLjI3LmVkaXRcIixcImFnLjI3LnZpZXdcIixcImFnLjguZWRpdFwiLFwiYWcuOC52aWV3XCIsXCJhZy4yMy5lZGl0XCIsXCJhZy4yMy52aWV3XCIsXCJhZy4yNS5lZGl0XCIsXCJhZy4yNS52aWV3XCJdIiwiY29nbml0bzp1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjA4MTk5OTM5LCJjdXN0b206b3N0eXBlIjoiYWRtaW4iLCJpYXQiOjE2MDgxOTYzMzl9.Oi8hxYpbpYn4S54aX3OAGCcxHSDxEmtKqjee2f0YTsf5UZ036q-laC_FaqvdT4uMoQEsbmRN7yX1tnbMzMh9GTU-xPam19NNo4i1uiBOwk_H1QPA0Qgkf1OSveFPhZ41h6_J39c7eISJHeZtZH07l4B2a8Px06LqCa6GDBGN3IbyxuNIWZQr5IsrGztsgyZrFw008MP2BY7Vixiklnmea3PK5_PWc7-K0_AoKy7Q7-xVACZJca6yKoCQQ6BE7Ug6zZ-25ev_ReDW9du9cJ-7niKdGCuxfEWgDBxeq2IXBAqcvI5hn4qtO0QrLHMrAftmGt2NH3vc1L00-J5FxgrcXA";

    @Test
    public void checkAuthority() {
        // given
        RbacRequest rbacRequest = RbacRequest.builder()
                .token(token)
                .methodName("GET")
                .apiPath("/api/coupons")
                .build();
        boolean isAuthority = false;

        // when
        BDDMockito
                .given(rbacProperties.getCoupon())
                .willReturn(Arrays.asList("/api/coupons"));
        boolean actual = rbacService.checkAuthority(rbacRequest);

        // then
        assertEquals(isAuthority, actual);
    }

    @Test
    public void getContainsUrlTest() {
        List<String> paths = Arrays.asList("/api/coupons");
        boolean actual = rbacService.getContainsUrl(paths, "/api/coupons");

        assertEquals(true, actual);
    }

    @Test
    public void obtainAuthorizationTokenTest() {
        // given

        // when
        String actual = rbacService.obtainAuthorizationToken(token);

        // then
        assertNotNull(actual);
    }

    @Test(expected = Exception.class)
    public void decodeTokenClaimsTest() {
        Claims claims = rbacService.decodeTokenClaims(token);
    }

    @Test
    public void convertToListTest() {
        // given
        String privileges = "[\"ag.2.edit\",\"ag.2.view\"]";

        // when
        List<String> actual = rbacService.convertToList(privileges);

        // then
        assertNotNull(actual);
    }

    @Test
    public void convertToListTest2() throws Exception {
        // given
        String privileges = "";

        // when
        List<String> actual = rbacService.convertToList(privileges);

        // then
        assertNotNull(actual);
    }

    @Test
    public void isValidAuthorityTest() {
        // given
        String methodName = "GET";
        List<String> authorityList = new ArrayList<>();
        authorityList.add("ag.1.view");
        String apiPath = "/api/coupons";

        // when
        BDDMockito
                .given(rbacProperties.getCoupon())
                .willReturn(Arrays.asList("/api/coupons"));

        boolean actual = rbacService.isValidAuthority(methodName, apiPath, authorityList);

        assertEquals(true, actual);
    }

    @Test
    public void isValidAuthorityTest2() {
        // given
        String methodName = "GET";
        List<String> authorityList = new ArrayList<>();
        authorityList.add("ag.2.view");
        String apiPath = "/api/couponCodess";

        // when
        BDDMockito
                .given(rbacProperties.getCouponCode())
                .willReturn(Arrays.asList("/api/couponCodess"));

        boolean actual = rbacService.isValidAuthority(methodName, apiPath, authorityList);

        assertEquals(true, actual);
    }

    @Test
    public void getMenusTest() {
        String apiPath = "/api/coupons";
        BDDMockito
                .given(rbacProperties.getCoupon())
                .willReturn(Arrays.asList("/api/coupons"));

        // when
        Menus actual = rbacService.getMenus(apiPath);


        // then
        assertEquals(Menus.COUPON.getId(), actual.getId());
    }

}
