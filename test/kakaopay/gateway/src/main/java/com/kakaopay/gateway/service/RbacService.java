package com.kakaopay.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kakaopay.gateway.config.RbacProperties;
import com.kakaopay.gateway.vo.Menus;
import com.kakaopay.gateway.vo.RbacConstants;
import com.kakaopay.gateway.vo.RbacRequest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.impl.DefaultJwtParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class RbacService {

    @Autowired
    private RbacProperties rbacProperties;


    public RbacRequest convertToRbacRequest(ServerHttpRequest request) {
        List<String> tokens = request.getHeaders().get(RbacConstants.AUTHORIZATION);
        if (tokens == null || tokens.isEmpty()) {
            return null;
        }

        HttpMethod method = request.getMethod();
        if (method == null) {
            return null;
        }

        RequestPath path = request.getPath();

        return RbacRequest.builder()
                .token(tokens.get(0))
                .methodName(method.name())
                .apiPath(path.toString())
                .build();
    }

    public boolean checkAuthority(RbacRequest request) {
        String authorizationToken = obtainAuthorizationToken(request.getToken());

        if (authorizationToken != null) {
            try {
                Claims claims = decodeTokenClaims(authorizationToken);
                List<String> authorityList = obtainPrivileges(claims);
                return isValidAuthority(request.getMethodName(), request.getApiPath(), authorityList);
            } catch (Exception e) {
                log.warn("Jwt processing failed: {}", e.getMessage());
            }
        }

        return false;
    }

    public String obtainAuthorizationToken(String token) {
        try {
            token = URLDecoder.decode(token, "UTF-8");
            String[] parts = token.split(" ");
            if (parts.length == 2) {
                String scheme = parts[0];
                String credentials = parts[1];
                return RbacConstants.BEARER.matcher(scheme).matches() ? credentials : null;
            }
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Claims decodeTokenClaims(String token) {
        String[] splitToken = token.split("\\.");
        String unsignedToken = splitToken[0] + "." + splitToken[1] + ".";

        DefaultJwtParser parser = new DefaultJwtParser();
        Jwt<?, ?> jwt = parser.parse(unsignedToken);
        return (Claims) jwt.getBody();
    }

    public List<String> obtainPrivileges(Claims claims) {
        String privileges = claims.get(RbacConstants.CUSTOM_PRIVILEGES, String.class);
        List<String> roles = convertToList(privileges);

        log.info("roles: {}", roles);

        if (roles.isEmpty()) {
            return Collections.emptyList();
        }

        return roles.stream()
                .map(String::new)
                .collect(toList());
    }

    public List<String> convertToList(String privileges) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(privileges, List.class);
        } catch (Exception e) {
            log.error("convert error: {}", e);
        }

        return Collections.emptyList();
    }

    public boolean isValidAuthority(String methodName, String apiPath, List<String> authorityList) {
        Menus menus = getMenus(apiPath);
        if (menus == null) {
            return false;
        }

        String authorityName = getAuthorityName(methodName, menus);
        if (authorityList.contains(authorityName)) {
            return true;
        }

        return false;
    }

    public Menus getMenus(String apiPath) {

        if (getContainsUrl(rbacProperties.getCoupon(), apiPath)) {
            return Menus.COUPON;
        }

        if ( getContainsUrl(rbacProperties.getCouponCode(), apiPath)) {
            return Menus.COUPON_CODE;
        }

        return null;
    }

    public boolean getContainsUrl(List<String> paths, String apiPath) {
        return paths.stream()
                .anyMatch(apiPath::contains);
    }

    public String getAuthorityName(String methodName, Menus menus) {
        if (StringUtils.equalsAny(methodName, "GET")) {
            return "ag." + menus.getId() + ".view";
        }

        return "ag." + menus.getId() + ".edit";
    }
}
