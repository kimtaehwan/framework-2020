package com.kakaopay.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "spring.mvc.rbac-filter.user-group.allow")
public class RbacProperties {
    private List<String> coupon;

    private List<String> couponCode;
}
