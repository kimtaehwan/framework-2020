package com.kakaopay.gateway.vo;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Privilege {
    List<String> authorityList;
}
