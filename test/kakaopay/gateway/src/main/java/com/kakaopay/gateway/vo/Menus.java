package com.kakaopay.gateway.vo;

import java.util.HashMap;
import java.util.Map;

public enum Menus {
    COUPON(1, "coupon"),
    COUPON_CODE(2, "coupon-code")
    ;

    private int id;
    private String name;

    Menus(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private static final Map<String, Menus> byName = new HashMap<>();
    static {
        for (Menus menus : Menus.values()) {
            if (byName.put(menus.getName(), menus) != null) {
                throw new IllegalArgumentException("duplicate name: " + menus.getName());
            }
        }
    }

    public static Menus valueOfName(String name) {
        return byName.get(name);
    }
}
