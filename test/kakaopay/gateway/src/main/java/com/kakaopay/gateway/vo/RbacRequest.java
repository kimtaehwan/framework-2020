package com.kakaopay.gateway.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class RbacRequest {
    private String token;
    private String methodName;
    private String apiPath;
}
