package com.kakaopay.gateway.vo;

import java.util.regex.Pattern;

public class RbacConstants {
    public static final String AUTHORIZATION = "Authorization";
    public static final String CUSTOM_PRIVILEGES = "custom:privileges";

    public static final Pattern BEARER = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE);
}
