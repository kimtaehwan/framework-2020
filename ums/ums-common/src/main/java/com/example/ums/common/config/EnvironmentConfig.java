package com.example.ums.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component(value = "environmentConfig")
public class EnvironmentConfig {

    @Value("${spring.redis.host}")
    private String redisHost;
}
