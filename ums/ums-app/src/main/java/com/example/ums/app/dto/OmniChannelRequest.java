package com.example.ums.app.dto;

import lombok.*;

public class OmniChannelRequest {

    @Getter
    @Builder(toBuilder = true)
    @ToString
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Create {
        private String notificationReferenceId;
    }
}
