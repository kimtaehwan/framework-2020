package com.example.ums.app.controller;

import com.example.ums.app.dto.NotificationResponse;
import com.example.ums.app.dto.OmniChannelRequest;
import com.example.ums.common.config.EnvironmentConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class OmniChannelController {

    @Autowired
    private EnvironmentConfig environmentConfig;

    @PostMapping(value = {"/notifications"}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity handleNotificationRequest(@RequestHeader("deploymentToken") String deploymentToken,
                                                    @RequestHeader("serverVersion") String serverVersion,
                                                    @RequestBody List<OmniChannelRequest.Create> omniChannelRequestList) {
        List<NotificationResponse> responseList = new ArrayList<>();

        log.info("redis.host: {}", environmentConfig.getRedisHost());
        log.info("deploymentToken: {}", deploymentToken);
        log.info("serverVersion: {}", serverVersion);
        log.info("omniChannelRequestList: {}", omniChannelRequestList.toString());
        return ResponseEntity.ok(responseList);
    }
}
