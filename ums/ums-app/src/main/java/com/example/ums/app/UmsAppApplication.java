package com.example.ums.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example"})
public class UmsAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(UmsAppApplication.class, args);
    }

}
