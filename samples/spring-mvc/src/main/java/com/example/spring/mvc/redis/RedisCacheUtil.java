package com.example.spring.mvc.redis;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class RedisCacheUtil {

    private static RedisTemplate<Object, Object> redisTemplate = null;

    @PostConstruct
    public void initInternal() {
        log.info("########## PostConstruct");
        log.error("########## PostConstruct");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$");
    }

    public static RedisTemplate<Object, Object> redisTemplate() {
        return redisTemplate;
    }
}
