package com.kkoma.algorithm.eleven;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class FindMaxSum {
    public static int findMaxSum(List<Integer> list) {
        if (list == null || list.isEmpty() || list.size() < 2) {
            throw new UnsupportedOperationException("Waiting to be implemented.");
        }

        List<Integer> reverseList = list.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());

        return reverseList.get(0) + reverseList.get(1);
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(5, 9, 7, 11);
        System.out.println(findMaxSum(list));
    }
}
