package com.kkoma.algorithm.eleven;

public class Veterinarian {
    private int MAX = 10000;
    private int front = 0;
    private int rear = 0;
    private String[] queue = new String[MAX];

    public void accept(String petName) {
        if (petName == null) {
            throw new UnsupportedOperationException("Waiting to be implemented.");
        }

        if (rear == MAX) {
            return;
        }

        queue[rear++] = petName;

    }

    public String heal() {
        if (rear == 0) {
            throw new UnsupportedOperationException("Waiting to be implemented.");
        }

        return queue[front++];

    }

    public static void main(String[] args) {
        Veterinarian veterinarian = new Veterinarian();
        veterinarian.accept("Barkley");
        veterinarian.accept("Mittens");
        System.out.println(veterinarian.heal());
        System.out.println(veterinarian.heal());
    }
}
