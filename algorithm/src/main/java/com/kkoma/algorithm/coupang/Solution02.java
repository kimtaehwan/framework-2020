package com.kkoma.algorithm.coupang;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Solution02 {
    public enum GradeEnum {
        A_PLUS(1, "A+"),
        A_ZERO(2, "A0"),
        A_MINUS(3, "A-"),
        B_PLUS(4, "B+"),
        B_ZERO(5, "B0"),
        B_MINUS(6, "B-"),
        C_PLUS(7, "C+"),
        C_ZERO(8, "C0"),
        C_MINUS(9, "C-"),
        D_PLUS(10, "D+"),
        D_ZERO(11, "D0"),
        D_MINUS(12, "D-"),
        F(13, "F");

        private int id;
        private String name;

        GradeEnum(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }
        public String getName() {
            return name;
        }

        private static final Map<String, GradeEnum> byName = new HashMap<>();
        static {
            for (GradeEnum grade : GradeEnum.values()) {
                if (byName.put(grade.getName(), grade) != null) {
                    throw new IllegalArgumentException("duplicate name: " + grade.getName());
                }
            }
        }

        public static GradeEnum valueOfName(String name) {
            return byName.get(name);
        }
    };

    public static void main(String[] args) {
        System.out.println("hello solution02");
        String[] grades = {"DM0106 D-", "PL6677 B+", "DM0106 B+", "DM0106 B+", "PL6677 C0", "GP0000 A0"}	;

        String[] answer = solution(grades);

        log.info("answer: {}", answer);
    }

    private static String[] solution(String[] grades) {

        Map<String, String> mapGrades = new HashMap<>();

        for (String grade : grades) {
            String[] split = grade.split(" ");
            String gradeNo = split[0];
            String gradeValue = split[1];

            // 기존에 등록된게 있으면 실행
            if (mapGrades.containsKey(gradeNo)) {
                // 학정번호로 value 를 가져온다.
                String existedValue = mapGrades.get(gradeNo);
                boolean isChange = isChangeExistedGradeEnum(existedValue, gradeValue);

                log.info("isChange: {}", isChange);
                if (isChange) {
                    mapGrades.put(gradeNo, gradeValue);
                }
            } else {
                mapGrades.put(split[0], split[1]);
            }
        }

        // mapGrades 를 학점순으로 정렬하고, 출력한다.
        List<String> keySetList = new ArrayList<>(mapGrades.keySet());
        Collections.sort(keySetList, (o1, o2) -> mapGrades.get(o1).compareTo(mapGrades.get(o2)));

        log.info("mapGrades: {}", mapGrades);
        log.info("keySetList: {}", keySetList);

        int i = 0;
        String[] answer = new String[keySetList.size()];
        Iterator<String> iter = mapGrades.keySet().iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            String value = (String) mapGrades.get(key);
            answer[i++] = key + " " + value;
        }

        return answer;
    }

    private static boolean isChangeExistedGradeEnum(String existedValue, String newValue) {
        GradeEnum existedEnum =  GradeEnum.valueOfName(existedValue);
        GradeEnum newEnum = GradeEnum.valueOfName(newValue);

        if ( existedEnum.getId() > newEnum.getId()) {
            return true;
        }

        return false;
    }
}
