package com.kkoma.algorithm.coupang;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class Solution03 {

    public static void main(String[] args) {
        System.out.println("hello solution03");
        int[] arr = {5, 3, 9, 13};
        int n = 7;

        boolean result = solution(arr, n);
        log.info("result: {}", result);
    }

    /**
     * 서로 다른 자연수들로 이루어진 배열 arr
     * 배열을 sorting 한다.
     * 숫자 n 보다 클 경우, 배열을
     * @param a
     * @param s
     * @return
     */
    private static boolean solution(int[] arr, int n) {
        boolean answer = false;

        Arrays.sort(arr);

        int lastIndex = Arrays.binarySearch(arr, n);

        if (lastIndex < 0) {
            lastIndex = arr.length - 1;
        }

        int maxSum = arr[lastIndex -1] + arr[lastIndex];
        if (maxSum < n) {
            return false;
        }

        for ( int i = 0; i < lastIndex; i++) {
            int sum = arr[i] + arr[i+1];
            if (sum == n) {
                return true;
            }
        }

        log.info("index: {}", Arrays.binarySearch(arr, n));

        return answer;
    }
}
