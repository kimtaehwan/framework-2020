package com.kkoma.algorithm.coupang;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Solution01 {
    public static void main(String[] args) {
        System.out.println("hello solution01");
        int[] A = {1, 1, 1, 1};
        int S = 3;

        int result = solution(A, S);

        log.info("result: {}", result);
    }

    private static int solution(int[] a, int s) {
        int count = 0;

        for (int i = 0; i < a.length - 1; i++) {
            int sum = a[i];

            for (int j = i+1; j <= a.length - 1; j++) {
                sum = sum + a[j];

                if (sum < s) {
                    continue;
                }

                if ( sum == s || sum > s ) {
                    count++;
                    break;
                }
            }
        }

        return count;
    }
}
