package com.kkoma.algorithm.queue;

public class QueueImpl implements Queue{
    private int front;
    private int rear;
    private int size;
    private char[] queue;

    public QueueImpl(int size) {
        this.front = -1;
        this.rear = -1;
        this.size = size;
        this.queue = new char[this.size];
    }

    @Override
    public boolean isEmpty() {
        if (front == rear) {
            front = -1;
            rear = -1;
        }

        return (front == rear);
    }

    @Override
    public boolean isFull() {
        return (rear == this.size -1);
    }

    @Override
    public void enqueue(char item) {
        if (isFull()) {
            System.out.println("Queue is Full");
            return;
        }

        queue[++rear] = item;
    }

    @Override
    public char dequeue() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return 0;
        }

        ++front;
        char item = queue[front];
        System.out.println("Deleted Item: " +item);

        /**
         * front 포인터는 삭제할 위치에 있는 상태이므로 다음과 같이 (front + 1) % size 로 설정
         * front + 1 로 설정하면 front 포인터가 마지막 요소에 위치했을 경우,
         * ArrayOutOfBoundException 이 발생하기 때문에 (front _1) % size 로 설정해줌.
         * ex) queue 의 size 가 5일때,  index 범위는 0 ~ 4
         * index of front 3: (3+1) % 5 = 4
         * index of front 4: (4+1) % 5 = 0
         */
        if (isEmpty()) {
            front = -1;
            rear = -1;
        }

        return item;
    }

    @Override
    public char peek() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
            return 0;
        }

        // front 포인터는 삭제한 위치에 있으므로 +1을 해줘서 첫번째 요소를 추출하도록 지정
        return queue[front + 1];
    }

    @Override
    public void clear() {
        if (isEmpty()) {
            System.out.println("Queue is empty");
            return;
        }

        front = -1;
        rear = -1;
        queue = new char[size];
    }

    @Override
    public void print() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
        }

        System.out.println("Queue: ");

        // front 포인터는 -1 또는 삭제된 요소의 위치에 있기 때문에,
        // +1 위치를 시작점으로 지정
        for (int i = front + 1; i <= rear; i++) {
            System.out.print(queue[i] + " ");
        }
        System.out.println();
    }
}
