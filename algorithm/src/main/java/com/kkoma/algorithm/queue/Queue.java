package com.kkoma.algorithm.queue;

public interface Queue {
    boolean isEmpty();
    boolean isFull();
    void enqueue(char item);
    char dequeue();
    char peek();
    void clear();
    void print();
}
