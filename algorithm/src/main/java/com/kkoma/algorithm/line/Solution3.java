package com.kkoma.algorithm.line;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class Solution3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<String> strings = new ArrayList<>();
        int longestLength = 0;

        for (int i = 0; i < n; i++) {
            strings.add(sc.next().trim());
            longestLength = getLongestLength(longestLength, strings.get(i));
        }

        String firstEndLine = getFirstEndLine(longestLength);
        System.out.println(String.format("%s", firstEndLine));

        String empty = "";
        String emptyLine = getLine(longestLength, empty);
        System.out.println(emptyLine);

        for (String s : strings) {
            System.out.println(getLine(longestLength, s));
        }

        System.out.println(emptyLine);
        System.out.println(firstEndLine);
    }

    private static String getLine(int longestLength, String s) {
        int spaceSize = longestLength - s.length();

        if (spaceSize == 0) {
           return String.format("# %s #", s);
        }

        return String.format("# %s %" + spaceSize + "s#", s, "");
    }

    private static String getFirstEndLine (int longestLength) {
        int defaultSharp = 4;
        StringBuilder sb = new StringBuilder();

        for ( int i = 0; i < longestLength + defaultSharp; i++ ) {
            sb.append('#');
        }

        return sb.toString();
    }

    private static int getLongestLength(int longestLength, String s) {
        if ( s == null || s.length() < longestLength ) {
            return longestLength;
        }

        return s.length();
    }

}
