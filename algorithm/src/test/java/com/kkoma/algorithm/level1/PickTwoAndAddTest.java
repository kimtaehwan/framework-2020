package com.kkoma.algorithm.level1;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class PickTwoAndAddTest {
    PickTwoAndAdd pickTwoAndAdd = new PickTwoAndAdd();

    @Test
    void solution() {
        int[] numbers = {2, 1, 3, 4, 1};
        int[] result = {2, 3, 4, 5, 6, 7};

        int[] actual = pickTwoAndAdd.solution(numbers);

        log.info("actual: {}", actual);

        assertArrayEquals(result, actual);
    }
}