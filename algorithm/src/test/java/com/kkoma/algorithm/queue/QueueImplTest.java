package com.kkoma.algorithm.queue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueueImplTest {

    @Test
    public void queueTest() {
        Queue queue = new QueueImpl(5);

        boolean isEmpty = queue.isEmpty();

        boolean isFull = queue.isFull();

        queue.enqueue('q');

        char item = queue.dequeue();
        assertEquals('q', item);

        queue.enqueue('I');
        item = queue.peek();
        assertEquals('I', item);

        queue.clear();

        queue.enqueue('g');
        queue.enqueue('o');
        queue.enqueue('o');
        queue.enqueue('d');
        queue.print();

        queue.clear();
        queue.print();

    }
}