# Sftp

## Reference 
* https://blog.pavelsklenar.com/spring-integration-sftp-upload-example/
* https://penthegom.tistory.com/6
* https://github.com/pajikos/java-examples/blob/master/spring-sftp-upload-demo/src/main/java/com/pavelsklenar/SftpConfig.java

## code
* https://github.com/pajikos/java-examples/tree/master/spring-sftp-download-demo

## Sftp
* https://docs.spring.io/spring-integration/reference/html/sftp.html

## Spring boot Integration
* https://fabxoe.tistory.com/84
* https://sup2is.github.io/2020/08/12/what-is-spring-integration.html

## Samples
* https://github.com/pajikos/java-examples/tree/master/spring-sftp-upload-demo

```java
package emart.store.com.batch.common.util;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class SFTPUtil {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private Session session = null;
	private Channel channel = null;
	private ChannelSftp channelSftp = null;

	/**
	 * 서버와 연결에 필요한 값들을 가져와 초기화 시킴
	 * @param host 서버 주소
	 * @param userName 접속에 사용될 아이디
	 * @param password 비밀번호
	 * @param port 포트번호
	 */
	public void init(String host, String userName, String password, int port) {
		
		LOGGER.debug("**** sftp 채널 생성 init [주소] : " + host);
		//System.out.println("**** sftp 채널 생성 init [주소] : " + host);
		//JSch 객체 생성
		JSch jsch = new JSch();

		try {
			//세션객체 생성 ( user , host, port )
			session = jsch.getSession(userName, host, port);

			//password 설정
			session.setPassword(password);

			//세션관련 설정정보 설정
			java.util.Properties config = new java.util.Properties();

			//호스트 정보 검사하지 않는다.
			config.put("StrictHostKeyChecking", "no");
			config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setConfig(config);

			//접속
			session.connect();

			//sftp 채널 접속
			channel = session.openChannel("sftp");
			channel.connect();

		} catch (JSchException e) {
			e.printStackTrace();
		}
		channelSftp = (ChannelSftp) channel;

	}

	/**
	 * 하나의 파일을 업로드 한다/
	 * @param dir 저장시킬주소(서버)
	 * @param file 저장할 파일
	 */
	public boolean upload(String dir, File file){
		boolean result = true;
		FileInputStream in = null;
		
		//System.out.println("**** sftp upload start");

		try{ //파일을 가져와서 inputStream에 넣고 저장경로를 찾아 put
			in = new FileInputStream(file);
			channelSftp.cd(dir);
			channelSftp.put(in,file.getName());
		}catch(SftpException se){
			result = false;
			se.printStackTrace();
		}catch(FileNotFoundException fe){
			result = false;
			fe.printStackTrace();
		}finally{
			try{
				in.close();
			} catch(IOException ioe){
				ioe.printStackTrace();
			}
		}
		//System.out.println("**** sftp upload end");	
		return result;
	}

	/**
	 * 하나의 파일을 다운로드 한다.
	 * @param dir 저장할 경로(서버)
	 * @param fileNm 다운로드할 파일
	 * @param path 저장될 공간
	 * @return
	 */
	public boolean download(String dir, String fileNm, String path) {
		boolean result = true;
		InputStream in = null;
		FileOutputStream out = null;
		int bufferSize = 1024 * 8;
		byte[] buffer = new byte[bufferSize];

		try{ //경로탐색후 inputStream에 데이터를 넣음
			channelSftp.cd(dir);
			in = channelSftp.get(fileNm);

			try {
				out = new FileOutputStream(new File(path));
				int i;

				/* 버퍼 미사용
				 * while ((i = in.read()) != -1) { out.write(i); }
				 */
				
				/*버퍼 사용 */
				while ((i = in.read(buffer)) != -1) { 
					out.write(buffer,0,i); 
				}
				
			} catch (IOException e) {
				result = false;
				e.printStackTrace();
			} finally {
				try {
					out.close();
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}catch(SftpException se){
			result = false;
			se.printStackTrace();
		}

		return result;
	}

	// 파일서버와 세션 종료
	public void disconnect() {
		LOGGER.debug("**** sftp 채널 종료 ");
		//System.out.println("**** sftp 채널 종료 ");
		
		if(session.isConnected()) {
			channelSftp.quit();
			channel.disconnect();
			session.disconnect();
		}
	}
	
	public static void main(String [] args) {

		SFTPUtil util = new SFTPUtil();
		try {
			util.init("174.100.29.31", "emartweb", "emrtdevJul7&20" , 22);
			//util.download("/emartweb/batch/work_file/download","20200812_lof.sql","D:/site/emart/download/20200812_lof.sql");
			util.upload("/emartweb/batch/work_file/download", new File("D:/site/emart/download/20200827_TEST.sql"));
		}catch(Exception e) {
			e.printStackTrace();
			//System.out.println("TTTT");
		}finally {
			util.disconnect();
			
		}
	}
	
}
```