package com.utils.sftp;

import com.utils.sftp.config.SftpConfig.UploadGateway;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "sftp.port = 10022",
        "sftp.remote.directory.download.filter=*.xxx"
})
public class SftpApplicationTests {

    @Autowired
    private UploadGateway gateway;

    private static EmbeddedSftpServer server;

    @Value("${sftp.local.directory.download}")
    private String localDirectoryDownload;

    private static Path sftpFolder;

    @Test
    public void contextLoads() {
    }

    @BeforeClass
    public static void startServer() throws Exception {
        server = new EmbeddedSftpServer();
        server.setPort(10022);
        sftpFolder = Files.createTempDirectory("SFTP_UPLOAD_TEST");

        server.afterPropertiesSet();
        server.setHomeFolder(sftpFolder);

        // starting SFTP
        if (!server.isRunning()) {
            server.start();
        }
    }

    @Test
    public void uploadTest() throws Exception {
        log.info("localDirectoryDownload: {}", localDirectoryDownload);
        // Prepare phase
        Path tempFile = Files.createTempFile("UPLOAD_TEST", ".csv");

        // Prerequisites
        assertEquals(0, Files.list(sftpFolder).count());

        // test phase
        gateway.upload(tempFile.toFile());

        // Validation phase
        List<Path> paths = Files.list(sftpFolder).collect(Collectors.toList());
        assertEquals(1, paths.size());
        assertEquals(tempFile.getFileName(), paths.get(0).getFileName());
    }

}
