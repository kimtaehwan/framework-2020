package com.utils.redis.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Slf4j
@Configuration
@EnableRedisRepositories
@RequiredArgsConstructor
public class RedisConfiguration {

    private static final String REDIS_INSTANCE_TYPE_CLUSTER = "cluster";
    private static final String REDIS_INSTANCE_TYPE_SENTINEL = "sentinel";
    private static final String REDIS_INSTANCE_TYPE_STANDALONE = "standalone";

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private int redisPort;

    @Value("${service.redis.instance-type}")
    private String instanceType;

    @Value("${service.redis.cluster-nodes}")
    private List<String> clusterNodes;

    @Value("${service.redis.cluster-password}")
    private String clusterPassword;

    @Value("${service.redis.cluster-ssl}")
    private boolean clusterSsl;

    private final RedisProperties redisProperties;

    private void loadExternalFileConfig(RedisProperties redisProperties) {
        log.info("instanceType: {}", instanceType);

        if (instanceType.equals(REDIS_INSTANCE_TYPE_CLUSTER)) {
            if (clusterNodes != null && clusterPassword != null && clusterSsl) {
                if (redisProperties.getCluster() == null) {
                    redisProperties.setCluster(new RedisProperties.Cluster());
                }

                redisProperties.getCluster().setNodes(clusterNodes);
                redisProperties.setPassword(clusterPassword);
                redisProperties.setSsl(clusterSsl);
            }

            log.info("loadExternalFileConfig for Redis. instanceType={}, clusterNodes={}, isClusterPwdPresent={}, clusterSsl={}",
                    instanceType, clusterNodes, StringUtils.isEmpty(clusterPassword), clusterSsl
            );
        } else if (instanceType.equals(REDIS_INSTANCE_TYPE_SENTINEL)) {
            log.info("instanceType - Sentinel");
        } else if (instanceType.equals(REDIS_INSTANCE_TYPE_STANDALONE)) {
            log.info("instanceType - Standalone");
        }
    }

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        this.loadExternalFileConfig(redisProperties);

        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder lettucePoolingClientConfigurationBuilder = LettucePoolingClientConfiguration.builder();

        if (redisProperties.getLettuce() != null && redisProperties.getLettuce().getPool() != null) {
            lettucePoolingClientConfigurationBuilder = getPoolingClientForCluster();
        }

        LettuceConnectionFactory lettuceConnectionFactory;

        if ( Optional.ofNullable(redisProperties.getCluster())
                .map(RedisProperties.Cluster::getNodes)
                .map(List::isEmpty)
                .orElse(Boolean.TRUE)
                .equals(Boolean.FALSE)) {
            // Redis Cluster
            RedisClusterConfiguration redisClusterConfiguration = getRedisClusterConfigForCluster();
            lettuceConnectionFactory = new LettuceConnectionFactory(redisClusterConfiguration, lettucePoolingClientConfigurationBuilder.build());
        } else if (Optional.ofNullable(redisProperties.getSentinel())
                .map(RedisProperties.Sentinel::getNodes)
                .map(List::isEmpty)
                .orElse(Boolean.TRUE)
                .equals(Boolean.FALSE)
        ) {
            // Redis Sentinel
            lettuceConnectionFactory = null;
        } else {
            // Redis Standalone
            RedisStandaloneConfiguration redisStandaloneConfiguration =
                    new RedisStandaloneConfiguration(
                            redisProperties.getHost(),
                            redisProperties.getPort());
            redisStandaloneConfiguration.setPassword(redisProperties.getPassword());

            log.info("redisStandaloneConfiguration. hostName={}, port={}, isPwdPresent={}",
                    redisStandaloneConfiguration.getHostName(),
                    redisStandaloneConfiguration.getPort(),
                    redisStandaloneConfiguration.getPassword().isPresent());

            lettuceConnectionFactory = new LettuceConnectionFactory(redisStandaloneConfiguration, lettucePoolingClientConfigurationBuilder.build());
        }

        log.info("lettuceConnectionFactory. isUseSsl={}, isStartTls={}, isClusterAware={}, isRedisSentinelAware={}",
                lettuceConnectionFactory.isUseSsl(),
                lettuceConnectionFactory.isStartTls(),
                lettuceConnectionFactory.isClusterAware(),
                lettuceConnectionFactory.isRedisSentinelAware());

        return lettuceConnectionFactory;
    }

    private RedisClusterConfiguration getRedisClusterConfigForCluster() {
        RedisClusterConfiguration redisClusterConfiguration =
                new RedisClusterConfiguration(redisProperties.getCluster().getNodes());
        redisClusterConfiguration.setPassword(redisProperties.getPassword());

        if (redisProperties.getCluster().getMaxRedirects() != null) {
            redisClusterConfiguration.setMaxRedirects(redisProperties.getCluster().getMaxRedirects());
        }

        log.info("redisClusterConfiguration. clusterNodes={}, maxRedirects={}, isPwdPresent={}",
                redisClusterConfiguration.getClusterNodes(),
                redisClusterConfiguration.getMaxRedirects(),
                redisClusterConfiguration.getPassword().isPresent());

        return redisClusterConfiguration;
    }

    private LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder getPoolingClientForCluster() {
        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder lettucePoolingClientConfigurationBuilder = LettucePoolingClientConfiguration.builder();

        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMinIdle(redisProperties.getLettuce().getPool().getMinIdle());
        genericObjectPoolConfig.setMaxIdle(redisProperties.getLettuce().getPool().getMaxIdle());
        genericObjectPoolConfig.setMaxTotal(redisProperties.getLettuce().getPool().getMaxActive());

        lettucePoolingClientConfigurationBuilder.poolConfig(genericObjectPoolConfig);

        log.info("redisProperties.getLettuce().getPool() - minIdle={}, maxIdle={}, maxActive={}",
                redisProperties.getLettuce().getPool().getMinIdle(),
                redisProperties.getLettuce().getPool().getMaxIdle(),
                redisProperties.getLettuce().getPool().getMaxActive());

        if (redisProperties.isSsl()) {
            lettucePoolingClientConfigurationBuilder.useSsl();
        }

        return lettucePoolingClientConfigurationBuilder;
    }

    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return redisTemplate;
    }
}
