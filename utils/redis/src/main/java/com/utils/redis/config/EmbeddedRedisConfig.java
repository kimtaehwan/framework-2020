package com.utils.redis.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

@Slf4j
@Profile("local")
@Configuration
public class EmbeddedRedisConfig {

    @Value("${spring.redis.embedded-server.enabled:false}")
    private boolean embeddedServerEnabled = false;

    @Value("${spring.redis.embedded-server.port:6379}")
    private int embeddedServerPort = 6379;

    private RedisServer redisServer;

    @PostConstruct
    public void redisServer() throws IOException {

        if (embeddedServerEnabled) {
            if (redisServer == null || !redisServer.isActive()) {
                redisServer = RedisServer.builder()
                        .port(embeddedServerPort)
                        .build();

                redisServer.start();
                log.info("Embedded Redis server started. port={}", redisServer.ports());
            }
        }
    }

    @PreDestroy
    public void stopRedis() {
        if (redisServer != null) {
            redisServer.stop();
            log.info("Embedded Redis server stopped.");
        }
    }
}
