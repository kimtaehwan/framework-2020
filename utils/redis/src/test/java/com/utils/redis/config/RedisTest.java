package com.utils.redis.config;

import com.utils.redis.domain.Point;
import com.utils.redis.repository.PointRedisRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.filter;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private PointRedisRepository pointRedisRepository;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void register_point_test() {
        // given
        String id = "kka";
        LocalDateTime refreshTime = LocalDateTime.of(2018, 5, 26, 0, 0);
        Point point = Point.builder()
                .id(id)
                .amount(1000L)
                .refreshTime(refreshTime)
                .build();

        // when
        pointRedisRepository.save(point);

        // then
        Point savePoint = pointRedisRepository.findById(id).get();

        log.info("savePoint: {}", savePoint.toString());
        assertThat(savePoint.getAmount()).isEqualTo(1000L);
        assertThat(savePoint.getRefreshTime()).isEqualTo(refreshTime);
    }

    @Test
    public void testVop() {
        ValueOperations<Object, Object> vop = redisTemplate.opsForValue();
        vop.set("jdkSerial", "jdk");

        String result = (String) vop.get("jdkSerial");
        log.info("result: {}", result);
    }

    @Test
    public void testVop2() {
        String id = "kka";
        LocalDateTime refreshTime = LocalDateTime.of(2018, 5, 26, 0, 0);
        Point point = Point.builder()
                .id(id)
                .amount(1000L)
                .refreshTime(refreshTime)
                .build();
        ValueOperations<Object, Point> vop = redisTemplate.opsForValue();
        vop.set("test", point);

        Point actual = (Point) vop.get("test");
        log.info("result: {}", actual);
    }

    @Test
    public void testStrings() {
        final String key = "test";
        final ValueOperations<String, String> stringStringValueOperations = redisTemplate.opsForValue();
        stringStringValueOperations.set(key, "11");;
        final String result1 = stringStringValueOperations.get(key);

        assertThat(result1).isEqualTo("11");
    }

    @Test
    public void testList() {
        final String key = "test";

        final ListOperations<String, String> stringStringListOperations = redisTemplate.opsForList();

        stringStringListOperations.rightPush(key, "H");
        stringStringListOperations.rightPush(key, "e");
        stringStringListOperations.rightPush(key, "l");
        stringStringListOperations.rightPush(key, "l");
        stringStringListOperations.rightPush(key, "o");

        stringStringListOperations.rightPushAll(key, " ", "k", "k", "o", "m", "a");

        final String char1 = stringStringListOperations.index(key, 1);

        assertThat(char1).isEqualTo("e");
    }
}
