# Redis

## Reference
* https://docs.spring.io/spring-data/data-redis/docs/current/reference/html/#why-spring-redis
* https://coding-start.tistory.com/128
* https://coding-start.tistory.com/130
* Redis 번역
    * http://arahansa.github.io/docs_spring/redis.html
    
### local environment
* https://jojoldu.tistory.com/297
* https://github.com/ozimov/embedded-redis
* https://sabarada.tistory.com/105
* https://sabarada.tistory.com/106?category=856943

### AWS Elasticache setting
* https://keyholesoftware.com/2018/08/28/using-amazon-elasticache-for-redis-to-optimize-your-spring-boot-application/
* https://medium.com/jimmyberg-kim/aws-elasticache-for-redis-61376495c1d8
* https://ryulth.com/devnote/2020/08/09/java-lettuce-with-elasticache/

### Jedis vs Lettuce
* https://jojoldu.tistory.com/418

### Lettuce 
* https://ssoco.tistory.com/19

### Cluster 설명
* https://brunch.co.kr/@springboot/218
* https://meetup.toast.com/posts/226

### Jedit connection pool
* GenericObjectPoolConfig
    * https://theserverside.tistory.com/302

### Spring + Redis 연동
* https://arcsit.tistory.com/entry/SpringRedis-%EC%8A%A4%ED%94%84%EB%A7%81%EA%B3%BC-%EB%A0%88%EB%94%94%EC%8A%A4-%EC%97%B0%EB%8F%99

### Spring boot + Redis 연동
* https://sg-choi.tistory.com/180
