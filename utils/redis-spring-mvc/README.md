# Redis Spring MVC

## Jedis
* https://github.com/redis/jedis#jedis-cluster%EC%97%90

### Jedis Cluster
* https://hakurei.tistory.com/132
* Jedis Pool
  * https://ozofweird.tistory.com/m/337
  
### [spring-data-redis Jedis](https://github.com/spring-projects/spring-data-redis/blob/master/src/main/java/org/springframework/data/redis/connection/jedis/JedisConnectionFactory.java#L304)

### [spring-data-redis Lettuce](https://github.com/spring-projects/spring-data-redis/blob/master/src/test/java/org/springframework/data/redis/connection/lettuce/LettuceConnectionFactoryUnitTests.java)
  
## Reference
* Spring Project 생성
    * https://whitepaek.tistory.com/55
* Spring MVC Sample
    * https://whitepaek.tistory.com/41
* encoding UTF-8 설정
    * https://gmlwjd9405.github.io/2019/01/01/spring-utf8.html
* Spring MVC -XML 설정을 Java 로 작성
    * https://gitonigawara.github.io/Spring_MVC_no_xml/
* Properties 파일에서 정보 가져오기
    *  https://codevang.tistory.com/243
* Tomcat 구동시 library 오류
    * https://brunch.co.kr/@argent/26
    * ctrl + art + shift + s
* Spring Bean Lifecycle
    * http://wonwoo.ml/index.php/post/1820
* Spring Bean Test
    * https://blog.naver.com/writer0713/221373247634
* webapp resource module setting
    * https://blog.naver.com/writer0713/221373247634
* embedded redis setting 
    * https://jojoldu.tistory.com/297
* connectionFactory.afterPropertiesSet();
  * https://github.com/spring-projects/spring-data-redis/blob/master/src/test/java/org/springframework/data/redis/connection/lettuce/LettuceConnectionFactoryUnitTests.java