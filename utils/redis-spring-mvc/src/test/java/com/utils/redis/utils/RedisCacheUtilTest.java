package com.utils.redis.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring/*.xml", "classpath:WEB-INF/spring/appServlet/*.xml"})
public class RedisCacheUtilTest {

    @Autowired
    private RedisCacheUtil redisCacheUtil;

    @Test
    public void testVop() {
        ValueOperations<Object, Object> vop = redisCacheUtil.redisTemplate().opsForValue();
        vop.set("jdkSerial", "jdk123");

        String result = (String) vop.get("jdkSerial");
        log.info("result: {}", result);
    }
}