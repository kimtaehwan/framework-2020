package com.utils.redis.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Slf4j
@Configuration
public class EmbeddedRedisConfig {
    private boolean embeddedServerEnabled = true;
    private int embeddedServerPort = 6379;
    private RedisServer redisServer;

    @PostConstruct
    public void redisServer() throws Exception {
        log.info("redisServer ##########");
        if (embeddedServerEnabled) {
            if (redisServer == null || !redisServer.isActive()) {
                redisServer = RedisServer.builder()
                        .port(embeddedServerPort)
                        .build();

                redisServer.start();
                log.info("##### Embedded Redis server started. port={}", redisServer.ports());
            }
        }
    }

    @PreDestroy
    public void stopRedis() {
        if (redisServer != null) {
            redisServer.stop();
            log.info("Embedded Redis Server stopped.");
        }
    }
}
