package com.utils.redis.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {

    @GetMapping(value = "/")
    public ModelAndView index(Model model) {

        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        mv.addObject("Title", "index title");
        mv.addObject("END", "한글");

        return mv;
    }
    @GetMapping("/home")
    public String getHome() {
        return "Spring + Maven + 한글";
    }

}
