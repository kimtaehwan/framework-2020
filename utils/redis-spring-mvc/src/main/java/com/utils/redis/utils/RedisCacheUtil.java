package com.utils.redis.utils;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.masterslave.StatefulRedisMasterSlaveConnection;
import io.lettuce.core.support.ConnectionPoolSupport;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class RedisCacheUtil {

    private static JedisConnectionFactory jcf			    = null;
    private static LettuceConnectionFactory lettuceConnectionFactory = null;
    private static RedisTemplate<Object, Object> template   = null;

    public static GenericObjectPool<StatefulRedisConnection<String, String>> pool = null;


    @PostConstruct
    public void initInternal() {
        log.info("########## RedisCacheUtil initInternal()");

        RedisURI node = RedisURI.create("clustercfg.cas-redis-poc.bcskg5.use1.cache.amazonaws.com", 6379);

        RedisClusterClient clusterClient = RedisClusterClient.create(node);


    }


    public static RedisTemplate<Object, Object> redisTemplate()
    {
        return template;
    }

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {

        RedisStandaloneConfiguration serverConfig = new RedisStandaloneConfiguration("localhost", 6379);

        return new LettuceConnectionFactory(serverConfig);
    }
}
