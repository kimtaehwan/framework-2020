## build
### Angular
```
$ ng build --prod --output-path ../server/dist
```

### angular new tab with post data 
* https://gist.github.com/germainlecourtoissloop/c6b87ce1482bd74a01dfd80c395e9a64

### Auth Guard
* https://medium.com/@ryanchenkie_40935/angular-authentication-using-route-guards-bf7a4ca13ae3

### SyncGuard
* https://stackoverflow.com/questions/40589878/multiple-canactivate-guards-all-run-when-first-fails
