# Fuse - Angular

Material Design Admin Template with Angular 8 and Angular Material

## update
### version 8 -> 9
* ng update @angular/core@8 @angular/cli@8
* ng update angular-in-memory-web-api
* ng update @angular/core@9 @angular/cli@9
* ng update
* ng update @angular/material@9 --force
    * material update 시에 cdk 도 update 됨.
* ng update @angular/flex-layout@9.0.0-beta.31
* ng update @swimlane/ngx-dnd@8.1.0 --force
