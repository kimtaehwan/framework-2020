export const environment = {
    production: true,
    hmr       : false,
    apiBaseUrl: '/api',
    authUrl: '/auth'
};
