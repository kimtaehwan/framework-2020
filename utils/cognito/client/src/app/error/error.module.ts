import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './error.component';
import {RouterModule} from '@angular/router';

const routes = [
    {
        path: '**',
        component: ErrorComponent
    }
];

@NgModule({
  declarations: [ErrorComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes)
  ]
})
export class ErrorModule { }
