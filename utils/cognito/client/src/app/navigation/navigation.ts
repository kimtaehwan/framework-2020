import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'customer-support',
                title    : 'Customer Support',
                translate: 'NAV.CUSTOMER-SUPPORT',
                type     : 'collapsable',
                icon     : 'icon-customer-support', // \e800
                children : [

                    {
                        id          : 'application-list',
                        title       : 'Application List',
                        translate   : 'NAV.APPLICATION-LIST',
                        type        : 'item',
                        url         : '/cs/application/list',
                        children : [
                            {
                                id       : 'detail',
                                title    : 'Detail',
                                translate: 'NAV.CRUD.DETAIL',
                                type     : 'item',
                                url      : '/cs/application/view',
                                hidden   : true
                            },
                        ]
                    }
                ]
            },
            {
                id       : 'sample',
                title    : 'Sample',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                url      : '/sample/list',
                children: [
                    {
                        id       : 'create',
                        title    : 'Create',
                        translate: 'NAV.CRUD.CREATE',
                        type     : 'item',
                        url      : '/sample/create'
                    },
                    {
                        id       : 'edit',
                        title    : 'Edit',
                        translate: 'NAV.CRUD.EDIT',
                        type     : 'item',
                        url      : '/sample/edit'
                    },
                    {
                        id       : 'view',
                        title    : 'View',
                        translate: 'NAV.CRUD.VIEW',
                        type     : 'item',
                        url      : '/sample/view'
                    }
                ],
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            }
        ]
    }
];
