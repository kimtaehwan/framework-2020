export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'CRUD': {
                CREATE: 'Create',
                EDIT: 'Edit',
                VIEW: 'View',
                DETAIL: 'Detail',
            },
            'APPLICATIONS': 'Applications',
            'CUSTOMER-SUPPORT': 'Customer Support',
            'APPLICATION-LIST': 'Application List',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            }
        }
    }
};
