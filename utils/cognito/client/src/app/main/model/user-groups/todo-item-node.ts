export class TodoItemNode {
    id: number;
    name: string;
    children: TodoItemNode[];

    constructor() {
        this.id = 0;
        this.name = '';
        this.children = [];
    }
}
