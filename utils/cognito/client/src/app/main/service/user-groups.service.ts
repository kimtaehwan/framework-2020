import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {TodoItemNode} from '../model/user-groups/todo-item-node';

@Injectable({
  providedIn: 'root'
})
export class UserGroupsService {

  constructor(
      private httpClient: HttpClient
  ) { }

  getMenuList(url: string): Observable<HttpResponse<TodoItemNode>> {
      return this.httpClient.get<TodoItemNode>(url, {observe: 'response'});
  }
}
