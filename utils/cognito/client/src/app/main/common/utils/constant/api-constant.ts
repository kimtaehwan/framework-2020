import {environment} from '../../../../../environments/environment';

export class ApiConstant {

    public static API_BASE_URL = environment.apiBaseUrl;
    public static AUTH_URL = environment.authUrl;
    public static API_USER_GROUPS_MENU_LIST_URL = environment.apiBaseUrl + '/menuList';
}
