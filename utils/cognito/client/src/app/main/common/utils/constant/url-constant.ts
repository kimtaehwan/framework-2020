import {ApiConstant} from './api-constant';

export class UrlConstant {

    static getMenuListUrl(): string {
        return ApiConstant.API_USER_GROUPS_MENU_LIST_URL;
    }
}
