import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { SampleComponent } from './sample.component';
import {AuthGuard} from '../../auth/guard/auth.guard';
import {MaterialModule} from '../common/utils/material/material.module';
import {RbacGuard} from '../../auth/guard/rbac.guard';

const routes = [
    {
        path     : 'sample',
        component: SampleComponent,
        canActivate: [
            AuthGuard, RbacGuard
        ]
    }
];

@NgModule({
    declarations: [
        SampleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MaterialModule,

        FuseSharedModule
    ],
    exports     : [
        SampleComponent
    ]
})

export class SampleModule
{
}
