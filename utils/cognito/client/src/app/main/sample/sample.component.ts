import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';

@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class SampleComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
    }

    linkDcs(): void {
        console.log('linkDcs');
        const mapForm = document.createElement('form');
        mapForm.target = '_blank';
        mapForm.method = 'POST';
        mapForm.action = 'http://localhost:3000/forward';

        const obj = JSON.parse(this.obj);

        Object.keys(obj).forEach((param) => {
            const mapInput = document.createElement('input');
            mapInput.type = 'hidden';
            mapInput.name = param;
            mapInput.setAttribute('value', obj[param]);
            mapForm.appendChild(mapInput);
        });

        document.body.appendChild(mapForm);

        console.log('mapForm: ', mapForm);
        mapForm.submit();
        console.log('submit');
    }

    obj = "{\n" +
    "    \"level\": \"onSuccess\",\n" +
    "    \"token_type\": \"bearer\",\n" +
    "    \"expires_in\": \"7200\",\n" +
    "    \"accessToken\": \"eyJraWQiOiIyYmV0c0xTV2cwYmRneUpScWFmYURVaTZ4d3B4UkdLOHUwM1wvN3FlVEpPVT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI2N2NiZGE3Mi05ODkxLTQ3NmEtYTFhNi00ZTcwN2QzYzM4Y2YiLCJldmVudF9pZCI6ImQ3YzhlOWIzLWYyMTktNGQ1ZC04MDk3LTE4OTA1ZDVkNzdlNSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MDk5MjQyMjUsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1ub3J0aGVhc3QtMi5hbWF6b25hd3MuY29tXC9hcC1ub3J0aGVhc3QtMl9pSXE4R3g2TU0iLCJleHAiOjE2MDk5MjYwMjUsImlhdCI6MTYwOTkyNDIyNSwianRpIjoiY2Y5YjliNjgtMTAzNC00ZTI1LThiMzMtY2RiZDAxMmNjZTBiIiwiY2xpZW50X2lkIjoiMzVkMm1kaWxscWtkNG5jM3RqaGZiZWhxZDkiLCJ1c2VybmFtZSI6ImFkbWluIn0.VVBCwLIPcFcI4fPC1TMiRon1OV9_ncxztX3b9BnlyblkrHyrNuotNuER8pQ7pqEdetTQCiJOLfmR4qQaQ0DNrVGg6MASYhiQsgeSJcotkiH4FfdibCkmVm16tOctzl9F6jG5osj_378QfetrKn2I2o-eeNodHocpsX19vxPjgSmiqO9cBvkq_X85ULCV4wmFtyzOc0JrB3lF6XAEkS7u7xgVYxAlAX6VZgMIiM6p_r6Be6DzzB7Me_Zp7ACxBnn3FejEx1lC1v7dMFAkhNf7386CCWOdB5K4PfMvt_Yg-pmU9cJjwau9gyggmtN9xH1Dkxtn9emPdEqeBtaprMfQVw\",\n" +
    "    \"idToken\": \"eyJraWQiOiJicHErWm45eG56aHF3R2NpOU9EeG9DOTcxOXJpbXdmQ2ViS0dMNmJHSGRRPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI2N2NiZGE3Mi05ODkxLTQ3NmEtYTFhNi00ZTcwN2QzYzM4Y2YiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmFwLW5vcnRoZWFzdC0yLmFtYXpvbmF3cy5jb21cL2FwLW5vcnRoZWFzdC0yX2lJcThHeDZNTSIsImNvZ25pdG86dXNlcm5hbWUiOiJhZG1pbiIsImF1ZCI6IjM1ZDJtZGlsbHFrZDRuYzN0amhmYmVocWQ5IiwiZXZlbnRfaWQiOiJkN2M4ZTliMy1mMjE5LTRkNWQtODA5Ny0xODkwNWQ1ZDc3ZTUiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYwOTkyNDIyNSwiY3VzdG9tOmRjczpwcml2aWxlZ2VzIjoiW3tcImlkXCI6MSxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MixcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MyxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6NCxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6NSxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6NixcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6NyxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6OCxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6OSxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MTAsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjExLFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoxMixcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MTMsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjE0LFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoxNSxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MTYsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjE3LFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoxOCxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MTksXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjIwLFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoyMSxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MjIsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjIzLFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoyNCxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MjUsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfSx7XCJpZFwiOjI2LFwidmlld1wiOnRydWUsXCJlZGl0XCI6dHJ1ZX0se1wiaWRcIjoyNyxcInZpZXdcIjp0cnVlLFwiZWRpdFwiOnRydWV9LHtcImlkXCI6MjgsXCJ2aWV3XCI6dHJ1ZSxcImVkaXRcIjp0cnVlfV0iLCJleHAiOjE2MDk5MjYwMjUsImN1c3RvbTpkY3NEYXRhIjoie1wicGVybWlzc2lvbnNcIjp7XCJEQ1MtQ3VzdG9tZXJTdXBwb3J0XCI6W1widmlld1wiLFwiZWRpdFwiXSxcIkRDUy1GSU9uYm9hcmRpbmdcIjpbXCJ2aWV3XCIsXCJlZGl0XCJdLFwiRENTLUpvdXJuZXlcIjpbXCJ2aWV3XCJdfSxcImZpTGlzdFwiOltcIkFMTFwiLFwiY2l0aVwiLFwiaGRmY1wiXSxcIkpTRVNTSU9OSURcIjpcImhReTNSdDJlbWZROGY5aU5hU2M2T3dpNk9vNHZtSDNJdmhaeWtmcFkuZmlzZXJ2ZXIxXCIsXCJPbmRvdFNlc3Npb25JZFwiOlwiNzEzMjQyNmE0MzY0MzU2MzQxNDI2NzQ1NDI2Yjc2NTg1MzVhNjk2YzYyNDM0ZjRiNDc3ODZmNzM0YzQzNmY1MzM0Njc3MjcyNzA2YjY5MzQ1ODRjMzA2MTM0Mzg2YjQ1NjEzODc2NmI1YTc4NmY0NTM0NzA1NTU4NGQ3OTM3NmRcIn0iLCJpYXQiOjE2MDk5MjQyMjUsImVtYWlsIjoid2loMDIwMkBnbWFpbC5jb20ifQ.bqIhzDSwSObFS_n7xiqsHUrM8niaEOcAQdUiTJ8QxBIw7wtLoFucMIvFDOfYqpDuQ3ukXNzJ_XcSCzYORxeB9WC8xybLg41OY9sq302n003L6LWsC3HWdyXP-GEnK6pNeHiGVQvOgTV-RGykgD4cpdaHoN-9jf9EmmCCu4G7U_R7_1uJsjnmlKVJaaDxRxhJipYfxbmVvdBJDTeQ2sz8FsT6-yw-qczrV_RqmC1EH_vk5ghxA2vZtYeQ4u6rxuQUl3zlNJz4FwVpu0a6g96mOhM1dsKFTI9hyCdvVSVfN5cerZrvxFY95GV3s0tx0NpNpMhlaTF1Ql3MfUqNSfIt3Q\"\n" +
    "}";
}
