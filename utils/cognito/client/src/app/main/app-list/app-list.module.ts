import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppListComponent } from './app-list.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '**',
        component: AppListComponent
    }
];

@NgModule({
  declarations: [AppListComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes)
  ]
})
export class AppListModule { }
