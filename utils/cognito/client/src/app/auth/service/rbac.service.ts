import { Injectable } from '@angular/core';
import {navigation} from '../../navigation/navigation';
import {TodoItemNode} from '../../main/model/user-groups/todo-item-node';
import {AuthUtilService} from '../util/auth-util.service';

@Injectable({
  providedIn: 'root'
})
export class RbacService {

    urlNavigation = {};
    currentEditable: boolean;

    rbacInfo = {};
    rbacFlatInfo = [];
    private rbacGroups: string[] = [];

  constructor(
      private authUtilService: AuthUtilService
  ) {
      this.flattenNavigation(navigation);
  }

  flattenNavigation = (obj) => {
      console.log('# RbacService - obj: ', obj);
      if (obj['translate'] && obj['url']) {
          this.urlNavigation[obj['url'].split('/list').join('')] = obj;

          console.log('# this.urlNavigation: ', this.urlNavigation);
      }

      Object.keys(obj).forEach((key) => {
          if (typeof obj[key] === 'object') {
              this.flattenNavigation(obj[key]);
          }
      });
  };

  flattenMenuList = (obj) => {
      console.log('# flattenMenuList: ', obj);
      if (obj['expandable'] === false) {
          obj.privileges = {};
          this.rbacFlatInfo.push(obj);
      }

      Object.keys(obj).forEach(key => {
          if (typeof obj[key] === 'object') {
              this.flattenMenuList(obj[key]);
          }
      });
  };

  getPrivilege(name: string, type: string): boolean {
      console.log('# getPrivilege - name: ', name);
      console.log('# getPrivilege - type: ', type);

      if (name === 'NAV.LOGOUT') {
          return true;
      }

      console.log('# this.rbacInfo: ', this.rbacInfo);
      console.log('# !this.rbacInfo: ', !this.rbacInfo);

      if (!this.rbacInfo) {
          this.rbacInfo = JSON.parse(localStorage.getItem('rbac_info'));
          this.rbacInfo = {};
          return false;
      }

      if (!!this.rbacInfo[name]) {
          return this.rbacInfo[name].privileges[type];
      }
  }

  updateRBAC(menu: TodoItemNode): void {
      this.rbacInfo = {};
      this.flattenMenuList(menu);

      console.log('this.rbacFlatInfo: ', this.rbacFlatInfo);

      this.rbacGroups = this.authUtilService.getRbacGroups();
      console.log('this.rbacGroups: ', this.rbacGroups);

      this.setItemRbacInfo();
  }

  setItemRbacInfo(): void {
      this.rbacGroups.forEach((name) => {
          console.log('name: ', name);
          const splited = name.split('.');

          if (splited[0] === 'ag') {
              this.rbacFlatInfo.forEach((flat) => {
                  console.log('1. flat: ', flat);
                  if(splited[1] === flat.id.toString() && !!splited[2]) {
                      flat.privileges[splited[2]] = true;
                      this.rbacInfo[flat.name] = flat;

                      console.log('2. flat: ', flat);
                      console.log('3. this.rbacInfo: ',this.rbacInfo);
                  }
              });
          }
      });

      localStorage.setItem('rbac_info', JSON.stringify(this.rbacInfo));
  }
}
