import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {url} from 'inspector';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForwardService {

  constructor(private httpClient: HttpClient) { }

  getToken(url: string): Observable<HttpResponse<any>> {
      return this.httpClient.get<any>(url, {observe: 'response'});
  }
}
