
import {ApiConstant} from '../../main/common/utils/constant/api-constant';
import {RbacService} from './rbac.service';
import {UrlConstant} from '../../main/common/utils/constant/url-constant';
import {UserGroupsService} from '../../main/service/user-groups.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    TOKEN_NAME = 'access_token';

  constructor(
      private httpClient: HttpClient,
      private jwtHelper: JwtHelperService,
      private rbacService: RbacService,
      private userGroupsService: UserGroupsService,
      private router: Router
  ) { }

  isAuthenticated(): boolean {
      const token = this.getToken();
      return !!token ? !this.isTokenExpired(token) : false;
  }

  getToken(): string {
      return localStorage.getItem(this.TOKEN_NAME);
  }

  isTokenExpired(token: string): boolean {
      return this.jwtHelper.isTokenExpired(token);
  }

  login(basicToken: string, level: string, secretCode: string, otpCode: string): Observable<any> {
      const authorization = new HttpHeaders({
          Authorization: basicToken
      });

      const headers = {
          headers: authorization
      };

      const body: any = {};

      if (!!level) {
          body.level = level;
          body.secretCode = secretCode;
          body.code = otpCode;
      }

      return this.httpClient.post<any>(ApiConstant.AUTH_URL, body, headers);
  }

  redirectLastPage(): void {
      console.log('# authenticationService - redirectLastPage()');
      const url = UrlConstant.getMenuListUrl();

      this.userGroupsService.getMenuList(url)
          .subscribe(
              (resp) => {
                  console.log('resp.body: ', resp.body);
                  this.rbacService.updateRBAC(resp.body);

                  const lastPage = localStorage.getItem('last_page') || '/cs/application/list';
                  const lastQueryParam = localStorage.getItem('last_query_param');
                  const navigationExtras = {};

                  if (!!lastQueryParam) {
                      navigationExtras['queryParams'] = JSON.parse(lastQueryParam);
                  }

                  console.log('# lastPage: ', lastPage);
                  console.log('# lastQueryParam: ', lastQueryParam);
                  console.log('# navigationExtras: ', navigationExtras);
                  console.log('# this.router: ', this.router);
                  this.router.navigateByUrl(lastPage, {skipLocationChange: false})
                      .then(() => {
                          this.router.navigate([lastPage], navigationExtras);
                      });
              }
          );
  }
}
