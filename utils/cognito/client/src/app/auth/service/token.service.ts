import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  getAccessToken(): string {
      console.log('# getAccessToken()');

      return localStorage.getItem('access_token');
  }
}
