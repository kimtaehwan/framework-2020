import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForwardComponent } from './forward.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../../main/common/utils/material/material.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';

const routes: Routes = [
    {
        path: '**',
        component: ForwardComponent
    }
];

@NgModule({
  declarations: [ForwardComponent],
  imports: [
    CommonModule,

      RouterModule.forChild(routes),

      MaterialModule,
      FuseSharedModule
  ]
})
export class ForwardModule { }
