import { Component, OnInit } from '@angular/core';
import {ForwardService} from '../service/forward.service';
import {ApiConstant} from '../../main/common/utils/constant/api-constant';

@Component({
  selector: 'app-forward',
  templateUrl: './forward.component.html',
  styleUrls: ['./forward.component.scss']
})
export class ForwardComponent implements OnInit {

  constructor(private forwardService: ForwardService) { }

  ngOnInit(): void {
      console.log('forward', localStorage.getItem('access_token'));
      const url = ApiConstant.AUTH_URL + '/token';

      this.forwardService.getToken(url)
          .subscribe(
              (resp) => {
                  console.log('resp.body: ', resp.body);
                  // TODO: access_token 이 valid 할 경우, localStorage 에 setItem
                  // localStorage.setItem('cas_dcs_data', resp.body);
                  // localStorage.setItem('access_token', resp.body);
              }
          );

  }

}
