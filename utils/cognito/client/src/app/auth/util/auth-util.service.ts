import { Injectable } from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthUtilService {

    RBAC_GROUPS = new Map<string, object>();

  constructor(
      private jwtHelper: JwtHelperService
  ) {
      this.initRbacGroups();
  }

  initRbacGroups(): void {
      this.RBAC_GROUPS.set('DCS-CustomerSupport', {id: 9, name: 'application-list'});
      this.RBAC_GROUPS.set('DCS-FIOnboarding', {id: 20, name: 'financialinstitutions'});
      this.RBAC_GROUPS.set('DCS-Journey', {id: 10, name: 'journey'});
      this.RBAC_GROUPS.set('DCS-JourneyPoint', {id: 11, name: 'journey-point'});
      this.RBAC_GROUPS.set('DCS-Process', {id: 12, name: 'process'});
      this.RBAC_GROUPS.set('DCS-Product', {id: 13, name: 'product'});
      this.RBAC_GROUPS.set('DCS-Consent', {id: 14, name: 'consent'});
      this.RBAC_GROUPS.set('DCS-Eligibility', {id: 15, name: 'eligibility'});
      this.RBAC_GROUPS.set('DCS-E-Signature', {id: 16, name: 'esignature'});
      this.RBAC_GROUPS.set('DCS-ApplicationForm', {id: 17, name: 'application-form'});
      this.RBAC_GROUPS.set('DCS-PolicyContent', {id: 26, name: 'policy-contents'});
      this.RBAC_GROUPS.set('DCS-ClosingPage', {id: 28, name: 'closing-page'});
      this.RBAC_GROUPS.set('DCS-CreditUnderwriting', {id: 25, name: 'credit-underwriting'});
      this.RBAC_GROUPS.set('DCS-GLOBAL', {id: 21, name: 'global'});
      this.RBAC_GROUPS.set('DCS-EXPORT_DATA', {id: 27, name: 'export-data'});
      this.RBAC_GROUPS.set('DCS-DEFAULT_LIST', {id: 100, name: 'default-list'});
  }

  getRbacGroups(): string[] {
      const token = localStorage.getItem('access_token');
      const decoded = this.jwtHelper.decodeToken(token);
      const dcsData = JSON.parse(decoded['custom:dcsData']);
      const rbacGroups = [];

      Object.keys(dcsData.permissions).forEach((key) => {
          for (let permission in dcsData.permissions[key]) {
              const rbac = this.convertRbac(key, dcsData.permissions[key][permission]);
              rbacGroups.push(rbac);
          }

          console.log('# rbacGroups: ', rbacGroups);
      });

      return rbacGroups;
  }

  convertRbac(key, permission): string {
      console.log('permission: ', permission);
      const mapValue = this.RBAC_GROUPS.get(key);
      const results = [];

      results.push("ag");
      results.push(mapValue["id"]);
      results.push(permission);

      return results.join(".").toString();
  }
}
