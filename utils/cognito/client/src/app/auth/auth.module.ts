import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
    },
    {
        path: 'forward',
        loadChildren: () => import('./forward/forward.module').then(m => m.ForwardModule)
    },
    {
        path: '**',
        redirectTo: 'forward'
    }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

      RouterModule.forChild(routes)
  ]
})
export class AuthModule { }
