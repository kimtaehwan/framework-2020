import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {reject} from 'q';
import {RbacService} from '../service/rbac.service';

@Injectable({
  providedIn: 'root'
})
export class RbacGuard implements CanActivate {

    constructor(
        private rbacService: RbacService,
        private router: Router
    ) {
    }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('### route.paramMap.get(\'method\'): ', route.paramMap.get('method'));

      return new Promise((resolve, reject) => {

          let targetMethod = route.paramMap.get('method') || 'view';
          console.log('### targetMethod: ', targetMethod);

          if (targetMethod !== 'view') {
              targetMethod = 'edit';
          }

          console.log('### route.routeConfig: ', route.routeConfig);
          console.log('### route.routeConfig.path: ', route.routeConfig.path);
          let currentPath = '';
          if (!!route.routeConfig && !!route.routeConfig.path) {
              currentPath = '/' + route.routeConfig.path.split('/:method').join('').split('/list').join('');
          }

          console.log('### currentPath: ', currentPath);
          console.log('### this.rbacService.urlNavigation: ', this.rbacService.urlNavigation);

          let privilege = false;
          if (!!this.rbacService.urlNavigation[currentPath]) {
              privilege = this.rbacService.getPrivilege(this.rbacService.urlNavigation[currentPath]['translate'], targetMethod);

              this.setCurrentEditable(currentPath, targetMethod);
          }
          console.log('### privilege: ', privilege);
          if (!privilege) {
              this.router.navigate(['/error/404']);
              return reject(false);
          }

          return resolve(true);
      });
  }

    setCurrentEditable(currentPath: string, targetMethod: string): void {
        const name = this.rbacService.urlNavigation[currentPath]['translate'];
        const type = 'edit';

        this.rbacService.currentEditable = this.rbacService.getPrivilege(name, type);
        console.log('currentEditable: ', this.rbacService.currentEditable);
    }
}
