import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FuseConfigService} from '../../../@fuse/services/config.service';
import {AuthenticationService} from '../service/authentication.service';
import {fuseAnimations} from '../../../@fuse/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    returnUrl: string;

  constructor(
      private _fuseConfigService: FuseConfigService,
      private _formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this._fuseConfigService.config = {
          layout: {
              navbar: {
                  hidden: true
              },
              toolbar: {
                  hidden: true
              },
              footer: {
                  hidden: true
              },
              sidepanel: {
                  hidden: true
              }
          }
      };
  }

  ngOnInit(): void {
      this.initForm();

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      console.log('returnUrl: ', this.returnUrl);

      localStorage.removeItem('access_token');
      localStorage.removeItem('cas_dcs_data');
  }

  initForm(): void {
      this.loginForm = this._formBuilder.group({
          username: ['', [
              Validators.required
          ]],
          password: ['', Validators.required]
      });
  }

  onSubmit(): void {
      const basicToken = this.generateBasicToken();

      console.log('onSubmit - basicToken: ', basicToken);
      this.authenticationService.login(basicToken, null, null, null)
          .subscribe(
              (resp) => {
                  console.log('login - response: ', resp);
                  if (resp.level === 'onSuccess') {
                      localStorage.setItem('access_token', resp.idToken);

                      this.authenticationService.redirectLastPage();
                  }
              },
              (error) => {
                  console.log('login - error: ', error);
              }
          );
  }

  generateBasicToken(): string {
      const username = this.loginForm.get('username').value;
      const password = this.loginForm.get('password').value;
      const token = `${username}:${password}`;

      return `Basic ${btoa(token)}`;
  }

}
