import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/guard/auth.guard';
import {SampleComponent} from './main/sample/sample.component';
import {RbacGuard} from './auth/guard/rbac.guard';

const appRoutes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'error',
        loadChildren: () => import('./error/error.module').then(m => m.ErrorModule)
    },
    {
        path: 'cs/application/list',
        loadChildren: () => import('./main/app-list/app-list.module').then(m => m.AppListModule),
        canActivate: [
            AuthGuard, RbacGuard
        ]
    },
    {
        path      : '**',
        redirectTo: 'auth'
    }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
        appRoutes
    )
  ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
