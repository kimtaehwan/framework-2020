var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv');
dotenv.config({overwrite: true});

/**
 * 1. cors 가 app.use 보다 먼저 실행되어야 함.
 * 2. .env 셋팅은 require cognito 보다 먼저 실행되어야 함.
  */
const whiteList = [];

if (!process.env.NODE_ENV) {
  dotenv.config({
    overwrite: true,
    path: '.env.local'
  });

  whiteList.push('http://localhost:4200');
  whiteList.push('http://localhost:5200');

  console.log('# COGNITO_REGION: ', process.env.COGNITO_REGION);
  console.log('# COGNITO_USER_POOL_ID: ', process.env.COGNITO_USER_POOL_ID);
  console.log('# COGNITO_APP_CLIENT_ID: ', process.env.COGNITO_APP_CLIENT_ID);
}

const corsOptionsDelegate = function (req, callback) {
  let corsOptions = {origin: false};

  if ( whiteList.indexOf(req.header('Origin')) !== -1 ) {
    corsOptions = {origin: true};
  }

  callback(null, corsOptions);
};


const cognito = require('./auth/cognito');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var menuRouter = require('./routes/api/menu');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'dist'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(cors(corsOptionsDelegate));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'dist')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/menuList', menuRouter);
app.use(cognito().getRouter());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function setEnv() {
  const nodeEnv = process.env.NODE_ENV;

  if (nodeEnv === 'production') {
    dotenv.config({
      overwrite: true,
      path: path.joing(__dirname, '.env.production')
    });
  } else {
    dotenv.config({
      overwrite: true,
      path: path.join(__dirname, '.env.local')
    });
  }

  console.log('# nodeEnv: ', nodeEnv);
  console.log('# COGNITO_REGION: ', process.env.COGNITO_REGION);
  console.log('# COGNITO_USER_POOL_ID: ', process.env.COGNITO_USER_POOL_ID);
  console.log('# COGNITO_APP_CLIENT_ID: ', process.env.COGNITO_APP_CLIENT_ID);
}

module.exports = app;
