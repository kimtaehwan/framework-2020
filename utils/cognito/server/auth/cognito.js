const express = require('express');

const bearerToken = require('express-bearer-token');
const jwt_decode = require('jwt-decode');
const basicAuth = require("basic-auth");
const CognitoExpress = require("cognito-express");
const util = require("util");
const NodeCache = require( "node-cache" );
const cache = new NodeCache();

const AWS = require('aws-sdk');

AWS.config.update({
    region: process.env.COGNITO_REGION,
    accessKeyId: 'AKIA3Y34A37JVFC4DSBI',
    secretAccessKey: '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'
});

const AmazonCognitoIdentity = require('amazon-cognito-identity-js');

const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();

console.log('cognitoIdentityServiceProvider: ', cognitoIdentityServiceProvider);

const TEMP_NEW_PASSWORD = 'Ondot@1234';
const TOKEN_EXPIRATION = process.env.TOKEN_EXPIRATION;

class AwsCognito {
    constructor(options) {
        console.log('constructor - AwsCognito');

        this.router = express.Router();

        this.router.use(bearerToken());

        this.router.post('/auth', authentication);
        this.router.post('/auth/confirm-registration', confirmRegistration);
        this.router.post('/auth/forgot-password', forgotPassword);
        this.router.post('/auth/modify-privileges', modifyPrivileges);
        this.router.post('/forward', forward);
        this.router.get('/auth/token', cacheToken);
    }

    getRouter() {
        return this.router;
    }

    getThis() {
        return this;
    }
}

const authentication = async (req, res, next) => {
    console.log('authentication: ', req.token);

    // token 이 존재할 때
    if (!!req.token) {
        // refreshSession
    }

    /**
     * token 이 Invalid 할 때,
     * ex) username, password 사용해서 Login
     */
    const authenticationData = getAuthenticationData(req);
    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(getUserData(authenticationData.Username));

    console.log('1. authenticationData: ', authenticationData);
    console.log('2. authenticationDetails: ', authenticationDetails);
    console.log('3. cognitoUser: ', cognitoUser);

    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: async (result) => {
            // jwtToken 에, custom:dcs:priviliges 속성이 들어가고 있음.
            console.log('1. result.idToken: ', result.getIdToken())

            res.header("Cache-Control", "no-store");
            res.header("Pragma", "no-cache");
            res.json({
                level: "onSuccess",
                token_type: "bearer",
                expires_in: TOKEN_EXPIRATION,
                accessToken: result.getAccessToken().getJwtToken(),
                idToken: result.getIdToken().getJwtToken(),
            });
        },
        newPasswordRequired: async (userAttributes, requiredAttributes) => {
            console.log('### newPasswordRequired - userAttributes: ', userAttributes);
            console.log('### newPasswordRequired - requiredAttributes: ', requiredAttributes);
            delete userAttributes.email_verified;

            cognitoUser.completeNewPasswordChallenge(TEMP_NEW_PASSWORD, {}, this);
        },
        onFailure: async (err) => {
            console.log('# onFailure: ', err);
            if (err.code === 'PasswordResetRequiredException') {

            }
            res.status(401).json(err);
        }
    });

    // return res.status(200).send();
};

const confirmRegistration = async (req, res, next) => {
    const authenticationData = getAuthenticationData(req);
    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(getUserData(authenticationData.Username));

    console.log('1. authenticationData: ', authenticationData);
    console.log('2. authenticationDetails: ', authenticationDetails);
    console.log('3. cognitoUser: ', cognitoUser);


    cognitoUser.confirmRegistration(TEMP_NEW_PASSWORD, true,function(err, result) {
        if (err) {
            console.log(err);
            res.json(err);
        }
        console.log('call result: ' + result);
        res.json(result);
    });
};

// const forgotPassword = async (req, res, next) => {
//     const username = req.body.name;
//
//     const cognitoUser = new AmazonCognitoIdentity.CognitoUser(getUserData(username));
//
//     cognitoUser.forgotPassword({
//         onSuccess: function (result) {
//             // not come to here
//             console.log("onSuccessPassword", result);
//             res.json(result);
//         },
//         onFailure: function (error) {
//             console.error("onFailurePassword", error);
//             res.status(400).json(error);
//             /*
//                 { code: 'LimitExceededException',
//                   name: 'LimitExceededException',
//                   message: 'Attempt limit exceeded, please try after some time.' }
//             */
//         }
//     });
// };

const forward = async (req, res, next) => {
    console.log('forward: ', req);

    const decode = jwt_decode(req.body.idToken);
    console.log('decode: ', decode);

    console.log('1. custom:dcsData', decode['custom:dcsData']);

    cache.set('access_token', req.body.idToken);
    console.log('cache: ', cache.get('access_token'));
    res.render('index.html');
};

const cacheToken = async (req, res, next) => {
    console.log('cache: ', cache.get('access_token'));
    res.json(cache.get('access_token'));
};

const forgotPassword = async (req, res, next) => {
    const authenticationData = getAuthenticationData(req);
    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(getUserData(authenticationData.Username));
    const code = req.body.code;

    console.log('code: ', code);

    cognitoUser.forgotPassword({
        onSuccess: async (result) => {
            console.log('CodeDeliveryData from forgotPassword: ', result);
        },
        onFailure: async (err) => {
            console.log('forgotPassword: ', err);
        },
        inputVerificationCode: async (data) => {
            console.log('inputVerificationCode - data: ', data);
            cognitoUser.confirmPassword(code, TEMP_NEW_PASSWORD, {
                onSuccess() {
                    console.log('Password confirmed');
                },
                onFailure(err) {
                    console.log('Password not confirmed!');
                    res.json(err);
                }
            });
        }
    });
};

const getAuthenticationData = (req) => {
    return {
        Username: getCredentials(req).name,
        Password: getCredentials(req).pass
    };
};

const getCredentials = (req) => {
    return basicAuth(req);
};

const getUserData = (username) => {
    return {
        Username: username,
        Pool: getCognitoUserPool()
    };
};

const getCognitoUserPool = () => {
    const USER_POOL_DATA = {
        ClientId: process.env.COGNITO_APP_CLIENT_ID,
        UserPoolId: process.env.COGNITO_USER_POOL_ID
    };

    console.log('USER_POOL_DATA: ', USER_POOL_DATA);
    return new AmazonCognitoIdentity.CognitoUserPool(USER_POOL_DATA);
};

const modifyPrivileges = async (req, res, next) => {
    console.log('modifyPrivileges - req.body: ', req.body);

    const username = req.body.name;
    const privileges = req.body.privileges;
    const dcsData = req.body.dcsData;

    let attParams = {
        UserAttributes: [
            /* required */
            {
                Name: "custom:dcs:privileges" /* required */,
                Value: JSON.stringify(privileges),
            },
            /* more items */
            {
                Name: "custom:dcsData" /* required */,
                Value: JSON.stringify(dcsData)
            }
        ],
        UserPoolId: process.env.COGNITO_USER_POOL_ID /* required */,
        Username: username /* required */,
    };
    console.log('cognitoIdentityServiceProvider: ', cognitoIdentityServiceProvider);

    try {
        let result = await cognitoIdentityServiceProvider
            .adminUpdateUserAttributes(attParams)
            .promise();
        console.log('result: ', result);
        res.json(result);
    } catch (e) {
        console.error(e);
        res.status(500).json(e);
        return;
    }

    return res.status(200).send();
};

module.exports = (options) => {
    if (!options) {
        options = {};
    }

    const awsCognito = new AwsCognito(options);
    return awsCognito.getThis();
};
