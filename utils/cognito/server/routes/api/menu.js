const express = require('express');
const asyncify = require('express-asyncify');
const axios = require('axios');

let router = asyncify(express.Router());

let options = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    },
    method: '',
    url: '',
    data: {}
};

router.get('/', async (req, res, next) => {
    console.log('menuList start');

    try {
        res.json(SAMPLE_DATA);
    } catch (e) {
        console.error('error menuList');
    }
});


const SAMPLE_DATA = {
    "id": 1,
    "name": "NAV.QUICK-ACCESS",
    "level": 0,
    "children": [
        {
            "id": 2,
            "name": "NAV.DASHBOARD",
            "level": 1,
            "expandable": false
        },
        {
            "id": 3,
            "name": "NAV.CUSTOMER-SUPPORT",
            "level": 1,
            "children": [
                {
                    "id": 9,
                    "name": "NAV.APPLICATION-LIST",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 4,
            "name": "NAV.JOURNEY-MANAGEMENT",
            "level": 1,
            "children": [
                {
                    "id": 10,
                    "name": "NAV.JOURNEY",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 11,
                    "name": "NAV.JOURNEY-POINT",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 12,
                    "name": "NAV.PROCESS",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 29,
                    "name": "NAV.JOURNEY-V2",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 5,
            "name": "NAV.CONTENT-MANAGEMENT",
            "level": 1,
            "children": [
                {
                    "id": 13,
                    "name": "NAV.PRODUCT",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 14,
                    "name": "NAV.CONSENT",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 15,
                    "name": "NAV.ELIGIBILITY",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 16,
                    "name": "NAV.E-SIGNATURE",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 17,
                    "name": "NAV.APPLICATION-FORM",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 26,
                    "name": "NAV.POLICY-CONTENT",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 28,
                    "name": "NAV.CLOSING-PAGE",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 6,
            "name": "NAV.FI-MANAGEMENT",
            "level": 1,
            "children": [
                {
                    "id": 20,
                    "name": "NAV.FI-ONBOARDING",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 7,
            "name": "NAV.IT",
            "level": 1,
            "children": [
                {
                    "id": 21,
                    "name": "NAV.GLOBAL",
                    "level": 2,
                    "expandable": false
                },
                {
                    "id": 27,
                    "name": "NAV.EXPORT-DATA",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 8,
            "name": "NAV.REPORTS",
            "level": 1,
            "expandable": false
        },
        {
            "id": 22,
            "name": "NAV.USER-ACCESS-MANAGEMENT",
            "level": 1,
            "children": [
                {
                    "id": 23,
                    "name": "NAV.USER_GROUPS",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        },
        {
            "id": 24,
            "name": "NAV.POLICY-MANAGEMENT",
            "level": 1,
            "children": [
                {
                    "id": 25,
                    "name": "NAV.CREDIT-UNDERWRITING",
                    "level": 2,
                    "expandable": false
                }
            ],
            "expandable": true
        }
    ],
    "expandable": true
};

module.exports = router;
